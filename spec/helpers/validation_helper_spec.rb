require 'rails_helper'

RSpec.describe ValidationHelper, type: :helper do
    it "valid NIF" do   
        expect(helper.validadorNIF_DNI_NIE("66631284P")).not_to eq(nil)      
    end

    it "valid Pasaport" do   
        expect(helper.validatorPasaport({pasaport: "SPO785764S"})).not_to eq(nil)      
    end

    it "invalid CIF" do   
        expect(helper.validadorCIF("Q2826000H")).not_to eq(nil)      
    end
end