require 'rails_helper'

RSpec.describe Admin::OrganizationExporterHelper, type: :helper do
    let!(:organization) { create(:organization, user: create(:user, :lobby))}

    it "stream_query_rows_private" do   
        expect(helper.stream_query_rows_private(Organization.all)).not_to eq(nil) 
    end

    
end