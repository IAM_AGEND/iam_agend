require 'rails_helper'

RSpec.describe Admin::ExportsHelper, type: :helper do
    let!(:export) { create(:export)}

    it "stream_query_rows_specific" do   
       
        expect(helper.stream_query_rows_specific(export)).not_to eq(nil) 
    end

    it "order_fields" do   
        expect(helper.order_fields).not_to eq(nil) 
    end

    it "order_fields_to_s" do   
        expect(helper.order_fields_to_s).not_to eq(nil) 
    end

    
end