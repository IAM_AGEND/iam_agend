require 'rails_helper'

RSpec.describe Admin::StreamingHelper, type: :helper do

    it "execute_stream_query" do   
        expect(Admin::StreamingHelper.execute_stream_query("COPY (Select * from organizations) TO STDOUT (delimiter ';', FORMAT CSV,HEADER );")).not_to eq(nil)      
    end

    it "non query execute_stream_query" do
        expect(Admin::StreamingHelper.execute_stream_query("Select * from organizations")).not_to eq(nil)      
    end
    
end