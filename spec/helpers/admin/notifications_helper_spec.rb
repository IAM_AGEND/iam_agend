RSpec.describe Admin::NotificationsHelper, type: :helper do

    it "get_num_active_notifications" do   
        expect(helper.get_num_active_notifications()).not_to eq(nil)      
    end

    it "active_notification" do   
        expect(helper.active_notification({status: "Pendiente"})).not_to eq(nil)      
    end

    it "historic_notification" do   
        expect(helper.historic_notification({status: "Pendiente"})).not_to eq(nil)      
    end

    it "get_types_notification active" do   
        expect(helper.get_types_notification()).not_to eq(nil)      
    end

    it "get_types_notification historic" do   
        expect(helper.get_types_notification(type: "historic")).to eq(nil)      
    end

    it "get_types_action_notification" do   
        expect(helper.get_types_action_notification()).not_to eq(nil)      
    end


end