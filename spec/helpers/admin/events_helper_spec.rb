require 'rails_helper'

RSpec.describe Admin::EventsHelper, type: :helper do
    let!(:event) { create(:event)}
    let!(:position) { create(:position)}

    let!(:user) { create(:user)}
    let!(:lobby) { create(:user,:lobby)}

    

    it "admin_or_mananger_edit?" do     
        allow(view).to receive(:current_user).and_return(user)     
        expect(helper.admin_or_mananger_edit?).not_to eq(nil) 
    end

    it "check_filter_lobby_activity" do 
        expect(helper.check_filter_lobby_activity).not_to eq(nil) 
    end

    it "calculate_firstname" do   
        allow(view).to receive(:current_user).and_return(lobby) 
        expect(helper.calculate_firstname(event)).not_to eq(nil) 
        allow(view).to receive(:current_user).and_return(user) 
        expect(helper.calculate_firstname(event)).not_to eq(nil)

        event.lobby_contact_firstname = "xxxx"
        expect(helper.calculate_firstname(event)).to eq("xxxx") 
    end

    it "calculate_lastname" do   
        allow(view).to receive(:current_user).and_return(lobby) 
        expect(helper.calculate_lastname(event)).not_to eq(nil) 
        allow(view).to receive(:current_user).and_return(user) 
        expect(helper.calculate_lastname(event)).not_to eq(nil)

        event.lobby_contact_lastname = "xxxx"
        expect(helper.calculate_lastname(event)).to eq("xxxx") 
    end

    it "calculate_phone" do   
        allow(view).to receive(:current_user).and_return(lobby) 
        expect(helper.calculate_phone(event)).not_to eq(nil) 
        allow(view).to receive(:current_user).and_return(user) 
        expect(helper.calculate_phone(event)).not_to eq(nil)

        event.lobby_contact_phone = "923484848"
        expect(helper.calculate_phone(event)).to eq("923484848")
    end

    it "calculate_email" do   
        allow(view).to receive(:current_user).and_return(lobby) 
        expect(helper.calculate_email(event)).not_to eq(nil) 
        allow(view).to receive(:current_user).and_return(user) 
        expect(helper.calculate_email(event)).not_to eq(nil)

        event.lobby_contact_email = "xxxx@xx.es"
        expect(helper.calculate_email(event)).to eq("xxxx@xx.es")
    end

    it "event_expired_search_options" do   
        expect(helper.event_expired_search_options(nil)).not_to eq(nil) 
    end

    it "event_status_search_options" do   
        expect(helper.event_status_search_options(nil)).not_to eq(nil) 
    end

    it "holder_name_by_position_id" do   
        expect(helper.holder_name_by_position_id(position.id)).not_to eq(nil) 
    end

    it "event_attachments_download_dropdown_id" do   
        expect(helper.event_attachments_download_dropdown_id(event)).not_to eq(nil) 
    end
end