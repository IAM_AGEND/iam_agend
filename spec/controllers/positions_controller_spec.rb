require 'rails_helper'

RSpec.describe PositionsController, type: :controller do
  before(:each) do
    sign_in create(:user, :admin)
  end
  
  context 'Autocomplete' do
    it 'GET Autocomplete' do
      position = create(:position)
      get :autocomplete_position_title,{position: position}
      expect(response.status).to eq(200) 
    end
  end

end

