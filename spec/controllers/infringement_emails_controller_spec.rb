require 'rails_helper'

RSpec.describe InfringementEmailsController, type: :controller do
  before(:each) do
    sign_in create(:user)
  end

  describe "GET #new" do
    it 'success 200' do
      get :new
      expect(response.status).to eq(200)
    end
  end

  describe "POST #create" do
    it 'success 200' do
      post :create, {:infringement_email => {subject: "xx", link: "xx", description: "xx"}}
      expect(response.status).to eq(200)
      post :create, {:infringement_email => {subject: "xx", link: "xx", description: "xx", affected: "xx", affected_referer: "xxx"}}
      expect(response.status).to eq(302)
    end
  end
  
end

