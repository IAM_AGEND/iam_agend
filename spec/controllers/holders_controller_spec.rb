require 'rails_helper'

RSpec.describe HoldersController, type: :controller do
  before(:each) do
    sign_in create(:user, :admin)
  end

  let(:valid_attributes) {
    attributes_for :holder
  }

  let(:invalid_attributes) {
    attributes_for :holder, :invalid
  }

  context 'with valid params' do
    it 'creates a new holder' do
      expect {
        holder = build(:holder)
        post :create, holder: holder.attributes
      }.to change(Holder, :count).by(1)
    end
  end

  context 'with invalid params' do
    it 'creates a new holder' do
      expect {
        post :create, holder: invalid_attributes
      }.to change(Holder, :count).by(0)
    end
  end

 
  context 'with valid params' do
    it 'update a new holder' do
      holder = create(:holder)
      put :update,{id: holder.id, holder: holder.attributes}
      expect(response.status).to eq(302)
    end
  end

  context 'with invalid params' do
    it 'update a new holder' do
      holder = create(:holder)
      holder.first_name = nil
      put :update, {id: holder.id, holder: holder.attributes}
      expect(response.status).to eq(200)
    end
  end


  describe "GET #index" do
    it 'should inherit behavior from Parent' do
      expect(HoldersController.superclass).to eq(AdminController)
    end

    it 'assigns all holders as @holders' do
      create(:holder)
      holders=Holder.all
      get :index
      expect(assigns(:holders).count).to eq(holders.count)
    end

  end

  describe "GET #active" do
    it 'get status 200 holders active' do
      create(:holder)
      get :active
      expect(response.status).to eq(200)
    end

    it 'get status 200 holders active csv' do
      create(:holder)
      get :active, format: :csv
      expect(response.status).to eq(200)
    end
    
  end

  describe "GET #show" do
    it 'get status 200 holders show' do
      holder = create(:holder)
      get :show, id: holder.id
      expect(response.status).to eq(200)
    end
  end

  describe "GET #new" do
    it 'get status 200 holders new' do
      get :new, user_id: User.all.first.id
      expect(response.status).to eq(200)
    end
  end

  describe "GET #edit" do
    it 'get status 200 holders edit' do
      holder = create(:holder)
      get :edit, id: holder.id
      expect(response.status).to eq(200)
    end
  end

  describe "GET #destroy" do
    it 'get status 302 holders destroy' do
      holder = create(:holder)
      get :destroy, id: holder.id
      expect(response.status).to eq(302)
    end

    it 'get status 302 holders destroy positions' do
      holder = create(:holder)
      create(:position, holder: holder)
      get :destroy, id: holder.id
      expect(response.status).to eq(302)
    end
  end

  describe "GET #mail" do
    it 'get status 302 holders edit' do
      holder = create(:holder)
      get :mail, id: holder.id
      expect(response.status).to eq(302)
    end

    it 'get status mail with userkey' do
      holder = create(:holder, user_key: 5)
      create(:user, email: 'xxx@xxx.es', user_key: 5)

      get :mail, {id: holder.id, type_mail: 'titular'}
      expect(response.status).to eq(302)
    end

    it 'get status mail with userkey and mail nil' do
      holder = create(:holder, user_key: 5)

      get :mail, {id: holder.id, type_mail: 'titular'}
      expect(response.status).to eq(302)
    end

    it 'get status mail gestor with userkey' do
      holder = create(:holder, user_key: 5)
      create(:user, email: 'xxx@xxx.es', user_key: 5)
      create(:manage, user: create(:user, email: 'xxx1@xxx.es'), holder: holder)

      get :mail, {id: holder.id, type_mail: 'gestor'}
      expect(response.status).to eq(302)
    end

    it 'get status mail gestor with userkey and mail nil' do
      holder = create(:holder, user_key: 5)
      create(:manage, user: create(:user), holder: holder)

      get :mail, {id: holder.id, type_mail: 'gestor'}
      expect(response.status).to eq(302)
    end
  end

  describe "GET #send_mail" do
    it 'get status 302' do
      holder = create(:holder)
      manage_email = create(:manage_email)
      get :send_mail,{id: holder.id, manage_email: manage_email.attributes}
      expect(response.status).to eq(302)
    end

    it 'get status with holder' do
      holder = create(:holder)
      manage_email = create(:manage_email)
      manage_email.type_data = "Holder"
      get :send_mail,{id: holder.id, manage_email: manage_email.attributes}
      expect(response.status).to eq(302)
    end

    it 'get status without sender' do
      holder = create(:holder)
      manage_email = create(:manage_email)
      manage_email.type_data = "Holder"
      manage_email.sender = nil
      get :send_mail,{id: holder.id, manage_email: manage_email.attributes}
      expect(response.status).to eq(200)
    end

  end

  describe 'GET export_all' do
    it 'export all' do
      get :export_all
      expect(response.status).to eq(200)
    end
  end
end

