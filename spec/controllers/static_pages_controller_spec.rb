require 'rails_helper'

RSpec.describe StaticPagesController, type: :controller do
  before(:each) do
    sign_in create(:user)
  end

  describe "GET #code_of_conduct" do
    it 'success 200' do
      get :code_of_conduct
      expect(response.status).to eq(200)
    end
  end

  describe "GET #accessibility" do
    it 'success 200' do
      get :accessibility
      expect(response.status).to eq(200)
    end
  end

  describe "GET #websitemap" do
    it 'success 200' do
      get :websitemap
      expect(response.status).to eq(200)
    end
  end
end

