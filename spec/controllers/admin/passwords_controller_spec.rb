require 'rails_helper'

RSpec.describe Admin::PasswordsController, type: :controller do
  before(:each) do
    sign_in create(:user, :admin)
  end
  
  describe "GET #edit" do
    it 'status 200' do
     
      get :edit
      expect(response.status).to eq(200)
    end
  end

  describe "PUT #update" do
    it 'status 302' do
     
      put :update, {
        user: 
        {
          password: "xxxxxxxxxx", 
          password_confirmation: "xxxxxxxxxx"
        }}
      expect(response.status).to eq(200)
    end
  end

  
  
end

