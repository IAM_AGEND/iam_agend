require 'rails_helper'

RSpec.describe Admin::NewslettersController, type: :controller do
  before(:each) do
    sign_in create(:user, :admin)
  end
  

  describe "GET #index" do
    it 'status 200' do
      create(:interest, name: "Otros", code: "other")
      get :index
      expect(response.status).to eq(200)
    end
    
  end

  describe "GET #show" do
    it 'status 200' do
      create(:interest)
      newsletter = build(:newsletter)
      interest = create(:interest, name: "Otros", code: "other")
      newsletters_interest = NewslettersInterest.new(interest: interest, newsletter: newsletter)
      newsletter.interest = interest
      newsletter.interests << interest
      newsletter.save
      get :show, id: newsletter.id
      expect(response.status).to eq(200)
    end
    
  end

  describe "GET #new" do
    it 'status 200' do
      create(:interest, name: "Otros", code: "other")
      get :new
      expect(response.status).to eq(200)
    end
    
  end

  describe "POST #create" do
    it 'status 302' do
      create(:interest, name: "Otros")
      newsletter = build(:newsletter)
      interest = create(:interest, name: "Otros", code: "other")
      newsletters_interest = NewslettersInterest.new(interest: interest, newsletter: newsletter)
      newsletter.interest = interest
      newsletter.interests << interest
      newsletter.save
      post :create, newsletter: newsletter.attributes.merge!({interest_ids: ["","#{interest.id}"]})
      expect(response.status).to eq(302)
    end
    
  end
  
  describe "GET #edit" do
    it 'status 200' do
      create(:interest, name: "Otros", code: "other")
      newsletter = build(:newsletter)
      interest = create(:interest, name: "Otros", code: "other")
      newsletters_interest = NewslettersInterest.new(interest: interest, newsletter: newsletter)
      newsletter.interest = interest
      newsletter.interests << interest
      newsletter.save
      get :edit, id: newsletter.id
      expect(response.status).to eq(200)
    end
    
  end

  describe "PUT #update" do
    it 'status 302' do
      create(:interest, name: "Otros", code: "other")
      newsletter = build(:newsletter)
      interest = create(:interest, name: "Otros", code: "other")
      newsletters_interest = NewslettersInterest.new(interest: interest, newsletter: newsletter)
      newsletter.interest = interest
      newsletter.interests << interest
      newsletter.save
      put :update, {id: newsletter.id, newsletter: newsletter.attributes.merge!({interest_ids: ["","#{interest.id}"]})}
      expect(response.status).to eq(302)
    end
    
  end

  describe "GET #destroy" do
    it 'status 302' do
      create(:interest, name: "Otros", code: "other")
      newsletter = build(:newsletter)
      interest = create(:interest, name: "Otros", code: "other")
      newsletters_interest = NewslettersInterest.new(interest: interest, newsletter: newsletter)
      newsletter.interest = interest
      newsletter.interests << interest
      newsletter.save
      get :destroy, id: newsletter.id
      expect(response.status).to eq(302)
    end
    
  end

  describe "GET #deliver" do
    it 'status 302' do
      create(:interest, name: "Otros", code: "other")
      newsletter = build(:newsletter)
      interest = create(:interest, name: "Otros", code: "other")
      newsletters_interest = NewslettersInterest.new(interest: interest, newsletter: newsletter)
      newsletter.interest = interest
      newsletter.interests << interest
      newsletter.save
      get :deliver, id: newsletter.id
      expect(response.status).to eq(302)
    end
    
  end

end

