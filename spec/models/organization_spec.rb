require 'rails_helper'

describe Organization do

  let(:organization) { build(:organization) }
  let!(:range_subvention) { create(:range_subvention, item_id: organization.id, item_type: "Organization")}

  it { should respond_to(:range_1?) }
  it { should respond_to(:range_2?) }
  it { should respond_to(:range_3?) }
  it { should respond_to(:range_4?) }

  it { should respond_to(:federation?) }
  it { should respond_to(:association?) }
  it { should respond_to(:lobby?) }

  

  it "entity_type_id" do
    expect(organization.entity_type_id).not_to eq(nil)
  end

  it "expired_date" do
    organization.renovation_date = Time.zone.now
    expect(organization.expired_date).not_to eq(nil)
  end

  it "status_type" do
    organization.renovation_date = Time.zone.now - 3.years
    expect(organization.status_type).not_to eq(nil)

    organization.renovation_date = Time.zone.now - 2.years + 3.day
    expect(organization.status_type).not_to eq(nil)

    organization.renovation_date = Time.zone.now 
    expect(organization.status_type).not_to eq(nil)

    organization.invalidated_at = Time.zone.now - 5.day
    expect(organization.status_type).not_to eq(nil)


    organization.canceled_at = Time.zone.now - 5.day

    organization.invalidated_at = nil
    expect(organization.status_type).not_to eq(nil)
  end

  it "invalidate_organization" do
    organization.invalidate = true
    organization.invalidated_at = nil 
    expect(organization.invalidate_organization).not_to eq(nil)   
  end

  it "validate_organization" do
    organization.validate = true
    organization.invalidated_at = Time.zone.now - 5.day
    expect(organization.validate_organization).to eq(nil)    
  end

  it "not valid organization attributes" do
    organization.public_term = false
    organization.renovation_date = Time.zone.now - 5.years
    expect(organization).to be_valid    
  end

  it "multiple range method" do
    
    expect(organization.get_range_1_import).to eq('')  
    
    organization.own_lobby_activity = true
    organization.subvention_public_administration = true
    organization.range_subventions << range_subvention
    organization.save
    expect(organization.get_range_1_import).not_to eq("")  
  end

  it "multiple address method" do
    organization.address = Address.new(address: "xxxx")
    expect(organization.address_address).not_to eq(nil)  
    
    expect(organization.legal_representant_address).to eq(nil)  
  end

  it "get_text_group_public_administration" do
    organization.own_lobby_activity = true
    organization.in_group_public_administration = true
    expect(organization.get_text_group_public_administration).to eq(nil)  

    organization.in_group_public_administration = false
    expect(organization.get_text_group_public_administration).to eq('')  
  end

  it "get_subvention_public_administration" do
    organization.own_lobby_activity = true
    expect(organization.get_subvention_public_administration).not_to eq(nil)  

    organization.own_lobby_activity = false
    expect(organization.get_subvention_public_administration).to eq("")  
  end

  it "get_contract_turnover" do
    organization.own_lobby_activity = true
    organization.contract = true
    expect(organization.get_contract_turnover).to eq(nil)  

    organization.own_lobby_activity = false
    expect(organization.get_contract_turnover).to eq("")  
  end

  it "get_contract_total_budget" do
    organization.own_lobby_activity = true
    organization.contract = true
    expect(organization.get_contract_total_budget).to eq(nil)  

    organization.own_lobby_activity = false
    expect(organization.get_contract_total_budget).to eq("")  
  end

  it "get_contract_breakdown" do
    organization.own_lobby_activity = true
    organization.contract = true
    expect(organization.get_contract_breakdown).to eq(nil)  

    organization.own_lobby_activity = false
    expect(organization.get_contract_breakdown).to eq("")  
  end

  it "get_contract_financing" do
    organization.own_lobby_activity = true
    organization.contract = true
    expect(organization.get_contract_financing).to eq(nil)  

    organization.own_lobby_activity = false
    expect(organization.get_contract_financing).to eq("")  
  end

  it "status" do
  
    expect(organization.status).to eq(:active)  

    organization.invalidated_at = Time.zone.now

    expect(organization.status).to eq(:inactive) 

    organization.termination_date = Time.zone.now

    expect(organization.status).to eq(:terminated) 
  
  end

  it "get_fiscal_year" do
    organization.own_lobby_activity = true
    expect(organization.get_fiscal_year).to eq(2018)  

    organization.own_lobby_activity = false
    expect(organization.get_fiscal_year).to eq("")  
  end

  it "get_range_fund" do
    organization.own_lobby_activity = true
    expect(organization.get_range_fund).not_to eq(nil)  

    organization.own_lobby_activity = false
    expect(organization.get_range_fund).to eq("")  
  end

  it "get_subvention" do
    organization.own_lobby_activity = true
    expect(organization.get_subvention).to eq(false)  

    organization.own_lobby_activity = false
    expect(organization.get_subvention).to eq("")  
  end

  it "get_in_group_public_administration" do
    organization.own_lobby_activity = true
    expect(organization.get_in_group_public_administration).to eq(false)  

    organization.own_lobby_activity = false
    expect(organization.get_in_group_public_administration).to eq("")  
  end

  it "get_contract" do
    organization.own_lobby_activity = true
    expect(organization.get_contract).to eq(true)  

    organization.own_lobby_activity = false
    expect(organization.get_contract).to eq("")  
  end

  it "get_category" do
    organization.category = create(:category)
    expect(organization.get_category).not_to eq(nil)  

    organization.category = nil
    expect(organization.get_category).to eq("")  
  end

  it "multiple fiscal year" do
    represented_entity = create(:represented_entity, organization: organization)
    organization.represented_entities << represented_entity
    organization.foreign_lobby_activity = true
    organization.save
    expect(organization.fiscal_year_1).not_to eq(nil)  

    expect(organization.fiscal_year_4).not_to eq(nil) 

    organization.foreign_lobby_activity = false
    expect(organization.fiscal_year_1).not_to eq(nil)   
  end

  it "multiple represented_entity" do
    represented_entity = create(:represented_entity, organization: organization)
    organization.represented_entities << represented_entity
    organization.foreign_lobby_activity = true
    organization.save
    expect(organization.represented_entity_1).not_to eq(nil)  

    expect(organization.represented_entity_2).not_to eq(nil)  
   
    organization.foreign_lobby_activity = false
    expect(organization.represented_entity_1).not_to eq(nil)   
  end

  it "multiple range fund" do
    represented_entity = create(:represented_entity, organization: organization)
    organization.represented_entities << represented_entity
    organization.foreign_lobby_activity = true
    organization.save
    expect(organization.range_fund_1).not_to eq(nil)  

    expect(organization.range_fund_2).not_to eq(nil)  
   
    organization.foreign_lobby_activity = false
    expect(organization.range_fund_1).not_to eq(nil)   
  end

  it "multiple subvention" do
    represented_entity = create(:represented_entity, organization: organization)
    organization.represented_entities << represented_entity
    organization.foreign_lobby_activity = true
    organization.save
    expect(organization.subvention_1).not_to eq(nil)  

    expect(organization.subvention_2).not_to eq(nil) 
   
    organization.foreign_lobby_activity = false
    expect(organization.subvention_1).not_to eq(nil)   
  end

  it "multiple contract" do
    represented_entity = create(:represented_entity, organization: organization)
    organization.represented_entities << represented_entity
    organization.foreign_lobby_activity = true
    organization.save
    expect(organization.contract_1).not_to eq(nil)  

    expect(organization.contract_2).not_to eq(nil)  
   
    organization.foreign_lobby_activity = false
    expect(organization.contract_1).not_to eq(nil)   
  end

  it "multiple in_group_public_administration" do
    represented_entity = create(:represented_entity, organization: organization)
    organization.represented_entities << represented_entity
    organization.foreign_lobby_activity = true
    organization.save
    expect(organization.in_group_public_administration_1).not_to eq(nil)  

    expect(organization.in_group_public_administration_2).not_to eq(nil)  

    organization.foreign_lobby_activity = false
    expect(organization.in_group_public_administration_1).not_to eq(nil)   
  end

  it "multiple text_group_public_administration" do
    represented_entity = create(:represented_entity, organization: organization, in_group_public_administration: true)
    organization.represented_entities << represented_entity
    organization.foreign_lobby_activity = true
    organization.save
    expect(organization.text_group_public_administration_1).to eq(nil)  

    expect(organization.text_group_public_administration_2).not_to eq(nil)  

   
    organization.foreign_lobby_activity = false
    expect(organization.text_group_public_administration_1).not_to eq(nil)   
  end

  it "multiple subvention_public_administration" do
    represented_entity = create(:represented_entity, organization: organization)
    organization.represented_entities << represented_entity
    organization.foreign_lobby_activity = true
    organization.save
    expect(organization.subvention_public_administration_1).not_to eq(nil)  

    expect(organization.subvention_public_administration_2).not_to eq(nil) 

   
    organization.foreign_lobby_activity = false
    expect(organization.subvention_public_administration_1).not_to eq(nil)   
  end

  it "multiple contract_turnover" do
    represented_entity = create(:represented_entity, organization: organization, contract: true)
    organization.represented_entities << represented_entity
    organization.foreign_lobby_activity = true
    organization.save
    expect(organization.contract_turnover_1).to eq(nil)  

    expect(organization.contract_turnover_2).not_to eq(nil) 

   
    organization.foreign_lobby_activity = false
    expect(organization.contract_turnover_1).not_to eq(nil)   
  end

  it "multiple contract_total_budget" do
    represented_entity = create(:represented_entity, organization: organization, contract: true)
    organization.represented_entities << represented_entity
    organization.foreign_lobby_activity = true
    organization.save
    expect(organization.contract_total_budget_1).to eq(nil)  

    expect(organization.contract_total_budget_2).not_to eq(nil)  

   
    organization.foreign_lobby_activity = false
    expect(organization.contract_total_budget_1).not_to eq(nil)   
  end

  it "multiple contract_breakdown" do
    represented_entity = create(:represented_entity, organization: organization, contract: true)
    organization.represented_entities << represented_entity
    organization.foreign_lobby_activity = true
    organization.save
    expect(organization.contract_breakdown_1).to eq(nil)  

    expect(organization.contract_breakdown_2).not_to eq(nil)  

   
    organization.foreign_lobby_activity = false
    expect(organization.contract_breakdown_1).not_to eq(nil)   
  end

  it "multiple contract_financing" do
    represented_entity = create(:represented_entity, organization: organization, contract: true)
    organization.represented_entities << represented_entity
    organization.foreign_lobby_activity = true
    organization.save
    expect(organization.contract_financing_1).to eq(nil)  

    expect(organization.contract_financing_2).not_to eq(nil)  

   
    organization.foreign_lobby_activity = false
    expect(organization.contract_financing_1).not_to eq(nil)   
  end

  it "multiple get range foreing" do
    represented_entity = create(:represented_entity, organization: organization, subvention_public_administration: true)
    represented_entity.represented_range_subventions << create(:range_subvention, item_id: represented_entity.id, item_type: "RepresentedEntity")
    organization.represented_entities << represented_entity
    organization.foreign_lobby_activity = true
    organization.save
    expect(organization.get_range_1_1_import).not_to eq(nil)  

    expect(organization.get_range_1_2_import).not_to eq(nil)  

   
    organization.foreign_lobby_activity = false
    expect(organization.get_range_1_1_import).not_to eq(nil)   
  end

  it "self_employed_lobby" do
    organization.own_lobby_activity = nil
    organization.foreign_lobby_activity = true
    expect(organization.self_employed_lobby).to eq(false)  

   
    organization.own_lobby_activity = true
    organization.foreign_lobby_activity = false
    expect(organization.self_employed_lobby).to eq(true)   
  end

  it "old_organization" do
    expect(organization.old_organization).not_to eq(nil)  

  end

  it "is_physical_person" do
    expect(organization.is_physical_person).not_to eq(nil)  

  end

  it "is_person" do
    expect(organization.is_person).not_to eq(nil) 
  end

  it "set_expired_date" do
    expect(organization.set_expired_date).not_to eq(nil) 
    organization.inscription_date = nil 
    expect(organization.set_expired_date).not_to eq(nil) 

  end

  it "employee_lobby" do
    organization.foreign_lobby_activity = true
    expect(organization.employee_lobby).to eq(true)  

  end

  it "set_invalidate" do
    expect(organization.set_invalidate).not_to eq(nil)  

  end

  it "interest?" do
    expect(organization.interest?(create(:interest))).to eq(false)  

  end

  it "canceled?" do
    expect(organization.canceled?).not_to eq(nil)  

  end

  it "change_password" do
    organization.user = create(:user,:lobby)
    expect(organization.change_password("qwer")).not_to eq(nil)  

  end

  it "notification_effect_full_name" do
    expect(organization.notification_effect_full_name).to eq(nil)  

  end

  it "user_name" do
    organization.user = create(:user, :lobby)
    expect(organization.user_name).not_to eq(nil)  

  end

  it "user_email" do
    organization.user = create(:user, :lobby)
    expect(organization.user_email).not_to eq(nil)  

  end

  it "user_phones" do
    organization.user = create(:user, :lobby)
    expect(organization.user_phones).not_to eq(nil)  

  end

  it "Should be valid" do
    expect(organization).to be_valid
  end

  it "Should not be valid when inscription_reference already exists" do
    create(:organization, inscription_reference: "XYZ")
    another_organization = build(:organization, inscription_reference: "XYZ")

    expect(another_organization).to be_valid
  end

  it "should not be valid whitout name" do
    organization.name = nil

    expect(organization).to be_valid
  end

  it "should be valid whitout user" do
    #It changed due to the inclusion of Associations and Federations
    organization.user = nil

    expect(organization).to be_valid
  end

  it "should not be valid without category defined" do
    organization.category = nil

    expect(organization).to be_valid
  end

  describe "#fullname" do
    it "Should return first_surname and second_surname when they are defined" do
      organization.name = "Name"
      organization.first_surname = ""
      organization.second_surname = ""

      expect(organization.fullname).to eq "Name"
    end

    it "Should return first_surname and second_surname when they are defined" do
      organization.name = "Name"
      organization.first_surname = "FirstSurname"
      organization.second_surname = ""

      expect(organization.fullname).to eq "Name FirstSurname"
    end

    it "Should return first_surname and second_surname when they are defined" do
      organization.name = "Name"
      organization.first_surname = "FirstSurname"
      organization.second_surname = "SecondSurname"

      expect(organization.fullname).to eq "Name FirstSurname SecondSurname"
    end

    it "Should return business_name when they are defined" do
      organization.business_name = "Name"

      expect(organization.fullname).to eq "Name"
    end
  end

  it "document_identifier" do
    expect(organization.send(:document_identifier)).to eq(nil)
    organization.identifier_type = "DNI/NIF"
    expect(organization.send(:document_identifier)).to eq(nil)

    organization.identifier_type = "NIE"
    expect(organization.send(:document_identifier)).to eq(nil)

    organization.identifier_type = "NIF"
    expect(organization.send(:document_identifier)).to eq(nil)

    organization.identifier_type = "Pasaporte"
    expect(organization.send(:document_identifier)).to eq(nil)


    organization.identifier_type = nil
    expect(organization.send(:document_identifier)).not_to eq(nil)
  end

  it "name_entity" do
    expect(organization.send(:name_entity)).to eq(nil)

    organization.business_name = "xxxxxx"
    expect(organization.send(:name_entity)).not_to eq(nil)

    organization.business_name = nil
    organization.name = nil
    expect(organization.send(:name_entity)).to eq(nil)
  end

  it "terms" do
    organization.certain_term = false
    expect(organization.send(:terms)).not_to eq(nil)

  end

  it "registered_lobby_activity" do
    organization.foreign_lobby_activity = false
    expect(organization.send(:registered_lobby_activity)).not_to eq(nil)

  end

  it "renovation_historic" do
    organization.renovation_date = Time.zone.now
    expect(organization.send(:renovation_historic)).to eq(nil)

  end

  it "should have a correct legal representant full name" do
    legal_representant = build(:legal_representant)
    organization.legal_representant = legal_representant
    legal_representant_full_name = organization.legal_representant.fullname

    expect(organization.legal_representant_full_name).to eq(legal_representant_full_name)
  end

  describe "#set_inscription_date" do

    it "should set inscription_date with current date" do
      organization.inscription_date = nil

      organization.save
      expect(organization.inscription_date).not_to eq(nil)
    end

    it "should not set inscription_date" do
      organization.inscription_date = Date.yesterday

      organization.save

      expect(organization.inscription_date).not_to eq(nil)
    end

  end

  describe "#set_invalidate" do

    it "should set inscription_date with current date" do
      organization.invalidated_at = nil
      organization.invalidated_reasons = nil

      organization.save

      expect(organization.invalidated?).to eq(false)
    end

  end

end
