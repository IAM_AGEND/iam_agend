require 'rails_helper'

describe Event do

  let(:event) { create(:event) }
  let(:event_canceled) { create(:event) }
  let(:event_declined) { create(:event) }
  let(:event_accepted) { create(:event) }
  let!(:user) { create(:user, :lobby)}
  let!(:holder) { create(:holder)}
  let!(:manage) { create(:manage, user: user, holder: holder)}
  let!(:position) {create(:position, holder: holder)}
  let!(:participant) {create(:participant, position: position)}
  let!(:lobby_organization) { create(:organization,user: user)}

  it "Should be valid" do
    DataPreference.find_or_create_by(title: "min_length",content_data: 0,type_data: 1) if DataPreference.find_by(title: "min_length").blank?
    event.declined_reasons = "xxxxxx"
    expect(event).to be_valid
  end

  it "validate_description" do
    DataPreference.find_or_create_by(title: "min_length",content_data: 10,type_data: 1) if DataPreference.find_by(title: "min_length").blank?
    event.declined_reasons = "xxxxxx"
    event.description = ""
    expect(event).not_to be_valid
  end

  it "validate_attendees" do
    DataPreference.find_or_create_by(title: "min_length",content_data: 10,type_data: 1) if DataPreference.find_by(title: "min_length").blank?
    event.declined_reasons = "xxxxxx"
    event.multi_attendees = false
    expect(event).not_to be_valid
  end

  it "Scheduled_range" do
    DataPreference.find_or_create_by(title: "min_length",content_data: 0,type_data: 1) if DataPreference.find_by(title: "min_length").blank?
    event.declined_reasons = "xxxxxx"
    event.scheduled = Time.zone.now - 4.years
    expect(event).not_to be_valid
  end

  it "Scheduled_range_published" do
    DataPreference.find_or_create_by(title: "min_length",content_data: 0,type_data: 1) if DataPreference.find_by(title: "min_length").blank?
    event.declined_reasons = "xxxxxx"
    event.published_at = Time.zone.now - 4.years
    expect(event).not_to be_valid
  end


  it "Scheduled_role" do
    event.declined_reasons = nil
    event.scheduled = nil
    expect(event).not_to be_valid
  end

  it "cancel_event" do
    event_canceled.cancel=true
    event_canceled.canceled_at = nil
    event_canceled.organization = lobby_organization
    event_canceled.lobby_activity = true
    expect(event_canceled.cancel_event).not_to eq(nil) 
  end

  it "decline_event" do
    event_declined.decline=true
    event_declined.declined_at = nil
    event_declined.organization = lobby_organization
    event_declined.lobby_activity = true
    expect(event_declined.decline_event).not_to eq(nil) 
  end

  it "accept_event" do
    event_accepted.accept=true
    event_accepted.accepted_at = nil
    event_accepted.organization = lobby_organization
    event_accepted.lobby_activity = true
    expect(event_accepted.accept_event).not_to eq(nil) 
  end

  it "lobby_expired" do
    expect(event.lobby_expired).to eq(nil) 
    event.organization = lobby_organization
    event.lobby_activity = true
    event.old_event_lobby = true
    expect(event.lobby_expired).to eq("old_event") 
    event.old_event_lobby = false
    event.organization.renovation_date = Time.zone.now - 3.years
    expect(event.lobby_expired).to eq("new_event_expired")

    event.organization.renovation_date = Time.zone.now
    expect(event.lobby_expired).to eq("new_event")
  end

  it "lobby_expired_formatted" do
    expect(event.lobby_expired_formatted).to eq('') 
    event.old_event_lobby = true
    event.organization = lobby_organization
    event.lobby_activity = true
    expect(event.lobby_expired_formatted).not_to eq("") 
  end

  it "start_time" do
    expect(event.start_time).not_to eq(nil) 
     
  end

  it "valid_scheduled" do
    expect(event.valid_scheduled).to eq(nil)
     
  end

  it "searches params nil" do
    expect(Event.searches(nil)).not_to eq(nil)      
  end

  it "contributed_by" do
    event.position = position
    
    expect(event.contributed_by).not_to eq(nil)      
  end

  it "position_names" do
    participant.event_id = event.id
    expect(event.position_names).not_to eq(nil)      
  end

  it "user_name" do
    expect(event.user_name).not_to eq(nil)      
  end

  it "holder_position" do
    expect(event.holder_position).not_to eq(nil)      
  end

  it "lobby_user_name" do
    expect(event.lobby_user_name).not_to eq(nil)      
  end

  it "status_id" do
    expect(event.status_id).not_to eq(nil)      
  end

  it "holder_name" do
    expect(event.holder_name).not_to eq(nil) 
     
  end

  it "Should be invalid if no title defined" do
    event.title = nil

    expect(event).not_to be_valid
  end

  it "Should be invalid if no position defined" do
    event.position = nil

    expect(event).not_to be_valid
  end

  it "Should be invalid if event not scheduled" do
    event.scheduled = nil

    expect(event).to be_valid
  end

  it "Should be invalid if event not lobby_activity" do
    event.lobby_activity = nil

    expect(event).not_to be_valid
  end

  it "Should be invalid if event not published_at" do
    event.published_at = nil
    event.declined_reasons = "xxxxxx"
    expect(event).to be_valid
  end

  it "Should be invalid if event not location" do
    event.location = nil

    expect(event).not_to be_valid
  end

  it "Should be invalid if event as lobby_activity without organization" do
    event.lobby_activity = true
    event.organization = nil

    expect(event).not_to be_valid
  end

  it "Should be invalid if participant are not unique" do
    event = create(:event)
    participant = create(:participant)
    event.participants << participant
    event.participants << participant

    expect(event).not_to be_valid
  end

  it "Should be invalid when position event assigned as participant too" do
    event = create(:event)
    event.participants << create(:participant,
                                 participants_event: event)
    event.participants << create(:participant,
                                 participants_event: event,
                                 position: event.position)

    expect(event).not_to be_valid
  end

  describe ".by_title" do
    let!(:event1) { create(:event, title: "This event rocks!") }
    let!(:event2) { create(:event, title: "This event is awesome!") }

    it "Should return events matching given string" do
      expect(Event.title("event").count).to eq(3)
    end

    it "Should return events matching exact title" do
      expect(Event.title("This event is awesome!")).to eq([event2])
    end
  end

  describe ".by_holders" do
    let!(:event) { create(:event) }
    let!(:event2) { create(:event) }

    it "Should return all events from given holders ids" do
      expect(Event.by_holders([event.position.holder.id])).to eq([event])
    end
  end

  describe ".by_participant_holders" do
    let!(:event)       { create(:event) }
    let!(:event2)      { create(:event) }
    let!(:participant) { create(:participant, participants_event: event) }

    it "Should return all events where given holders ids acts as participants" do
      expect(Event.by_participant_holders([participant.position.holder.id])).to eq([event])
    end
  end

  describe ".by_holder_name" do
    let!(:holder) { create(:holder, :with_position, first_name: "John", last_name: "Doe") }
    let!(:event)  { create(:event, position: holder.current_position) }

    it "Should return all events where holders contains given name" do
      expect(Event.by_holder_name("John Doe")).to eq([event])
    end
  end

  describe ".managed_by" do

    let!(:user)   { create(:user, :user) }
    let!(:manage) { create(:manage, user: user) }
    let!(:position) { create(:position, holder: manage.holder) }
    let!(:event) { create(:event, position: position) }
    let!(:event_as_participant) { create(:event) }
    let!(:participant) { create(:participant, position: position, participants_event: event_as_participant) }

    it "Should return all events where given user holders acts as holders or participants" do
      expect(Event.managed_by(user)).not_to eq(nil)
    end

  end

  describe ".ability_events" do

    it "Should return all events ids where given user holders are present as holders" do
      event = create(:event)
      manage = create(:manage, holder: event.position.holder)

      expect(Event.ability_events(manage.user)).to eq([event.id])
    end

    it "Should return all events ids where given user holders are present as participants" do
      event = create(:event)
      participant = create(:participant, participants_event: event)
      manage = create(:manage, holder: participant.position.holder)

      expect(Event.ability_events(manage.user)).to eq([event.id])
    end

  end

  describe ".ability_titular_events" do

    it "Should return all events ids where given user holders are present as holders" do
      event = create(:event)
      manage = create(:manage, holder: event.position.holder)

      expect(Event.ability_titular_events(manage.user)).to eq([event.id])
    end

  end

  describe ".ability_participants_events" do
    it "Should return all events ids where given user holders are present as participants" do
      event = create(:event)
      participant = create(:participant, participants_event: event)
      manage = create(:manage, holder: participant.position.holder)

      expect(Event.ability_participants_events(manage.user)).to eq([event.id])
    end

  end

  describe ".searches" do
    let!(:holder) { create(:holder, :with_position, first_name: "John", last_name: "Doe") }
    let!(:event) { create(:event, position: holder.current_position, title: "Some amazing title",
                                  lobby_activity: false, status: "accepted") }

    it "Should return events by given holder name" do
      params = {}
      params[:person] = "John"
      expect(Event.searches(params)).not_to eq(nil)
    end

    it "Should return events by given title name" do
      params = {}
      params[:person] = "Doe"
      expect(Event.searches(params)).not_to eq(nil)
    end

    it "Should return events by given status" do
      params = {}
      params[:title] = "Some amazing title"
      params[:status] = 1 # accepted
      expect(Event.searches(params)).to eq([event])
    end

    it "Should return events by different status" do
      params = {}
      params[:title] = "Some amazing title"
      params[:status] = 2
      expect(Event.searches(params)).not_to eq([event])
    end

  end

  describe "lobby organizations' events" do
    let!(:organization_user) { create(:user, :lobby) }

    it "lobby user should create event with status on_request" do
      event = create(:event, title: 'Event on request', user: organization_user)
      event.save
      expect(event.status).to eq('requested')
    end
  end

  describe "regular organizations' events" do
    let!(:organization_user) { create(:user, :user) }

    it "regular user should create event with status accepted" do
      event = create(:event, title: 'Event on request', user: organization_user)
      event.save

      expect(event.status).to eq('accepted')
    end
  end

  describe "regular organizations' events" do
    let!(:organization_user) { create(:user, :admin) }

    it "admin user should create event with status accepted" do
      event = create(:event, title: 'Event on request', user: organization_user)
      event.save

      expect(event.status).to eq('accepted')
    end
  end

  describe "only can be canceled accepted events" do
    let!(:organization_user) { create(:user, :user) }

    it "accepted events can be canceled" do
      event = create(:event, title: 'Event on request', user: organization_user)

      event.canceled_at = Time.zone.today
      event.canceled_reasons = 'test'
      event.declined_reasons = "xxxxxx"
      expect(event).to be_valid
    end

    it "accepted events can be canceled" do
      event = create(:event, title: 'Event on request', user: organization_user)
      event.status = 'done'
      event.canceled_at = Time.zone.today
      event.canceled_reasons = 'test'

      expect(event).not_to be_valid
    end

  end

end
