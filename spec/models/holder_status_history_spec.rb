require 'rails_helper'

RSpec.describe HolderStatusHistory, type: :model do
  let(:holder_status_history) { build(:holder_status_history) }
  let(:position) { build(:position) }

  it "Should be valid" do
    expect(holder_status_history).to be_valid
  end

  it "generateHistory" do
    expect(HolderStatusHistory.generateHistory(position)).not_to eq(nil)
    position.to = Time.zone.now
    expect(HolderStatusHistory.generateHistory(position)).not_to eq(nil)

    position.to = nil
    expect(HolderStatusHistory.generateHistory(position, "xxx")).not_to eq(nil)
  end
end
