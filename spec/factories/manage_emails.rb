FactoryGirl.define do
  factory :manage_email do
    sender "xx@xx.es"
    fields_cc "xx@xx.es"
    fields_cco "xx@xx.es"
  end
end
