FactoryGirl.define do
  factory :export do
    fields {{ :scheduled_field => true,:title_field => true,:position_id_field  => true,:job_field  => true,
      :location_field  => true,:description_field  => true,:status_field  => true,:holders_field  => true,
      :other_attendees_field  => true,:created_at_field  => true,:published_at_field  => true,:slug_field  => true,
      :accepted_at_field  => true,:canceled_at_field  => true,:canceled_reasons_field  => true,
      :declined_at_field  => true,:declined_reasons_field  => true,:organization_name_field  => true,
      :lobby_activity_field  => true,:lobby_expired_field  => true,:represented_entities_field  => true,:lobby_agent_field  => true,
      :lobby_contact_firstname_field  => true,:lobby_contact_lastname_field  => true,:lobby_contact_email_field  => true,
      :lobby_contact_phone_field  => true,:lobby_scheduled_field  => true,
      :notes_field  => true,:user_id_field  => true,:updated_at_field  => true,:general_remarks_field  => true, :position_title_field => true,
      :event_agent_field => true, :attendees => true}}
    filter_fields {{status: ['Solicitado', 'Aceptado', 'Realizado', 'Rechazado', 'Cancelado'], title: 'xxx', people: 'xxx',
      start_date: Time.zone.now, end_date: Time.zone.now, start_date_published: Time.zone.now, end_date_published: Time.zone.now}}
  end

end
