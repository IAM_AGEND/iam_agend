require 'rails_helper'
require 'public_organization_importer'

describe PublicOrganizationImporter do
  let(:importer) { PublicOrganizationImporter }

  describe 'read csv' do

    it "it does nothing without the csv" do
      #PublicOrganizationImporter.get_file).and_return(nil)
      expect(PublicOrganizationImporter.send(:get_file, '')).to eq(nil)
    end

    it "it creates correct associations" do
      file_route = Rails.root.join('spec', 'fixtures', 'associations.csv')
      PublicOrganizationImporter.stub(:get_file).and_return(File.open(file_route).read)
      PublicOrganizationImporter.parse_associations
      association = Organization.last
      expect(association.try(:entity_type)).not_to eq('')
    end

    it "it creates correct federations" do
      file_route = Rails.root.join('spec', 'fixtures', 'federations.csv')
      PublicOrganizationImporter.stub(:get_file).and_return(File.open(file_route).read)
      PublicOrganizationImporter.parse_federations
      federation = Organization.last
      expect(federation.try(:entity_type)).not_to eq('')
    end

  end

end
