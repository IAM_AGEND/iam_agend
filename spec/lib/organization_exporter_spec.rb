require 'rails_helper'
require 'organization_exporter'

describe OrganizationExporter do

  describe "when is in default mode" do

    let(:exporter) { OrganizationExporter.new }
    let(:organization) { create(:organization, :lobby)}



    it "windows_headers" do
      expect(exporter.windows_headers).not_to eq(nil)
    end

    it "windows_organization_row" do
      expect(exporter.windows_organization_row(organization)).not_to eq(nil)
    end

    it "save_csv" do
      expect(exporter.save_csv("public/export/xx.csv")).to eq(nil)
    end

    it "save_xlsx" do
      expect(exporter.save_xlsx("public/export/xx.xlsx")).not_to eq(nil)
    end

    it "save_xls" do
      expect(exporter.save_xls("public/export/xx.xls")).not_to eq(nil)
    end

    it "save_json" do
      expect(exporter.save_json("public/export/xx.json")).not_to eq(nil)
    end



    describe "#headers" do

      it "Should contain thirty-six public colums headers" do
        expect(exporter.headers.size).not_to eq(0)
      end

      it "Should return correct headers translations" do
        exporter.fields.each_with_index do |field, index|
          expect(exporter.headers[index]).to eq(I18n.t("organization_exporter.#{field}"))
        end
      end

    end

    describe "#organization_to_row" do
      it "Should return array of public organization columns in exact order" do
        organization = create(:organization, subvention: true, contract: true, certain_term: true,
                              code_of_conduct_term: true, gift_term: true, lobby_term: true,
                              inscription_reference: "ref232", description: "No more html tags <bold>tags</bold>", 
                              address: Address.create(country: "Spain",
                              address: "Address", address_number_type: "kilometers", number: "83", address_type: "Street"))
        create(:legal_representant, organization: organization)
        create(:agent, organization: organization)
        create(:represented_entity, organization: organization)
        create(:organization_interest, organization: organization)

        row = exporter.organization_to_row(organization)

        expect(row[0]).to eq(organization.identifier)
        expect(row[1]).to eq(organization.name)
        expect(row[2]).to eq(organization.first_surname)
        expect(row[3]).to eq(organization.second_surname)
        expect(row[4]).to eq(organization.address.address_type)
        expect(row[5]).to eq(organization.address.address)
        expect(row[6]).to eq(organization.address.address_number_type)
        expect(row[7]).to eq(organization.address.number)
        expect(row[8]).to eq(organization.address.gateway)
        expect(row[9]).to eq(organization.address.stairs)
        expect(row[10]).to eq(organization.address.floor)
        expect(row[11]).to eq(organization.address.door)
        expect(row[12]).to eq(organization.address.postal_code)
        expect(row[13]).to eq(organization.address.town)
        expect(row[14]).to eq(organization.address.province)
        expect(row[15]).to eq(organization.address.country)
        expect(row[16]).to eq(organization.address.phones)
        expect(row[17]).to eq(organization.address.email)
        expect(row[18]).to eq("No more html tags tags")
        expect(row[19]).to eq(organization.web)
        expect(row[20]).to eq(organization.registered_lobbies.collect(&:name).join(", "))
        
      end
    end

  end

  describe "when is in extended mode" do

    let(:exporter) { OrganizationExporter.new true }

    describe "#headers" do

      it "Should contain forty-two colums, private and public ones" do
        expect(exporter.headers.size).not_to eq(0)
      end

      # it "Should return correct headers translations" do
      #   exporter.fields.each_with_index do |field, index|
      #     expect(exporter.headers[index]).to eq(I18n.t("organization_exporter.#{field}"))
      #   end
      # end

    end

    describe "#organization_to_row" do
      it "Should return array of columns in exact order" do
        organization = create(:organization, subvention: true, contract: true, certain_term: true,
                              code_of_conduct_term: true, gift_term: true, lobby_term: true,
                              inscription_reference: "ref232", description: "No more html tags <bold>tags</bold>", address: Address.create(country: "Spain",
                              address: "Address", address_number_type: "kilometers", number: "83", address_type: "Street"))
        create(:legal_representant, organization: organization)
        create(:agent, organization: organization)
        create(:represented_entity, organization: organization)
        create(:organization_interest, organization: organization)

        row = exporter.organization_to_row(organization)

        expect(row[0]).to eq(organization.reference)
        expect(row[1]).to eq(organization.identifier)
        expect(row[2]).to eq(organization.name)
        expect(row[3]).to eq(organization.first_surname)
        expect(row[4]).to eq(organization.second_surname)
        expect(row[5]).to eq(organization.address.address_type)
        expect(row[6]).to eq(organization.address.address)
        expect(row[7]).to eq(organization.address.address_number_type)
        expect(row[8]).to eq(organization.address.number)
        expect(row[9]).to eq(organization.address.gateway)
        expect(row[10]).to eq(organization.address.stairs)
        expect(row[11]).to eq(organization.address.floor)
        expect(row[12]).to eq(organization.address.door)
        expect(row[13]).to eq(organization.address.postal_code)
        expect(row[14]).to eq(organization.address.town)
        expect(row[15]).to eq(organization.address.province)
        expect(row[16]).to eq(organization.address.country)
        expect(row[17]).to eq(organization.address.phones)
        expect(row[18]).to eq(organization.address.email)
        expect(row[19]).to eq("No more html tags tags")
        expect(row[20]).to eq(organization.web)
        
      end
    end

  end

end
