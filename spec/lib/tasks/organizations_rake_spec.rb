require 'spec_helper'

require "rake"

describe "organizations rake" do
  before do
    Rake.application.rake_require "tasks/organizations"
    Rake::Task.define_task(:environment)
  end

  describe "#add_categories" do
    let :run_rake_task do
      Rake::Task["organizations:add_categories"].reenable
      Rake.application.invoke_task "organizations:add_categories"
    end

    context "add_categories success" do
      it "add_categories" do
        run_rake_task

       
        expect(Category.all.count).not_to eq(0)
      end
    end
  end

  describe "#add_interests" do
    let :run_rake_task do
      Rake::Task["organizations:add_interests"].reenable
      Rake.application.invoke_task "organizations:add_interests"
    end

    context "add_interests success" do
      it "add_interests" do
        run_rake_task

       
        expect(Interest.all.count).not_to eq(0)
      end
    end
  end


  describe "#add_registered_lobbies" do
    let :run_rake_task do
      Rake::Task["organizations:add_registered_lobbies"].reenable
      Rake.application.invoke_task "organizations:add_registered_lobbies"
    end

    context "add_registered_lobbies success" do
      it "add_registered_lobbies" do
        run_rake_task

       
        expect(RegisteredLobby.all.count).not_to eq(0)
      end
    end
  end

  describe "#update_registered_lobbies_names" do
    let :run_rake_task do
      Rake::Task["organizations:update_registered_lobbies_names"].reenable
      Rake.application.invoke_task "organizations:update_registered_lobbies_names"
    end

    context "update_registered_lobbies_names success" do
      it "update_registered_lobbies_names" do
        registered = create(:registered_lobby)
        registered.name= "no_record"
        registered.save


        run_rake_task

       
        expect(RegisteredLobby.find_by(name: "Ninguno")).not_to eq(nil)
      end
    end
  end

end

