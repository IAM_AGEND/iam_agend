require 'spec_helper'

require "rake"

describe "interest rake" do
  before do
    Rake.application.rake_require "tasks/interest"
    Rake::Task.define_task(:environment)
  end

  describe "#insert_code" do
    let :run_rake_task do
      Rake::Task["interest:insert_code"].reenable
      Rake.application.invoke_task "interest:insert_code"
    end

    context "insert_code success" do
      it "add_interests" do
        run_rake_task     
        expect(Interest.all.where(code: nil).count).to eq(0)
      end
    end
  end 

end