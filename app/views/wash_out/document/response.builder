xml.instruct! :xml, :version => '1.0', :encoding => 'UTF-8'
xml.tag! "soapenv:Envelope", "xmlns:soapenv" => "http://schemas.xmlsoap.org/soap/envelope/",
                            "xmlns:soapenc" => "http://schemas.xmlsoap.org/soap/encoding/",
                            "xmlns:xsd" => "http://www.w3.org/2001/XMLSchema",
                            "xmlns:xsi" => "http://www.w3.org/2001/XMLSchema-instance" do
  if !header.nil?
    xml.tag! "soapenv:Header" do
      xml.tag! "p352:#{@action_spec[:response_tag]}", "xmlns:p352" => "http://wsintbrg.bareg.iam.es"  do
        wsdl_data xml, header
      end
    end
else
    xml.tag! "soapenv:Header"
  end
  xml.tag! "soapenv:Body" do
    #xml.tag! "p352:#{@action_spec[:response_tag]}", "xmlns:p352" => "http://wsintbrg.bareg.iam.es" do
      wsdl_data xml, result
    #end
  end
end