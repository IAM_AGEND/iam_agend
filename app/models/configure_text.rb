class ConfigureText < ActiveRecord::Base
    validates :code, presence: true
    validates_uniqueness_of :code
end