class EventRepresentedEntity < ActiveRecord::Base

  belongs_to :event
  belongs_to :organization
  belongs_to :events_organizations

  validates :name, presence: true

end
