class HoldersAppointment < ActiveRecord::Base
  self.primary_key = :id

  def full_name_comma_with_active
    (self.last_name.to_s.custom_titleize.delete(','). +', '+self.first_name.to_s.custom_titleize.delete(',') + "#{" ("+I18n.t('main.form.no_active').upcase+")" if !self.to.blank?}")
  rescue 
    ""
  end
  
  def readonly?
    true
  end
end
