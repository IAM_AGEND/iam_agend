class SendEmail < ActiveRecord::Base
    validates :auto_start_date, presence: :true, if: :auto_enable
    validates :auto_hour, presence: :true, if: :auto_enable
    validates :auto_frecuence, presence: :true, if: :auto_enable
    validates :auto_user_type, presence: :true, if: :auto_enable
    #validates :auto_excluded_emails, presence: :true, if: :auto_enable
    validates :manual_hour, presence: :true, if: :manual_enable
    validates :manual_from_date, presence: :true, if: :manual_enable
    validates :manual_to_date, presence: :true, if: :manual_enable
    validates :manual_user_type, presence: :true, if: :manual_enable
    #validates :manual_excluded_emails, presence: :true, if: :manual_enable

    def auto_enable
        return false if self.auto_send.to_s == "false"
        true
    end

    def manual_enable
        return false if self.manual_send.to_s == "false"
        true
    end

    def next_send_date
        if !self.auto_start_date.blank? && !self.auto_hour.blank?
            if Time.zone.today < self.auto_start_date
                "#{self.auto_start_date}"
            elsif Time.zone.today == self.auto_start_date 
                if Time.zone.now.hour < self.auto_hour.to_time.hour
                    "#{self.auto_start_date}"
                elsif Time.zone.now.hour == self.auto_hour.to_time.hour && Time.zone.now.min < self.auto_hour.to_time.min
                    "#{self.auto_start_date}"
                else
                    over_next_send_date(self.auto_start_date)
                end
            else
                over_next_send_date(self.auto_start_date)
            end
        end
    end

    def over_next_send_date(date)
        end_of_month = get_end_of_month(date)
        months_diff = ((Date.today - self.auto_start_date)/30).round
        frecuence = get_frecuence
        if end_of_month.blank?
            if months_diff != 0
                date + (frecuence * months_diff).month
            else
                date + frecuence.month
            end
        else
            if months_diff != 0
                date = date + (frecuence * months_diff).month
            else
                date = date + frecuence.month
            end
            get_end_of_month(date, nil, true)
        end
    end

    def next_send_events_from_date(date)
        if !self.auto_start_date.blank? && !self.auto_hour.blank?
            if Time.zone.today < self.auto_start_date
                date
            elsif Time.zone.today == self.auto_start_date 
                if Time.zone.now.hour < self.auto_hour.to_time.hour
                    date
                elsif Time.zone.now.hour == self.auto_hour.to_time.hour && (Time.zone.now - self.auto_hour.to_time) <= 300
                    date
                else
                    over_next_send_events_from_date(date)
                end
            else
                over_next_send_events_from_date(date)
            end
        end
    end

    def over_next_send_events_from_date(date)
        months_diff = ((Date.today - self.auto_start_date)/30).round
        frecuence = get_frecuence
        
        if months_diff == 0
            date + frecuence.month
        else
            date + (frecuence * months_diff).month
        end
    end

    def next_send_events_to_date(date)
        if !self.auto_start_date.blank? && !self.auto_hour.blank?
            if Time.zone.today < self.auto_start_date
                date
            elsif Time.zone.today == self.auto_start_date 
                if Time.zone.now.hour < self.auto_hour.to_time.hour
                    date
                elsif Time.zone.now.hour == self.auto_hour.to_time.hour && (Time.zone.now - self.auto_hour.to_time) <= 300
                    date
                else
                    over_next_send_date(date)
                end
            else
                over_next_send_date(date)
            end
        end
    end


    def get_end_of_month(date, diff = nil, not_null = false)
        if !date.blank?
            if diff.blank?
                end_of_month = Date.new(date.year, date.month, -1)
                if end_of_month == date || not_null
                    end_of_month
                else
                    nil
                end
            else
                date = date + diff.month
                end_of_month = Date.new(date.year, date.month, -1)
                end_of_month
            end
        else
            nil
        end
    end

    def get_frecuence
        case self.auto_frecuence
        when 1
            1
        when 2 
            2
        when 3 
            3
        when 4 
            6
        when 5
            12
        end
    end

    def list_auto_send_emails        
        list = []
        if self.auto_user_type != 2
            Holder.all.each do |holder|
                if !holder.user_email.blank? && (self.is_auto_all_emails || (self.is_auto_included_emails && self.auto_included_emails.include?(holder.user_email)) || (self.is_auto_excluded_emails && !self.auto_excluded_emails.include?(holder.user_email)))
                    if Position.where(holder_id: holder.id, to: nil).any?
                        list.push("#{holder.full_name} (#{holder.user_email})")
                    end
                end
            end
        end
        if self.auto_user_type != 1
            Manage.all.each do |m|
                email = User.find(m.user_id).try(:email)
                if !email.blank? && (self.is_auto_all_emails || (self.is_auto_included_emails && self.auto_included_emails.include?(email)) || (self.is_auto_excluded_emails && !self.auto_excluded_emails.include?(email)))
                    if Position.where(holder_id: m.holder_id, to: nil).any?
                        list.push("#{m.user.try(:full_name)} (#{email})")
                    end    
                end
            end
        end
        list
    end

    def list_emails_events
        list_with = []
        list_without = []
        if self.auto_user_type != 2
            Holder.all.each do |holder|
                if !holder.user_email.blank? && (self.is_auto_all_emails || (self.is_auto_included_emails && self.auto_included_emails.include?(holder.user_email)) || (self.is_auto_excluded_emails && !self.auto_excluded_emails.include?(holder.user_email)))
                    if Position.where(holder_id: holder.id, to: nil).any?
                        if self.next_send_events_from_date(self.auto_from_date) &&
                            self.next_send_events_to_date(self.auto_to_date) &&
                            Event.where("holder_id in (?) and published_at::date >= ? and published_at::date <= ?  ",
                            holder.id, 
                            self.next_send_events_from_date(self.auto_from_date), 
                            self.next_send_events_to_date(self.auto_to_date) ).any?
                            list_with.push("#{holder.full_name} (#{holder.user_email})")
                        else
                            list_without.push("#{holder.full_name} (#{holder.user_email})")
                        end
                    end
                end
            end
        end
        if self.auto_user_type != 1
            Manage.all.each do |m|
                email = User.find(m.user_id).try(:email)
                if !email.blank? && (self.is_auto_all_emails || (self.is_auto_included_emails && self.auto_included_emails.include?(email)) || (self.is_auto_excluded_emails && !self.auto_excluded_emails.include?(email)))
                    if Position.where(holder_id: m.holder_id, to: nil).any?
                        if self.next_send_events_from_date(self.auto_from_date) && 
                            self.next_send_events_to_date(self.auto_to_date) && 
                            Event.where("user_id = ? and published_at::date >= ? and published_at::date <= ?  ",
                            m.user_id, 
                            self.next_send_events_from_date(self.auto_from_date), 
                            self.next_send_events_to_date(self.auto_to_date) ).any?
                            list_with.push("#{m.user.try(:full_name)} (#{email})")
                        else
                            list_without.push("#{m.user.try(:full_name)} (#{email})")
                        end
                    end    
                end
            end
        end
        list_without = list_without - list_with
        [list_with, list_without]
    end

    def list_manual_send_emails
        list = []
        if self.manual_user_type != 2
            Holder.all.each do |holder|
                if !holder.user_email.blank? && (self.is_manual_all_emails || (self.is_manual_included_emails && self.manual_included_emails.include?(holder.user_email)) || (self.is_manual_excluded_emails && !self.manual_excluded_emails.include?(holder.user_email)))
                    if Position.where(holder_id: holder.id, to: nil).any?
                        list.push("#{holder.full_name} (#{holder.user_email})")
                    end
                end
            end
        end
        if self.manual_user_type != 1
            Manage.all.each do |m|
                email = User.find(m.user_id).try(:email)
                if !email.blank? && (self.is_manual_all_emails || (self.is_manual_included_emails && self.manual_included_emails.include?(email)) || (self.is_manual_excluded_emails && !self.manual_excluded_emails.include?(email)))
                    if Position.where(holder_id: m.holder_id, to: nil).any?
                        list.push("#{m.user.try(:full_name)} (#{email})")
                    end    
                end
            end
        end
        list
    end

    def list_manual_event_emails
        list_with = []
        list_without = []
        if self.manual_user_type != 2
            Holder.all.each do |holder|
                if !holder.user_email.blank? && (self.is_manual_all_emails || (self.is_manual_included_emails && self.manual_included_emails.include?(holder.user_email)) || (self.is_manual_excluded_emails && !self.manual_excluded_emails.include?(holder.user_email)))
                    if Position.where(holder_id: holder.id, to: nil).any?
                        if self.manual_from_date.to_date && 
                            self.manual_to_date.to_date &&
                            Event.where("holder_id in (?) and published_at::date >= ? and published_at::date <= ?  ",
                            holder.id, 
                            self.manual_from_date.to_date, 
                            self.manual_to_date.to_date ).any?
                            list_with.push("#{holder.full_name} (#{holder.user_email})")
                        else
                            list_without.push("#{holder.full_name} (#{holder.user_email})")
                        end
                    end
                end
            end
        end
        if self.manual_user_type != 1
            Manage.all.each do |m|
                email = User.find(m.user_id).try(:email)
                if !email.blank? && (self.is_manual_all_emails || (self.is_manual_included_emails && self.manual_included_emails.include?(email)) || (self.is_manual_excluded_emails && !self.manual_excluded_emails.include?(email)))
                    if Position.where(holder_id: m.holder_id, to: nil).any?
                        if Event.where("user_id = ? and published_at::date >= ? and published_at::date <= ?  ",
                            m.user_id, 
                            self.manual_from_date.to_date, 
                            self.manual_to_date.to_date).any?
                            list_with.push("#{m.user.try(:full_name)} (#{email})")
                        else
                            list_without.push("#{m.user.try(:full_name)} (#{email})")
                        end
                    end    
                end
            end
        end
        [list_with, list_without]
    end
end
