class Newsletter < ActiveRecord::Base
  has_many :newsletters_interests, dependent: :destroy
  has_many :interests, through: :newsletters_interests, dependent: :destroy
  has_many :organizations_newsletters, dependent: :destroy
  belongs_to :interest


  validates :subject, presence: true
  validates :body, presence: true
  #validates :interest_id, presence: true
  validates :interests, presence: true

  def list_of_recipient_emails
    list_of_mails = []
    hash_of_recipient_emails.each do |organization|
      list_of_mails << organization[:email]
    end
    list_of_mails << admin_email
  end

  def hash_of_recipient_emails
    hash_emails = []
    if self.draft?
      #organizations =Organization.where(organization_interests: { interest: interest }).joins(:organization_interests) #, communication_term: true)
      
      
      organizations =Organization.joins(:organization_interests).where("organizations.id = organization_interests.organization_id AND organization_interests.interest_id IN (?)", self.interest_ids)
      
      organizations.each do |organization|
        if organization.user_email.blank?
          if !organization.address.email.blank?
            hash_emails.push({:id => organization.id,
              :name => organization.fullname, 
              :email => organization.address.email })
          else !organization.email.blank?
            hash_emails.push({:id => organization.id,
              :name => organization.fullname, 
              :email => organization.email })
          end
        else
          hash_emails.push({:id => organization.id,
            :name => organization.fullname, 
            :email => organization.user_email })
        end
      end
    else
      registered_organizations = self.organizations_newsletters
      registered_organizations.each do |organization_newsletter|
        hash_emails.push({:id => organization_newsletter.organization_id, 
          :name => organization_newsletter.organization.fullname, 
          :email => organization_newsletter.email_send })
      end
    end

    hash_emails.uniq
  end

  def admin_email
    "registrodelobbies@madrid.es"
  end

  def draft?
    sent_at.nil?
  end

  def get_interests
    interest = ""
    self.interests.each {|i| interest = interest + (interest.blank? ? i.name + "." : "<br>" + i.name + ".") }      
    interest
  end

  def deliver
    hash_of_recipient_emails.each do |recipient_email|
      if valid_email?(recipient_email[:email])
        begin
          UserMailer.newsletter(self, recipient_email[:email]).deliver_now

          organization_send_newsletter({
            :organization_id => recipient_email[:id],
            :newsletter_id => self.id,
            :email_send => recipient_email[:email],
          })
          log_delivery(recipient_email[:email])
        rescue => e
       
          begin
            Rails.logger.error("SMS-ERROR: #{e}")
          rescue
          end
          log_delivery(recipient_email[:email], :email_error)
        end
      end
    end
    if valid_email?( admin_email)
      begin
        UserMailer.newsletter(self,  admin_email).deliver_now

        log_delivery( admin_email)
      rescue => e
        begin
          Rails.logger.error("SMS-ERROR: #{e}")
        rescue
        end
        log_delivery( admin_email, :email_error)
      end
    end
  end

  private

    def valid_email?(email)
      email.match(/\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i)
    end

    def organization_send_newsletter(attributtes)
      organizations_newsletter=OrganizationsNewsletter.new(attributtes)
      organizations_newsletter.save
    end

    def log_delivery(recipient_email, action=:email)
      if recipient_email == admin_email
        Log.activity(nil, "admin_email", self)
      else
        organization = Organization.where(email: recipient_email).first
        Log.activity(organization, action, self)
      end
    end
end
