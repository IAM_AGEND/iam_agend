class User < ActiveRecord::Base
  extend UserHelper
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
        :recoverable, :rememberable, :trackable, :validatable
  enum role: { user: 0, admin: 1, lobby: 2 }

  after_initialize :set_default_role, if: :new_record?
  after_initialize :set_active, if: :new_record?

  has_many :parametrizations
  has_many :events
  has_many :manages, dependent: :destroy
  has_many :holders, through: :manages
  belongs_to :organization

  validates :first_name, :last_name, :email, presence: true
  validates :password, presence: true,
                       if: :encrypted_password_changed?,
                       on: :update

  validate :manages_uniqueness
  # validates_format_of :email,:with => /\A[^@\s]+@([^@\s]+\.)+[^@\s]+\z/
  # validates_format_of :phones, :with => /\A([7,6,9]{1})+([0-9]{8})|([9]{1})+([0-9]{8});([9]{1})+([0-9]{8})+\z/, allow_blank: true
  # validates_format_of :movil_phone, :with => /\A([7,6]{1})+([0-9]{8})|([7,6]{1})+([0-9]{8});([7,6]{1})+([0-9]{8})+\z/, allow_blank: true


  accepts_nested_attributes_for :manages, reject_if: :all_blank, allow_destroy: true

  # searchable do
  #   integer :id 
  #   integer :role_id
  #   string :last_name
  #   string :second_last_name
  # end

  scope :active, -> { where(active: true) }

  # def role_id
  #   User.roles[role]
  # end
  
  def change_role_view
    self.role = role_view
  end

  def user_holder?
    return false if self.user_key.blank?
    holder = Holder.find_by(user_key: self.user_key)
    !holder.blank? 
  end

  def user_holder_active?
    return false if self.user_key.blank?
    holder = Holder.find_by(user_key: self.user_key)
    return false if holder.blank? 

    !holder.positions.current.blank?
  end

  def user_manages?
    return false if self.manages.blank?
    self.manages.count > 0
  end

  def name
    (second_last_name.to_s + " " + last_name.to_s + ", " + first_name.to_s).strip
  end

  def email_name
    (last_name.to_s + " " + second_last_name.to_s + ", " + first_name.to_s).strip
  end

  def full_name(separator = ' ')
    (self.first_name.delete(',') + "#{separator}" + self.last_name.delete(',') + ' ' + "#{self.second_last_name.try{|x| x.delete(',')}}").mb_chars.to_s.strip
  end

  def full_name_comma
    full_name(', ')
  end

  def full_name_comma_reverse
    (self.last_name.delete(',') + ' ' + "#{self.second_last_name.try{|x| x.delete(',')}}" + (', ') + self.first_name.delete(',')).mb_chars.to_s.strip
  end

  def manages_uniqueness
    manages = self.manages.reject(&:marked_for_destruction?)
    errors.add(:base, I18n.t('backend.participants_uniqueness')) unless manages.map{|x| x.holder_id}.uniq.count == manages.to_a.count
  end

  def soft_delete
    update_attribute(:deleted_at, Time.zone.now)
  end

  private

  def set_active
    self.active ||= true
  end

  def set_default_role
    self.role ||= :user
  end

  def self.create_from_uweb(role, data)
    return if data["CLAVE_IND"].blank?
    exist = User.find_by(user_key: data['CLAVE_IND'].try{|x| x.strip})
    user = exist.blank? ? User.new : exist

    user.user_key = data["CLAVE_IND"].strip unless data["CLAVE_IND"].try{|x| x.strip}.blank?
    unless data["TELEFONO"].try{|x| x.strip}.blank?
      user.phones = ""
      user.movil_phone = ""
      data["TELEFONO"].strip .split(';').each do |phone|
        if !phone.match(/\A([7,6]{1})+([0-9]{8})|([7,6]{1})+([0-9]{8});([7,6]{1})+([0-9]{8})+\z/).blank?
          user.movil_phone = user.movil_phone + ";" + phone
        else
          user.phones = user.phones + ";" + phone
        end
      end
      user.phones = data["TELEFONO"].strip 
    end

    user.first_name = data["NOMBRE_USUARIO"].strip unless data["NOMBRE_USUARIO"].try{|x| x.strip}.blank?
    user.last_name = data["APELLIDO1_USUARIO"].strip unless data["APELLIDO1_USUARIO"].try{|x| x.strip}.blank?
    user.second_last_name = data["APELLIDO2_USUARIO"].strip unless data["APELLIDO2_USUARIO"].try{|x| x.strip}.blank?
    user.identificator = data["DNI"].strip unless data["DNI"].try{|x| x.strip}.blank?
    user.personal_code = data["NUM_PERSONAL"].strip unless data["NUM_PERSONAL"].try{|x| x.strip}.blank?
    user.positions = "#{data["CARGO"].strip} - #{data["UNIDAD"].strip}" if !data["CARGO"].try{|x| x.strip}.blank? && !data["UNIDAD"].try{|x| x.strip}.blank?

    if data["MAIL"].try{|x| x.strip}.blank?
      user.email = Faker::Internet.email 
    else
      user.email = data["MAIL"].try{|x| x.strip} unless data["MAIL"].blank?
    end

    if !data["BAJA_LOGICA"].blank? && data["BAJA_LOGICA"].try{|x| x.strip}.to_i == 0
      user.active = 1
    elsif !data["BAJA_LOGICA"].blank? && data["BAJA_LOGICA"].try{|x| x.strip}.to_i == 1
      user.active = 0
    end
    user.password = SecureRandom.uuid unless user.persisted?
    user.password_confirmation= user.password unless user.persisted?
    user.role = role
    user
  end
end
