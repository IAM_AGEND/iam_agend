class Notification < ActiveRecord::Base

    validates :status, presence: true

    scope :pending_view, ->(){ where("status in (?)", [I18n.t('backend.notification_status.pending'),
        I18n.t('backend.notification_status.recover')])}
    scope :historic_view, ->(){ where("status not in (?)", [I18n.t('backend.notification_status.pending'),
        I18n.t('backend.notification_status.recover')])}

    def self.get_cod_tipos
        {:insert => ["876","619"], :update => ["877","620"], :delete => ["878","621"],  :reneue => ["879"]}.freeze
    end


    def self.get_cod_tipos_array
      [
        [I18n.t('backend.notification_action.insert'),'876'],
        [I18n.t('backend.notification_action.update'),'877'],
        [I18n.t('backend.notification_action.delete'),'878'],
        [I18n.t('backend.notification_action.reneue'),'879']
      ]
    end

    def self.get_tipo_doc
        {"N" => "DNI/NIF", "R" => "NIE", "P" => "Pasaporte"}.freeze
    end

    def self.get_tipo_num
        {"KM." => "Km", "NUM" => "Num", "S/N" => "S/N"}.freeze
    end

    def self.get_statuses
        {
            :pending => I18n.t('backend.notification_status.pending'),
            :recover => I18n.t('backend.notification_status.recover'),
            :reject => I18n.t('backend.notification_status.reject'),
            :permit => I18n.t('backend.notification_status.permit'),
            :manual_permit => I18n.t('backend.notification_status.manual_permit')
        }.freeze
    end

    def self.get_columns
        [
            :id,
            :full_name,
            :type_action,
            :status,
            :status_date,
            :errors_detected,
            :created_at_convert
        ]
    end

    def full_name
      if !self.modification_data["organization"].blank?         
        if self.modification_data["organization"]["business_name"].blank?
          str = self.modification_data["organization"]["name"]
          str += " #{self.modification_data["organization"]["first_surname"]}"  if self.modification_data["organization"]["first_surname"].present?
          str += " #{self.modification_data["organization"]["second_surname"]}" if self.modification_data["organization"]["second_surname"].present?
        else 
          str = self.modification_data["organization"]["business_name"]
        end
        return str
      else
        aux_name = ""
        aux_first_surname = ""
        aux_second_surname = ""
        aux_social = ""
        
        formulario = update_data(self)
        datos =formulario.try{|x| x["datosComunes"]}.try{|x| x['datosInteresado']}.try{|x| x["variables"]}.try{|x| x["variable"]}

        return "No data" if datos.blank?

        datos.each do |value|
            case value["clave"].to_s
            when "COMUNES_INTERESADO_APELLIDO1"
                aux_first_surname = value["valor"] 
            when "COMUNES_INTERESADO_APELLIDO2"
                aux_second_surname = value["valor"] 
            when "COMUNES_INTERESADO_NOMBRE"
                aux_name = value["valor"] 
            when "COMUNES_INTERESADO_RAZONSOCIAL"
                aux_social = value["valor"] 
            end
        end
        aux_social.blank? ? "#{aux_name} #{aux_first_surname} #{aux_second_surname}" : aux_social
      end
    rescue 
        ""
    end

    def reference
      if !self.modification_data["organization"].blank?
        organization = Organization.new(self.modification_data["organization"])        
        return organization.try(:reference)
      else
        aux_reference = ""
        
        data = Hash.from_xml(modification_data["peticionIniciarExpediente"]["xmlDatosEntrada"].blank? ? '' : modification_data["peticionIniciarExpediente"]["xmlDatosEntrada"].gsub("<![CDATA[",'').gsub("]]>",''))
       

        return "No data" if data.blank?

        data_reference = data['intercambioIAM']['datosAnotacion']
        aux_reference = data_reference['numAnotacion']
      end
    rescue 
        ""
    end

    def identifier
      if !self.modification_data["organization"].blank?
        organization = Organization.new(self.modification_data["organization"])        
        return organization.try(:identifier)
      else
        aux_identifier = ""

        formulario = update_data(self)
        datos =formulario.try{|x| x["datosComunes"]}.try{|x| x['datosInteresado']}.try{|x| x["variables"]}.try{|x| x["variable"]}

        return "No data" if datos.blank?

        datos.each do |value|
            case value["clave"].to_s
            when "COMUNES_INTERESADO_NUMIDENT"
              aux_identifier = value["valor"] 
            end
        end

        aux_identifier
      end
    rescue 
        ""
    end

    def user_email
      if !self.modification_data["organization"].blank?
        organization = Organization.new(self.modification_data["organization"])        
        return organization.try(:user).try(:email)
      else
        aux_identifier = ""
        formulario = update_data(self)
        datos =formulario.try{|x| x['datosEspecificos']}.try{|x| x["variables"]}.try{|x| x["variable"]}

        return "No data" if datos.blank?

        datos.each do |value|
            case value["clave"].to_s
            when "COMUNES_COMUNICACION_EMAIL","COMUNICACION_EMAIL"         
              aux_identifier =value["valor"]
            end
        end

        aux_identifier
      end
    rescue 
        ""
    end

    def type_action
       return I18n.t('backend.notification_action.insert') if Notification.get_cod_tipos[:insert].include?(self.type_anotation.to_s)
       return I18n.t('backend.notification_action.update') if Notification.get_cod_tipos[:update].include?(self.type_anotation.to_s)
       return I18n.t('backend.notification_action.delete') if Notification.get_cod_tipos[:delete].include?(self.type_anotation.to_s)
       return I18n.t('backend.notification_action.reneue') if Notification.get_cod_tipos[:reneue].include?(self.type_anotation.to_s)
       ""
    rescue
        ""
    end

    def status_date
        self.change_status_at.strftime("%d/%m/%Y")
    rescue
        ""
    end

    def errors_detected
      Notification.set_errors_detected(self)
      require 'json'

      aux = "<ul>"

      JSON[self.validations_description].each do |error|
        aux = aux + "<li>- #{error}</li>"
      end

      aux = aux + "</ul>"

      aux.html_safe
    rescue
      ""
    end

    def created_at_convert
        self.created_at.strftime("%d/%m/%Y")
    rescue
        ""
    end

    def self.set_errors_detected(notification)
        organization = notification.saveInicioExpediente(notification)
        organization.valid?
        org = Organization.find_by(identifier: organization.identifier)
        if notification.type_action != I18n.t('backend.notification_action.insert') && !org.blank? && organization.user.try(:email) == org.user.try(:email)
          if organization.errors.messages[:"user.email"].present?
            organization.errors.messages[:"user.email"].delete_at(0)
          end
          if organization.errors.messages[:"user.password"].present?
            organization.errors.messages[:"user.password"].delete_at(0)
          end
          if organization.errors.count == 1 && organization.errors.messages[:"user"].present?
            organization.errors.messages[:"user"].delete_at(0)
          end
        end
        if !organization.blank? && (organization.errors.full_messages.count > 0)
          notification.validations_description = organization.errors.full_messages  
        else
          notification.validations_description = ""
        end
      notification.save!
    end

    

    def update_data(notification)
        data = Hash.from_xml(notification.modification_data["peticionIniciarExpediente"]["xmlDatosEntrada"].blank? ? '' : notification.modification_data["peticionIniciarExpediente"]["xmlDatosEntrada"].gsub("<![CDATA[",'').gsub("]]>",''))
      
        return nil if data.blank?      
        idformularios = data["borrador"].try {|x| x["formulario"]}
        idformularios = data["intercambioIAM"].try {|x| x["formulario"]} if idformularios.blank?
        formulario_aux = nil

        verificacion_aux = false

        idformularios.each do |formulario|
            idformulario = formulario.try {|x| x["nombre"]}
            verificacion_aux = true if !verificacion_aux && (Notification.get_cod_tipos[:insert].include?(notification.modification_data["peticionIniciarExpediente"]["xmlDatosEntrada"].to_s) || Notification.get_cod_tipos[:insert].include?(idformulario.to_s))
            verificacion_aux = true if !verificacion_aux && (Notification.get_cod_tipos[:update].include?(notification.modification_data["peticionIniciarExpediente"]["xmlDatosEntrada"].to_s) || Notification.get_cod_tipos[:update].include?(idformulario.to_s))
            verificacion_aux = true if !verificacion_aux && (Notification.get_cod_tipos[:delete].include?(notification.modification_data["peticionIniciarExpediente"]["xmlDatosEntrada"].to_s) || Notification.get_cod_tipos[:delete].include?(idformulario.to_s))
            if verificacion_aux
            formulario_aux = formulario
            break;
            end
        end
        formulario_aux
    end

    # ------------------------------------------------------------------------
    # FUNCION SAVE_INICIO_EXPEDIENTE (PARAMS = {})
    # ------------------------------------------------------------------------
    def saveInicioExpediente(notification)
      if !notification.modification_data["organization"].blank?
        begin
          notification.modification_data["organization"]["id"] = Organization.find_by(identifier: modification_data["organization"]["identifier"]).try(:id) if !notification.modification_data["organization"]["identifier"].blank? && notification.type_action != I18n.t('backend.notification_action.insert')
          notification.modification_data["organization"]["user_attributes"]["id"] = User.find_by(organization_id: notification.modification_data["organization"]["id"]).try(:id) if !modification_data["organization"]["user_attributes"].blank? && notification.type_action != I18n.t('backend.notification_action.insert') && !notification.modification_data["organization"]["id"].blank?
        
          organization = Organization.new(notification.modification_data["organization"])
          if !notification.modification_data["organization"]["range_subventions_attributes"].blank? && notification.modification_data["organization"]["range_subventions_attributes"].count == organization.range_subventions.count
            notification.modification_data["organization"]["range_subventions_attributes"].each.with_index do |rs,index|
              notification.modification_data["organization"]["range_subventions_attributes"].select{|k,v| k == rs.first}.first.second["id"] = organization.range_subventions[index].try(:id) if rs.second["id"].blank?
            end
          end
          if !notification.modification_data["organization"]["represented_entities_attributes"].blank? && notification.modification_data["organization"]["represented_entities_attributes"].count == organization.represented_entities.count
            notification.modification_data["organization"]["represented_entities_attributes"].each.with_index do |rs,index|
              notification.modification_data["organization"]["represented_entities_attributes"].select{|k,v| k == rs.first}.first.second["id"] = organization.represented_entities[index].try(:id) if rs.second["id"].blank?
            end
          end
          organization.id = Organization.find_by(identifier: organization.identifier).try(:id) if notification.type_action != I18n.t('backend.notification_action.insert')
          organization.entity_type = 2
          data = Hash.from_xml(notification.modification_data["peticionIniciarExpediente"]["xmlDatosEntrada"].blank? ? '' : notification.modification_data["peticionIniciarExpediente"]["xmlDatosEntrada"].gsub("<![CDATA[",'').gsub("]]>",''))
          data_reference = data['intercambioIAM']['datosAnotacion']
          organization.reference = data_reference['numAnotacion'].to_s.strip
          organization
        rescue
          formulario = update_data(notification)
          numident = formulario.try{|x| x['datosComunes']}.try{|x| x['datosInteresado']}.try{|x| x['variables']}.try{|x| x['variable']}.try{|x| x.find{|y| y['clave'].to_s == 'COMUNES_INTERESADO_NUMIDENT'}}.try{|x| x["valor"]}.to_s.strip
        
          organization =  Organization.find_by(identifier: numident) if notification.type_action != I18n.t('backend.notification_action.insert')
          organization = Organization.new if organization.blank?
          organization.id = Organization.find_by(identifier: numident).try(:id) 
          data = Hash.from_xml(notification.modification_data["peticionIniciarExpediente"]["xmlDatosEntrada"].blank? ? '' : notification.modification_data["peticionIniciarExpediente"]["xmlDatosEntrada"].gsub("<![CDATA[",'').gsub("]]>",''))
          data_reference = data['intercambioIAM']['datosAnotacion']
          organization.reference = data_reference['numAnotacion'].to_s.strip
          organization.inscription_date = data_reference['fechaAlta'].to_date if !data_reference['fechaAlta'].blank?
          date = "#{data_reference['horaPresentacion'][0..1]}:#{data_reference['horaPresentacion'][2..3]}:#{data_reference['horaPresentacion'][4..5]}".to_datetime if !data_reference['horaPresentacion'].blank? && data_reference['horaPresentacion'].length == 6
          organization.inscription_date = organization.inscription_date&.change({hour: date.hour, min: date.min, sec: date.sec}) if date
          # if notification.status != Notification.get_statuses[:permit]
          #   organization.id = nil
          #   organization = organization.dup
          # end
  
          if !organization.blank? 
            organization.entity_type = 2
            organization.renovation_date = Time.zone.now if Notification.get_cod_tipos[:reneue].include?(notification.modification_data["peticionIniciarExpediente"]["codTipoExpediente"].to_s) || Notification.get_cod_tipos[:insert].include?(notification.modification_data["peticionIniciarExpediente"]["codTipoExpediente"].to_s)
            organization.inscription_date = Time.zone.now if Notification.get_cod_tipos[:insert].include?(notification.modification_data["peticionIniciarExpediente"]["codTipoExpediente"].to_s)
  
            organization.canceled_at = Time.zone.now if Notification.get_cod_tipos[:delete].include?(notification.modification_data["peticionIniciarExpediente"]["codTipoExpediente"].to_s)
            organization.modification_date = Time.zone.now if Notification.get_cod_tipos[:update].include?(notification.modification_data["peticionIniciarExpediente"]["codTipoExpediente"].to_s)
            organization.entity_type = 2        
  
            organization = list_docs_formats(notification.modification_data["peticionIniciarExpediente"]["listDocs"], notification.modification_data["peticionIniciarExpediente"]["listFormatos"], organization)
            organization = guardado_parametros_formulario(formulario, organization, notification.modification_data["peticionIniciarExpediente"]["codTipoExpediente"])
            if organization.reference_changed? && notification.type_action == I18n.t('backend.notification_action.reneue') && !organization.reference_was.nil?
              organization.reference = organization.reference_was
            end
            if organization.inscription_date_changed? && notification.type_action != I18n.t('backend.notification_action.insert') && !organization.inscription_date_was.nil?
              organization.inscription_date = organization.inscription_date_was
            end
            
            organization 
          else
            nil
          end
        end
      else
        formulario = update_data(notification)
        numident = formulario.try{|x| x['datosComunes']}.try{|x| x['datosInteresado']}.try{|x| x['variables']}.try{|x| x['variable']}.try{|x| x.find{|y| y['clave'].to_s == 'COMUNES_INTERESADO_NUMIDENT'}}.try{|x| x["valor"]}.to_s.strip
      
        organization =  Organization.find_by(identifier: numident) if notification.type_action != I18n.t('backend.notification_action.insert')
        organization = Organization.new if organization.blank?
        organization.id = Organization.find_by(identifier: numident).try(:id) 
        data = Hash.from_xml(notification.modification_data["peticionIniciarExpediente"]["xmlDatosEntrada"].blank? ? '' : notification.modification_data["peticionIniciarExpediente"]["xmlDatosEntrada"].gsub("<![CDATA[",'').gsub("]]>",''))
        data_reference = data['intercambioIAM']['datosAnotacion']
        organization.reference = data_reference['numAnotacion'].to_s.strip
        organization.inscription_date = data_reference['fechaAlta'].to_date if !data_reference['fechaAlta'].blank?
        date = "#{data_reference['horaPresentacion'][0..1]}:#{data_reference['horaPresentacion'][2..3]}:#{data_reference['horaPresentacion'][4..5]}".to_datetime if !data_reference['horaPresentacion'].blank? && data_reference['horaPresentacion'].length == 6
        organization.inscription_date = organization.inscription_date&.change({hour: date.hour, min: date.min, sec: date.sec}) if date
        # if notification.status != Notification.get_statuses[:permit]
        #   organization.id = nil
        #   organization = organization.dup
        # end

        if !organization.blank? 
          organization.entity_type = 2
          organization.renovation_date = Time.zone.now if Notification.get_cod_tipos[:reneue].include?(notification.modification_data["peticionIniciarExpediente"]["codTipoExpediente"].to_s) || Notification.get_cod_tipos[:insert].include?(notification.modification_data["peticionIniciarExpediente"]["codTipoExpediente"].to_s)
          organization.inscription_date = Time.zone.now if Notification.get_cod_tipos[:insert].include?(notification.modification_data["peticionIniciarExpediente"]["codTipoExpediente"].to_s)

          organization.canceled_at = Time.zone.now if Notification.get_cod_tipos[:delete].include?(notification.modification_data["peticionIniciarExpediente"]["codTipoExpediente"].to_s)
          organization.modification_date = Time.zone.now if Notification.get_cod_tipos[:update].include?(notification.modification_data["peticionIniciarExpediente"]["codTipoExpediente"].to_s)
          organization.entity_type = 2        

          organization = list_docs_formats(notification.modification_data["peticionIniciarExpediente"]["listDocs"], notification.modification_data["peticionIniciarExpediente"]["listFormatos"], organization)
          organization = guardado_parametros_formulario(formulario, organization, notification.modification_data["peticionIniciarExpediente"]["codTipoExpediente"])
          if organization.reference_changed? && notification.type_action == I18n.t('backend.notification_action.reneue') && !organization.reference_was.nil?
            organization.reference = organization.reference_was
          end
          if organization.inscription_date_changed? && notification.type_action != I18n.t('backend.notification_action.insert') && !organization.inscription_date_was.nil?
            organization.inscription_date = organization.inscription_date_was
          end
          
          organization 
        else
          nil
        end
      end
    end

    def saveInicioExpedienteWith(organization, notification)
      if !notification.modification_data["organization"].blank?
        notification.modification_data["organization"]["user_attributes"]["id"] = User.find_by(organization_id: organization.id).try(:id)
        organization.assign_attributes(notification.modification_data["organization"])

        organization
      else
        #return nil
        formulario = update_data(notification)  
        numident = formulario.try{|x| x['datosComunes']}.try{|x| x['datosInteresado']}.try{|x| x['variables']}.try{|x| x['variable']}.try{|x| x.find{|y| y['clave'].to_s == 'COMUNES_INTERESADO_NUMIDENT'}}.try{|x| x["valor"]}
      
        if !organization.blank?  
          organization.renovation_date = Time.zone.now if Notification.get_cod_tipos[:reneue].include?(notification.modification_data["peticionIniciarExpediente"]["codTipoExpediente"].to_s) || Notification.get_cod_tipos[:insert].include?(notification.modification_data["peticionIniciarExpediente"]["codTipoExpediente"].to_s)
          organization.inscription_date = Time.zone.now if Notification.get_cod_tipos[:insert].include?(notification.modification_data["peticionIniciarExpediente"]["codTipoExpediente"].to_s)
       
          organization.canceled_at = Time.zone.now if Notification.get_cod_tipos[:delete].include?(notification.modification_data["peticionIniciarExpediente"]["codTipoExpediente"].to_s)
          organization.modification_date = Time.zone.now if Notification.get_cod_tipos[:update].include?(notification.modification_data["peticionIniciarExpediente"]["codTipoExpediente"].to_s)
          organization.entity_type = 2        

          organization = list_docs_formats(notification.modification_data["peticionIniciarExpediente"]["listDocs"], notification.modification_data["peticionIniciarExpediente"]["listFormatos"], organization)
          organization = guardado_parametros_formulario(formulario, organization, notification.modification_data["peticionIniciarExpediente"]["codTipoExpediente"])
        

          organization 
        else
          nil
        end
      end
    end

    # ------------------------------------------------------------------------
    # FUNCION LIST_DOCS_FORMATS (DOCS, FORMATS, ORGANIZATION)
    # ------------------------------------------------------------------------
    def list_docs_formats(docs, formats, organization)
      return organization if organization.blank?
      organization.organizations_documents.each {|x| x.destroy }

      if !docs.blank? && !organization.blank?
        docs.each do |doc, value|
          document = OrganizationsDocument.new(id_document: value, type_document: 0, organization: organization)
          organization.organizations_documents << document
        end
      end

      # if !formats.blank? && !organization.blank?
      #   formats.each do |doc|
      #     document = OrganizationsDocument.new(id_document: doc[:idTipoDocumento], type_document: 1, organization: organization)
      #     organization.organizations_documents << document
      #   end
      # end

      organization
    end

    # ------------------------------------------------------------------------
    # FUNCION GUARDADO_PARAMETROS_FORMULARIO (DATA, ORGANIZATION, CODTIPO)
    # ------------------------------------------------------------------------
    def guardado_parametros_formulario(data, organization, codTipo)
      return nil if data.blank? ||  !Notification.get_cod_tipos[:insert].include?(codTipo.to_s) && organization.blank? || data['datosComunes'].blank? && data['datosEspecificos'].blank?
      organization = get_datos_comunes( data['datosComunes'], organization, codTipo)
      organization = get_datos_especificos(data['datosEspecificos'], organization, codTipo)
     
      organization
    rescue
      nil
    end

    # ------------------------------------------------------------------------
    # FUNCION GET_DATOS_COMUNES (DATA, ORGANIZATION, CODTIPO)
    # ------------------------------------------------------------------------
    def get_datos_comunes(data, organization, codTipo)
      return organization if data.blank?
      organization = get_datos_interesado(data['datosInteresado'], organization, codTipo)
      organization = get_datos_representante(data['datosRepresentante'], organization, codTipo)
      organization = get_datos_notificacion(data['datosNotificacion'], organization, codTipo)
     
      organization
    rescue
      nil
    end

    # ------------------------------------------------------------------------
    # FUNCION GET_DATOS_ESPECIFICOS (DATA, ORGANIZATION, CODTIPO)
    # ------------------------------------------------------------------------
    def get_datos_especificos(data, organization, codTipo)
      return organization if data.try{|x| x["variables"]}.try{|x| x["variable"]}.blank?     
      require 'securerandom' 
      password = SecureRandom.hex(8)
      represented_entity = nil
      interest_ids = []
      registered_lobby_ids= []

      data["variables"]["variable"].each do |value|
        begin
          case value["clave"].to_s
          when "DECLARA1"
            organization.certain_term = !value["valor"].blank? && value["valor"].to_s == "true"
          when "DECLARA2"
            organization.code_of_conduct_term = !value["valor"].blank? && value["valor"].to_s == "true"
          when "DECLARA3"
            organization.other_term = !value["valor"].blank? && value["valor"].to_s == "true"
          when "DECLARA4"
            organization.gift_term = !value["valor"].blank? && value["valor"].to_s == "true"
          when "DECLARA5"
            organization.lobby_term = !value["valor"].blank? && value["valor"].to_s == "true"
          when "DECLARA6"
            organization.public_term = !value["valor"].blank? && value["valor"].to_s == "true"
          when "DECLARA7"
            organization.communication_term = !value["valor"].blank? && value["valor"].to_s == "true"
          when "REGISTRO_INSCRIPCION_COMUNIDAD_MADRID"
            exist = organization.organization_registered_lobbies.find_by(registered_lobby: RegisteredLobby.find_by(code: 6))
            if !value["valor"].blank? && value["valor"].to_s == "true" && exist.blank?
              aux = RegisteredLobby.find_by(code: 6)
              if !aux.blank?
                registered_lobby_ids.push(aux.try(:id))
                organization.organization_registered_lobbies << OrganizationRegisteredLobby.new(organization: organization, registered_lobby: aux)
              end
            else
              if !exist.blank? && value["valor"].to_s == "false"
                exist.destroy 
              elsif value["valor"].to_s == "true"
                aux = RegisteredLobby.find_by(code: 6)
                registered_lobby_ids.push(aux.try(:id)) if !aux.blank?
              end
            end              
          when "COMUNES_COMUNICACION_NOMBRE","COMUNICACION_NOMBRE"
            organization.association(:user).replace(User.new(active: 1, role: 2,password: password, password_confirmation: password), false) if organization.user.blank?
            organization.user.first_name = value["valor"] 
          when "REGISTRO_INSCRIPCION_OTROS"
            exist = organization.organization_registered_lobbies.find_by(registered_lobby: RegisteredLobby.find_by(code: 7))
            if !value["valor"].blank? && value["valor"].to_s == "true" && exist.blank?
              aux = RegisteredLobby.find_by(code: 7)
              if !aux.blank?
                registered_lobby_ids.push(aux.try(:id))
                organization.organization_registered_lobbies << OrganizationRegisteredLobby.new(organization: organization, registered_lobby: aux)
              end
            else
              if !exist.blank? && value["valor"].to_s == "false"
                exist.destroy 
              elsif value["valor"].to_s == "true"
                aux = RegisteredLobby.find_by(code: 7)
                registered_lobby_ids.push(aux.try(:id)) if !aux.blank?
              end
            end
          when "ACTIVIDAD_PROPIA_RADIO"
            organization.own_lobby_activity = !value["valor"].blank? && value["valor"].to_s == "SPROPIA"
          when "ACTIVIDAD_PROPIA_REAYUDAS"
            organization.subvention = !value["valor"].blank? && value["valor"].to_s.upcase == "S"
          when "REGISTRO_INSCRIPCION_UE"
            exist = organization.organization_registered_lobbies.find_by(registered_lobby: RegisteredLobby.find_by(code: 4))
            if !value["valor"].blank? && value["valor"].to_s == "true" && exist.blank?
              aux = RegisteredLobby.find_by(code: 4)
              if !aux.blank?
                registered_lobby_ids.push(aux.try(:id))
                organization.organization_registered_lobbies << OrganizationRegisteredLobby.new(organization: organization, registered_lobby: aux)
              end
            else
              if !exist.blank? && value["valor"].to_s == "false"
                exist.destroy 
              elsif value["valor"].to_s == "true"
                aux = RegisteredLobby.find_by(code: 4)
                registered_lobby_ids.push(aux.try(:id)) if !aux.blank?
              end
            end
          when "COMUNES_COMUNICACION_TIPODOC","COMUNICACION_TIPODOC"
            organization.association(:user).replace(User.new(active: 1, role: 2,password: password, password_confirmation: password), false) if organization.user.blank?
            organization.user.user_key_type = Notification.get_tipo_doc[value["valor"]]
          when "AYUDAS_FONDOS_PUBLICOS_IMPORTE1"
            if organization.range_subventions.blank? && !value["valor"].blank?
              if defined?(@range_sub1) && !@range_sub1.blank?
                @range_sub1.import = value["valor"]
              else
                @range_sub1 = RangeSubvention.new(import: value["valor"], item: organization)
              end
            elsif organization.range_subventions.count > 0 && !value["valor"].blank? && !organization.range_subventions[0].blank?
              organization.range_subventions[0].import = value["valor"]
            end
          when "AYUDAS_FONDOS_PUBLICOS_IMPORTE2"
            if organization.range_subventions.blank? && !value["valor"].blank?
              if defined?(@range_sub2) && !@range_sub2.blank?
                @range_sub2.import = value["valor"]
              else
                @range_sub2 = RangeSubvention.new(import: value["valor"], item: organization)
              end 
            elsif !organization.range_subventions[0].blank? && !value["valor"].blank?
              if !organization.range_subventions[1].blank?
                organization.range_subventions[1].import =value["valor"]
              else
                if defined?(@range_sub2) && !@range_sub2.blank?
                  @range_sub2.import = value["valor"]
                else
                  @range_sub2 = RangeSubvention.new(import: value["valor"], item: organization)
                end 
              end
            end
          when "REGISTRO_INSCRIPCION_CASTILLA_LA_MANCHA"
            exist = organization.organization_registered_lobbies.find_by(registered_lobby: RegisteredLobby.find_by(code: 2))
            if !value["valor"].blank? && value["valor"].to_s == "true" && exist.blank?
              aux = RegisteredLobby.find_by(code: 2)
              if !aux.blank?
                registered_lobby_ids.push(aux.try(:id))
                organization.organization_registered_lobbies << OrganizationRegisteredLobby.new(organization: organization, registered_lobby: aux)
              end
            else
              if !exist.blank? && value["valor"].to_s == "false"
                exist.destroy 
              elsif value["valor"].to_s == "true"
                aux = RegisteredLobby.find_by(code: 2)
                registered_lobby_ids.push(aux.try(:id)) if !aux.blank?
              end
            end
          when "COMUNES_COMUNICACION_TELEFONO","COMUNICACION_TELEFONO"
            organization.association(:user).replace(User.new(active: 1, role: 2,password: password, password_confirmation: password), false) if organization.user.blank?
            if self.type_action == I18n.t('backend.notification_action.reneue')
              organization.user.phones = value["valor"]
            else
              organization.user.phones = organization.user.phones.blank? ? value["valor"] : value["valor"].blank? ? organization.user.phones : organization.user.phones + ";" + value["valor"]
            end
          when "AYUDAS_FONDOS_PUBLICOS_IMPORTE3"
            if organization.range_subventions.blank? && !value["valor"].blank?
              if defined?(@range_sub3) && !@range_sub3.blank?
                @range_sub3.import = value["valor"]
              else
                @range_sub3 = RangeSubvention.new(import: value["valor"], item: organization)
              end
            elsif !organization.range_subventions[1].blank? && !value["valor"].blank?
              if !organization.range_subventions[2].blank?
                organization.range_subventions[2].import = value["valor"]
              else
                if defined?(@range_sub3) && !@range_sub3.blank?
                  @range_sub3.import = value["valor"]
                else
                  @range_sub3 = RangeSubvention.new(import: value["valor"], item: organization)
                end 
              end
            end
          when "GRUPO_TRABAJO_MESA_CONSEJO_NO"
            organization.in_group_public_administration = !value["valor"].blank? && value["valor"].to_s == "true" # ? true : organization.in_group_public_administration
          when "COMUNES_COMUNICACION_MOVIL","COMUNICACION_MOVIL"
            organization.association(:user).replace(User.new(active: 1, role: 2,password: password, password_confirmation: password), false) if organization.user.blank?
            organization.user.phones = organization.user.phones.blank? ? value["valor"] : value["valor"].blank? ? organization.user.phones : organization.user.phones + ";" + value["valor"]
          when "AREAS_INTERES1"
            exist = organization.organization_interests.find_by(interest: Interest.find_by(code: 1))
            if !value["valor"].blank? && value["valor"].to_s == "true" && exist.blank?
              aux = Interest.find_by(code: 1)
              if !aux.blank?
                interest_ids.push(aux.try(:id))
                organization.organization_interests << OrganizationInterest.new(organization: organization, interest: aux)
              end
            else
              if !exist.blank? && value["valor"].to_s == "false"
                exist.destroy 
              elsif value["valor"].to_s == "true"
                aux = Interest.find_by(code: 1)
                interest_ids.push(aux.try(:id)) if !aux.blank?
              end
            end
          when "AREAS_INTERES2"
            exist = organization.organization_interests.find_by(interest: Interest.find_by(code: 2))
            if !value["valor"].blank? && value["valor"].to_s == "true" && exist.blank?
              aux = Interest.find_by(code: 2)
              if !aux.blank?
                interest_ids.push(aux.try(:id))
                organization.organization_interests << OrganizationInterest.new(organization: organization, interest: aux)
              end
            else
              if !exist.blank? && value["valor"].to_s == "false"
                exist.destroy 
              elsif value["valor"].to_s == "true"
                aux = Interest.find_by(code: 2)
                interest_ids.push(aux.try(:id)) if !aux.blank?
              end
            end
          when "AREAS_INTERES3"
            exist = organization.organization_interests.find_by(interest: Interest.find_by(code: 3))
            if !value["valor"].blank? && value["valor"].to_s == "true" && exist.blank?
              aux = Interest.find_by(code: 3)
              if !aux.blank?
                interest_ids.push(aux.try(:id))
                organization.organization_interests << OrganizationInterest.new(organization: organization, interest: aux)
              end
            else
              if !exist.blank? && value["valor"].to_s == "false"
                exist.destroy
              elsif value["valor"].to_s == "true"
                aux = Interest.find_by(code: 3)
                interest_ids.push(aux.try(:id)) if !aux.blank?
              end
            end
          when "AREAS_INTERES4"
            exist = organization.organization_interests.find_by(interest: Interest.find_by(code: 4))
            if !value["valor"].blank? && value["valor"].to_s == "true" && exist.blank?
              aux = Interest.find_by(code: 4)
              if !aux.blank?
                interest_ids.push(aux.try(:id))
                organization.organization_interests << OrganizationInterest.new(organization: organization, interest: aux)
              end
            else
              if !exist.blank? && value["valor"].to_s == "false"
                exist.destroy
              elsif value["valor"].to_s == "true"
                aux = Interest.find_by(code: 4) 
                interest_ids.push(aux.try(:id)) if !aux.blank?
              end
            end
          when "AREAS_INTERES5"
            exist = organization.organization_interests.find_by(interest: Interest.find_by(code: 5))
            if !value["valor"].blank? && value["valor"].to_s == "true" && exist.blank?
              aux = Interest.find_by(code: 5)
              if !aux.blank?
                interest_ids.push(aux.try(:id))
                organization.organization_interests << OrganizationInterest.new(organization: organization, interest: aux)
              end
            else
              if !exist.blank? && value["valor"].to_s == "false"
                exist.destroy
              elsif value["valor"].to_s == "true"
                aux = Interest.find_by(code: 5)
                interest_ids.push(aux.try(:id)) if !aux.blank?
              end
            end
          when "AREAS_INTERES6"
            exist = organization.organization_interests.find_by(interest: Interest.find_by(code: 6))
            if !value["valor"].blank? && value["valor"].to_s == "true" && exist.blank?
              aux = Interest.find_by(code: 6)
              if !aux.blank?
                interest_ids.push(aux.try(:id))
                organization.organization_interests << OrganizationInterest.new(organization: organization, interest: aux)
              end
            else
              if !exist.blank? && value["valor"].to_s == "false"
                exist.destroy
              elsif value["valor"].to_s == "true"
                aux = Interest.find_by(code: 6)
                interest_ids.push(aux.try(:id)) if !aux.blank?
              end
            end
          when "AREAS_INTERES7"
            exist = organization.organization_interests.find_by(interest: Interest.find_by(code: 7))
            if !value["valor"].blank? && value["valor"].to_s == "true" && exist.blank?
              aux = Interest.find_by(code: 7)
              if !aux.blank?
                interest_ids.push(aux.try(:id))
                organization.organization_interests << OrganizationInterest.new(organization: organization, interest: aux)
              end
            else
              if !exist.blank? && value["valor"].to_s == "false"
                exist.destroy
              elsif value["valor"].to_s == "true"
                aux = Interest.find_by(code: 7)
                interest_ids.push(aux.try(:id)) if !aux.blank?
              end
            end
          when "AREAS_INTERES8"
            exist = organization.organization_interests.find_by(interest: Interest.find_by(code: 8))
            if !value["valor"].blank? && value["valor"].to_s == "true" && exist.blank?
              aux = Interest.find_by(code: 8)
              if !aux.blank?
                interest_ids.push(aux.try(:id))
                organization.organization_interests << OrganizationInterest.new(organization: organization, interest: aux)
              end
            else
              if !exist.blank? && value["valor"].to_s == "false"
                exist.destroy
              elsif value["valor"].to_s == "true"
                aux = Interest.find_by(code: 8)
                interest_ids.push(aux.try(:id)) if !aux.blank?
              end
            end
          when "AREAS_INTERES9"
            exist = organization.organization_interests.find_by(interest: Interest.find_by(code: 9))
            if !value["valor"].blank? && value["valor"].to_s == "true" && exist.blank?
              aux = Interest.find_by(code: 9)
              if !aux.blank?
                interest_ids.push(aux.try(:id))
                organization.organization_interests << OrganizationInterest.new(organization: organization, interest: aux)
              end
            else
              if !exist.blank? && value["valor"].to_s == "false"
                exist.destroy
              elsif value["valor"].to_s == "true"
                aux = Interest.find_by(code: 9)
                interest_ids.push(aux.try(:id)) if !aux.blank?
              end
            end
          when "AREAS_INTERES10"
            exist = organization.organization_interests.find_by(interest: Interest.find_by(code: 10))
            if !value["valor"].blank? && value["valor"].to_s == "true" && exist.blank?
              aux = Interest.find_by(code: 10)
              if !aux.blank?
                interest_ids.push(aux.try(:id))
                organization.organization_interests << OrganizationInterest.new(organization: organization, interest: aux)
              end
            else
              if !exist.blank? && value["valor"].to_s == "false"
                exist.destroy
              elsif value["valor"].to_s == "true"
                aux = Interest.find_by(code: 10)
                interest_ids.push(aux.try(:id)) if !aux.blank?
              end
            end
          when "AREAS_INTERES11"
            exist = organization.organization_interests.find_by(interest: Interest.find_by(code: 11))
            if !value["valor"].blank? && value["valor"].to_s == "true" && exist.blank?
              aux = Interest.find_by(code: 11)
              if !aux.blank?
                interest_ids.push(aux.try(:id))
                organization.organization_interests << OrganizationInterest.new(organization: organization, interest: aux)
              end
            else
              if !exist.blank? && value["valor"].to_s == "false"
                exist.destroy
              elsif value["valor"].to_s == "true"
                aux = Interest.find_by(code: 11)
                interest_ids.push(aux.try(:id)) if !aux.blank?
              end
            end
          when "AREAS_INTERES12"
            exist = organization.organization_interests.find_by(interest: Interest.find_by(code: 12))
            if !value["valor"].blank? && value["valor"].to_s == "true" && exist.blank?
              aux = Interest.find_by(code: 12)
              if !aux.blank?
                interest_ids.push(aux.try(:id))
                organization.organization_interests << OrganizationInterest.new(organization: organization, interest: aux)
              end
            else
              if !exist.blank? && value["valor"].to_s == "false"
                exist.destroy
              elsif value["valor"].to_s == "true"
                aux = Interest.find_by(code: 12)
                interest_ids.push(aux.try(:id)) if !aux.blank?
              end
            end
          when "AREAS_INTERES13"
            exist = organization.organization_interests.find_by(interest: Interest.find_by(code: 13))
            if !value["valor"].blank? && value["valor"].to_s == "true" && exist.blank?
              aux = Interest.find_by(code: 13)
              if !aux.blank?
                interest_ids.push(aux.try(:id))
                organization.organization_interests << OrganizationInterest.new(organization: organization, interest: aux)
              end
            else
              if !exist.blank? && value["valor"].to_s == "false"
                exist.destroy
              elsif value["valor"].to_s == "true"
                aux = Interest.find_by(code: 13)
                interest_ids.push(aux.try(:id)) if !aux.blank?
              end
            end
          when "AREAS_INTERES14"
            exist = organization.organization_interests.find_by(interest: Interest.find_by(code: 14))
            if !value["valor"].blank? && value["valor"].to_s == "true" && exist.blank?
              aux = Interest.find_by(code: 14)
              if !aux.blank?
                interest_ids.push(aux.try(:id))
                organization.organization_interests << OrganizationInterest.new(organization: organization, interest: aux)
              end
            else
              if !exist.blank? && value["valor"].to_s == "false"
                exist.destroy
              elsif value["valor"].to_s == "true"
                aux = Interest.find_by(code: 14)
                interest_ids.push(aux.try(:id)) if !aux.blank?
              end
            end
          when "AREAS_INTERES15"
            exist = organization.organization_interests.find_by(interest: Interest.find_by(code: 15))
            if !value["valor"].blank? && value["valor"].to_s == "true" && exist.blank?
              aux = Interest.find_by(code: 15)
              if !aux.blank?
                interest_ids.push(aux.try(:id))
                organization.organization_interests << OrganizationInterest.new(organization: organization, interest: aux)
              end
            else
              if !exist.blank? && value["valor"].to_s == "false"
                exist.destroy
              elsif value["valor"].to_s == "true"
                aux = Interest.find_by(code: 15)
                interest_ids.push(aux.try(:id)) if !aux.blank?
              end
            end
          when "AREAS_INTERES16"
            exist = organization.organization_interests.find_by(interest: Interest.find_by(code: 16))
            if !value["valor"].blank? && value["valor"].to_s == "true" && exist.blank?
              aux = Interest.find_by(code: 16)
              if !aux.blank?
                interest_ids.push(aux.try(:id))
                organization.organization_interests << OrganizationInterest.new(organization: organization, interest: aux)
              end
            else
              if !exist.blank? && value["valor"].to_s == "false"
                exist.destroy
              elsif value["valor"].to_s == "true"
                aux = Interest.find_by(code: 16)
                interest_ids.push(aux.try(:id)) if !aux.blank?
              end
            end
          when "AREAS_INTERES17"
            exist = organization.organization_interests.find_by(interest: Interest.find_by(code: 17))
            if !value["valor"].blank? && value["valor"].to_s == "true" && exist.blank?
              aux = Interest.find_by(code: 17)
              if !aux.blank?
                interest_ids.push(aux.try(:id))
                organization.organization_interests << OrganizationInterest.new(organization: organization, interest: aux)
              end
            else
              if !exist.blank? && value["valor"].to_s == "false"
                exist.destroy
              elsif value["valor"].to_s == "true"
                aux = Interest.find_by(code: 17)
                interest_ids.push(aux.try(:id)) if !aux.blank?
              end
            end
          when "AREAS_INTERES18"
            exist = organization.organization_interests.find_by(interest: Interest.find_by(code: 18))
            if !value["valor"].blank? && value["valor"].to_s == "true" && exist.blank?
              aux = Interest.find_by(code: 18)
              if !aux.blank?
                interest_ids.push(aux.try(:id))
                organization.organization_interests << OrganizationInterest.new(organization: organization, interest: aux)
              end
            else
              if !exist.blank? && value["valor"].to_s == "false"
                exist.destroy
              elsif value["valor"].to_s == "true"
                aux = Interest.find_by(code: 18)
                interest_ids.push(aux.try(:id)) if !aux.blank?
              end
            end
          when "AREAS_INTERES19"
            exist = organization.organization_interests.find_by(interest: Interest.find_by(code: 19))
            if !value["valor"].blank? && value["valor"].to_s == "true" && exist.blank?
              aux = Interest.find_by(code: 19)
              if !aux.blank?
                interest_ids.push(aux.try(:id))
                organization.organization_interests << OrganizationInterest.new(organization: organization, interest: aux)
              end
            else
              if !exist.blank? && value["valor"].to_s == "false"
                exist.destroy
              elsif value["valor"].to_s == "true"
                aux = Interest.find_by(code: 19)
                interest_ids.push(aux.try(:id)) if !aux.blank?
              end
            end
          when "AREAS_INTERES20"
            exist = organization.organization_interests.find_by(interest: Interest.find_by(code: 20))
            if !value["valor"].blank? && value["valor"].to_s == "true" && exist.blank?
              aux = Interest.find_by(code: 20)
              if !aux.blank?
                interest_ids.push(aux.try(:id))
                organization.organization_interests << OrganizationInterest.new(organization: organization, interest: aux)
              end
            else
              if !exist.blank? && value["valor"].to_s == "false"
                exist.destroy
              elsif value["valor"].to_s == "true"
                aux = Interest.find_by(code: 20)
                interest_ids.push(aux.try(:id)) if !aux.blank?
              end
            end
          when "AREAS_INTERES21"
            exist = organization.organization_interests.find_by(interest: Interest.find_by(code: 21))
            if !value["valor"].blank? && value["valor"].to_s == "true" && exist.blank?
              aux = Interest.find_by(code: 21)
              if !aux.blank?
                interest_ids.push(aux.try(:id))
                organization.organization_interests << OrganizationInterest.new(organization: organization, interest: aux)
              end
            else
              if !exist.blank? && value["valor"].to_s == "false"
                exist.destroy
              elsif value["valor"].to_s == "true"
                aux = Interest.find_by(code: 21)
                interest_ids.push(aux.try(:id)) if !aux.blank?
              end
            end
          when "AREAS_INTERES22"
            exist = organization.organization_interests.find_by(interest: Interest.find_by(code: 22))
            if !value["valor"].blank? && value["valor"].to_s == "true" && exist.blank?
              aux = Interest.find_by(code: 22)
              if !aux.blank?
                interest_ids.push(aux.try(:id))
                organization.organization_interests << OrganizationInterest.new(organization: organization, interest: aux)
              end
            else
              if !exist.blank? && value["valor"].to_s == "false"
                exist.destroy
              elsif value["valor"].to_s == "true"
                aux = Interest.find_by(code: 22)
                interest_ids.push(aux.try(:id)) if !aux.blank?
              end
            end
          when "AREAS_INTERES23"
            exist = organization.organization_interests.find_by(interest: Interest.find_by(code: 23))
            if !value["valor"].blank? && value["valor"].to_s == "true" && exist.blank?
              aux = Interest.find_by(code: 23)
              if !aux.blank?
                interest_ids.push(aux.try(:id))
                organization.organization_interests << OrganizationInterest.new(organization: organization, interest: aux)
              end
            else
              if !exist.blank? && value["valor"].to_s == "false"
                exist.destroy
              elsif value["valor"].to_s == "true"
                aux = Interest.find_by(code: 23)
                interest_ids.push(aux.try(:id)) if !aux.blank?
              end
            end
          when "AREAS_INTERES24"
            exist = organization.organization_interests.find_by(interest: Interest.find_by(code: 24))
            if !value["valor"].blank? && value["valor"].to_s == "true" && exist.blank?
              aux = Interest.find_by(code: 24)
              if !aux.blank?
                interest_ids.push(aux.try(:id))
                organization.organization_interests << OrganizationInterest.new(organization: organization, interest: aux)
              end
            else
              if !exist.blank? && value["valor"].to_s == "false"
                exist.destroy
              elsif value["valor"].to_s == "true"
                aux = Interest.find_by(code: 24)
                interest_ids.push(aux.try(:id)) if !aux.blank?
              end
            end
          when "AREAS_INTERES25"
            exist = organization.organization_interests.find_by(interest: Interest.find_by(code: 25))
            if !value["valor"].blank? && value["valor"].to_s == "true" && exist.blank?
              aux = Interest.find_by(code: 25)
              if !aux.blank?
                interest_ids.push(aux.try(:id))
                organization.organization_interests << OrganizationInterest.new(organization: organization, interest: aux)
              end
            else
              if !exist.blank? && value["valor"].to_s == "false"
                exist.destroy
              elsif value["valor"].to_s == "true"
                aux = Interest.find_by(code: 25)
                interest_ids.push(aux.try(:id)) if !aux.blank?
              end
            end
          when "AREAS_INTERES26"
            exist = organization.organization_interests.find_by(interest: Interest.find_by(code: 26))
            if !value["valor"].blank? && value["valor"].to_s == "true" && exist.blank?
              aux = Interest.find_by(code: 26)
              if !aux.blank?
                interest_ids.push(aux.try(:id))
                organization.organization_interests << OrganizationInterest.new(organization: organization, interest: aux)
              end
            else
              if !exist.blank? && value["valor"].to_s == "false"
                exist.destroy
              elsif value["valor"].to_s == "true"
                aux = Interest.find_by(code: 26)
                interest_ids.push(aux.try(:id)) if !aux.blank?
              end
            end
          when "AREAS_INTERES27"
            exist = organization.organization_interests.find_by(interest: Interest.find_by(code: 27))
            if !value["valor"].blank? && value["valor"].to_s == "true" && exist.blank?
              aux = Interest.find_by(code: 27)
              if !aux.blank?
                interest_ids.push(aux.try(:id))
                organization.organization_interests << OrganizationInterest.new(organization: organization, interest: aux)
              end
            else
              if !exist.blank? && value["valor"].to_s == "false"
                exist.destroy
              elsif value["valor"].to_s == "true"
                aux = Interest.find_by(code: 27)
                interest_ids.push(aux.try(:id)) if !aux.blank?
              end
            end
          when "AREAS_INTERES28"
            exist = organization.organization_interests.find_by(interest: Interest.find_by(code: 28))
            if !value["valor"].blank? && value["valor"].to_s == "true" && exist.blank?
              aux = Interest.find_by(code: 28)
              if !aux.blank?
                interest_ids.push(aux.try(:id))
                organization.organization_interests << OrganizationInterest.new(organization: organization, interest: aux)
              end
            else
              if !exist.blank? && value["valor"].to_s == "false"
                exist.destroy
              elsif value["valor"].to_s == "true"
                aux = Interest.find_by(code: 28)
                interest_ids.push(aux.try(:id)) if !aux.blank?
              end
            end
          when "AREAS_INTERES29"
            exist = organization.organization_interests.find_by(interest: Interest.find_by(code: 29))
            if !value["valor"].blank? && value["valor"].to_s == "true" && exist.blank?
              aux = Interest.find_by(code: 29)
              if !aux.blank?
                interest_ids.push(aux.try(:id))
                organization.organization_interests << OrganizationInterest.new(organization: organization, interest: aux)
              end
            else
              if !exist.blank? && value["valor"].to_s == "false"
                exist.destroy
              elsif value["valor"].to_s == "true"
                aux = Interest.find_by(code: 29)
                interest_ids.push(aux.try(:id)) if !aux.blank?
              end
            end
          when "AREAS_INTERES30"
            exist = organization.organization_interests.find_by(interest: Interest.find_by(code: 30))
            if !value["valor"].blank? && value["valor"].to_s == "true" && exist.blank?
              aux = Interest.find_by(code: 30)
              if !aux.blank?
                interest_ids.push(aux.try(:id))
                organization.organization_interests << OrganizationInterest.new(organization: organization, interest: aux)
              end
            else
              if !exist.blank? && value["valor"].to_s == "false"
                exist.destroy
              elsif value["valor"].to_s == "true"
                aux = Interest.find_by(code: 30)
                interest_ids.push(aux.try(:id)) if !aux.blank?
              end
            end
          when "AYUDAS_FONDOS_PUBLICOS_SI"
            organization.subvention_public_administration = !value["valor"].blank? && value["valor"].to_s == "true"
          when "ACTIVIDAD_PROPIA_EJANUAL"
            organization.fiscal_year = value["valor"]
          when "COMUNES_COMUNICACION_EMAIL","COMUNICACION_EMAIL"
            organization.association(:user).replace(User.new(active: 1, role: 2,password: password, password_confirmation: password), false) if organization.user.blank?
            if !organization.user.blank? && !organization.user.id.blank? && organization.user.email != value["valor"].strip
              organization.user.password = password
              organization.user.password_confirmation = password
            end
            organization.user.email = value["valor"].strip
          when "ENTIDAD_PRIVADA_SIN_ANIMO_LUCRO_DESGLOSE_CUANTIAS"
            organization.contract_breakdown = value["valor"]
          when "COMUNES_COMUNICACION_NUMIDENT","COMUNICACION_NUMIDENT"
            organization.association(:user).replace(User.new(active: 1, role: 2,password: password, password_confirmation: password), false) if organization.user.blank?
            organization.user.user_key =value["valor"]
          when "AYUDAS_FONDOS_PUBLICOS_ENTIDAD1"
            if organization.range_subventions.blank? && !value["valor"].blank?
              if defined?(@range_sub1) && !@range_sub1.blank?
                @range_sub1.entity_name = value["valor"]
              else
                @range_sub1 = RangeSubvention.new(entity_name: value["valor"], item: organization)
              end
            elsif !organization.range_subventions[0].blank? && !value["valor"].blank?
              organization.range_subventions[0].entity_name = value["valor"]
            end
          when "AYUDAS_FONDOS_PUBLICOS_ENTIDAD2"
            if organization.range_subventions.blank? && !value["valor"].blank?
              if defined?(@range_sub2) && !@range_sub2.blank?
                @range_sub2.entity_name = value["valor"]
              else
                @range_sub2 = RangeSubvention.new(entity_name: value["valor"], item: organization)
              end
            elsif !organization.range_subventions[0].blank? && !value["valor"].blank?
              if !organization.range_subventions[1].blank?
                organization.range_subventions[1].entity_name = value["valor"]
              else
                if defined?(@range_sub2) && !@range_sub2.blank?
                  @range_sub2.entity_name = value["valor"]
                else
                  @range_sub2 = RangeSubvention.new(entity_name: value["valor"], item: organization)
                end
              end
            end
          when "AYUDAS_FONDOS_PUBLICOS_ENTIDAD3"
            if organization.range_subventions.blank? && !value["valor"].blank?
              if defined?(@range_sub3) && !@range_sub3.blank?
                @range_sub3.entity_name = value["valor"]
              else
                @range_sub3 = RangeSubvention.new(entity_name: value["valor"], item: organization)
              end
            elsif !organization.range_subventions[1].blank? && !value["valor"].blank?
              if !organization.range_subventions[2].blank?
                organization.range_subventions[2].entity_name = value["valor"]
              else
                if defined?(@range_sub3) && !@range_sub3.blank?
                  @range_sub3.entity_name = value["valor"]
                else
                  @range_sub3 = RangeSubvention.new(entity_name: value["valor"], item: organization)
                end
              end
            end
          when "ENTIDAD_PRIVADA_SIN_ANIMO_LUCRO_PRESUPUESTO"
            organization.contract_total_budget = value["valor"]
          when "FINALIDAD"
            if value["valor"].to_s == "INS" 
              if organization.organizations_renewals.blank? && organization.id.blank?
                organization.renovation_date = self.created_at
                renewal= OrganizationsRenewal.new(organization: organization,renovation_date: organization.renovation_date, expiration_date: organization.expired_date, active: true)
                organization.organizations_renewals << renewal
                renewal.save
              end
            elsif value["valor"].to_s == "REN"
              self.update(type_anotation: "879")
              organization.renovation_date = self.created_at
              if !organization.organizations_renewals.blank? && organization.organizations_renewals.find_by(renovation_date: organization.renovation_date).blank?
                organization.organizations_renewals.each do |ren|
                  ren.active = false
                  ren.save
                end
              end
              if organization.organizations_renewals.find_by(renovation_date: organization.renovation_date).blank?  
                renewal= OrganizationsRenewal.new(organization: organization,renovation_date: organization.renovation_date, expiration_date: organization.expired_date, active: true)
                organization.organizations_renewals << renewal
                renewal.save
              end       
            end
          when "AYUDAS_FONDOS_PUBLICOS_NO"
            organization.subvention_public_administration = false if !value["valor"].blank? && value["valor"].to_s == "true" # ? false : organization.subvention_public_administration
          when "ACTIVIDAD_PROPIA_CELEBRA"
            organization.contract = !value["valor"].blank? && value["valor"].to_s.upcase == "S"
          when "REGISTRO_INSCRIPCION_CNMC"
            exist = organization.organization_registered_lobbies.find_by(registered_lobby: RegisteredLobby.find_by(code: 3))
            if !value["valor"].blank? && value["valor"].to_s == "true" && exist.blank?
              aux = RegisteredLobby.find_by(code: 3)
              if !aux.blank?
                registered_lobby_ids.push(aux.try(:id))
                organization.organization_registered_lobbies << OrganizationRegisteredLobby.new(organization: organization, registered_lobby: aux)
              end
            else
              if !exist.blank? && value["valor"].to_s == "false"
                exist.destroy 
              elsif value["valor"].to_s == "true"
                aux = RegisteredLobby.find_by(code: 3)
                registered_lobby_ids.push(aux.try(:id)) if !aux.blank?
              end
            end
          when "GRUPO_TRABAJO_MESA_CONSEJO_SI"          
            organization.in_group_public_administration = !value["valor"].blank? && value["valor"].to_s == "true" ? true : false
          when "REGISTRO_INSCRIPCION_RADIO"
            exist = organization.organization_registered_lobbies.find_by(registered_lobby: RegisteredLobby.find_by(code: 1))
            if !value["valor"].blank? && value["valor"].to_s == "NOREG"  && exist.blank?
              aux = RegisteredLobby.find_by(code: 1)
              if !aux.blank?
                registered_lobby_ids.push(aux.try(:id))
                organization.organization_registered_lobbies << OrganizationRegisteredLobby.new(organization: organization, registered_lobby: aux)
              end
            else
              if !exist.blank? && value["valor"].to_s == "false"
                exist.destroy 
              elsif value["valor"].to_s == "true"
                aux = RegisteredLobby.find_by(code: 1)
                registered_lobby_ids.push(aux.try(:id)) if !aux.blank?
              end
            end
          when "GRUPO_TRABAJO_MESA_CONSEJO"
            organization.text_group_public_administration = value["valor"]
          when "ENTIDAD_PRIVADA_ANIMO_LUCRO_VOLUMEN"
            organization.contract_turnover = value["valor"]
          when "ACTIVIDAD_PROPIA_FONDOS"
            begin
              organization.range_fund = value["valor"].to_i == 0 ? nil : value["valor"].to_i - 1
            rescue
            end
          when "REGISTRO_INSCRIPCION_GENERAL"
            exist = organization.organization_registered_lobbies.find_by(registered_lobby: RegisteredLobby.find_by(code: 5))
            if !value["valor"].blank? && value["valor"].to_s == "true" && exist.blank?
              aux = RegisteredLobby.find_by(code: 5)
              if !aux.blank?
                registered_lobby_ids.push(aux.try(:id))
                organization.organization_registered_lobbies << OrganizationRegisteredLobby.new(organization: organization, registered_lobby: aux)
              end
            else
              if !exist.blank? && value["valor"].to_s == "false"
                exist.destroy 
              elsif value["valor"].to_s == "true"
                aux = RegisteredLobby.find_by(code: 5)
                registered_lobby_ids.push(aux.try(:id)) if !aux.blank?
              end
            end
          when "ENTIDAD_PRIVADA_SIN_ANIMO_LUCRO_FUENTES_FINANCIACION"
            organization.contract_financing = value["valor"]
          when "COMUNES_COMUNICACION_APELLIDO2","COMUNICACION_APELLIDO2"
            organization.association(:user).replace(User.new(active: 1, role: 2,password: password, password_confirmation: password), false) if organization.user.blank?
            organization.user.second_last_name = value["valor"]
          when "COMUNES_COMUNICACION_APELLIDO1","COMUNICACION_APELLIDO1"
            organization.association(:user).replace(User.new(active: 1, role: 2,password: password, password_confirmation: password), false) if organization.user.blank?
            organization.user.last_name = value["valor"]
          when "DNI_USUARIO"
            organization.association(:user).replace(User.new(active: 1, role: 2,password: password, password_confirmation: password), false) if organization.user.blank?
            organization.user.identificator = value["valor"]
          when "REGISTRO_INSCRIPCION_OTROSDESC"
            organization.other_registered_lobby_desc = value["valor"]
          when "FONDOS_REPRESENTA"
            represented_entity = RepresentedEntity.new(organization: organization) if represented_entity.nil? && (!value["valor"].blank? || organization.represented_entities.count == 0)
            represented_entity.range_fund = value["valor"] if !represented_entity.nil?
          when "EJERCICIO_REPRESENTA"
            represented_entity = RepresentedEntity.new(organization: organization) if represented_entity.nil? && (!value["valor"].blank? || organization.represented_entities.count == 0)
            represented_entity.fiscal_year = value["valor"] if !represented_entity.nil?
          when "APELLIDO1_REPRESENTA"
            represented_entity = RepresentedEntity.new(organization: organization) if represented_entity.nil? && (!value["valor"].blank? || organization.represented_entities.count == 0)
            represented_entity.first_surname = value["valor"] if !represented_entity.nil?
          when "NOMBRE_REPRESENTA"
            represented_entity = RepresentedEntity.new(organization: organization) if represented_entity.nil? && (!value["valor"].blank? || organization.represented_entities.count == 0)
            represented_entity.name = value["valor"] if !represented_entity.nil?
          when "APELLIDO2_REPRESENTA"
            represented_entity = RepresentedEntity.new(organization: organization) if represented_entity.nil? && (!value["valor"].blank? || organization.represented_entities.count == 0)
            represented_entity.second_surname = value["valor"] if !represented_entity.nil?
          when "ENTIDAD_CON_REPRESENTA"
            represented_entity = RepresentedEntity.new(organization: organization) if represented_entity.nil? && (!value["valor"].blank? || organization.represented_entities.count == 0)
            represented_entity.contract = !value["valor"].blank? && value["valor"].to_s == "true" if !represented_entity.nil?
          when "ENTIDAD_AYUDA_REPRESENTA"
            represented_entity = RepresentedEntity.new(organization: organization) if represented_entity.nil? && (!value["valor"].blank? || organization.represented_entities.count == 0)
            represented_entity.subvention = !value["valor"].blank? && value["valor"].to_s == "true" if !represented_entity.nil?
          when "ACTIVIDAD_AJENA_RADIO"
            organization.foreign_lobby_activity = !value["valor"].blank? && (value["valor"].to_s == "true" || value["valor"].to_s == "SAJ")
          when "FECHA_REPRESENTA"
            represented_entity = RepresentedEntity.new(organization: organization) if represented_entity.nil? && (!value["valor"].blank? || organization.represented_entities.count == 0)
            represented_entity.from = value["valor"] if !represented_entity.nil?
          when "DNI_REPRESENTA"        
            represented_entity = RepresentedEntity.new(organization: organization) if represented_entity.nil? && (!value["valor"].blank? || organization.represented_entities.count == 0) 
            represented_entity.identifier = value["valor"] if !represented_entity.nil?
          when "BAJA_RAZONSOCIAL"
            represented_entity = RepresentedEntity.new(organization: organization) if represented_entity.nil? && (!value["valor"].blank? || organization.represented_entities.count == 0)
            represented_entity.business_name = value["valor"] if !represented_entity.nil?
          when "BAJA_NUMIDENT"
            represented_entity = RepresentedEntity.new(organization: organization) if represented_entity.nil? && (!value["valor"].blank? || organization.represented_entities.count == 0)
            represented_entity.identifier = value["valor"] if !represented_entity.nil?
          when "BAJA_NOMBRE"
            represented_entity = RepresentedEntity.new(organization: organization) if represented_entity.nil? && (!value["valor"].blank? || organization.represented_entities.count == 0)
            represented_entity.name = value["valor"] if !represented_entity.nil?
          when "BAJA_APELLIDO1"
            represented_entity = RepresentedEntity.new(organization: organization) if represented_entity.nil? && (!value["valor"].blank? || organization.represented_entities.count == 0)
            represented_entity.first_surname = value["valor"] if !represented_entity.nil?
          when "BAJA_APELLIDO2"
            represented_entity = RepresentedEntity.new(organization: organization) if represented_entity.nil? && (!value["valor"].blank? || organization.represented_entities.count == 0)
            represented_entity.second_surname = value["valor"] if !represented_entity.nil?
          when "CODLOBBY"
            #organization.id = value["valor"] if !value["valor"].blank?
          when "CHECK_A1"
            organization.category = Category.find_by(code: 2) if (!value["valor"].blank? && value["valor"].to_s == "true")
          when "CHECK_A2"
            organization.category = Category.find_by(code: 3) if (!value["valor"].blank? && value["valor"].to_s == "true")
          when "CHECK_A3"
            organization.category = Category.find_by(code: 4) if (!value["valor"].blank? && value["valor"].to_s == "true")
          when "CHECK_A4"
            organization.category = Category.find_by(code: 5) if (!value["valor"].blank? && value["valor"].to_s == "true")
          when "CHECK_A5"
            organization.category = Category.find_by(code: 6) if (!value["valor"].blank? && value["valor"].to_s == "true")
          when "CHECK_A6"
            organization.category = Category.find_by(code: 7) if (!value["valor"].blank? && value["valor"].to_s == "true")
          when "CHECK_A7"
            organization.category = Category.find_by(code: 8) if (!value["valor"].blank? && value["valor"].to_s == "true")
          when "CHECK_A8"
            organization.category = Category.find_by(code: 9) if (!value["valor"].blank? && value["valor"].to_s == "true")
          when "CHECK_A9"
            organization.category = Category.find_by(code: 10) if (!value["valor"].blank? && value["valor"].to_s == "true")
          when "CHECK_A10"
            organization.category = Category.find_by(code: 11) if (!value["valor"].blank? && value["valor"].to_s == "true")
          when "CHECK_A11"
            organization.category = Category.find_by(code: 12) if (!value["valor"].blank? && value["valor"].to_s == "true")
          when "CHECK_B1"
            organization.category = Category.find_by(code: 13) if (!value["valor"].blank? && value["valor"].to_s == "true")
          when "CHECK_B2"
            organization.category = Category.find_by(code: 14) if (!value["valor"].blank? && value["valor"].to_s == "true")
          when "CHECK_B3"
            organization.category = Category.find_by(code: 15) if (!value["valor"].blank? && value["valor"].to_s == "true")
          when "CHECK_B4"
            organization.category = Category.find_by(code: 16) if (!value["valor"].blank? && value["valor"].to_s == "true")
          when "CHECK_B5"
            organization.category = Category.find_by(code: 17) if (!value["valor"].blank? && value["valor"].to_s == "true")
          when "CHECK_B6"
            organization.category = Category.find_by(code: 18) if (!value["valor"].blank? && value["valor"].to_s == "true")
          when "CHECK_FONDO1"       
            organization.range_fund =  (!value["valor"].blank? && value["valor"].to_s == "true") ? 0 : organization.range_fund
          when "CHECK_FONDO2"       
            organization.range_fund =  (!value["valor"].blank? && value["valor"].to_s == "true") ? 1 : organization.range_fund
          when "CHECK_FONDO3"       
            organization.range_fund =  (!value["valor"].blank? && value["valor"].to_s == "true") ? 2 : organization.range_fund
          when "CHECK_FONDO4"       
            organization.range_fund =  (!value["valor"].blank? && value["valor"].to_s == "true") ? 3 : organization.range_fund
          end
        rescue 
        end
      end     
      
      if represented_entity.nil?
        if organization.represented_entities.count > 0
          represented_entity = organization.represented_entities.last
        else
          represented_entity = RepresentedEntity.new(organization: organization)
        end
      end
      organization.association(:represented_entities).add_to_target represented_entity  
      organization.assign_attributes({interest_ids: interest_ids, registered_lobby_ids: registered_lobby_ids})

      if organization.range_subventions.blank?
        organization.association(:range_subventions).add_to_target(@range_sub1) if defined?(@range_sub1) && !@range_sub1.blank?
        organization.association(:range_subventions).add_to_target(@range_sub2) if defined?(@range_sub2) && !@range_sub2.blank?
        organization.association(:range_subventions).add_to_target(@range_sub3) if defined?(@range_sub3) && !@range_sub3.blank?
      end 

      if self.type_action == "Renovación"
        organization.renovation_date = self.created_at
      elsif self.type_action == "Alta"
        organization.inscription_date = self.created_at
        organization.modification_date = ""
        organization.renovation_date = ""
        organization.canceled_at = ""
      elsif self.type_action == "Modificación"
        organization.modification_date = self.created_at
      elsif self.type_action == "Baja"
        organization.canceled_at = self.created_at
      end
      organization
    end

    # ------------------------------------------------------------------------
    # FUNCION GET_DATOS_INTERESADO (DATA, ORGANIZATION, CODTIPO)
    # ------------------------------------------------------------------------
    def get_datos_interesado(data, organization, codTipo)
      return organization if data.try{|x| x["variables"]}.try{|x| x["variable"]}.blank?
      is_sin_lucro = false
      is_con_lucro = false
      category = nil

      data["variables"]["variable"].each do |value|
        begin
          case value["clave"].to_s
          when "COMUNES_INTERESADO_NUMIDENT"
            organization.identifier = value["valor"]
          when "COMUNES_INTERESADO_PORTAL"
            organization.address = Address.new if organization.address.blank?
            organization.address.gateway = value["valor"]
          when "COMUNES_INTERESADO_ENTIDAD_SIN_ANIMO_LUCRO"         
            is_sin_lucro=true  if !value["valor"].blank? && value["valor"].to_s != "false"           
          when "COMUNES_INTERESADO_CATEGORIA_ENTIDAD_SIN"
            #if is_sin_lucro
              cat_p = Category.find_by(code: 19)
              
              if !cat_p.blank? 
                cat = Category.find_by(category_id: cat_p.id, code: value["valor"])
                if !cat.blank?
                  organization.category = cat
                  organization.category_id = cat.id
                  category =  cat.id
                  
                end
              end
            #end
          when "COMUNES_INTERESADO_FINALIDAD"
            organization.description = value["valor"]
          when "COMUNES_INTERESADO_PLANTA"
            organization.address = Address.new if organization.address.blank?
            organization.address.floor = value["valor"]
          when "COMUNES_INTERESADO_TIPONUM"
            organization.address = Address.new if organization.address.blank?
            organization.address.address_number_type = Notification.get_tipo_num[value["valor"]]
          when "COMUNES_INTERESADO_CHECKEMAIL"
            organization.check_mail = value["valor"].blank?  ? false : value["valor"]
          when "COMUNES_INTERESADO_APELLIDO1"
            organization.first_surname = value["valor"]        
          when "COMUNES_INTERESADO_APELLIDO2"
            organization.second_surname = value["valor"]
          when "COMUNES_INTERESADO_MUNICIPIO"
            organization.address = Address.new if organization.address.blank?
            organization.address.town = value["valor"]
          when "COMUNES_INTERESADO_TIPODOC"
            organization.identifier_type = Notification.get_tipo_doc[value["valor"]]
          when "COMUNES_INTERESADO_ESCALERA"
            organization.address = Address.new if organization.address.blank?
            organization.address.stairs = value["valor"]
          when "COMUNES_INTERESADO_ENTIDAD_CON_ANIMO_LUCRO" 
            is_con_lucro=true  if !value["valor"].blank? && value["valor"].to_s != "false"
          when "COMUNES_INTERESADO_CATEGORIA_ENTIDAD_CON"
            #if is_con_lucro
              cat_p = Category.find_by(code: 20)
              
              if !cat_p.blank? 
                cat = Category.find_by(category_id: cat_p.id, code: value["valor"])
                if !cat.blank?
                  organization.category = cat
                  organization.category_id = cat.id
                  category =  cat.id
                end
              end
            #end
          when "COMUNES_INTERESADO_TELEFONO"
            organization.address = Address.new if organization.address.blank?
            if self.type_action == I18n.t('backend.notification_action.reneue')
              organization.address.phones = value["valor"]
            else
              organization.address.phones = organization.address.phones.blank? ? value["valor"] : value["valor"].blank? ? organization.address.phones : organization.address.phones + ";" + value["valor"]
            end
          when "COMUNES_INTERESADO_PERSONA_FISICA"
            if !value["valor"].blank? && value["valor"].to_s == "true"
              cat = Category.find_by(code: 1) 
              category = cat.id
              organization.category = cat
            end
          when "COMUNES_INTERESADO_PUERTA"
            organization.address = Address.new if organization.address.blank?
            organization.address.door = value["valor"]
          when "COMUNES_INTERESADO_CHECKSMS"
            organization.check_sms = value["valor"].blank? ? false : value["valor"]
          when "COMUNES_INTERESADO_PROVINCIA"
            organization.address = Address.new if organization.address.blank?
            organization.address.province = Province.find_by(name: value["valor"].upcase).try(:name)
          when "COMUNES_INTERESADO_PAIS"
            organization.address = Address.new if organization.address.blank?
            organization.address.country = Country.find_by(name: value["valor"].upcase).try(:name)
          when "COMUNES_INTERESADO_TIPOVIA"
            organization.address = Address.new if organization.address.blank?
            organization.address.address_type = RoadType.find_by(code: value["valor"]).try(:name)        
          when "COMUNES_INTERESADO_MOVIL"
            organization.address = Address.new if organization.address.blank?
            organization.address.phones = organization.address.phones.blank? ? value["valor"] : value["valor"].blank? ? organization.address.phones : organization.address.phones + ";" + value["valor"]
          when "COMUNES_INTERESADO_NUMERO"
            organization.address = Address.new if organization.address.blank?
            organization.address.number = value["valor"]
          when "COMUNES_INTERESADO_RAZONSOCIAL"
            organization.business_name = value["valor"]
          when "COMUNES_INTERESADO_CODPOSTAL"
            organization.address = Address.new if organization.address.blank?
            organization.address.postal_code = value["valor"]
          when "COMUNES_INTERESADO_NOMBRE"
            organization.name = value["valor"]
          when "COMUNES_INTERESADO_EMAIL"
            organization.address = Address.new if organization.address.blank?
            organization.address.email = value["valor"]
          when "COMUNES_INTERESADO_WEB"
            organization.web = value["valor"]    
          when "COMUNES_INTERESADO_NOMBREVIA"
            organization.address = Address.new if organization.address.blank?
            organization.address.address = value["valor"]
          end
        rescue
        end
      end

      organization.assign_attributes(category_id: category)
      organization
    end

    # ------------------------------------------------------------------------
    # FUNCION GET_DATOS_REPRESENTANTE (DATA, ORGANIZATION, CODTIPO)
    # ------------------------------------------------------------------------
    def get_datos_representante(data, organization, codTipo)
      return organization if data.try{|x| x["variables"]}.try{|x| x["variable"]}.blank?
      data["variables"]["variable"].each do |value|
        begin
          case value["clave"].to_s
          when "COMUNES_REPRESENTANTE_NOMBREVIA"
            organization.legal_representant = LegalRepresentant.new(organization: organization, address: Address.new) if organization.legal_representant.blank?
            organization.legal_representant.address.address = value["valor"]
          when "COMUNES_REPRESENTANTE_TIPONUM"
            organization.legal_representant = LegalRepresentant.new(organization: organization, address: Address.new) if organization.legal_representant.blank?
            organization.legal_representant.address.address_number_type = value["valor"]
          when "COMUNES_REPRESENTANTE_MOVIL"
            organization.legal_representant = LegalRepresentant.new(organization: organization, address: Address.new) if organization.legal_representant.blank?
            if self.type_action == I18n.t('backend.notification_action.reneue')
              organization.legal_representant.address.phones = value["valor"]
            else
              organization.legal_representant.address.phones = organization.legal_representant.address.phones.blank? ? value["valor"] : value["valor"].blank? ? organization.legal_representant.address.phones : organization.legal_representant.address.phones + ";" + value["valor"]
            end
          when "COMUNES_REPRESENTANTE_NUMIDENT"
            organization.legal_representant = LegalRepresentant.new(organization: organization, address: Address.new) if organization.legal_representant.blank?
            organization.legal_representant.identifier = value["valor"]
          when "COMUNES_REPRESENTANTE_TELEFONO"
            organization.legal_representant = LegalRepresentant.new(organization: organization, address: Address.new) if organization.legal_representant.blank?
            organization.legal_representant.address.phones = organization.legal_representant.address.phones.blank? ? value["valor"] : value["valor"].blank? ? organization.legal_representant.address.phones : organization.legal_representant.address.phones + ";" + value["valor"]
          when "COMUNES_REPRESENTANTE_CHECKSMS"
            organization.legal_representant.check_sms = value["valor"].blank? ? false : value["valor"]
          when "COMUNES_REPRESENTANTE_MUNICIPIO"
            organization.legal_representant = LegalRepresentant.new(organization: organization, address: Address.new) if organization.legal_representant.blank?
            organization.legal_representant.address.town = value["valor"]
          when "COMUNES_REPRESENTANTE_PROVINCIA"
            organization.legal_representant = LegalRepresentant.new(organization: organization, address: Address.new) if organization.legal_representant.blank?
            organization.legal_representant.address.province = Province.find_by(name: value["valor"].upcase).try(:name)
          when "COMUNES_REPRESENTANTE_ESCALERA"
            organization.legal_representant = LegalRepresentant.new(organization: organization, address: Address.new) if organization.legal_representant.blank?
            organization.legal_representant.address.stairs = value["valor"]
          when "COMUNES_REPRESENTANTE_PLANTA"
            organization.legal_representant = LegalRepresentant.new(organization: organization, address: Address.new) if organization.legal_representant.blank?
            organization.legal_representant.address.floor = value["valor"]
          when "COMUNES_REPRESENTANTE_NUMERO"
            organization.legal_representant = LegalRepresentant.new(organization: organization, address: Address.new) if organization.legal_representant.blank?
            organization.legal_representant.address.number = value["valor"]
          when "COMUNES_REPRESENTANTE_PORTAL"
            organization.legal_representant = LegalRepresentant.new(organization: organization, address: Address.new) if organization.legal_representant.blank?
            organization.legal_representant.address.gateway = value["valor"]
          when "COMUNES_REPRESENTANTE_CHECKEMAIL"
            organization.legal_representant.check_mail = value["valor"].blank? ? false : value["valor"] if !organization.legal_representant.blank?
          when "COMUNES_REPRESENTANTE_NOMBRE"
            organization.legal_representant = LegalRepresentant.new(organization: organization, address: Address.new) if organization.legal_representant.blank?
            organization.legal_representant.name = value["valor"]
          when "COMUNES_REPRESENTANTE_TIPODOC"
            organization.legal_representant = LegalRepresentant.new(organization: organization, address: Address.new) if organization.legal_representant.blank?
            organization.legal_representant.identifier_type = Notification.get_tipo_doc[value["valor"]]
          when "COMUNES_REPRESENTANTE_RAZONSOCIAL"
            organization.legal_representant = LegalRepresentant.new(organization: organization, address: Address.new) if organization.legal_representant.blank?
            organization.legal_representant.business_name = value["valor"]
          when "COMUNES_REPRESENTANTE_EMAIL"
            organization.legal_representant = LegalRepresentant.new(organization: organization, address: Address.new) if organization.legal_representant.blank?
            organization.legal_representant.address.email = value["valor"]
          when "COMUNES_REPRESENTANTE_PUERTA"
            organization.legal_representant = LegalRepresentant.new(organization: organization, address: Address.new) if organization.legal_representant.blank?
            organization.legal_representant.address.door = value["valor"]
          when "COMUNES_REPRESENTANTE_CODPOSTAL"
            organization.legal_representant = LegalRepresentant.new(organization: organization, address: Address.new) if organization.legal_representant.blank?
            organization.legal_representant.address.postal_code = value["valor"]
          when "COMUNES_REPRESENTANTE_PAIS"
            organization.legal_representant = LegalRepresentant.new(organization: organization, address: Address.new) if organization.legal_representant.blank?
            organization.legal_representant.address.country = Country.find_by(name: value["valor"].upcase).try(:name)
          when "COMUNES_REPRESENTANTE_TIPOVIA"
            organization.legal_representant = LegalRepresentant.new(organization: organization, address: Address.new) if organization.legal_representant.blank?
            organization.legal_representant.address.address_type = RoadType.find_by(code: value["valor"]).try(:name)
          when "COMUNES_REPRESENTANTE_APELLIDO1"
            organization.legal_representant = LegalRepresentant.new(organization: organization, address: Address.new) if organization.legal_representant.blank?
            organization.legal_representant.first_surname = value["valor"]
          when "COMUNES_REPRESENTANTE_APELLIDO2"
            organization.legal_representant = LegalRepresentant.new(organization: organization, address: Address.new) if organization.legal_representant.blank?
            organization.legal_representant.second_surname = value["valor"]               
          end
        rescue
        end
      end
      organization
    end

    # ------------------------------------------------------------------------
    # FUNCION GET_DATOS_NOTIFICACION (DATA, ORGANIZATION, CODTIPO)
    # ------------------------------------------------------------------------
    def get_datos_notificacion(data, organization, codTipo)
      return organization if data.try{|x| x["variables"]}.try{|x| x["variable"]}.blank?
      
     
      data["variables"]["variable"].each do |value|
        begin
          case value["clave"].to_s
          when "COMUNES_NOTIFICACION_TIPODOC"          
            organization.notification_effect = NotificationEffect.new(organization: organization, address: Address.new) if organization.notification_effect.blank?
            organization.notification_effect.identifier_type = Notification.get_tipo_doc[value["valor"]]
          when "COMUNES_NOTIFICACION_TIPONUM"
            organization.notification_effect = NotificationEffect.new(organization: organization, address: Address.new) if organization.notification_effect.blank?
            organization.notification_effect.address.address_number_type = Notification.get_tipo_num[value["valor"]]
          when "COMUNES_NOTIFICACION_EMAIL"
            organization.notification_effect = NotificationEffect.new(organization: organization, address: Address.new) if organization.notification_effect.blank?
            organization.notification_effect.address.email = value["valor"]
          when "COMUNES_NOTIFICACION_MOVIL"
            organization.notification_effect = NotificationEffect.new(organization: organization, address: Address.new) if organization.notification_effect.blank?
            if self.type_action == I18n.t('backend.notification_action.reneue')
              organization.notification_effect.address.phones = value["valor"]
            else
              organization.notification_effect.address.phones = organization.notification_effect.address.phones.blank? ? value["valor"] : value["valor"].blank? ? organization.notification_effect.address.phones : organization.notification_effect.address.phones + ";" + value["valor"]      
            end
          when "COMUNES_NOTIFICACION_NOMBRE"
            organization.notification_effect = NotificationEffect.new(organization: organization, address: Address.new) if organization.notification_effect.blank?
            organization.notification_effect.name = value["valor"]
          when "COMUNES_NOTIFICACION_PUERTA"
            organization.notification_effect = NotificationEffect.new(organization: organization, address: Address.new) if organization.notification_effect.blank?
            organization.notification_effect.address.door = value["valor"]
          when "COMUNES_NOTIFICACION_PAIS"
            organization.notification_effect = NotificationEffect.new(organization: organization, address: Address.new) if organization.notification_effect.blank?
            organization.notification_effect.address.country = Country.find_by(name: value["valor"].upcase).try(:name)
          when "COMUNES_NOTIFICACION_NOMBREVIA"
            organization.notification_effect = NotificationEffect.new(organization: organization, address: Address.new) if organization.notification_effect.blank?
            organization.notification_effect.address.address = value["valor"]
          when "COMUNES_NOTIFICACION_NUMERO"
            organization.notification_effect = NotificationEffect.new(organization: organization, address: Address.new) if organization.notification_effect.blank?
            organization.notification_effect.address.number = value["valor"]
          when "COMUNES_NOTIFICACION_PLANTA"
            organization.notification_effect = NotificationEffect.new(organization: organization, address: Address.new) if organization.notification_effect.blank?
            organization.notification_effect.address.floor = value["valor"]
          when "COMUNES_NOTIFICACION_ESCALERA"
            organization.notification_effect = NotificationEffect.new(organization: organization, address: Address.new) if organization.notification_effect.blank?
            organization.notification_effect.address.stairs = value["valor"]
          when "COMUNES_NOTIFICACION_CODPOSTAL"
            organization.notification_effect = NotificationEffect.new(organization: organization, address: Address.new) if organization.notification_effect.blank?
            organization.notification_effect.address.postal_code = value["valor"]
          when "COMUNES_NOTIFICACION_MUNICIPIO"
            organization.notification_effect = NotificationEffect.new(organization: organization, address: Address.new) if organization.notification_effect.blank?
            organization.notification_effect.address.town = value["valor"]
          when "COMUNES_NOTIFICACION_TELEFONO"
            organization.notification_effect = NotificationEffect.new(organization: organization, address: Address.new) if organization.notification_effect.blank?
            organization.notification_effect.address.phones = organization.notification_effect.address.phones.blank? ? value["valor"] : value["valor"].blank? ? organization.notification_effect.address.phones : organization.notification_effect.address.phones + ";" + value["valor"]      
          when "COMUNES_NOTIFICACION_PORTAL"
            organization.notification_effect = NotificationEffect.new(organization: organization, address: Address.new) if organization.notification_effect.blank?
            organization.notification_effect.address.gateway = value["valor"]
          when "COMUNES_NOTIFICACION_PROVINCIA"
            organization.notification_effect = NotificationEffect.new(organization: organization, address: Address.new) if organization.notification_effect.blank?
            organization.notification_effect.address.province = Province.find_by(name: value["valor"].upcase).try(:name)
          when "COMUNES_NOTIFICACION_APELLIDO1"
            organization.notification_effect = NotificationEffect.new(organization: organization, address: Address.new) if organization.notification_effect.blank?
            organization.notification_effect.first_surname = value["valor"]
          when "COMUNES_NOTIFICACION_RAZONSOCIAL"
            organization.notification_effect = NotificationEffect.new(organization: organization, address: Address.new) if organization.notification_effect.blank?
            organization.notification_effect.business_name = value["valor"]
          when "COMUNES_NOTIFICACION_APELLIDO2"
            organization.notification_effect = NotificationEffect.new(organization: organization, address: Address.new) if organization.notification_effect.blank?
            organization.notification_effect.second_surname = value["valor"]        
          when "COMUNES_NOTIFICACION_NUMIDENT"
            organization.notification_effect = NotificationEffect.new(organization: organization, address: Address.new) if organization.notification_effect.blank?
            organization.notification_effect.identifier = value["valor"]
          when "COMUNES_NOTIFICACION_TIPOVIA"
            organization.notification_effect = NotificationEffect.new(organization: organization, address: Address.new) if organization.notification_effect.blank?
            organization.notification_effect.address.address_type = RoadType.find_by(code: value["valor"]).try(:name)
          end
        rescue
        end
      end
      organization
    end
end