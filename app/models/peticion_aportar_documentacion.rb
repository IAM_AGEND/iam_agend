class PeticionAportarDocumentacion < WashOut::Type
    type_name 'PeticionAportarDocumentacion'
    map idExpediente: :integer, 
        refExpediente: :string, 
        xmlDatosEntrada: :string,  
        listDocumentos: ArrayDocs, 
        listFormatos: ArrayFormatos,
        usuario: :string
  end



