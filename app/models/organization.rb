class Organization < ActiveRecord::Base
  extend Admin::OrganizationExporterHelper
  include ValidationHelper
  attr_accessor :invalidate, :validate, :mailer
  

  enum range_fund: [:range_1, :range_2, :range_3, :range_4]
  enum entity_type: { association: 0, federation: 1, lobby: 2 }

  has_many :represented_entities, dependent: :destroy, inverse_of: :organization
  has_many :agents, dependent: :destroy
  has_many :organizations_documents, dependent: :destroy
  has_many :organizations_renewals, dependent: :destroy
  has_many :range_subventions,as: :item, dependent: :destroy
  has_many :organization_interests, dependent: :destroy
  has_many :organizations_newsletters, dependent: :destroy
  has_many :interests, through: :organization_interests, dependent: :destroy
  has_many :events_organizations, dependent: :destroy
  has_many :events, :through => :events_organizations
  has_many :organization_registered_lobbies, dependent: :destroy
  has_many :registered_lobbies, through: :organization_registered_lobbies, dependent: :destroy
  has_many :attachments, dependent: :destroy
  has_many :event_represented_entities, dependent: :destroy, inverse_of: :event
  has_many :event_agents, dependent: :destroy, inverse_of: :event
  has_many :send_mailer_historics, dependent: :destroy
  has_one :user, dependent: :destroy
  has_one :legal_representant, dependent: :destroy

  has_one :notification_effect, dependent: :destroy
  belongs_to :category
  belongs_to :address, dependent: :destroy
  belongs_to :organization_status

  accepts_nested_attributes_for :legal_representant, update_only: true, allow_destroy: true
  accepts_nested_attributes_for :notification_effect, update_only: true, allow_destroy: true
  accepts_nested_attributes_for :user
  accepts_nested_attributes_for :represented_entities, allow_destroy: true, reject_if: :all_blank
  accepts_nested_attributes_for :organizations_documents, allow_destroy: true, reject_if: :all_blank
  accepts_nested_attributes_for :address, allow_destroy: true
  accepts_nested_attributes_for :range_subventions, allow_destroy: true, reject_if: :all_blank
  accepts_nested_attributes_for :registered_lobbies, allow_destroy: true, reject_if: :all_blank
  accepts_nested_attributes_for :attachments, allow_destroy: true, reject_if: :all_blank

  before_validation :invalidate_organization, :validate_organization
  after_create :set_dates

  validates_associated :user

  scope :active, -> { where(invalidated_at: nil, canceled_at: nil) }
  scope :invalidated, -> { where('invalidated_at is not null') }
  scope :validated, -> { where('invalidated_at is null') }
  scope :expired, -> { where('invalidated_at is null') }
  scope :next_expired, -> { where('invalidated_at is null') }
  scope :lobbies, -> { where('entity_type = ?', 2) }
  scope :full_like, ->(name) { where("identifier ilike ? OR business_name ilike ? OR name ilike ? OR first_surname ilike ? OR second_surname ilike ?", name, name, name, name,name ) }


  def old_organization
    self.inscription_date.blank? && self.renovation_date.blank? || !self.inscription_date.blank? && self.inscription_date < Time.zone.parse("2021-05-24") && self.renovation_date.blank? || !self.renovation_date.blank? && self.renovation_date < Time.zone.parse("2021-05-24") 
  end

  def is_physical_person
    category.try(:code).to_i == 1
  end

  def is_person
    validadorNIF_DNI_NIE(identifier) || validatorPasaport(identifier)
  end

  def entity_type_id
    Organization.entity_types[entity_type]
  end

  def expired_date
    data_year = DataPreference.find_by(title: "expired_year")
    data_year = data_year.blank? ? 2 : data_year.content_data.to_i
    if !self.renovation_date.blank?
      return self.renovation_date + data_year.year
    elsif  !self.inscription_date.blank?
      return self.inscription_date + data_year.year
    else
      return nil
    end
  end

  def set_expired_date
    data_year = DataPreference.find_by(title: "expired_year")
    data_year = data_year.blank? ? 2 : data_year.content_data.to_i
    reference = Time.zone.parse("2020-07-01")
    if self.renovation_date.blank? && (self.inscription_date.blank? || self.inscription_date.to_date < reference.to_date)
      date = reference + data_year.years
    else
      date = expired_date
    end
    date
  rescue
    nil
  end

  def status_type
    old_status = self.status
    data_days = DataPreference.find_by(title: "alert_first")
    data_days = data_days.blank? ? 60 : data_days.content_data.to_i
    if !self.canceled_at.blank? && self.canceled_at <= Time.zone.now
      self.status = 'inactive'
      self.save(validate: false) if old_status.to_s != self.status.to_s
      data = I18n.t('backend.status_type.inactive')
    elsif !set_expired_date.blank? && set_expired_date < Time.zone.now      
      self.status = 'expired'
      self.save(validate: false) if old_status.to_s != self.status.to_s
      data = I18n.t('backend.status_type.expired')
    elsif ((self.invalidated_at.blank? || Date.today.to_date < self.invalidated_at.to_date) && ((self.canceled_at.blank? || Date.today.to_date < self.canceled_at.to_date) || self.entity_type_id.to_i!=2)) &&
     (!set_expired_date.blank? && Time.zone.now < set_expired_date  && set_expired_date <= Time.zone.now+2.months)
     self.status = 'active'
      self.save(validate: false) if old_status.to_s != self.status.to_s
      data= I18n.t('backend.status_type.active')
    elsif (self.invalidated_at.blank? || Date.today.to_date < self.invalidated_at.to_date) && ((self.canceled_at.blank? || Date.today.to_date < self.canceled_at.to_date) || self.entity_type_id.to_i!=2)
      self.status = 'active'
      self.save(validate: false) if old_status.to_s != self.status.to_s
      data =I18n.t('backend.status_type.active')      
    elsif (!self.invalidated_at.blank? && Date.today.to_date >= self.invalidated_at.to_date ) && ((self.canceled_at.blank? || Date.today.to_date < self.canceled_at.to_date) || self.entity_type_id.to_i!=2)        
      self.status = 'invalidated'
      self.save(validate: false) if old_status.to_s != self.status.to_s
      data=I18n.t('backend.status_type.invalidated')
    else     
      self.status = 'inactive'
      self.save(validate: false) if old_status.to_s != self.status.to_s
      data=I18n.t('backend.status_type.inactive')
    end   
    begin
      if old_status.to_s != self.status.to_s || self.organization_status.blank?
        self.organization_status = OrganizationStatus.find_by(code: self.status.to_s)
        self.save(validate: false)
      end
    rescue
    end
    data
  rescue 
    nil
  end

  def status_type_code
    old_status = self.status
    data_days = DataPreference.find_by(title: "alert_first")
    data_days = data_days.blank? ? 60 : data_days.content_data.to_i
    if !self.canceled_at.blank? && self.canceled_at <= Time.zone.now
      self.status = 'inactive'
      self.save(validate: false) if old_status.to_s != self.status.to_s      
    elsif !set_expired_date.blank? && set_expired_date < Time.zone.now      
      self.status = 'expired'
      self.save(validate: false) if old_status.to_s != self.status.to_s     
    elsif ((self.invalidated_at.blank? || Date.today.to_date < self.invalidated_at.to_date) && ((self.canceled_at.blank? || Date.today.to_date < self.canceled_at.to_date) || self.entity_type_id.to_i!=2)) &&
     (!set_expired_date.blank? && Time.zone.now < set_expired_date  && set_expired_date <= Time.zone.now+2.months)
     self.status = 'active'
      self.save(validate: false) if old_status.to_s != self.status.to_s      
    elsif (self.invalidated_at.blank? || Date.today.to_date < self.invalidated_at.to_date) && ((self.canceled_at.blank? || Date.today.to_date < self.canceled_at.to_date) || self.entity_type_id.to_i!=2)
      self.status = 'active'
      self.save(validate: false) if old_status.to_s != self.status.to_s    
    elsif (!self.invalidated_at.blank? && Date.today.to_date >= self.invalidated_at.to_date ) && ((self.canceled_at.blank? || Date.today.to_date < self.canceled_at.to_date) || self.entity_type_id.to_i!=2)        
      self.status = 'invalidated'
      self.save(validate: false) if old_status.to_s != self.status.to_s     
    else     
      self.status = 'inactive'
      self.save(validate: false) if old_status.to_s != self.status.to_s      
    end   
    begin
      if old_status.to_s != self.status.to_s || self.organization_status.blank?
        self.organization_status = OrganizationStatus.find_by(code: self.status.to_s)
        self.save(validate: false)
      end
    rescue
    end
    self.status
  rescue 
    nil
  end

  def management_type
    if ((self.invalidated_at.blank? || Date.today.to_date < self.invalidated_at.to_date) && ((self.canceled_at.blank? || Date.today.to_date < self.canceled_at.to_date) || self.entity_type_id.to_i!=2)) &&
    (!set_expired_date.blank? && Time.zone.now < set_expired_date  && set_expired_date <= Time.zone.now+2.months)
      I18n.t('backend.management_type.management')
    else
      ""
    end
  rescue
    nil
  end

  def modification_email_date
    if self.modification_date.blank? && self.renovation_date.blank?
      return nil
    elsif self.modification_date.blank? && !self.renovation_date.blank?
      return self.renovation_date
    elsif !self.modification_date.blank? && self.renovation_date.blank?
      return self.modification_date
    elsif !self.modification_date.blank? && !self.renovation_date.blank? && self.modification_date.to_date > self.renovation_date.to_date
      return self.modification_date
    else
      self.renovation_date
    end
  rescue
    self.modification_date
  end

  def invalidate_organization
    return if invalidate.to_s != 'true' || !invalidated_at.nil?
    self.invalidated_at = Time.zone.today
  end

  def validate_organization
    return if validate.to_s != 'true' || invalidated_at.nil?
    self.invalidated_at = nil
    self.invalidated_reasons = nil
  end

  %i(address legal_representant notification_effect).each do |cardinal|
    %i(address_type address address_number_type number gateway stairs floor door
      postal_code town province country phones email).each do |ordinal|
      define_method "#{cardinal}_#{ordinal}" do
        if cardinal.to_s == "address"
          self.try(:address).try(ordinal)
        else
          self.try(cardinal).try(:address).try(ordinal)
        end
      end
    end
  end

  
  (1..4).each do |cardinal|
    %i(import entity_name).each do |ordinal|
      define_method "get_range_#{cardinal}_#{ordinal}" do
        if self.own_lobby_activity && self.subvention_public_administration && self.range_subventions.count > (cardinal - 1)
          self.range_subventions[cardinal - 1].try(ordinal)
        else 
          ""
        end
      end
    end
  end

  def fullname
    if business_name.blank?
      str = name
      str += " #{first_surname}"  if first_surname.present?
      str += " #{second_surname}" if second_surname.present?
    else 
      str = business_name
    end
    str
  end

  def get_text_group_public_administration
    if self.own_lobby_activity && self.in_group_public_administration
      self.text_group_public_administration
    else 
      ""
    end
  end

  def get_subvention_public_administration
    if self.own_lobby_activity
      self.subvention_public_administration
    else 
      ""
    end
  end


  def get_contract_turnover
    if self.own_lobby_activity && self.contract
      self.contract_turnover
    else 
      ""
    end
  end

  def get_contract_total_budget
    if self.own_lobby_activity && self.contract
      self.contract_total_budget
    else 
      ""
    end
  end

  def get_contract_breakdown
    if self.own_lobby_activity && self.contract
      self.contract_breakdown
    else 
      ""
    end
  end

  def get_contract_financing
    if self.own_lobby_activity && self.contract
      self.contract_financing
    else 
      ""
    end
  end


  def legal_representant_full_name
    legal_representant.fullname if legal_representant
  end


  def notification_effect_full_name
    notification_effect.fullname if notification_effect
  end


  def user_name
    user.full_name if user
  end

  def user_email
    user.email if user
  end

  def user_phones
    user.phones if user
  end

  def status_older
    if termination_date.blank? && invalidated_at.blank?
      :active
    elsif termination_date.present?
      :terminated
    else
      :inactive
    end
  end

  def get_fiscal_year
    if self.own_lobby_activity
      self.fiscal_year
    else 
      ""
    end
  end

  def get_range_fund
    if self.own_lobby_activity
      self.range_fund
    else 
      ""
    end
  end

  def get_subvention
    if self.own_lobby_activity
      self.subvention
    else 
      ""
    end
  end

  def get_in_group_public_administration
    if self.own_lobby_activity
      self.in_group_public_administration
    else 
      ""
    end
  end

  def get_contract
    if self.own_lobby_activity
      self.contract
    else 
      ""
    end
  end

  def get_category
    if self.category.blank?
      ""
    else
      self.category.name
    end
  end

  (1..4).each do |cardinal|
    define_method "fiscal_year_#{cardinal}" do
      entity_fiscal_year(cardinal - 1)
    end
  end

  (1..4).each do |cardinal|
    define_method "represented_entity_#{cardinal}" do
      represented_entity(cardinal - 1)
    end
  end

  (1..4).each do |cardinal|
    define_method "range_fund_#{cardinal}" do
      entity_range_fund(cardinal - 1)
    end
  end

  (1..4).each do |cardinal|
    define_method "subvention_#{cardinal}" do
      entity_subvention(cardinal - 1)
    end
  end

  (1..4).each do |cardinal|
    define_method "contract_#{cardinal}" do
      entity_contract(cardinal - 1)
    end
  end

  (1..4).each do |cardinal|
    define_method "in_group_public_administration_#{cardinal}" do
      entity_in_group_public_administration(cardinal - 1)
    end
  end

  (1..4).each do |cardinal|
    define_method "text_group_public_administration_#{cardinal}" do
      entity_text_group_public_administration(cardinal - 1)
    end
  end

  (1..4).each do |cardinal|
    define_method "subvention_public_administration_#{cardinal}" do
      entity_subvention_public_administration(cardinal - 1)
    end
  end

  (1..4).each do |cardinal|
    define_method "contract_turnover_#{cardinal}" do
      entity_contract_turnover(cardinal - 1)
    end
  end

  (1..4).each do |cardinal|
    define_method "contract_total_budget_#{cardinal}" do
      entity_contract_total_budget(cardinal - 1)
    end
  end

  (1..4).each do |cardinal|
    define_method "contract_breakdown_#{cardinal}" do
      entity_contract_breakdown(cardinal - 1)
    end
  end

  (1..4).each do |cardinal|
    define_method "contract_financing_#{cardinal}" do
      entity_contract_financing(cardinal - 1)
    end
  end

  (1..4).each do |pre|
    (1..4).each do |cardinal|
      %i(import entity_name).each do |ordinal|
        define_method "get_range_#{pre}_#{cardinal}_#{ordinal}" do
          entity_range(pre - 1, cardinal - 1, ordinal)
        end
      end
    end
  end
 

  def self_employed_lobby
    #represented_entities.count.zero?
    if !self.own_lobby_activity.blank?
      self.own_lobby_activity
    else 
      self.own_lobby_activity.blank? && !self.foreign_lobby_activity
    end
  end

  def employee_lobby
    #!self_employed_lobby
    self.foreign_lobby_activity
  end

  def set_dates
    self.inscription_date = Date.current if inscription_date.blank?
    save
  end

  def set_invalidate
    self.invalidate = false
    save
  end

  def interest?(id)
    interests.pluck(:id).include?(id)
  end

  def invalidated?
    invalidated_at.present?
  end

  def canceled?
    !canceled_at.nil?
  end

  def change_password(user_password)
    user.update(:password => user_password, :password_confirmation => user_password)
  end

  private 

  def document_identifier
    if identifier_type.to_s =="DNI/NIF" && !validadorNIF_DNI_NIE(identifier)
      self.errors.add(:identifier, "El documento no tiene una validación correcta de DNI/NIF como persona física")    
    elsif  identifier_type.to_s =="NIF" && !validadorCIF(identifier)
      self.errors.add(:identifier, "El documento no tiene una validación correcta de NIF como persona jurídica")  
    elsif identifier_type.to_s =="NIE" && !validadorNIF_DNI_NIE(identifier)
      self.errors.add(:identifier, "El documento no tiene una validación correcta de NIE") 
    elsif identifier_type.to_s =="Pasaporte" && !validatorPasaport(identifier)
      self.errors.add(:identifier, "El documento no tiene una validación correcta de Pasaporte")   
    end

    if identifier_type.blank?
      self.errors.add(:identifier_type, "Es necesario incluir el tipo de documento del lobby")  
    end
  end

  def name_entity
    if (name.blank? || first_surname.blank?) && business_name.blank?
      self.errors.add(:name, "Se debe rellenar el nombre y primer apellido o la razón social, son campos obligatorios")
    elsif (!name.blank? || !first_surname.blank?) && !business_name.blank?
      self.errors.add(:name, "Solo puede rellenarse el nombre y primer apellido o la razón social, no ambos")     
    end

    if !business_name.blank? && (!first_surname.blank? || !second_surname.blank?)
      self.errors.add(:name, "Los apellidos solo se pueden rellenar con el nombre")
    end
  end

  def registered_lobby_activity
    if !self.foreign_lobby_activity
      self.represented_entities.each {|x| x.destroy}
    end
  end

  def terms
    if !self.certain_term || !self.code_of_conduct_term || !self.other_term || !self.gift_term || !self.lobby_term || !self.public_term
      self.errors.add(:terms, "Debe aceptar todos los terminos obligatorios de la declaración responsable")
    end
  end

  def renovation_historic
    if !renovation_date.blank?
      error = false
      organizations_renewals.each {|x| x.renovation_date > renovation_date ? error=true : ''}

        
      self.errors.add(:renovation_date, "Debe tener una fecha de renovación más actualizada") if error
    end
  end


    def represented_entity(num)
      if self.foreign_lobby_activity && !num.blank?
        if self.represented_entities.count > num
          self.represented_entities[num].fullname
        else
          ""
        end
      else 
        ""
      end
    end
    

    def entity_fiscal_year(num)
      if self.foreign_lobby_activity && !num.blank?
        if self.represented_entities.count > num
          self.represented_entities[num].fiscal_year
        else
          ""
        end
      else 
        ""
      end
    end

    def entity_range_fund(num)
      if self.foreign_lobby_activity && !num.blank?
        if self.represented_entities.count > num
          self.represented_entities[num].range_fund
        else
          ""
        end
      else 
        ""
      end
    end

    def entity_subvention(num)
      if self.foreign_lobby_activity && !num.blank?
        if self.represented_entities.count > num
          self.represented_entities[num].subvention
        else
          ""
        end
      else 
        ""
      end
    end

    def entity_contract(num)
      if self.foreign_lobby_activity && !num.blank?
        if self.represented_entities.count > num
          self.represented_entities[num].contract
        else
          ""
        end
      else 
        ""
      end
    end

    def entity_in_group_public_administration(num)
      if self.foreign_lobby_activity && !num.blank?
        if self.represented_entities.count > num
          self.represented_entities[num].in_group_public_administration
        else
          ""
        end
      else 
        ""
      end
    end

    def entity_text_group_public_administration(num)
      if self.foreign_lobby_activity && !num.blank?
        if self.represented_entities.count > num && self.represented_entities[num].in_group_public_administration
          self.represented_entities[num].text_group_public_administration
        else
          ""
        end
      else 
        ""
      end
    end

    def entity_subvention_public_administration(num)
      if self.foreign_lobby_activity && !num.blank?
        if self.represented_entities.count > num     
          self.represented_entities[num].subvention_public_administration
        else
          ""
        end
      else 
        ""
      end
    end


    def entity_contract_turnover(num)
      if self.foreign_lobby_activity && !num.blank?
        if self.represented_entities.count > num && self.represented_entities[num].contract
          self.represented_entities[num].contract_turnover
        else
          ""
        end
      else 
        ""
      end
    end

    def entity_contract_total_budget(num)
      if self.foreign_lobby_activity && !num.blank?
        if self.represented_entities.count > num && self.represented_entities[num].contract  
          self.represented_entities[num].contract_total_budget
        else
          ""
        end
      else 
        ""
      end
    end

    def entity_contract_breakdown(num)
      if self.foreign_lobby_activity && !num.blank?
        if self.represented_entities.count > num && self.represented_entities[num].contract   
          self.represented_entities[num].contract_breakdown
        else
          ""
        end
      else 
        ""
      end
    end

    def entity_contract_financing(num)
      if self.foreign_lobby_activity && !num.blank?
        if self.represented_entities.count > num && self.represented_entities[num].contract    
          self.represented_entities[num].contract_financing
        else
          ""
        end
      else 
        ""
      end
    end

    def entity_range(num, range, col)
      if self.foreign_lobby_activity && !num.blank? && !range.blank?
        if self.represented_entities.count > num && self.represented_entities[num].represented_range_subventions.count > range && self.represented_entities[num].try(:subvention_public_administration)
          self.represented_entities[num].represented_range_subventions[range].try(col)
        else
          ""
        end
      else 
        ""
      end
    end
end
