class EventUpdateSearch
  def self.update_search(event)
    puts "==========================="
    puts event.try(:position).try(:holder).try(:full_name)
    event.holder_id = event.try(:position).try(:holder_id).to_s
    event.holder_name = event.try(:position).try(:holder).try(:full_name)
    event.participants_ids = ""
    event.participants_name = ""
    event.participants.each do |p|      
      event.participants_name = event.participants_name + (event.participants_name.blank? ? "" : ";") + p.try(:position).try(:holder).try(:full_name)
      event.participants_ids = event.participants_ids + (event.participants_ids.blank? ? "" : ";") + p.try(:position).try(:holder_id).to_s
    end 

    event.attendees_name = ""
    event.attendees.each do |a|      
      event.attendees_name = event.attendees_name + (event.attendees_name.blank? ? "" : ";") + a.name 
    end
    event.area_id = event.try(:position).try(:area_id).to_s
    event.area_title = event.try(:position).try(:area).try(:title).to_s
    event.position_title = event.try(:position).try(:title).to_s

  

    event.save(validate: false)
  rescue =>e
    puts e
  end
end
