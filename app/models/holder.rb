class Holder < ActiveRecord::Base
  extend Admin::HolderActiveEventsHelper
  has_many :manages, dependent: :delete_all
  has_many :users, through: :manages
  has_many :areas, through: :positions
  has_many :positions, -> {order("positions.start desc")}, dependent: :delete_all

  accepts_nested_attributes_for :positions, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :manages, reject_if: :all_blank, allow_destroy: true

  validates :first_name, presence: true
  validates :last_name, presence: true
  #validate #:must_have_position, on: :update

  scope :by_name, (lambda do |name|
    condition = ["((first_name || ' ' || last_name) ILIKE :name) or (last_name ILIKE :name) or (first_name ILIKE :name)", name: "%#{name}%"]
    where(condition).includes(positions: [:titular_events, :participants_events])
  end)

  def full_name
    (self.first_name.to_s.delete(',')+' '+self.last_name.to_s.delete(',')).mb_chars.to_s
  end

  def full_name_comma
    (self.last_name.to_s.delete(',')+', '+self.first_name.to_s.delete(',')).mb_chars.to_s
  end

  def full_name_comma_with_active
    (self.last_name.to_s.delete(',')+', '+self.first_name.to_s.delete(',') + "#{" ("+I18n.t('main.form.no_active')+")" if self.current_position.blank?}").mb_chars.to_s
  end

  def current_position
    self.positions.current.first
  end

  def size_current_position
    self.positions.current.size
  end

  def user_email
    user = User.find_by(user_key: self.user_key)
    return if user.blank? || !user.blank? && user.user_key.blank? || self.user_key.blank?
    user.try(:email)
  end

  def self.managed_by (user_id)
    joins(:manages).where("manages.user_id" => user_id)
  end

  def self.create_from_uweb(data)
    return if data["CLAVE_IND"].blank?
    exist = Holder.find_by(user_key: data["CLAVE_IND"].try{|x| x.strip})
    holder = exist.blank? ?  Holder.new : exist
  
    holder.user_key =  data["CLAVE_IND"].strip unless data["CLAVE_IND"].try{|x| x.strip}.blank?
    holder.first_name = data["NOMBRE_USUARIO"].strip unless data["NOMBRE_USUARIO"].try{|x| x.strip}.blank?
    holder.last_name = data["APELLIDO1_USUARIO"].strip unless data["APELLIDO1_USUARIO"].try{|x| x.strip}.blank?
    holder.last_name = (holder.last_name + " #{data["APELLIDO2_USUARIO"].strip}").strip unless data["APELLIDO2_USUARIO"].try{|x| x.strip}.blank?
    holder.personal_code =  data["NUM_PERSONAL"].strip unless data["NUM_PERSONAL"].try{|x| x.strip}.blank?
    holder
  end

  def num_events_holder(pos, parametrize = {})
    events = Event.joins(:position => [:holder]).where("holders.id=? AND positions.id=?",self.id,pos).where("status = 1 OR status = 2")
    if parametrize[:search_start_date].present? && parametrize[:search_end_date].present?
     events=events.where("cast(events.scheduled as date) BETWEEN cast(? as date)  AND cast(? as date)  AND (events.scheduled IS NULL OR cast(events.scheduled as date) BETWEEN cast(? as date)  AND cast(? as date))",Time.zone.parse(parametrize[:search_start_date]),Time.zone.parse(parametrize[:search_end_date]),Time.zone.parse(parametrize[:search_start_date]),Time.zone.parse(parametrize[:search_end_date]))
    elsif parametrize[:search_start_date].present?
      events=events.where("cast(events.scheduled as date) >= cast(? as date) ",Time.zone.parse(parametrize[:search_start_date]))
    elsif parametrize[:search_end_date].present?
      events=events.where("cast(events.scheduled as date)  <= cast(? as date)  AND (events.scheduled IS NULL OR cast(events.scheduled as date)  <= cast(? as date))",Time.zone.parse(parametrize[:search_end_date]),Time.zone.parse(parametrize[:search_end_date]))
    end
    events.uniq.count
  rescue
    events = Event.joins(:position => [:holder]).where("holders.id=? AND positions.id=?",self.id,pos).where("status = 1 OR status = 2").uniq.count
  end

  def num_events_participants(pos, parametrize = {})
    events = Event.joins(:participants => [:position => [:holder]]).where("holders.id=? AND positions.id=?",self.id,pos).where("status = 1 OR status = 2")
    if parametrize[:search_start_date].present? && parametrize[:search_end_date].present?
      events=events.where("cast(events.scheduled as date) BETWEEN cast(? as date)  AND cast(? as date)  AND (events.scheduled IS NULL OR cast(events.scheduled as date) BETWEEN cast(? as date)  AND cast(? as date))",Time.zone.parse(parametrize[:search_start_date]),Time.zone.parse(parametrize[:search_end_date]),Time.zone.parse(parametrize[:search_start_date]),Time.zone.parse(parametrize[:search_end_date]))
    elsif parametrize[:search_start_date].present?
      events=events.where("cast(events.scheduled as date) >= cast(? as date) ",Time.zone.parse(parametrize[:search_start_date]))
    elsif parametrize[:search_end_date].present?
      events=events.where("cast(events.scheduled as date)  <= cast(? as date)  AND (events.scheduled IS NULL OR cast(events.scheduled as date)  <= cast(? as date))",Time.zone.parse(parametrize[:search_end_date]),Time.zone.parse(parametrize[:search_end_date]))
    end
    events.uniq.count
  rescue
    Event.joins(:participants => [:position => [:holder]]).where("holders.id=? AND positions.id=?",self.id,pos).where("status = 1 OR status = 2").uniq.count
  end

  private

    def must_have_position
      if positions.empty? || positions.all? {|child| child.marked_for_destruction? }
        errors.add(:base, I18n.translate('backend.must_have_position'))
      end
    end
end
