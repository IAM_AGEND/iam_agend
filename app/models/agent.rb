class Agent < ActiveRecord::Base

  belongs_to :organization
  has_many :attachments, dependent: :destroy

  FILE_CONTENT_TYPES = [
    'application/pdf','image/jpeg', 'image/png',
    'application/txt', 'text/plain', 'application/msword', 'text/csv',
    'application/msexcel', 'application/vnd.ms-excel',
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    'application/vnd.oasis.opendocument.text','application/x-vnd.oasis.opendocument.text',
    'application/rtf', 'application/x-rtf', 'text/rtf', 'text/richtext',
    'application/doc', 'application/docx', 'application/xls', 'application/xlsx',
    'application/x-soffice', 'application/octet-stream'].freeze 

  # has_attached_file :file_allow_data,
  #   path: ":rails_root/public/system/:attachment/:id/:style/:normalized_file_allow_data_name",
  #   url: "/system/:attachment/:id/:style/:normalized_file_allow_data_name"
  

  validates :name, :from, presence: true
  #validates :allow_public_data, presence: true
  validate :data_from_to
  # validates :file_allow_data, presence: true
  validates :attachments, presence: true
  # validates_attachment_content_type :file_allow_data, content_type: FILE_CONTENT_TYPES,
  #   message: I18n.t('backend.allowed_file_content_types')
  after_validation :cleanup_paperclip_duplicate_errors


  accepts_nested_attributes_for :attachments, allow_destroy: true

  def fullname
    str = name
    str += " #{first_surname}"  if first_surname.present?
    str += " #{second_surname}" if second_surname.present?
    str
  end

  def list_fullname
    str = ""
    str += " #{first_surname}"  if first_surname.present?
    str += " #{second_surname}" if second_surname.present?
    str += ", #{name}"
    str
  end

  scope :by_organization, ->(organization_id) { where(organization_id: organization_id) }
  scope :from_active_organizations, -> { joins(:organization).where('canceled_at is NULL AND invalidated_at is NULL') }
  scope :active, -> { where("agents.to is NULL OR cast(agents.to as date) >= cast(? as date)", Date.current) }

  # searchable do
  #   text :name, :first_surname, :second_surname
  #   integer :organization_id
  # end

  Paperclip.interpolates :normalized_file_allow_data_name do |attachment, style|
    attachment.instance.file_allow_data_file_name
  end

  def date_available?
    if self.from.present? && self.to.present?
      return true if Date.today >= self.from && Date.today <= self.to
    elsif self.from.present? && !self.to.present?
      return true if Date.today >= self.from
    end
  end

  private

    def cleanup_paperclip_duplicate_errors
      errors.delete(:file_allow_data_content_type)
    end

    def data_from_to
      if !self.to.blank? && self.from >= self.to 
        errors.add :from, " debe ser menor a la fecha de fin de la habilitación"
        errors.add :to, " debe ser mayor a la fecha de inicio de la habilitación"
      end
    end

end
