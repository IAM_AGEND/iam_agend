class Interest < ActiveRecord::Base

  has_many :organization_interests, dependent: :destroy
  has_many :organizations, through: :organization_interests

  validates :code, presence: true

  scope :form_order, -> { all.order("CASE WHEN name = 'Otros' THEN 'zzz' ELSE name END ASC ")}

  # searchable do
  #   integer :id, multiple: true
  # end

end
