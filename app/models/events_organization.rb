class EventsOrganization < ActiveRecord::Base
    
    belongs_to :organization
    belongs_to :event
    has_many :event_represented_entities, dependent: :destroy, inverse_of: :event
    has_many :event_agents, dependent: :destroy, inverse_of: :event

    validates :organization_id, presence: true

    accepts_nested_attributes_for :event_represented_entities, allow_destroy: true,reject_if: :all_blank
    accepts_nested_attributes_for :event_agents, allow_destroy: true, reject_if: :all_blank

end
 