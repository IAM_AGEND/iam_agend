class OrganizationStatus < ActiveRecord::Base

  validates :name, :code, presence: true
  validates :name, uniqueness: true
  validates :code, uniqueness: true

end
