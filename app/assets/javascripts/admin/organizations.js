$(function(){
  $('#nested-legal-representant-wrapper').bind('cocoon:after-insert', function() {
    $("#legal_representant_link").hide();
  });
  $('#nested-legal-representant-wrapper').bind("cocoon:after-remove", function() {
    if ($("#legal_representant_link").length > 0) {
      $("#legal_representant_link").show();
    } else {
      link = $('<a href="#" id="cancel-link">Cancelar Eliminación</a>')
      link.bind("click", restore_legal_representant);
      link.appendTo('#nested-legal-representant-wrapper > .small-12.columns');
    }
  });
  if ($('#nested-legal-representant-wrapper .legal-representant').length > 0) {
    $("#legal_representant_link").hide();
  }

  $('#nested-notification-effect-wrapper').bind('cocoon:after-insert', function() {
    $("#notification_effect_link").hide();
  });
  $('#nested-notification-effect-wrapper').bind("cocoon:after-remove", function() {
    if ($("#notification_effect_link").length > 0) {
      $("#notification_effect_link").show();
    } else {
      link = $('<a href="#" id="cancel-link">Cancelar Eliminación</a>')
      link.bind("click", restore_notification_effect);
      link.appendTo('#nested-notification-effect-wrapper > .small-12.columns');
    }
  });
  if ($('#nested-notification-effect-wrapper .notification-effect').length > 0) {
    $("#notification_effect_link").hide();
  }

  function toggle_hidden(id) {
    if ($(id).val() == "true") {
      $(id).val("false");
    } else {
      $(id).val("true");
    }
  }

  function change_field_others(){
    var aux = false;
    if($('#organization_registered_lobby_ids').val()){
      for(var i=0; i < $('#organization_registered_lobby_ids').val().length; i++) {
        if($('#organization_registered_lobby_ids').val()[i]==$('#organization_id_other').val().split(' ')[1]){
          aux = true;
          break;
        }
      }
    }

    if (aux){
      $('#content_other_field').show();
      $('#organization_other_registered_lobby_desc').show();
    }else {
      $('#content_other_field').hide();
      $('#organization_other_registered_lobby_desc').hide();
    }
  }

  $('#organization_registered_lobby_ids').bind('change', function(e){
    change_field_others();
  });

  $('#organization_other_registered_lobby_desc').ready(function(e){
    change_field_others();
  });

  $('#invalidate_link').bind('click', function(e){
    e.preventDefault();
    toggle_hidden('#organization_invalidate');
    $('#invalidate-reason').toggle();
  });

  $('#validate_link').bind('click', function(e){
    e.preventDefault();
    toggle_hidden('#organization_validate');
    $('#invalidate-reason').hide();
    $('#validate_link').toggleClass('success');
  });

  if ($("#organization_own_lobby_activity_true").is(":checked")) {
    $('#lobbies-data-content').show();
  };

  $("#organization_own_lobby_activity_false").click(function(){
    $('#lobbies-data-content').hide();
  });

  $("#organization_own_lobby_activity_true").click(function(){
    $('#lobbies-data-content').show();
  });

  if ($("#organization_foreign_lobby_activity_true").is(":checked")) {
    $("#represented-entities-content").show();
  };

  $("#organization_foreign_lobby_activity_false").click(function(){
    $("#represented-entities-content").hide();
  });

  $("#organization_foreign_lobby_activity_true").click(function(){
    $("#represented-entities-content").show();
  });

  
  if ($("#organization_in_group_public_administration").val() == "true"){
    $('#lobbies-group-public-data-content').show();
  } else {
    $('#lobbies-group-public-data-content').hide();
  }

  $("#organization_in_group_public_administration").change(function(){
    if ($(this).val() == "true"){
      $('#lobbies-group-public-data-content').show();
    } else {
      $('#lobbies-group-public-data-content').hide();
    }
  })

  if ($("#organization_subvention_public_administration").val() == "true") {
    $('#lobbies-suvention-public-data-content').show();
  } else {
    $('#lobbies-suvention-public-data-content').hide();
  }

  $("#organization_subvention_public_administration").change(function(){
    if ($(this).val() == "true"){
      $('#lobbies-suvention-public-data-content').show();
    } else {
      $('#lobbies-suvention-public-data-content').hide();
    }
  })
 
});

function toggleFields(event, item) { 
  if (event.target.value == "true") {
    item.show();
  } else {
    item.hide();
  }
}

function restore_legal_representant(e){
  e.preventDefault();
  $('#nested-legal-representant-wrapper .legal-representant').show();
  $(this).remove();
  $('#organization_legal_representant_attributes__destroy').val(0);
}

function restore_notification_effect(e){
  e.preventDefault();
  $('#nested-notification-effect-wrapper .notification-effect').show();
  $(this).remove();
  $('#organization_notification_effect_attributes__destroy').val(0);
}


function copy_data(type) {
  if(type == 'lobby') {
    $('#organization_notification_effect_attributes_identifier').val($('#organization_identifier').val());
    $('#organization_notification_effect_attributes_business_name').val($('#organization_business_name').val());
    $('#organization_notification_effect_attributes_name').val($('#organization_name').val());
    $('#organization_notification_effect_attributes_first_surname').val($('#organization_first_surname').val());
    $('#organization_notification_effect_attributes_second_surname').val($('#organization_second_surname').val());

    $('#organization_notification_effect_attributes_address_attributes_country').val($('#organization_address_attributes_country').val());
    $('#organization_notification_effect_attributes_address_attributes_province').val($('#organization_address_attributes_province').val());
    $('#organization_notification_effect_attributes_address_attributes_town').val($('#organization_address_attributes_town').val());
    $('#organization_notification_effect_attributes_address_attributes_address_type').val($('#organization_address_attributes_address_type').val());
    $('#organization_notification_effect_attributes_address_attributes_address').val($('#organization_address_attributes_address').val());
    $('#organization_notification_effect_attributes_address_attributes_address_number_type').val($('#organization_address_attributes_address_number_type').val());
    $('#organization_notification_effect_attributes_address_attributes_number').val($('#organization_address_attributes_number').val());
    $('#organization_notification_effect_attributes_address_attributes_gateway').val($('#organization_address_attributes_gateway').val());
    $('#organization_notification_effect_attributes_address_attributes_stairs').val($('#organization_address_attributes_stairs').val());
    $('#organization_notification_effect_attributes_address_attributes_floor').val($('#organization_address_attributes_floor').val());
    $('#organization_notification_effect_attributes_address_attributes_door').val($('#organization_address_attributes_door').val());
    $('#organization_notification_effect_attributes_address_attributes_postal_code').val($('#organization_address_attributes_postal_code').val());
    $('#organization_notification_effect_attributes_address_attributes_email').val($('#organization_address_attributes_email').val());
    $('#organization_notification_effect_attributes_address_attributes_phones').val($('#organization_address_attributes_phones').val());
    $('#organization_notification_effect_attributes_address_attributes_movil_phone').val($('#organization_address_attributes_movil_phone').val());
  } else if(type == 'legal_representant') {
    $('#organization_notification_effect_attributes_identifier').val($('#organization_legal_representant_attributes_identifier').val());
    $('#organization_notification_effect_attributes_business_name').val($('#organization_legal_representant_attributes_business_name').val());
    $('#organization_notification_effect_attributes_name').val($('#organization_legal_representant_attributes_name').val());
    $('#organization_notification_effect_attributes_first_surname').val($('#organization_legal_representant_attributes_first_surname').val());
    $('#organization_notification_effect_attributes_second_surname').val($('#organization_legal_representant_attributes_second_surname').val());

    $('#organization_notification_effect_attributes_address_attributes_country').val($('#organization_legal_representant_attributes_address_attributes_country').val());
    $('#organization_notification_effect_attributes_address_attributes_province').val($('#organization_legal_representant_attributes_address_attributes_province').val());
    $('#organization_notification_effect_attributes_address_attributes_town').val($('#organization_legal_representant_attributes_address_attributes_town').val());
    $('#organization_notification_effect_attributes_address_attributes_address_type').val($('#organization_legal_representant_attributes_address_attributes_address_type').val());
    $('#organization_notification_effect_attributes_address_attributes_address').val($('#organization_legal_representant_attributes_address_attributes_address').val());
    $('#organization_notification_effect_attributes_address_attributes_address_number_type').val($('#organization_legal_representant_attributes_address_attributes_address_number_type').val());
    $('#organization_notification_effect_attributes_address_attributes_number').val($('#organization_legal_representant_attributes_address_attributes_number').val());
    $('#organization_notification_effect_attributes_address_attributes_gateway').val($('#organization_legal_representant_attributes_address_attributes_gateway').val());
    $('#organization_notification_effect_attributes_address_attributes_stairs').val($('#organization_legal_representant_attributes_address_attributes_stairs').val());
    $('#organization_notification_effect_attributes_address_attributes_floor').val($('#organization_legal_representant_attributes_address_attributes_floor').val());
    $('#organization_notification_effect_attributes_address_attributes_door').val($('#organization_legal_representant_attributes_address_attributes_door').val());
    $('#organization_notification_effect_attributes_address_attributes_postal_code').val($('#organization_legal_representant_attributes_address_attributes_postal_code').val());
    $('#organization_notification_effect_attributes_address_attributes_email').val($('#organization_legal_representant_attributes_address_attributes_email').val());
    $('#organization_notification_effect_attributes_address_attributes_phones').val($('#organization_legal_representant_attributes_address_attributes_phones').val());
    $('#organization_notification_effect_attributes_address_attributes_movil_phone').val($('#organization_legal_representant_attributes_address_attributes_movil_phone').val());
  }
}
