// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require foundation
//= require dependencies/foundation-datepicker
//= require_tree ./dependencies/foundation-datepicker-locales
//= require social-share-button
//= require organizations
//= require infringement_email
//= require respond.min
//= require rem.min
//= require tinymce

$(function() {
  $(document).foundation();

  tinymce.init({
    selector : "textarea:not(.mceNoEditor)",
    language : 'es',
    menubar: false,
    tabfocus_elements: ':prev,:next'
    //toolbar1: "undo redo | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
    //toolbar2: "cut copy paste | bullist numlist | outdent indent blockquote | link image media | forecolor | table | subscript superscript | charmap | code",
  });

  
});

$(document).ready(function() {
  $('.js-example-basic-multiple').select2();
});


function show_calendar() {
  show("#month_calendar");
  hide("#list");
  hide("#week_calendar");
  pre_calendar_url('month');
};

function show_list() {
  show("#list");
  hide("#month_calendar");
  hide("#week_calendar");
  pre_calendar_url('list');
};

function show_week() {
  hide("#month_calendar");
  hide("#list");
  show("#week_calendar");
  pre_calendar_url('week');
};

function show_month() {
  show("#month_calendar");
  hide("#list");
  hide("#week_calendar");
  pre_calendar_url('month');
};

function show_cal_params(param){
  switch(param) {
    case "0":
      show_list();
      break;
    case "1": 
      show_month();
      break;
    case "2":
      show_week();
      break;
  }
}

function show(id){
  $(id).css("visibility", "inherit");
  $(id).css("display", "block");
};

function hide(id){
  $(id).css("visibility", "hidden");
  $(id).css("display", "none");
};

function pre_calendar_url(type) {
  switch(type) {
    case "list":
      $('#calendar').val("0");
      break;
    case "month": 
    $('#calendar').val("1");
      break;
    case "week":
      $('#calendar').val("2");
      break;
  }
}

function send_form() {
  form = $('.dates_form')  
  $("#set_date").val($("#select_year").val()+"-"+$("#select_month").val()+"-"+$("#select_day").val())
  if(!$.isDate($("#set_date"))) {
      $("#set_date").val($("#select_year").val()+"-"+$("#select_month").val()+"-1")
       $('#select_day').val(1)
  }      
  if ($("#tab_calendar").val() == 'today') {
    $("#tab_calendar").val("")
  }
  form.submit()
}

$.isDate = function (inputField) {
  isValid = true;
  var currVal = $(inputField).val();
  if (currVal == '')
      return false;

  //Declarar Regex 
  var rxDatePattern = /^(\d{4})(\/|-)(\d{1,2})(\/|-)(\d{1,2})$/;
  var dtArray = currVal.match(rxDatePattern);
  if (dtArray == null)
      return false;

  //formato mm/dd/yyyy.
  dtYear = dtArray[1];
  dtMonth = dtArray[3];
  dtDay= dtArray[5];

  if (dtMonth < 1 || dtMonth > 12) {
      return false;
  } else if (dtDay < 1 || dtDay > 31) {
      isValid = false;
  } else if ((dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) && dtDay == 31) {
      return false;
  } else if (dtMonth == 2) {
      var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
      if (dtDay > 29 || (dtDay == 29 && !isleap))
          return false;
  }

  return isValid;
};