class ManageMailer < ApplicationMailer
    def sender(emails, mail)
        @body = mail.try(:email_body)
        cco = mail.fields_cco.blank? ? nil : mail.fields_cco.scan(/\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b/i)
        cc = mail.fields_cc.blank? ? nil : mail.fields_cc.scan(/\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b/i)
        mail(to: emails, bcc: cco, cc: cc, from: mail.try(:sender), subject: mail.try(:subject))
        #mail(to: emails, from: mail.try(:sender), subject: mail.try(:subject))
    end  
end
