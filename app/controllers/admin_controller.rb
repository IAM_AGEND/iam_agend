class AdminController < ApplicationController
  include PublicActivity::StoreController

  before_filter :show_change_password
  before_action :authenticate_user!

  layout 'admin'
  rescue_from CanCan::AccessDenied do |exception|
    redirect_to admin_path, alert: t('backend.access_denied')
  end

  helper_method :get_title
  helper_method :current_user
  hide_action :current_user
  hide_action :get_title


  private

  def show_change_password
    redirect_to admin_edit_password_path(current_user) if  params[:controller] != 'admin/passwords' && !current_user.blank? && current_user.lobby? && current_user.changed_password  # you have to define maintenance?
  end

end
