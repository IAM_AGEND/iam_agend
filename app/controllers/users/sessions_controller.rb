class Users::SessionsController < Devise::SessionsController
  after_filter :after_login, :only => :create

  def create
    if current_user.try(:deleted_at)
      sign_out(current_user)
      redirect_to new_user_session_path, alert: t('devise.failure.locked')
    else
      super
    end
  rescue => e
    begin
      Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  private

  def after_login
    # if current_user.frist_init
    #   if current_user.last_sign_in_at.blank?
    #     current_user.changed_password = true
    #     current_user.save
    #     redirect_to admin_edit_password_path(current_user)
    #   elsif !current_user.changed_password
    #     current_user.frist_init = false
    #     current_user.save
    #   end
    # end
  end

end
