class AuthController < ApplicationController
    include AuthHelper
  def gettoken
    token = get_token_from_code params[:code]
    session[:azure_token] = token.to_hash
    redirect_to export_outlook_event_url(session[:event_id])
  rescue => e
    begin
      Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end
end
