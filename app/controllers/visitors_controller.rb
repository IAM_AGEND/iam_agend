class VisitorsController < ApplicationController  
  before_filter :set_holder, only: [:index, :agenda]

  def index
    get_events_array
    begin
        respond_to do |format|
          format.csv { stream_csv_report.delay  }
          format.html
          format.atom
        end
    rescue
    end
  rescue => e
    begin
      Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def show
    @event = Event.friendly.find(params[:id])
  rescue => e
    begin
      Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
    redirect_to visitors_path, alert: "No se puede acceder a esta información"
  end

  def agenda
    if @holder.blank?
      redirect_to visitors_path, alert: t('activerecord.models.holder.not_found')
    else
      index
      render :index
    end
  rescue => e
    begin
      Rails.logger.error("COD-00003: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def update_holders
    @holders =  HoldersAppointment.all.order(last_name: :asc)
  rescue => e
    begin
      Rails.logger.error("COD-00004: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  private

 
  def stream_csv_report
    stream_file("Agendas", "csv") do |stream|
      Event.stream_query_rows(@unpaginated_events, request.base_url) do |row_from_db|
        stream.write row_from_db
      end
    end
  end

  def set_holder
    @holder = Holder.where(id: params[:holder]).first if !params[:holder].blank?
    @holders = HoldersAppointment.all.order(last_name: :asc)
  end

  def get_events_array  
    events = Event.where("events.status in (1,2) AND (cast(events.published_at as date) <= cast(? as date) OR events.published_at IS NULL)", ActiveRecord::Base::sanitize(Time.zone.now.strftime("%Y-%m-%d").to_s)).select(:title,:id,:slug,:position_id,:scheduled).order(scheduled: :desc) 
    if !DataPreference.find_by(title: 'limit_events').try(:content_data).blank? && (current_user.blank? || !current_user.blank? && !current_user.admin? || !DataPreference.find_by(title: 'show_admin_limit').try(:content_data).blank? && DataPreference.find_by(title: 'show_admin_limit').try(:content_data) == "2")
      events = events.where("cast(events.scheduled as date) >= cast(? as date)", ActiveRecord::Base::sanitize(Time.zone.parse(DataPreference.find_by(title: 'limit_events').content_data)))
    end

    if !params[:holder].blank? 
      events = events.where("events.position_id in (?) OR events.id in (?)", Position.where(holder_id: params[:holder].to_i).select(:id),
        Participant.joins(:position).where("positions.holder_id = '?'", params[:holder].to_i).select(:event_id).select(:event_id))       
    end    

    if !params[:advanced_search].blank?  && params[:advanced_search] == 'advanced'
      events = events.where(lobby_activity: true) if !params[:lobby_activity].blank?

      if !params[:keyword].to_s.strip.blank?
        aux_key=params[:keyword].to_s.strip
        events = events.where("
          TRANSLATE(UPPER(events.title),'ÁÉÍÓÚ','AEIOU') LIKE TRANSLATE(UPPER(?),'ÁÉÍÓÚ','AEIOU') 
          OR TRANSLATE(UPPER(events.description),'ÁÉÍÓÚ','AEIOU') LIKE TRANSLATE(UPPER(?),'ÁÉÍÓÚ','AEIOU')
          OR events.position_id in (?)
          OR events.position_id in (?)
          OR events.position_id in (?)
          OR events.id in (?)
          OR events.id in (?)
        ", "%#{aux_key}%", 
        "%#{aux_key}%", 
        Position.where("TRANSLATE(UPPER(positions.title),'ÁÉÍÓÚ','AEIOU') LIKE TRANSLATE(UPPER(?),'ÁÉÍÓÚ','AEIOU')","%#{aux_key}%").select(:id).uniq, 
        Position.joins(:holder).where("TRANSLATE(UPPER(CONCAT(holders.first_name, ' ', holders.last_name)),'ÁÉÍÓÚ','AEIOU') LIKE TRANSLATE(UPPER(?),'ÁÉÍÓÚ','AEIOU')","%#{aux_key}%").select(:id), 
        Position.joins(:area).where("TRANSLATE(UPPER(areas.title),'ÁÉÍÓÚ','AEIOU') LIKE TRANSLATE(UPPER(?),'ÁÉÍÓÚ','AEIOU')","%#{aux_key}%").select(:id), 
        Participant.joins(:position => [:holder]).where("TRANSLATE(UPPER(CONCAT(holders.first_name, ' ', holders.last_name)),'ÁÉÍÓÚ','AEIOU') LIKE TRANSLATE(UPPER(?),'ÁÉÍÓÚ','AEIOU')","%#{aux_key}%").select(:event_id), 
        Attendee.where("TRANSLATE(UPPER(attendees.name),'ÁÉÍÓÚ','AEIOU') LIKE TRANSLATE(UPPER(?),'ÁÉÍÓÚ','AEIOU')","%#{aux_key}%").select(:event_id))
      end

      if !params[:from].blank? && !params[:to].blank?
        events = events.where("CAST(events.scheduled as date) BETWEEN CAST(? as date) AND  CAST(? as date)", Time.zone.parse(params[:from]), Time.zone.parse(params[:to]))
      elsif !params[:from].blank?
        events = events.where("CAST(events.scheduled as date) >= CAST(? as date) ", Time.zone.parse(params[:from]))
      elsif !params[:to].blank?
        events = events.where("CAST(events.scheduled as date) <= CAST(? as date) ", Time.zone.parse(params[:to]))
      end
    elsif params[:advanced_search].blank? || params[:advanced_search] == 'calendar'
      @date_start = nil
      @date_end = nil
      if params[:select].blank?
        begin
          @current_date_calendar = params[:set_date].blank? ? Time.zone.now.to_date : Time.zone.parse(params[:set_date]).to_date
        rescue
          @current_date_calendar = Time.zone.now.to_date
        end
        @date_start = @current_date_calendar
      else
        begin
          @current_date_calendar=Date.new(params[:select][:year].to_i, params[:select][:month].to_i, params[:select][:day].to_i)
        rescue
          @current_date_calendar = Date.new(params[:select][:year].to_i, params[:select][:month].to_i, 1)
        end
        if params[:tab_calendar].blank? 
          @date_start = @current_date_calendar
        elsif params[:tab_calendar] == "today"
          @current_date_calendar =  Time.zone.now.to_date
          @date_start = @current_date_calendar    
        elsif params[:tab_calendar] == "week"
          @date_start = @current_date_calendar - (@current_date_calendar.wday - 1)
          @date_end = @current_date_calendar + (7 - @current_date_calendar.wday) 
        elsif params[:tab_calendar] == "month"
          @date_start = Date.new(params[:select][:year].to_i, params[:select][:month].to_i, 1)
          @date_end = Date.new(params[:select][:year].to_i, params[:select][:month].to_i, -1)         
        end
      end
    
      if !@date_start.blank? && !@date_end.blank?
        events = events.where("CAST(events.scheduled as date) BETWEEN CAST(? as date) AND  CAST(? as date)", @date_start, @date_end)
      elsif !@date_start.blank?
        events = events.where("CAST(events.scheduled as date) BETWEEN CAST(? as date) AND  CAST(? as date)", @date_start, @date_start)
      end
    end
    @unpaginated_events = events
    @paginated_events = events.page(params[:page] || 1).per(100) 
  end  
  
end
