class StatisticsController < ApplicationController
  include StatisticsHelper
  def index
    @lobbies_categories_old = []
    count_old = 0
    Category.where(display: true, new_category: false).order(name: :asc).each do |c|
      count_old = count_old + c.organizations.lobby.count
      @lobbies_categories_old.push(["<td style='padding-left: 20px'>#{c.name}</td>".html_safe, c.organizations.lobby.count])
    end

    @lobbies_categories_new = []
    @lobbies_categories_new.push(["<td><p><b>Categorías de lobbies inscritos desde la aprobación de la Ley 10/2019,
      de 10 de abril, de Transparencia y de Participación de la Comunidad de
      Madrid, y de lobbies que han renovado su inscripción en base a dicha
      Ley </b></p></td>".html_safe, "<b>0</b>".html_safe])
    count_new = 0
    Category.where(display: true, new_category: true, category_id: nil).each do |c|
      cat_hijas = Category.where(category_id: c.id).order(name: :asc)
      if cat_hijas.count > 0
        c_count = 0
        cat_hijas.each do |ch|
          c_count += ch.organizations.lobby.count
        end
        @lobbies_categories_new.push(["<td style='padding-left: 20px'><b>#{c.name}</b></td>".html_safe, "<b>#{c_count}</b>".html_safe])
        cat_count = 0
          
        cat_hijas.each do |ch|
          @lobbies_categories_new.push(["<td style='padding-left: 40px'>#{ch.name}</td>".html_safe, ch.organizations.lobby.count])
          cat_count = cat_count +  ch.organizations.lobby.count
        end
        count_new = count_new + cat_count

        @lobbies_categories_new.each do |x| 
          x[1]="<b>#{cat_count}</b>".html_safe if x[0] == c.name
        end

      else
        @lobbies_categories_new.push(["<td style='padding-left: 20px'><b>#{c.name}</b></td>".html_safe, "<b>#{c.organizations.lobby.count}</b>".html_safe])
        count_new = count_new +  c.organizations.lobby.count
      end
    end
    
    @lobbies_categories_new[0][1] = "<b>#{count_new}</b>".html_safe

    

    @lobbies_interests = Interest.all.order(:name).map { |i| [i.name, i.organizations.lobby.count] }.to_h
    @count_all_interest = 0
    @lobbies_interests.each {|x|  @count_all_interest =  @count_all_interest + x[1] }

    date_year = DataPreference.find_by(title: "expired_year")
    date_year= date_year.blank? ? 2 : date_year.content_data
    data_days = DataPreference.find_by(title: "alert_first")
    data_days = data_days.blank? ? 60 : data_days.content_data.to_i
    reference = Date.parse("2020-07-01")

    @all_lobbies = Organization.lobbies
    @active_lobbies = Organization.lobbies.where(" ((invalidated_at is null or NOW() < invalidated_at) AND (canceled_at is null or NOW() < canceled_at) and (CASE WHEN organizations.renovation_date is not null THEN (organizations.renovation_date + interval '#{date_year} year')
    WHEN organizations.inscription_date is not null AND organizations.inscription_date > ('#{reference}') THEN (organizations.inscription_date + interval '#{date_year} year') 
    ELSE ('#{reference}') END) >= NOW())")
    
    @event_lobby_activity = Event.where("lobby_activity").where(status: [1, 2]).count

    date_filter= Date.new(2017,12,28)

    @events_count = Event.where(status: [1, 2]).where("scheduled >= '#{date_filter}'").count
    @holders_count = Holder.count
    @old_year = 2017
    @count_all = count_old
  rescue => e
    begin
      Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def lobbies
    @title = params[:title]
    time = Time.zone.now - data_year.year - data_days.days
    case params[:type]
      when "1"
        @lobbies = Organization.lobbies.where("(invalidated_at is null or invalidated_at >= '#{Time.zone.now.strftime("%F")}')")
      when "2"
        @lobbies = Organization.lobbies.where("(invalidated_at is null or invalidated_at >= '#{Time.zone.now.strftime("%F")}') AND (canceled_at is null or canceled_at >= '#{Time.zone.now.strftime("%F")}') AND (renovation_date IS NULL OR renovation_date > '#{(time).strftime("%F")}')")
      when "3"
        @lobbies = Organization.lobbies.where("renovation_date < '#{(time).strftime("%F")}'")
      when "4"
        @lobbies = Organization.lobbies.where("canceled_at <= '#{Time.zone.now.strftime("%F")}'")
      when "5"
        @lobbies = Organization.lobbies.where("id IN (?)", employee_lobbies_count(Organization.lobbies))
      when "6"
        @lobbies = Organization.lobbies.where("id IN (?)", self_employed_lobbies_count(Organization.lobbies))
      when "7"
        @lobbies = Organization.lobbies.where("id IN (?)", self_employed_and_employee_lobbies_count(Organization.lobbies))
      end
      @lobbies = @lobbies.page(params[:page]).per(100)
  rescue => e
    begin
      Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

end
