class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :browser
  before_action :authenticate
  before_action :authenticate_http_basic, if: :http_basic_auth_site?
  helper_method :events_home_path


  def authenticate_http_basic
    authenticate_or_request_with_http_basic do |username, password|
      username == Rails.application.secrets.http_basic_username &&
      password == Rails.application.secrets.http_basic_password
    end
  end

  def http_basic_auth_site?
    Rails.application.secrets.http_basic_auth
  end

  def change_language
    I18n.locale = params[:lang]
    redirect_to request.referer
  end

  def events_home_path(user, redirect_requested_or_declined_events)
    if user.user?
      if redirect_requested_or_declined_events
        status = ["requested", "declined"]
        lobby_activity = "1"
      else
        status = ["accepted", "done", "canceled"]
        lobby_activity = nil
      end
      events_path(utf8: "✓", search_title: "", search_person: "",
                  status: status, lobby_activity: lobby_activity,
                  controller: "events", action: "index")
    else
      events_path
    end
  end

  def handle_unverified_request
    new_user_session_path
  end

  def authenticate
    if !verify_authenticity_token.blank?
      sign_out(current_user)
      redirect_to new_user_session_path, alert: t('devise.failure.locked')
    end
  end

  def data_year
    aux = DataPreference.find_by(title: "expired_year")
    aux.blank? ? 2 : aux.content_data.to_i
  end

  def data_days
    aux = DataPreference.find_by(title: "alert_first")
    aux.blank? ? 60 : aux.content_data.to_i
  end

  def stream_file(filename, extension)
    response.headers.delete("Content-Length")
    response.headers["Cache-Control"] = "no-cache"
    response.headers["X-Accel-Buffering"] = "no"
    response.headers["Content-Type"] = "application/octet-stream"
    response.headers["Content-Disposition"] = "attachment; filename=#{filename}.#{extension}"
    response.status = 200

    yield response.stream
    ensure
    response.stream.close
  end

  def stream_csv_report(elements)

    stream_file("#{elements.name.gsub(" ", "_")}", "csv") do |stream|
      elements.model.stream_query_rows(elements) do |row_from_db|
        stream.write row_from_db
      end
    end
  end

  def stream_csv_report_private(elements, extended)

    stream_file("OrgLobbies_#{Time.zone.now.strftime("%Y%m%d%I%M%S")}", "csv") do |stream|
      elements.model.stream_query_rows_private(elements, extended) do |row_from_db|
        stream.write row_from_db
      end
    end
  end

  def browser
    @browser ||= Browser.new(
      request.headers["User-Agent"],
      accept_language: request.headers["Accept-Language"]
    )
  end
 
end
