class Admin::ParametrizationsController < AdminController
    load_and_authorize_resource except: [:update_configure_text]
    before_action :load_data, only: [:index]

    def index
        @mails = ManageEmail.all
        @data_preferences = DataPreference.all.order(created_at: :asc)      
        @configure_text = ConfigureText.all.order(created_at: :asc)      
        @send_mailer_historics = SendMailerHistoric.where(type_data: nil).order(created_at: :asc)
        @dp = SendEmail.all.first
        @dp = SendEmail.new() if @dp.blank?
        @errors = params[:errors] if !params[:errors].blank?
        params.delete :errors         
    rescue => e
        begin
            Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end  
    end

    def change_view
        if !params[:user].blank? && params[:user][:role_view] == "Gestor"
            current_user.role = 'user'
            current_user.role_view = 0
        end
        if !params[:user].blank? && params[:user][:role_view] == "Lobby"
            current_user.role = 'lobby'
            current_user.role_view = 2
            current_user.organization = Organization.lobbies.first
        end
        current_user.save
        
        redirect_to admin_path, notice: t('backend.change_view_success')
    rescue => e
        begin
            Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
    end

    def length_event_description
        dp = DataPreference.all
        params[:data_preference] ||={}
        params[:data_preference].each do |k,v|
            d = dp.find_by(title: k.to_s)
            if !d.blank? && d.content_data.to_s != v.to_s
                d.content_data = v.to_s
                if d.save
                    `whenever -i cellar`
                end
            end                
        end
        
        redirect_to admin_parametrizations_path(data_preferences: true), notice: "Parametros actualizados"
    rescue => e
        begin
            Rails.logger.error("COD-00003: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
    end

    def send_email
        @dp = SendEmail.all.first
        @dp = SendEmail.new() if @dp.blank?

        if @dp.update(send_email_valid_params)
            redirect_to admin_parametrizations_path(send_emails: true), notice: "Parametros actualizados"
        else
            flash[:alert] = t('backend.review_errors')
            redirect_to admin_parametrizations_path(send_emails: true, errors: @dp.errors.full_messages), method: :post
        end
    rescue => e
        begin
            Rails.logger.error("COD-00004: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
    end


    def update_configure_text
        if can? :read, ConfigureText
            dp = ConfigureText.find_by(id: params[:id])
            if !dp.blank? && dp.update(configure_text_params)
                redirect_to admin_parametrizations_path(configure_text: true), notice: "Contenidos actualizados"
            else
                redirect_to admin_parametrizations_path(configure_text: true), alert: "No se han podido actualizar los contenidos"
            end  
        else         
            redirect_to admin_parametrizations_path(configure_text: true), alert: "No se han podido actualizar los contenidos"
        end
    rescue => e
        begin
            Rails.logger.error("COD-00005: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
    end

    def download_log
        send_data File.open("#{Rails.root}/log/emails/email#{params[:parametrization_id]}.log").read, filename: "email#{params[:parametrization_id]}.log"
        rescue => e
        begin
            Rails.logger.error("COD-00006: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
    end
    
    private

    def send_email_valid_params
        params.require(:send_email).permit(:auto_send, :auto_date, :auto_hour, :auto_frecuence, :auto_user_type,
                                            :auto_excluded_emails, :manual_send, :manual_from_date, :manual_to_date,
                                            :manual_hour, :manual_user_type, :manual_excluded_emails, :manual_date,
                                            :auto_start_date, :auto_from_date, :auto_to_date, :is_auto_all_emails, :is_auto_included_emails,
                                            :is_auto_excluded_emails, :auto_included_emails, :is_manual_all_emails,
                                            :is_manual_included_emails, :manual_included_emails, :is_manual_excluded_emails,
                                            :manual_excluded_emails)
    end

    def configure_text_params
        params.require(:configure_text).permit(:content_title,:content_body)
    end

    def load_data
        @roles = ["Gestor", "Lobby"]
        @time_frames =[["Mensual", 1],["Trimestral", 2],["Anual", 3]]
        @selected_calendar =[["Día", 'day'],["Semana", 'week'],["Mes", 'month'],["Todo", 'all']]
        @limit_values = {"Todos" => "1", "Limitados" => "2"}
        @frecuences = [["Mensual", 1],["Bimestral", 2],["Trimestral", 3],["Semestral", 4], ["Anual", 5]]
        @user_types = {"Titulares" => 1, "Gestores" => 2, "Ambos" => 3}
        @emails_organizations = SendMailerHistoric.where(type_data: "Organizations").order(created_at: :asc).group_by{|e| e.created_at.strftime("%d-%m-%Y")}
        @emails_lobby_one_month_to_expiration = SendMailerHistoric.where(type_data: "LobbyOneMonthToExpiration").order(created_at: :asc).group_by{|e| e.created_at.strftime("%d-%m-%Y")}
        @emails_lobby_expires_today = SendMailerHistoric.where(type_data: "LobbyExpiresToday").order(created_at: :asc).group_by{|e| e.created_at.strftime("%d-%m-%Y")}
        @emails_lobby_renovation = SendMailerHistoric.where(type_data: "LobbyRenovation").order(created_at: :asc).group_by{|e| e.created_at.strftime("%d-%m-%Y")}
        @emails_lobby_modification_notification = SendMailerHistoric.where(type_data: "LobbyModificationNotification").order(created_at: :asc).group_by{|e| e.created_at.strftime("%d-%m-%Y")}
        @emails_lobby_modification_manual = SendMailerHistoric.where(type_data: "LobbyModificationManual").order(created_at: :asc).group_by{|e| e.created_at.strftime("%d-%m-%Y")}
        @emails_lobby_canceled = SendMailerHistoric.where(type_data: "LobbyCanceled").order(created_at: :asc).group_by{|e| e.created_at.strftime("%d-%m-%Y")}
    end
end
