module Admin
  class UsersController < AdminController
    load_and_authorize_resource
    before_action :load_holders, only: [:new, :edit, :update, :create]
    before_action :set_user, only: [:show, :edit, :update, :destroy, :disable]

    def new
      @user = User.new
    rescue => e
      begin
        Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def show
      redirect_to admin_users_path, alert: t('backend.error_user_active') if @user.blank? || @user.active.to_i==0
    rescue => e
      begin
        Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def index
      @search = search(params)
      # @users = User.includes(:manages => :holder).where(id: @search.hits.map(&:primary_key)).where(active: 1).order(last_name: :asc)
      @users = User.includes(:manages => :holder).where("users.id IN (?)", @search).where(active: 1).order(last_name: :asc)
      case params[:role].to_i
      when 3    
        @users = @users.where("users.user_key in (?) AND users.role=0", Holder.all.select("user_key")) 
      when 4    
        @users = @users.joins(:manages).where("users.role = 0").uniq
      else
      end
    
      @users = @users.reorder("last_name, first_name asc")
    rescue => e
      begin
        Rails.logger.error("COD-00003: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def create
      @user = User.new(user_params)
      @user.password = Faker::Internet.password(8)
      @user.frist_init = true
      @user.changed_password = true
      if @user.save
        uweb = UwebUpdateApi.new
        uweb.insert_profile_user(Rails.application.secrets.uweb_api_users_key, current_user.user_key, @user.user_key)
        uweb.insert_profile_user(Rails.application.secrets.uweb_api_holders_key, current_user.user_key, @user.user_key)
        uweb.insert_profile_user(Rails.application.secrets.uweb_api_admins_key, current_user.user_key, @user.user_key)
        redirect_to admin_users_path, notice: t('backend.successfully_created_record')
      else
        flash[:alert] = t('backend.review_errors')
        render :new
      end
    rescue => e
      begin
        Rails.logger.error("COD-00004: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def update
      remove_password_params if params[:user][:password].blank?
     
      if @user.update_attributes(user_params)
        redirect_to admin_users_path, notice: t('backend.successfully_updated_record')
      else
        flash[:alert] = t('backend.review_errors')
        render :edit
      end
    rescue => e
      begin
        Rails.logger.error("COD-00005: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def destroy
      #redirect_to admin_users_path, alert: t('backend.not_delete_user')
      if current_user == @user
        redirect_to admin_users_path, alert: t('backend.unable_to_perform_operation')
      else
        #@user.destroy
        @user.active=0
        if @user.save(validate: false)
          uweb = UwebUpdateApi.new
          uweb.remove_profile_user(Rails.application.secrets.uweb_api_admins_key, current_user.user_key, @user.user_key)
          uweb.remove_profile_user(Rails.application.secrets.uweb_api_userskey, current_user.user_key, @user.user_key)
          uweb.remove_profile_user(Rails.application.secrets.uweb_api_holders_key, current_user.user_key, @user.user_key)
          redirect_to admin_users_path, notice: t('backend.successfully_destroyed_record')
        else
          redirect_to admin_users_path, alert: t('backend.not_delete_user')
        end
      end
    rescue => e
      begin
        Rails.logger.error("COD-00006: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def disable
      @user.status = false
      @user.save
      redirect_to admin_users_path, notice: t('backend.successfully_disabled_record')
    rescue => e
      begin
        Rails.logger.error("COD-00007: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def assign_holder
      user = User.find(params[:format].to_i)
      @holder = Holder.new
      @holder.first_name = user.first_name
      @holder.last_name = "#{user.last_name} #{user.second_last_name}"
      @holder.save
      redirect_to edit_holder_path(@holder)
    rescue => e
      begin
        Rails.logger.error("COD-00008: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def revert
      current_user.role_view = ""
      current_user.role = 1
      current_user.organization_id = nil
      current_user.save
      redirect_to admin_path, notice: t('backend.admin_view_success')
    rescue => e
      begin
        Rails.logger.error("COD-00009: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def export_manages
      stream_file("Gestores", "csv") do |stream|
        User.manage_stream_query_rows(request.base_url) do |row_from_db|
          stream.write row_from_db
        end
      end
    rescue => e
      begin
        Rails.logger.error("COD-00010: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def uweb
      system("nohup ruby script/runner \"%x[rake madrid:import]\"&")
      redirect_to admin_users_path, notice: t('backend.updating_users')
    rescue => e
      begin
        Rails.logger.error("COD-00011: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    private

    def set_user
      @user = User.find(params[:id])
    end

    def user_params
      params.require(:user).permit(:first_name, :last_name, :second_last_name, :email,
                                   :password, :password_confirmation,
                                   :role, :role_view, :id, manages_attributes: [:id, :holder_id, :_destroy])
    end

    def load_holders
      @holders = Holder.all.order("last_name asc")
    end

    def remove_password_params
      params[:user].delete(:password)
      params[:user].delete(:password_confirmation)
    end

    def search(params)
      # User.search do 
      #   with(:role_id, params[:role]) if params[:role].present? && !params[:role].blank? && params[:role].to_i < 3 && params[:role_view].blank? 
      #   with(:role_view, params[:role_view]) if !params[:role_view].blank?
      #   paginate page: 1, per_page: User.all.count
      # end
      ids = []
      users = User.all
      users = users.where(role: params[:role]) if params[:role].present? && !params[:role].blank? && params[:role].to_i < 3 && params[:role_view].blank? 
      users = users.where(role_view: params[:role_view]) if !params[:role_view].blank?
      if !params[:keyword].blank?
        users = users.where(
          "TRANSLATE(UPPER(users.first_name),'ÁÉÍÓÚ','AEIOU') LIKE TRANSLATE(UPPER(?),'ÁÉÍÓÚ','AEIOU') 
          OR TRANSLATE(UPPER(users.last_name),'ÁÉÍÓÚ','AEIOU') LIKE TRANSLATE(UPPER(?),'ÁÉÍÓÚ','AEIOU')
          OR TRANSLATE(UPPER(users.second_last_name),'ÁÉÍÓÚ','AEIOU') LIKE TRANSLATE(UPPER(?),'ÁÉÍÓÚ','AEIOU')
          ",
          "%#{params[:keyword]}%","%#{params[:keyword]}%","%#{params[:keyword]}%")
      end
      users.each do |user|
        ids << user.id
      end
      ids
    end

  end
end
