module Admin
  class OrganizationsController < AdminController

    load_and_authorize_resource

    before_action :set_organization, only: [:show, :update, :edit]
    before_action :add_range_of_year, only: [:new, :edit, :update, :create]


    def index      
     search(params)
      
      respond_to do |format|
        format.csv { stream_csv_report_private(@organizations, true).delay  }
        format.html
      end
    rescue => e
      begin
        Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def show
      @legal_representant = @organization.legal_representant
      @notification_effect = @organization.notification_effect
      @user = @organization.user
      @represented_entities = @organization.represented_entities
      @interest = @organization.interests
      @agents = @organization.agents

      respond_to do |format|
        format.html
        format.pdf do
          render pdf: "file_name"   # Excluding ".pdf" extension.
        end
      end
    rescue => e
      begin
        Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def create
      @organization = Organization.new(organization_params)
      @organization.entity_type = 'lobby'
      if @organization.save
        if !@organization.renovation_date.blank?
          renewal= OrganizationsRenewal.new(organization: @organization,renovation_date: @organization.renovation_date, expiration_date: @organization.expired_date, active: true)
          @organization.organizations_renewals << renewal
          renewal.save
          @organization.save
        end
        date = I18n.l(@organization.try(:inscription_date), format: :short).to_s
        mail = ManageEmail.find_by(type_data: 'Organizations')
        if mail.blank?
          mail = ManageEmail.create!(type_data: 'Organizations', sender: "no-reply@madrid.es",
            fields_cc: "", 
            fields_cco: "registrodelobbies@madrid.es",
            subject: I18n.t('mailers.organizations.subject'), 
            email_body: I18n.t("mailers.organizations.body"))
        end
        mail.subject = mail.subject.gsub!("@organization@", @organization.try(:fullname))
        mail.email_body = mail.try(:email_body).blank? ? "" : mail.try(:email_body).to_s.gsub("@date@", date.blank? ? I18n.l(Time.zone.now, format: :short).to_s : date)
        .gsub("@organization@", @organization.try(:fullname).to_s)
        .gsub("@code@", @organization.try(:id).to_s)
        .gsub("@user@", @organization.try(:user).try(:email).to_s)
        .gsub("@password@", @organization.try(:user).try(:password).to_s)

        begin
          send_mailer_historic = SendMailerHistoric.create(type_data: "Organizations", organization: @organization, 
            sent_to: "#{@organization.try(:user).try(:email).blank? ? '' : @organization.try(:user).try(:email).to_s + ', '}#{mail.fields_cc.blank? ? '' : mail.fields_cc.to_s + ', '}#{mail.fields_cco.blank? ? '' : mail.fields_cco.to_s}".try(:strip))
          ManageMailer.sender(@organization.try(:user).try(:email), mail).deliver_now
        rescue =>e
          begin
            send_mailer_historic.update(sent: false) if !send_mailer_historic.blank?
            Rails.logger.error("SMS-ERROR: #{e}")
          rescue
          end
        end

        #OrganizationMailer.welcome(@organization).deliver_now
        redirect_to admin_organizations_path, notice: t('backend.successfully_created_record')
      else
        flash[:alert] = t('backend.review_errors')
        render :new
      end
    rescue => e
      begin
        Rails.logger.error("COD-00003: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def new
      @organization = Organization.new
      @organization.user = User.new
    rescue => e
      begin
        Rails.logger.error("COD-00004: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def edit
      @organization.build_user if @organization.user.blank?
    rescue => e
      begin
        Rails.logger.error("COD-00005: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def update
      
      change_contact = false
      unless @organization.user.blank?
        change_contact = true if !@organization.try(:user).try(:email).blank? && @organization.user.email.to_s != params[:organization].try{|x| x[:user_attributes]}.try{|x| x[:email]}
      end

      organization_params.merge({modification_date: Time.zone.now})
      
      renovation_date_changed = organization_params[:renovation_date].blank? ? false : @organization.renovation_date.blank? ? true : organization_params[:renovation_date].to_datetime.strftime("%d-%m-%Y %H:%M") != @organization.renovation_date.strftime("%d-%m-%Y %H:%M")
      canceled_at_changed = organization_params[:canceled_at].blank? ? false : @organization.canceled_at.blank? ? true : organization_params[:canceled_at].to_datetime.strftime("%d-%m-%Y %H:%M") != @organization.canceled_at.strftime("%d-%m-%Y %H:%M")
      if @organization.update_attributes(organization_params)
        if !@organization.renovation_date.blank? && @organization.organizations_renewals.find_by(renovation_date: @organization.renovation_date).blank?
          @organization.organizations_renewals.each do |ren|
            ren.active = false
            ren.save
          end
          renewal= OrganizationsRenewal.new(organization: @organization,renovation_date: @organization.renovation_date, expiration_date: @organization.expired_date, active: true)
          @organization.organizations_renewals << renewal
          renewal.save
          @organization.save
        end

        path = current_user.lobby? ? admin_organization_path(@organization) : admin_organizations_path
        mailer = params[:organization][:mailer] == "1" ? true : false 
        if change_contact
          @organization.user.password = Faker::Internet.password(8)
          @organization.user.save
          begin
            OrganizationMailer.update_contact(@organization.user).deliver_now if !mailer
          rescue =>e
            begin
              Rails.logger.error("SMS-ERROR: #{e}")
            rescue
            end
          end
        elsif !@organization.invalidated? && !@organization.canceled?
          begin 
            if !mailer
              if !@organization.renovation_date.blank? && renovation_date_changed
                mail = ManageEmail.find_by(type_data: "LobbyRenovation")
                send_mailer_historic = SendMailerHistoric.create(type_data: "LobbyRenovation", organization: @organization, 
                  sent_to: "#{@organization.try(:user).try(:email).blank? ? '' : @organization.try(:user).try(:email).to_s + ', '}#{mail.fields_cc.blank? ? '' : mail.fields_cc.to_s + ', '}#{mail.fields_cco.blank? ? '' : mail.fields_cco.to_s}".try(:strip))
                mail.subject = mail.subject.gsub!("@lobby_name@", @organization.try(:fullname).try(:to_s))
                mail.email_body = mail.try(:email_body).blank? ? "" : mail.try(:email_body).to_s.gsub("@lobby_name@", @organization.try(:fullname).try(:to_s))
                .gsub("@lobby_id@", @organization.try(:id).try(:to_s))
                .gsub("@renovation_date@", I18n.l(@organization.renovation_date, format: :long))
                .gsub("@expiration_date@", @organization.set_expired_date.blank? ? "" : I18n.l(@organization.set_expired_date, format: :long))
                .gsub("@show_lobby_url@", "<a href='#{organization_url(@organization)}'>#{organization_url(@organization)}</a>")
                ManageMailer.sender(@organization.try(:user).try(:email), mail).deliver_now
              else 
                email_date = @organization.modification_date.blank? ? @organization.modification_email_date :  @organization.modification_date
                mail = ManageEmail.find_by(type_data: 'LobbyModificationManual')
                send_mailer_historic = SendMailerHistoric.create(type_data: "LobbyModificationManual", organization: @organization, 
                  sent_to: "#{@organization.try(:user).try(:email).blank? ? '' : @organization.try(:user).try(:email).to_s + ', '}#{mail.fields_cc.blank? ? '' : mail.fields_cc.to_s + ', '}#{mail.fields_cco.blank? ? '' : mail.fields_cco.to_s}".try(:strip))
                mail.subject = mail.subject.gsub!("@lobby_name@", @organization.try(:fullname).try(:to_s))
                mail.email_body = mail.try(:email_body).blank? ? "" : mail.try(:email_body).to_s.gsub("@lobby_name@", @organization.try(:fullname).try(:to_s))
                .gsub("@lobby_id@", @organization.try(:id).try(:to_s))
                .gsub("@lobby_updated_at@", email_date.blank? ? "" : I18n.l(email_date, format: :long))
                .gsub("@show_lobby_url@", "<a href='#{organization_url(@organization)}'>#{organization_url(@organization)}</a>")
                ManageMailer.sender([@organization.try(:user).try(:email), @organization.try(:legal_representant).try(:address).try(:email)], mail).deliver_now
              end
            end
          rescue => e
            begin
              send_mailer_historic.update(sent: false) if !send_mailer_historic.blank?
              Rails.logger.error("SMS-ERROR: #{e}")
            rescue
            end
          end
        end

        if !mailer && !@organization.canceled_at.blank? && canceled_at_changed
          begin
            mail = ManageEmail.find_by(type_data: 'LobbyCanceled')
            send_mailer_historic = SendMailerHistoric.create(type_data: "LobbyCanceled", organization: @organization, 
              sent_to: "#{@organization.try(:user).try(:email).blank? ? '' : @organization.try(:user).try(:email).to_s + ', '}#{mail.fields_cc.blank? ? '' : mail.fields_cc.to_s + ', '}#{mail.fields_cco.blank? ? '' : mail.fields_cco.to_s}".try(:strip))
            mail.subject = mail.subject.gsub!("@lobby_name@", @organization.try(:fullname).try(:to_s))
            mail.email_body = mail.try(:email_body).blank? ? "" : mail.try(:email_body).to_s.gsub("@lobby_name@", @organization.try(:fullname).try(:to_s))
            .gsub("@lobby_id@", @organization.try(:id).try(:to_s))
            .gsub("@lobby_canceled_at@", @organization.canceled_at.blank? ? "" : @organization.canceled_at.strftime("%d-%m-%y").try(:to_s))
            .gsub("@show_lobby_url@", "<a href='#{organization_url(@organization)}'>#{organization_url(@organization)}</a>")
            ManageMailer.sender(@organization.try(:user).try(:email), mail).deliver_now
          rescue => e
            begin
              send_mailer_historic.update(sent: false) if !send_mailer_historic.blank?
              Rails.logger.error("SMS-ERROR: #{e}")
            rescue
            end
          end
        end
        
        if @organization.invalidated? && params[:organization][:invalidate]
          begin
            OrganizationMailer.invalidate(@organization).deliver_now if !mailer
          rescue =>e
            begin
              Rails.logger.error("SMS-ERROR: #{e}")
            rescue
            end
          end
        end
        
        
        redirect_to path, notice: t('backend.successfully_updated_record')
      else
        flash[:alert] = t('backend.review_errors')
        render :edit, show: params[:show]
      end
    rescue => e
      begin
        Rails.logger.error("COD-00006: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def destroy
      @organization = Organization.find(params[:id])
      @organization.canceled_at = Time.zone.now
      @organization.user.soft_delete unless @organization.user.nil?

      if @organization.save
        begin
          mail = ManageEmail.find_by(type_data: 'LobbyCanceled')
          send_mailer_historic = SendMailerHistoric.create(type_data: "LobbyCanceled", organization: @organization, 
            sent_to: "#{@organization.try(:user).try(:email).blank? ? '' : @organization.try(:user).try(:email).to_s + ', '}#{mail.fields_cc.blank? ? '' : mail.fields_cc.to_s + ', '}#{mail.fields_cco.blank? ? '' : mail.fields_cco.to_s}".try(:strip))
          mail.subject = mail.subject.gsub!("@lobby_name@", @organization.try(:fullname).try(:to_s))
          mail.email_body = mail.try(:email_body).blank? ? "" : mail.try(:email_body).to_s.gsub("@lobby_name@", @organization.try(:fullname).try(:to_s))
          .gsub("@lobby_id@", @organization.try(:id).try(:to_s))
          .gsub("@lobby_canceled_at@", @organization.canceled_at.blank? ? "" : @organization.canceled_at.strftime("%d-%m-%y").try(:to_s))
          .gsub("@show_lobby_url@", "<a href='#{organization_url(@organization)}'>#{organization_url(@organization)}</a>")
          ManageMailer.sender(@organization.try(:user).try(:email), mail).deliver_now
          
          redirect_to admin_organizations_path,
                    notice: t('backend.successfully_destroyed_record')
        rescue => e
          begin
            send_mailer_historic.update(sent: false) if !send_mailer_historic.blank?
            Rails.logger.error("SMS-ERROR: #{e}")
          rescue
          end
          redirect_to admin_organizations_path, 
                    alert: t('backend.no_mailer')
        end
      else
        flash[:alert] = t('backend.unable_to_perform_operation')
        redirect_to admin_organizations_path
      end
    rescue => e
      begin
        Rails.logger.error("COD-00007: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    private

      def organization_params
        params.require(:organization)
              .permit(:id,:reference, :identifier,:identifier_type,:business_name, :name, :first_surname, :second_surname,
                      :invalidate, :invalidated_reasons, :renovation_date,
                      :in_group_public_administration, :text_group_public_administration,
                      :subvention_public_administration, :contract_turnover, :contract_total_budget,
                      :contract_breakdown, :contract_financing, :public_term, :communication_term,
                      :web, :validate, :description, :category_id, :other_term,
                      :fiscal_year, :range_fund, :subvention, :contract,
                      :certain_term, :code_of_conduct_term, :gift_term, :lobby_term,
                      :inscription_date, :modification_date, :canceled_at,
                      :own_lobby_activity, :foreign_lobby_activity, :other_registered_lobby_desc,                   
                      address_attributes, legal_representant_attributes,
                      {notification_effect_attributes: [:identifier, :identifier_type,:business_name, :name, :first_surname, :second_surname, :_destroy, address_attributes]},
                      {user_attributes: [:id, :first_name, :last_name, :second_last_name, :role, :email, :active, :phones, :movil_phone,
                        :password, :password_confirmation]},                     
                      {interest_ids: []}, {registered_lobby_ids: []},
                      {range_subventions_attributes: [:id,  :import, :entity_name,:_destroy]},
                      {attachments_attributes: [:id, :file, :_destroy]}, represented_entities_attributes)
      end

      def address_attributes
        {address_attributes: [:country, :province, :town, :address_type, :address, :number, :gateway, :stairs,:floor,
          :door,:postal_code, :email, :phones, :movil_phone, :address_number_type]}
      end

      def legal_representant_attributes
        {legal_representant_attributes: [:identifier,:name,:identifier_type,:business_name, :first_surname, :second_surname, :_destroy,
          address_attributes]}
      end

      def represented_entities_attributes
        if params[:organization][:foreign_lobby_activity].to_s == 'true'
          return {represented_entities_attributes: [:id,:in_group_public_administration, :text_group_public_administration, 
            :subvention_public_administration, :contract_turnover, :contract_total_budget, :contract_breakdown,
            :contract_financing, :identifier,:identifier_type,:business_name, :name, :first_surname, :second_surname, :to,
            :from, :fiscal_year, :range_fund, :subvention, :contract, :category_id,
            address_attributes, legal_representant_attributes,
            {represented_range_subventions_attributes: [:id, :import, :entity_name,:_destroy]}, :_destroy]}
        else
          return {}
        end
      end

      def set_organization
        @organization = Organization.find(params[:id])
        @other_registered_lobby = ''
        @organization.registered_lobbies.each do |r_lobby|
          if r_lobby.id.to_i > 5
            @other_registered_lobby = r_lobby.name
            break
          end
        end
      end

      def search(params)
        @organizations = Organization.all
        date_year = DataPreference.find_by(title: "expired_year")
        date_year= date_year.blank? ? 2 : date_year.content_data
        data_days = DataPreference.find_by(title: "alert_first")
        data_days = data_days.blank? ? 60 : data_days.content_data.to_i
        reference = Date.parse("2020-07-01")

        if !params[:status_type].blank? 
          aux_status = ""
          if !params[:status_type].blank?
            params[:status_type].each do |s|
              case s
              when '1'
                aux_status = aux_status + " OR " if !aux_status.blank?
                aux_status = aux_status + " ((invalidated_at is null or NOW() < invalidated_at) AND (canceled_at is null or NOW() < canceled_at) and (CASE WHEN organizations.renovation_date is not null THEN (organizations.renovation_date + interval '#{date_year} year')
                  WHEN organizations.inscription_date is not null AND organizations.inscription_date > ('#{reference}') THEN (organizations.inscription_date + interval '#{date_year} year') 
                  ELSE ('#{reference}') END) >= NOW())"
              when '2'
                aux_status = aux_status + " OR " if !aux_status.blank?
                aux_status = aux_status + " ((canceled_at is null OR canceled_at >= NOW()) AND invalidated_at is not null and invalidated_at <= NOW() and (CASE WHEN organizations.renovation_date is not null THEN cast(organizations.renovation_date + interval '#{date_year} year' as date)
                  WHEN organizations.inscription_date is not null AND organizations.inscription_date > cast('#{reference}' as date) THEN cast(organizations.inscription_date + interval '#{date_year} year' as date) 
                  ELSE cast('#{reference}' as date) END) > NOW()) "
              when '3'
                aux_status = aux_status + " OR " if !aux_status.blank?
                aux_status = aux_status + " ((canceled_at is not null AND canceled_at <= NOW()) ) "
              when '4'
                aux_status = aux_status + " OR " if !aux_status.blank?
                time = Time.zone.now - data_year.year
                aux_status = aux_status + " (canceled_at is null and invalidated_at is null and ((renovation_date is null and (inscription_date is null or inscription_date < '#{time}')) or renovation_date < '#{time}'))"
              end
            end
          end
          if !params[:type_lobby].blank? && params[:type_lobby].to_i == 1
            aux_status = aux_status + " AND " if !aux_status.blank?
            aux_status = aux_status + " (renovation_date is not null and ((renovation_date + interval '#{date_year} year') - interval '#{data_days} day' < NOW()) or
            (renovation_date is null and inscription_date is not null and (inscription_date + interval '#{date_year} year') - interval '#{data_days} day' < NOW()) or
            (canceled_at is not null and (canceled_at - interval '#{data_days} day') < NOW()) )"
          end
          @organizations = @organizations.where(aux_status)
        end
  
        if params[:search_inscription_start_date].present? && params[:search_inscription_end_date].present?
          @organizations = @organizations.where("cast(organizations.inscription_date as date) BETWEEN cast(? as date)  AND cast(? as date)",Time.zone.parse(params[:search_inscription_start_date]),Time.zone.parse(params[:search_inscription_end_date]))
        elsif params[:search_inscription_start_date].present?
          @organizations = @organizations.where("organizations.inscription_date >= cast(? as date) ",Time.zone.parse(params[:search_inscription_start_date]))
        elsif params[:search_inscription_end_date].present?
          @organizations = @organizations.where("organizations.inscription_date  <= cast(? as date) ",Time.zone.parse(params[:search_inscription_end_date]))
        end
  
        if params[:search_renovation_start_date].present? && params[:search_renovation_end_date].present?
          @organizations = @organizations.where("cast(organizations.renovation_date as date) BETWEEN cast(? as date)  AND cast(? as date)",Time.zone.parse(params[:search_renovation_start_date]),Time.zone.parse(params[:search_renovation_end_date]))
        elsif params[:search_renovation_start_date].present?
          @organizations = @organizations.where("organizations.renovation_date >= cast(? as date) ",Time.zone.parse(params[:search_renovation_start_date]))
        elsif params[:search_renovation_end_date].present?
          @organizations = @organizations.where("organizations.renovation_date  <= cast(? as date) ",Time.zone.parse(params[:search_renovation_end_date]))
        end

        if params[:search_expired_start_date].present? && params[:search_expired_end_date].present?
          @organizations = @organizations.where("organizations.canceled_at is null and CASE WHEN organizations.renovation_date is not null THEN cast(organizations.renovation_date + interval '#{date_year} year' as date) BETWEEN cast(? as date) AND cast(? as date)
            WHEN organizations.inscription_date is not null AND organizations.inscription_date > cast('#{reference}' as date) THEN cast(organizations.inscription_date + interval '#{date_year} year' as date)  BETWEEN cast(? as date) AND cast(? as date)
            ELSE cast('#{reference}' as date) BETWEEN cast(? as date) AND cast(? as date) END", Date.parse(params[:search_expired_start_date]),Date.parse(params[:search_expired_end_date]),
            Date.parse(params[:search_expired_start_date]),Date.parse(params[:search_expired_end_date]),
            Date.parse(params[:search_expired_start_date]),Date.parse(params[:search_expired_end_date]))            
        elsif params[:search_expired_start_date].present?
          @organizations = @organizations.where("organizations.canceled_at is null and CASE WHEN organizations.renovation_date is not null THEN cast(organizations.renovation_date + interval '#{date_year} year' as date) >= cast(? as date)
            WHEN organizations.inscription_date is not null AND organizations.inscription_date > cast('#{reference}' as date) THEN cast(organizations.inscription_date + interval '#{date_year} year' as date)  >= cast(? as date)
            ELSE cast('#{reference}' as date) >=  cast(? as date) END", Date.parse(params[:search_expired_start_date]),Date.parse(params[:search_expired_start_date]),Date.parse(params[:search_expired_start_date])) 
        elsif params[:search_expired_end_date].present?
          @organizations = @organizations.where("organizations.canceled_at is null and CASE WHEN organizations.renovation_date is not null THEN cast(organizations.renovation_date + interval '#{date_year} year' as date) <= cast(? as date)
            WHEN organizations.inscription_date is not null AND organizations.inscription_date > cast('#{reference}' as date) THEN cast(organizations.inscription_date + interval '#{date_year} year' as date)  <= cast(? as date)
            ELSE cast('#{reference}' as date) <=  cast(? as date) END", Date.parse(params[:search_expired_end_date]),Date.parse(params[:search_expired_end_date]),Date.parse(params[:search_expired_end_date]))
        end
  
        if params[:keyword].present?
          @organizations = @organizations.where(
            "TRANSLATE(UPPER(organizations.name),'ÁÉÍÓÚ','AEIOU') LIKE TRANSLATE(UPPER(?),'ÁÉÍÓÚ','AEIOU') 
            OR TRANSLATE(UPPER(organizations.business_name),'ÁÉÍÓÚ','AEIOU') LIKE TRANSLATE(UPPER(?),'ÁÉÍÓÚ','AEIOU')
            OR TRANSLATE(UPPER(organizations.first_surname),'ÁÉÍÓÚ','AEIOU') LIKE TRANSLATE(UPPER(?),'ÁÉÍÓÚ','AEIOU')
            OR TRANSLATE(UPPER(organizations.second_surname),'ÁÉÍÓÚ','AEIOU') LIKE TRANSLATE(UPPER(?),'ÁÉÍÓÚ','AEIOU')
            OR TRANSLATE(UPPER(organizations.description),'ÁÉÍÓÚ','AEIOU') LIKE TRANSLATE(UPPER(?),'ÁÉÍÓÚ','AEIOU')
            ",
            "%#{params[:keyword]}%","%#{params[:keyword]}%","%#{params[:keyword]}%","%#{params[:keyword]}%","%#{params[:keyword]}%"            
          )
        end
       
        if params[:entity_type].blank? || params[:entity_type].to_i == 2
          @organizations = @organizations.where("organizations.entity_type = 2 OR organizations.entity_type is null")
        elsif params[:entity_type].to_i == -1
          @organizations = @organizations.where("organizations.entity_type not in (2) AND organizations.entity_type is not null")
        end

        if params[:search_organization_id].present?
          @organizations = @organizations.where("organizations.id = ?", params[:search_organization_id])
        end

        if params[:search_organization_identifier].present?
          @organizations = @organizations.where("TRANSLATE(UPPER(organizations.identifier),'ÁÉÍÓÚ','AEIOU') LIKE TRANSLATE(UPPER(?),'ÁÉÍÓÚ','AEIOU')", "%#{params[:search_organization_identifier]}%")
        end

        if params[:search_organization_user_email].present?         
          @organizations = @organizations.joins(:user).where("TRANSLATE(UPPER(users.email),'ÁÉÍÓÚ','AEIOU') LIKE TRANSLATE(UPPER(?),'ÁÉÍÓÚ','AEIOU')", "%#{params[:search_organization_user_email]}%") 
        end

        if !params[:type_lobby].blank?
          if params[:type_lobby].to_i == 2
            @organizations = @organizations.where("foreign_lobby_activity = true and own_lobby_activity=false") 
          end        

          if params[:type_lobby].to_i == 3
            @organizations = @organizations.where("own_lobby_activity = true and foreign_lobby_activity=false") 
          end
      end
       
        @paginated_organizations = Kaminari.paginate_array(sorting_option(params, @organizations.order(inscription_date: :desc))).page(params[:page]).per(100)
      end

      def add_range_of_year
        @years=[]
        @this_year = Time.current.year
        (2000..@this_year+5).each do |year|
          @years.push([year,year])
        end
      end
    
      def sorting_option(option, organizations)
        if !option[:sort_by].blank? && !option[:direction].blank?
          case option[:sort_by].to_s
          when 'lobby'
            if option[:direction].to_s == 'asc' 
              organizations.sort_by{|o| o.fullname}
            else
              organizations.sort_by{|o| o.fullname}.reverse
            end
          when 'lobby_created_at'
            if option[:direction].to_s == 'asc' 
              organizations.sort_by{|o| o.try(:inscription_date).blank? ? Time.zone.parse('1970-01-01') : o.inscription_date}
            else
              organizations.sort_by{|o| o.try(:inscription_date).blank? ? Time.zone.parse('1970-01-01') : o.inscription_date}.reverse
            end
          when 'lobby_renovation_at'
            if option[:direction].to_s == 'asc' 
              organizations.sort_by{|o| o.try(:renovation_date).blank? ? Time.zone.parse('1970-01-01') : o.renovation_date}
            else
              organizations.sort_by{|o| o.try(:renovation_date).blank? ? Time.zone.parse('1970-01-01') : o.renovation_date}.reverse
            end
          when 'lobby_expired_at'
            if option[:direction].to_s == 'asc' 
              organizations.sort_by{|o| o.try(:set_expired_date).blank? ? Time.zone.parse('1970-01-01') : o.set_expired_date}
            else
              organizations.sort_by{|o| o.try(:set_expired_date).blank? ? Time.zone.parse('1970-01-01') : o.set_expired_date}.reverse
            end
          when 'lobby_status'
            if option[:direction].to_s == 'asc'
              organizations.sort_by{|o| o.try(:status_type).blank? ? '' : o.status_type}
            else
              organizations.sort_by{|o| o.try(:status_type).blank? ? '' : o.status_type}.reverse
            end
          else
            organizations.sort_by{|o| o.try(:inscription_date).blank? ? Time.zone.parse('1970-01-01') : o.inscription_date}.reverse
          end
        else
          organizations.sort_by{|o| o.try(:inscription_date).blank? ? Time.zone.parse('1970-01-01') : o.inscription_date}.reverse
        end
      rescue
        organizations
      end
  end

end
