module Admin
  class OrganizationInterestsController < AdminController
    load_and_authorize_resource :organization

    def index
    rescue => e
      begin
        Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def update
      if @organization.update(organization_interests_params)
        redirect_to admin_organization_organization_interests_path(@organization),
                    notice: t('backend.successfully_updated_record')
      else
        flash[:alert] = @organization.errors.full_messages #t('backend.review_errors')
        render :index
      end
    rescue => e
      begin
        Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    private

      def organization_interests_params
        params.require(:organization).permit(:communication_term, {interest_ids: []})
      end

  end
end
