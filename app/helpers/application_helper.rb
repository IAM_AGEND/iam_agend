module ApplicationHelper

  def show_headline(params)
    return (render 'visitors/headline/holder').to_s unless params[:holder].blank?
    return (render 'visitors/headline/search').to_s unless params[:keyword].blank?
    return (render 'visitors/headline/last').to_s
  end

  def current_path_with_query_params(query_parameters)
    url_for(request.query_parameters.merge(query_parameters))
  end

  def current_url(new_params)
    url_for params.merge(new_params)
  end

  def show_date(date)
    output = date.present? ? I18n.l(date, format: :short) : '----------'
    output.html_safe
  end

  def current_language
    message = t('header.page_language') + ' '
    message += I18n.locale == :es ? t('header.spanish') : t('header.english')
    message.html_safe
  end

  def show_agenda_link(holder)
    link_to(holder.full_name,  agenda_path(holder.id,holder.full_name.parameterize,advanced_search: 'advanced')).html_safe
  end

  def show_agenda_link_array(holder_name, holder_id)
    return "" if holder_id.blank?
    link_to(holder_name,  agenda_path(holder_id,holder_name.try(:parameterize), advanced_search: params[:advanced_search])).html_safe
  rescue
    ""
  end

  def form_field_errors(form, field)
    if form.object.errors[field].any?
      content_tag :span, class: "error error_field_rails" do
        field_errors = form.object.errors[field]
        field_errors.join(", ")
      end
    end
  end

  def get_min_errors(error_name)
    return "" if error_name.blank?
    aux= error_name.to_s.split('.')

    return error_name  if aux.count <= 1
   
    I18n.t("#{aux[aux.count-2]}.#{aux[aux.count-1]}")
  end

  def tooltip(content, options = {}, html_options = {}, *parameters_for_method_reference)
    html_options[:title] = options[:tooltip]
    html_options[:class] = html_options[:class] || 'tooltip'
    content_tag("span", content, html_options)
  end

  def export_link(url)
    link_to url, class: "right hide-for-small-only" do
      content_tag(:div) do
        concat(content_tag(:span, "", class: "icon icon__export"))
        concat(content_tag(:span, t('main.export')))
      end
    end
  end

  def pdf_visitors_link(url)
    link_to url, class: "right hide-for-small-only" do
      content_tag(:div) do
        concat(content_tag(:span, "", class: "icon icon__export"))
        concat(content_tag(:span, t('main.export_pdf')))
      end
    end
  end

end
