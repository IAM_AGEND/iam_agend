require 'rails_rinku'
module TextWithLinksHelper

  def safe_html_with_links(html)
    return if html.nil?
    Rinku.auto_link(html, :all, 'target="_blank" rel="nofollow"').html_safe
  rescue => e
    begin
      Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

end
