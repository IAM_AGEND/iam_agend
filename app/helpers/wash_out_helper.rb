module WashOutHelper

    def wsdl_data_options(param)
      case controller.soap_config.wsdl_style
      when 'rpc'
        if param.map.present? || !param.value.nil?
          { :"xsi:type" => param.namespaced_type }
        else
          { :"xsi:nil" => true }
        end
      when 'document'
        {}
      else
        {}
      end
    rescue => e
      begin
        Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end
  
    def wsdl_data_attrs(param)
      param.map.reduce({}) do |memo, p|
        if p.respond_to?(:attribute?) && p.attribute?
          memo.merge p.attr_name => p.value
        else
          memo
        end
      end
    rescue => e
      begin
        Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end
  
    def wsdl_data(xml, params)
      params.each do |param|
        next if param.attribute?
  
        tag_name = param.name
        param_options = wsdl_data_options(param)
        param_options.merge! wsdl_data_attrs(param)
  
        if param.struct?
          if param.multiplied
            param.map.each do |p|
              attrs = wsdl_data_attrs p
              if p.is_a?(Array) || p.map.size > attrs.size
                blk = proc { wsdl_data(xml, p.map) }
              end
              attrs.reject! { |_, v| v.nil? }
              if ["resultadoIniciarExpediente","resultadoAportarDocumentacion"].include?(tag_name)
                xml.tag! "p352:#{tag_name}", param_options.merge(attrs).merge!({"xmlns:p352" => "http://wsintbrg.bareg.iam.es"}), &blk
              else
                xml.tag! tag_name, param_options.merge(attrs), &blk
              end
            end
          else
            if ["resultadoIniciarExpediente","resultadoAportarDocumentacion"].include?(tag_name)
              xml.tag! "p352:#{tag_name}", param_options.merge!({"xmlns:p352" => "http://wsintbrg.bareg.iam.es"}) do
                wsdl_data(xml, param.map)
              end
            else
              xml.tag! tag_name, param_options do
                wsdl_data(xml, param.map)
              end
            end
          end
        else
          if param.multiplied
            param.value = [] unless param.value.is_a?(Array)
            param.value.each do |v|
              if ["resultadoIniciarExpediente","resultadoAportarDocumentacion"].include?(tag_name)
                xml.tag! "p352:#{tag_name}", v, param_options.merge!({"xmlns:p352" => "http://wsintbrg.bareg.iam.es"})
              else
                xml.tag! tag_name, v, param_options
              end
            end
          else
            if ["resultadoIniciarExpediente","resultadoAportarDocumentacion"].include?(tag_name)
              xml.tag! "p352:#{tag_name}", param.value, param_options.merge!({"xmlns:p352" => "http://wsintbrg.bareg.iam.es"})
            else
              xml.tag! tag_name, param.value, param_options
            end
          end
        end
      end
    rescue => e
      begin
        Rails.logger.error("COD-00003: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end
  
    def wsdl_type(xml, param, defined=[])
      more = []
  
      if param.struct?
        if !defined.include?(param.basic_type)
          xml.tag! "xs:complexType", :name => param.basic_type do
            attrs, elems = [], []
            param.map.each do |value|
              more << value if value.struct?
              if value.attribute?
                attrs << value
              else
                elems << value
              end
            end
  
            if elems.any?
              xml.tag! "xs:sequence" do
                elems.each do |value|
                  if ["PeticionAportarDocumentacion"].include?(param.basic_type)
                    xml.tag! "xs:element", wsdl_occurence(value, false, {:name => value.name, :type => value.namespaced_type.gsub('xsd','xs')}, false)
                  else
                    xml.tag! "xs:element", wsdl_occurence(value, false, {:name => value.name, :type => value.namespaced_type.gsub('xsd','xs')}, true)
                  end
                end
              end
            end
  
            attrs.each do |value|
              xml.tag! "xs:attribute", wsdl_occurence(value, false, :name => value.attr_name, :type => value.namespaced_type.gsub('xsd','xs'))
            end
          end

          defined << param.basic_type
        elsif !param.classified?
          raise RuntimeError, "Duplicate use of `#{param.basic_type}` type name. Consider using classified types."
        end

        # definidos = true
        # ["PeticionIniciarExpediente","ResultadoIniciarExpediente","PeticionAportarDocumentacion","ResultadoAportarDocumentacion"].each do |p|
        #   if !defined.include?(p)
        #     definidos= false
        #     break
        #   end
        # end
        # if definidos
        #   ["PeticionIniciarExpediente","ResultadoIniciarExpediente","PeticionAportarDocumentacion","ResultadoAportarDocumentacion"].each do |p|
        #     xml.tag! "xs:element", :name =>  p.camelize(:lower) , "nillable" => 'true', type: "tns:#{p}"
        #   end
        # end
      end
  
      more.each do |p|
        wsdl_type xml, p, defined
      end
    rescue => e
      begin
        Rails.logger.error("COD-00004: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end
  
    def wsdl_occurence(param, inject, extend_with = {}, other= true)
      #data = {"#{'xsi:' if inject}nillable" => 'true'}
      data = {}
      if other
        if ["codRetorno","descError","refExpediente"].include?(extend_with[:name])
          data["#{'xsi:' if inject}minOccurs"] = 0
        end
        if param.multiplied       
          if ["idDocumento","formato"].include?(extend_with[:name])
            data["#{'xsi:' if inject}maxOccurs"] = 'unbounded'
          end
        end
      end
      extend_with.merge(data)
    rescue => e
      begin
        Rails.logger.error("COD-00005: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end
  end