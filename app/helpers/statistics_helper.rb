module StatisticsHelper
  def employee_lobbies_count(lobbies)
    lobbies.select { |l| l if l.self_employed_lobby && !l.employee_lobby }
  rescue => e
    begin
      Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def self_employed_lobbies_count(lobbies)
    lobbies.select { |l| l if !l.self_employed_lobby && l.employee_lobby}
  rescue => e
    begin
      Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def self_employed_and_employee_lobbies_count(lobbies)
    lobbies.select { |l| l if l.self_employed_lobby && l.employee_lobby }
  rescue => e
    begin
      Rails.logger.error("COD-00003: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def own_lobby_activity(lobbies)
    lobbies.where( "own_lobby_activity= true and foreign_lobby_activity=false")   
  rescue => e
    begin
      Rails.logger.error("COD-00004: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def foreign_lobby_activity(lobbies)
    lobbies.where("foreign_lobby_activity= true and own_lobby_activity=false" )
  rescue => e
    begin
      Rails.logger.error("COD-00005: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def own_and_foreign_lobby_activity(lobbies)
    lobbies.where(foreign_lobby_activity: true, own_lobby_activity: true)
  rescue => e
    begin
      Rails.logger.error("COD-00006: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def lobbies_active_agents(lobbies)
    Agent.where("organization_id in (?)", lobbies.pluck(:id)).active
  rescue => e
    begin
      Rails.logger.error("COD-00007: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

end
