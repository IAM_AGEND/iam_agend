module Admin::HolderActiveEventsHelper
    extend ActiveSupport::Concern 
    def stream_query_rows(export=nil,params= {}, url)        
        pos_ids = export.map(&:id).to_s.gsub(/\[|\]/,"") if !export.blank?
        count_events = "COALESCE(CAST((SELECT DISTINCT COUNT(events.*) FROM events WHERE events.position_id = positions.id AND (events.status = 1 OR events.status = 2)) as varchar),'-')"
        if params[:search_start_date].present? && params[:search_end_date].present?
            count_events = "COALESCE(CAST((SELECT DISTINCT COUNT(events.*) FROM events WHERE events.position_id = positions.id AND (events.status = 1 OR events.status = 2) AND cast(events.scheduled as date) BETWEEN cast('#{Time.zone.parse(params[:search_start_date])}' as date)  AND cast('#{Time.zone.parse(params[:search_end_date])}' as date)  AND (events.scheduled IS NULL OR cast(events.scheduled as date) BETWEEN cast('#{Time.zone.parse(params[:search_start_date])}' as date)  AND cast('#{Time.zone.parse(params[:search_end_date])}'as date))) as varchar),'-')"
        elsif params[:search_start_date].present?
            count_events = "COALESCE(CAST((SELECT DISTINCT COUNT(events.*) FROM events WHERE events.position_id = positions.id AND (events.status = 1 OR events.status = 2) AND cast(events.scheduled as date) >= cast('#{Time.zone.parse(params[:search_start_date])}'as date)) as varchar),'-')"
        elsif params[:search_end_date].present?
            count_events = "COALESCE(CAST((SELECT DISTINCT COUNT(events.*) FROM events WHERE events.position_id = positions.id AND (events.status = 1 OR events.status = 2) AND cast(events.scheduled as date) <= cast('#{Time.zone.parse(params[:search_end_date])}' as date) AND (events.scheduled IS NULL OR cast(events.scheduled as date)  <= cast('#{Time.zone.parse(params[:search_end_date])}' as date))) as varchar),'-')"
        end
        count_participants = "COALESCE(CAST((SELECT DISTINCT COUNT(events.*) FROM events, participants WHERE events.id = participants.event_id AND participants.position_id = positions.id AND (events.status = 1 OR events.status = 2)) as varchar),'-')"
        if params[:search_start_date].present? && params[:search_end_date].present?
            count_participants= "COALESCE(CAST((SELECT DISTINCT COUNT(events.*) FROM events, participants WHERE events.id = participants.event_id AND participants.position_id = positions.id AND (events.status = 1 OR events.status = 2) AND cast(events.scheduled as date) BETWEEN cast('#{Time.zone.parse(params[:search_start_date])}' as date)  AND cast('#{Time.zone.parse(params[:search_end_date])}' as date)  AND (events.scheduled IS NULL OR cast(events.scheduled as date) BETWEEN cast('#{Time.zone.parse(params[:search_start_date])}' as date)  AND cast('#{Time.zone.parse(params[:search_end_date])}' as date))) as varchar),'-')"
        elsif params[:search_start_date].present?
            count_participants= "COALESCE(CAST((SELECT DISTINCT COUNT(events.*) FROM events, participants WHERE events.id = participants.event_id AND participants.position_id = positions.id AND (events.status = 1 OR events.status = 2) AND cast(events.scheduled as date) >= cast('#{Time.zone.parse(params[:search_start_date])}' as date)) as varchar),'-')"
        elsif params[:search_end_date].present?
            count_participants= "COALESCE(CAST((SELECT DISTINCT COUNT(events.*) FROM events, participants WHERE events.id = participants.event_id AND participants.position_id = positions.id AND (events.status = 1 OR events.status = 2) AND cast(events.scheduled as date) <= cast('#{Time.zone.parse(params[:search_end_date])}' as date) AND (events.scheduled IS NULL OR cast(events.scheduled as date)  <= cast('#{Time.zone.parse(params[:search_end_date])}' as date))) as varchar),'-')"
        end
        
        query = "COPY (Select 
        COALESCE(UPPER(CONCAT(holders.last_name,', ',holders.first_name)),'-') as \"#{I18n.t('backend.holders_active.title')}\",
        COALESCE((SELECT users.email FROM users WHERE users.user_key = holders.user_key),'-') as \"#{I18n.t('backend.holder_csv.email')}\",
        COALESCE(CAST(to_char(positions.start,'dd/MM/yyyy') as varchar),'-') as \"#{I18n.t('backend.holders_active.start_date')}\",
        COALESCE(CAST(to_char(positions.to,'dd/MM/yyyy') as varchar),'-') as \"#{I18n.t('backend.holders_active.end_date')}\",
        #{count_events} as \"#{I18n.t('backend.holders_active.num_holders')}\",
        #{count_participants}  as \"#{I18n.t('backend.holders_active.num_asist')}\",
        COALESCE(positions.title,'-') as \"#{I18n.t('backend.holder_csv.position')}\",
        COALESCE((select CAST(areas.internal_id as varchar) from areas where positions.area_id=areas.id ) ,'-') as \"#{I18n.t('backend.holder_csv.cod_area')}\",
        COALESCE((select areas.title from areas where positions.area_id=areas.id ),'-') as \"#{I18n.t('backend.holder_csv.area')}\",
        CONCAT('#{url}','/agenda/',holders.id,'/',TRANSLATE(LOWER(holders.first_name),'áéíóú ','aeiou-'),'-',TRANSLATE(LOWER(holders.last_name),'áéíóú ','aeiou-')) as \"#{I18n.t('backend.public_url')}\"       
        FROM positions, holders 
        WHERE positions.holder_id=holders.id 
        #{"AND positions.id IN (#{pos_ids.to_s.gsub(/\[|\]/,'')})" unless pos_ids.blank?}
        ORDER BY \"#{I18n.t('backend.holders_active.title')}\" ASC
        ) TO STDOUT (delimiter ';', FORMAT CSV,HEADER );" 
        Admin::StreamingHelper.execute_stream_query(query).each { |x| yield x if block_given? } 
    rescue => e
        begin
          Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
    end

    def holders_stream_query_rows(export=nil, url)     
        pos_ids = export.map(&:id).to_s.gsub(/\[|\]/,"") if !export.blank?
        
        query = "COPY (Select 
        COALESCE(UPPER(CONCAT(holders.last_name,', ',holders.first_name)),'-') as \"#{I18n.t('backend.holders_active.title')}\",
        COALESCE(positions.title,'-') as \"#{I18n.t('backend.holder_csv.position')}\",
        COALESCE((select areas.title from areas where positions.area_id=areas.id ),'-') as \"#{I18n.t('backend.holder_csv.area')}\",
        COALESCE(CAST(to_char(positions.start,'dd/MM/yyyy') as varchar),'-') as \"#{I18n.t('backend.holders_active.start_date')}\",
        COALESCE(CAST(to_char(positions.to,'dd/MM/yyyy') as varchar),'-') as \"#{I18n.t('backend.holders_active.end_date')}\",
        COALESCE((SELECT users.email FROM users WHERE users.user_key = holders.user_key),'-') as \"#{I18n.t('backend.holder_csv.email')}\",
        COALESCE((select UPPER(users.first_name) from users WHERE manages.user_id = users.id),'-') as \"#{I18n.t('backend.holder_csv.user_name')}\",
        COALESCE((select UPPER(CONCAT(users.last_name, ' ', users.second_last_name)) from users WHERE manages.user_id = users.id),'-') as \"#{I18n.t('backend.holder_csv.user_last_name')}\",
        COALESCE('-') as \"#{I18n.t('backend.holder_csv.user_position')}\",
        COALESCE('-') as \"#{I18n.t('backend.holder_csv.user_area')}\",
        COALESCE((select UPPER(users.email) from users WHERE manages.user_id = users.id),'-') as \"#{I18n.t('backend.holder_csv.user_email')}\"
        FROM positions, holders INNER JOIN manages ON holders.id = manages.holder_id
        WHERE positions.holder_id=holders.id
        ORDER BY \"#{I18n.t('backend.holders_active.title')}\" ASC
        ) TO STDOUT (delimiter ';', FORMAT CSV,HEADER );" 
        Admin::StreamingHelper.execute_stream_query(query).each { |x| yield x if block_given? } 
    rescue => e
        begin
          Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
    end
end