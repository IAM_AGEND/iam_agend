module Admin::ExportsHelper
    extend ActiveSupport::Concern

    def stream_query_rows_specific(export=nil)
        return if export.blank?
        header=""
        filter_fields=""
        conn = ActiveRecord::Base.connection.raw_connection
        count = 0
        order_fields.each {|key| header = generate_header_sql(header,key.to_s, export.fields[key.to_s])  if export.fields[key.to_s] == "1" || export.fields[key.to_s].to_s == "true" }
        export.filter_fields.each {|k,v| filter_fields = generate_sql(filter_fields,k,v)  if !v.blank? }  
            
        Admin::StreamingHelper.execute_stream_query("COPY (Select #{header.html_safe unless header.blank?}  from events #{filter_fields unless filter_fields.blank?}) TO STDOUT (delimiter ';', FORMAT CSV,HEADER );").each  { |x| yield x if block_given? }
    rescue => e
        begin
          Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
    end

    def order_fields
        [
            :scheduled_field,:title_field,:position_id_field,:job_field,:location_field,:description_field,:status_field,:holders_field,
            :other_attendees_field,:created_at_field,:published_at_field,:slug_field,:accepted_at_field,:canceled_at_field,:canceled_reasons_field,
            :declined_at_field,:declined_reasons_field,:organization_name_field,:lobby_activity_field,:lobby_expired_field,:represented_entities_field,:lobby_agent_field,
            :lobby_contact_firstname_field,:lobby_contact_lastname_field,:lobby_contact_email_field,:lobby_contact_phone_field,:lobby_scheduled_field,
            :notes_field,:user_id_field,:updated_at_field,:general_remarks_field
        ]
    rescue => e
        begin
          Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
    end

    def order_fields_to_s
        order_fields.map {|o| o.to_s}
    rescue => e
        begin
          Rails.logger.error("COD-00003: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
    end

    private

    def generate_header_sql(header,k,v)
        concat_aux = header.blank? ? " " : ", "
        date_year = DataPreference.find_by(title: "expired_year")
        date_year= date_year.blank? ? 2 : date_year.content_data
        
        case k.gsub("_field",'')
        when "job"
            header = header + "#{concat_aux}COALESCE((Select positions.title from  positions where positions.id=events.position_id),'-') as \"#{ I18n.t("backend.exports.fields.#{k}") }\""
        when "holders"
            header = header + "#{concat_aux}COALESCE((Select string_agg(TRANSLATE(UPPER(CONCAT(holders.first_name,' ',holders.last_name)),'ÁÉÍÓÚ','AEIOU'),', ')  from participants,positions,holders where participants.event_id = events.id AND participants.position_id=positions.id AND positions.holder_id=holders.id), '-') as \"#{ I18n.t("backend.exports.fields.#{k}") }\""
        when "other_attendees"
            header = header + "#{concat_aux}COALESCE((Select string_agg(TRANSLATE(UPPER(attendees.name),'ÁÉÍÓÚ','AEIOU'),', ') from attendees where attendees.event_id = events.id), '-') as \"#{ I18n.t("backend.exports.fields.#{k}") }\""
        when "represented_entities"
            header = header + "#{concat_aux}COALESCE((Select string_agg(TRANSLATE(UPPER(event_represented_entities.name),'ÁÉÍÓÚ','AEIOU'),', ')  from event_represented_entities where event_represented_entities.event_id = events.id), '-') as \"#{ I18n.t("backend.exports.fields.#{k}") }\""
        when "lobby_agent"
            header = header + "#{concat_aux}COALESCE((Select string_agg(TRANSLATE(UPPER(event_agents.name),'ÁÉÍÓÚ','AEIOU'),', ')  from event_agents where event_agents.event_id = events.id), '-') as \"#{ I18n.t("backend.exports.fields.#{k}") }\""
        when "user_id"
            header = header + "#{concat_aux}COALESCE((Select CONCAT(users.first_name,' ',users.last_name) from users where users.id=events.user_id),'-') as \"#{ I18n.t("backend.exports.fields.#{k}") }\""
        when "position_id"
            header = header + "#{concat_aux}COALESCE((Select CONCAT(holders.first_name,' ',holders.last_name) from holders, positions where holders.id=positions.holder_id AND positions.id=events.position_id),'-') as \"#{ I18n.t("backend.exports.fields.#{k}") }\""
        when "position_title"
            header = header + "#{concat_aux}COALESCE((Select positions.title from positions where positions.id=events.position_id),'-') as \"#{ I18n.t("backend.exports.fields.#{k}") }\""
        when "event_agent"
            header = header + "#{concat_aux}COALESCE((Select event_agents.name from event_agents where events.id=event_agents.event_id),'-') as \"#{ I18n.t("backend.exports.fields.#{k}") }\""
        when "status"
            header = header + "#{concat_aux}CASE events.status WHEN 0 THEN 'SOLICITADO' WHEN 1 THEN 'ACEPTADO' WHEN 2 THEN 'REALIZADO' WHEN 3 THEN 'RECHAZADO' WHEN 4 THEN 'CANCELADO' ELSE '-' END as \"#{ I18n.t("backend.exports.fields.#{k}") }\""
        when "attendees"
            header = header + "#{concat_aux}COALESCE((Select string_agg(TRANSLATE(UPPER(attendees.name),'ÁÉÍÓÚ','AEIOU'),', ')  from attendees where attendees.event_id = events.id), '-') as \"#{ I18n.t("backend.exports.fields.#{k}") }\""
        when "lobby_activity"
            header = header + "#{concat_aux}CASE events.lobby_activity WHEN 't' THEN 'SI' WHEN 'f' THEN 'NO' ELSE '-' END as \"#{ I18n.t("backend.exports.fields.#{k}") }\""
        when "lobby_expired"
            header = header + "#{concat_aux}CASE WHEN events.old_event_lobby = 't' AND events.lobby_activity = 't' THEN 'Evento antiguo'
                WHEN events.lobby_activity = 't' AND NOW() > (select organizations.renovation_date + interval '#{date_year} year' from organizations where organizations.id = events.organization_id) THEN 'Evento caducado' 
                ELSE 'Evento sin caducar' END as \"#{ I18n.t("backend.exports.fields.#{k}") }\""
        when "scheduled"
            header = header + "#{concat_aux}COALESCE(CAST(events.scheduled as varchar),'-') as \"#{ I18n.t("backend.exports.fields.#{k}") }\""
        when "published_at"
            header = header + "#{concat_aux}COALESCE(CAST(events.published_at as varchar),'-') as \"#{ I18n.t("backend.exports.fields.#{k}") }\""
        when "updated_at"
            header = header + "#{concat_aux}COALESCE(CAST(events.updated_at as varchar),'-') as \"#{ I18n.t("backend.exports.fields.#{k}") }\""
        when "created_at"
            header = header + "#{concat_aux}COALESCE(CAST(events.created_at as varchar),'-') as \"#{ I18n.t("backend.exports.fields.#{k}") }\""
        when "canceled_at"
            header = header + "#{concat_aux}COALESCE(CAST(events.canceled_at as varchar),'-') as \"#{ I18n.t("backend.exports.fields.#{k}") }\""
        when "lobby_scheduled"
            header = header + "#{concat_aux}COALESCE(CAST(events.lobby_scheduled as varchar),'-') as \"#{ I18n.t("backend.exports.fields.#{k}") }\""
        when "accepted_at"
            header = header + "#{concat_aux}COALESCE(CAST(events.accepted_at as varchar),'-') as \"#{ I18n.t("backend.exports.fields.#{k}") }\""
        when "declined_at"
            header = header + "#{concat_aux}COALESCE(CAST(events.declined_at as varchar),'-') as \"#{ I18n.t("backend.exports.fields.#{k}") }\""
        when "description"
            header = header + "#{concat_aux}#{ Admin::StreamingHelper.transform_html_data("events.description")} as \"#{ I18n.t("backend.exports.fields.#{k}") }\""
        when "general_remarks" 
            header = header + "#{concat_aux}#{ Admin::StreamingHelper.transform_html_data("events.general_remarks")} as \"#{ I18n.t("backend.exports.fields.#{k}") }\""
        else     
            header = header + "#{concat_aux}COALESCE(events.#{k.gsub("_field",'')},'-') as \"#{ I18n.t("backend.exports.fields.#{k}") }\""
        end
    end

    def generate_sql(filter_fields,k,v)
        filter_fields = "" if filter_fields.nil?
        concat_aux = filter_fields.blank? || !filter_fields.include?("WHERE") ? "WHERE" : "AND"
        
        case k.to_s
        when "status"
            s = ""
            c = 0
            v.each do |status|
                if status != ""
                    c = c + 1
                    case status
                    when 'Solicitado'
                        c == 1 ? s = "0" : s = s + ",0"
                    when 'Aceptado' 
                        c == 1 ? s = "1" : s = s + ",1"
                    when 'Realizado'
                        c == 1 ? s = "2" : s = s + ",2"
                    when 'Rechazado'
                        c == 1 ? s = "3" : s = s + ",3"
                    when 'Cancelado' 
                        c == 1 ? s = "4" : s = s + ",4"
                    end
                end
            end
            
            filter_fields = filter_fields + " #{concat_aux} events.status IN (#{s.gsub(" ", ',').gsub(", ", '')})" if s != ""
        when "title"
            filter_fields = filter_fields + " #{concat_aux} TRANSLATE(UPPER(events.title),'ÁÉÍÓÚ','AEIOU') LIKE TRANSLATE(UPPER('%#{v.to_s}%'),'ÁÉÍÓÚ','AEIOU')"
        when "people"
            holders = "Select events.id from positions, holders where positions.holder_id = holders.id AND events.position_id = positions.id AND TRANSLATE(UPPER(CONCAT(holders.last_name,' ',holders.first_name)),'ÁÉÍÓÚ','AEIOU') LIKE TRANSLATE(UPPER('%#{v.to_s}%'),'ÁÉÍÓÚ','AEIOU')"
            attendees = "Select events.id from attendees where attendees.event_id = events.id AND TRANSLATE(UPPER(attendees.name),'ÁÉÍÓÚ','AEIOU') LIKE TRANSLATE(UPPER('%#{v.to_s}%'),'ÁÉÍÓÚ','AEIOU')"
            filter_fields = filter_fields + " #{concat_aux} events.id IN (#{holders}) OR events.id IN (#{attendees})"
        when "start_date"
            filter_fields = filter_fields + " #{concat_aux} cast(events.scheduled as date) >= cast('#{v.to_date.strftime("%Y-%m-%d")}' as date) "
        when "end_date"
            filter_fields = filter_fields + " #{concat_aux} cast(events.scheduled as date) <= cast('#{v.to_date.strftime("%Y-%m-%d")}' as date)"
        when "start_date_published"
            filter_fields = filter_fields + " #{concat_aux} cast(events.published_at as date) >= cast('#{v.to_date.strftime("%Y-%m-%d")}' as date)"
        when "end_date_published"
            filter_fields = filter_fields + " #{concat_aux} cast(events.published_at as date) <= cast('#{v.to_date.strftime("%Y-%m-%d")}' as date)"
        end
    end
end