module Admin::PreferencesHelper
    def get_list_preferences(type)
        if type.to_s == "show_calendar"
            @selected_calendar ||=[]
            @selected_calendar
        elsif type.to_s == "time_frame"
            @time_frames ||= []

            @time_frames
        end
    rescue => e
        begin
          Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
    end

end