module Admin::OrganizationExporterHelper
    extend ActiveSupport::Concern 
    def stream_query_rows_private(export=nil, extended = false)
        return if export.blank?
        filter = export.map(&:id).to_s.gsub(/\[|\]/,"")
        date_year = DataPreference.find_by(title: "expired_year")
        date_year= date_year.blank? ? 2 : date_year.content_data
        data_days = DataPreference.find_by(title: "alert_first")
        data_days = data_days.blank? ? 60 : data_days.content_data.to_i
        reference = Time.zone.parse("2020-07-01")

        aux_interest = ""
        i = 0
        Interest.all.order(name: :asc).each_with_index do |interest, index|
            if !interest.name.blank?
                aux_interest = aux_interest + (i.to_i==0 ? "" : ", ") + "COALESCE(CAST((select 'Sí' from organization_interests where organizations.id = organization_interests.organization_id AND organization_interests.interest_id = #{interest.id} limit 1) as varchar),'No') as \"#{interest.name}\""
                    i = i + 1
            end
        end

    #     COALESCE(CAST(CASE WHEN organizations.canceled_at is not null and organizations.canceled_at <= NOW() THEN '#{I18n.t('backend.status_type.inactive')}'
    #     WHEN (organizations.renovation_date + interval '#{date_year} year') < NOW() OR (organizations.renovation_date is null and (organizations.inscription_date is null or organizations.inscription_date < '#{reference}'::date) and ('#{reference}'::date + interval '#{date_year} year') < NOW()) THEN '#{I18n.t('backend.status_type.expired')}'
    #     WHEN ((organizations.renovation_date + interval '#{date_year} year') - interval '#{data_days} day' < NOW()) OR (organizations.invalidated_at is null OR NOW() < organizations.invalidated_at) AND (organizations.canceled_at is null OR NOW() < organizations.canceled_at OR organizations.entity_type != 2) THEN '#{ I18n.t('backend.status_type.active')}'
    # #     WHEN organizations.invalidated_at is not null AND NOW() >= organizations.invalidated_at AND (organizations.canceled_at is null OR NOW() < organizations.canceled_at OR organizations.entity_type != 2) THEN '#{ I18n.t('backend.status_type.invalidated')}'
    #     ELSE '#{I18n.t('backend.status_type.inactive')}' END  as varchar),'-') as \"#{I18n.t("organization_exporter.status")}\", 
    # COALESCE(CAST(CASE WHEN organizations.status = 'active' AND ((organizations.renovation_date + interval '#{date_year} year') - interval '#{data_days} day' < NOW()) THEN '#{I18n.t('backend.management_type.management')}'
    #     WHEN organizations.status = 'active' AND (organizations.renovation_date is null and ((organizations.inscription_date + interval '#{date_year} year') - interval '#{data_days} day' < NOW())) THEN '#{I18n.t('backend.management_type.management')}'
    #     WHEN organizations.status = 'active' AND (((organizations.canceled_at + interval '#{date_year} year') - interval '#{data_days} day' < NOW())) THEN '#{I18n.t('backend.management_type.management')}'
    #     ELSE '-' END  as varchar),'-') as \"#{I18n.t("organization_exporter.management")}\", 
    # COALESCE(CAST(CASE organizations.entity_type WHEN 0 THEN 'Asociación'
    #     WHEN 1 THEN 'Federación'
    #     WHEN 2 THEN 'Lobby'
    #     ELSE '-' END  as varchar),'-') as \"#{I18n.t("organization_exporter.entity_type")}\", 

        query = "COPY (Select distinct 
            #{" COALESCE(CAST(organizations.reference  as varchar),'-') as \"#{I18n.t("organization_exporter.reference")}\", " if extended}
            COALESCE(CAST(organizations.identifier  as varchar),'-') as \"#{I18n.t("organization_exporter.identifier")}\",
            COALESCE(CAST(CASE WHEN organizations.name is not null AND trim(organizations.name) != '' THEN organizations.name
            ELSE organizations.business_name END  as varchar), '-') as \"#{I18n.t("organization_exporter.name")}\",
            COALESCE(CAST(organizations.first_surname  as varchar),'-') as \"#{I18n.t("organization_exporter.first_surname")}\", 
            COALESCE(CAST(organizations.second_surname  as varchar),'-') as \"#{I18n.t("organization_exporter.second_surname")}\",
            COALESCE(CAST((select addresses.address_type from addresses where organizations.address_id = addresses.id)  as varchar),'-') as \"#{I18n.t("organization_exporter.address_address_type")}\",
            COALESCE(CAST((select addresses.address from addresses where organizations.address_id = addresses.id)  as varchar),'-') as \"#{I18n.t("organization_exporter.address_address")}\",
            COALESCE(CAST((select addresses.address_number_type from addresses where organizations.address_id = addresses.id)  as varchar),'-') as \"#{I18n.t("organization_exporter.address_address_number_type")}\",
            COALESCE(CAST((select addresses.number from addresses where organizations.address_id = addresses.id)  as varchar),'-') as \"#{I18n.t("organization_exporter.address_number")}\",
            COALESCE(CAST((select addresses.gateway from addresses where organizations.address_id = addresses.id)  as varchar),'-') as \"#{I18n.t("organization_exporter.address_gateway")}\",
            COALESCE(CAST((select addresses.stairs from addresses where organizations.address_id = addresses.id)  as varchar),'-') as \"#{I18n.t("organization_exporter.address_stairs")}\",
            COALESCE(CAST((select addresses.floor from addresses where organizations.address_id = addresses.id)  as varchar),'-') as \"#{I18n.t("organization_exporter.address_floor")}\",
            COALESCE(CAST((select addresses.door from addresses where organizations.address_id = addresses.id)  as varchar),'-') as \"#{I18n.t("organization_exporter.address_door")}\",
            COALESCE(CAST((select addresses.postal_code from addresses where organizations.address_id = addresses.id)  as varchar),'-') as \"#{I18n.t("organization_exporter.address_postal_code")}\",
            COALESCE(CAST((select addresses.town from addresses where organizations.address_id = addresses.id)  as varchar),'-') as \"#{I18n.t("organization_exporter.address_town")}\",
            COALESCE(CAST((select addresses.province from addresses where organizations.address_id = addresses.id)  as varchar),'-') as \"#{I18n.t("organization_exporter.address_province")}\",
            COALESCE(CAST((select addresses.country from addresses where organizations.address_id = addresses.id)  as varchar),'-') as \"#{I18n.t("organization_exporter.address_country")}\",
            COALESCE(CAST((select addresses.email from addresses where organizations.address_id = addresses.id)  as varchar),'-') as \"#{I18n.t("organization_exporter.address_email")}\",
            COALESCE(CAST((select addresses.phones from addresses where organizations.address_id = addresses.id)  as varchar),'-') as \"#{I18n.t("organization_exporter.address_phones")}\",
            #{Admin::StreamingHelper.transform_html_data("organizations.description")} as \"#{ I18n.t("organization_exporter.description") }\",
            COALESCE(CAST(organizations.web  as varchar),'-') as \"#{I18n.t("organization_exporter.web")}\", 
            COALESCE(CAST(organizations.registered_lobbies  as varchar),'-') as \"#{I18n.t("organization_exporter.registered_lobbies")}\",             
            COALESCE(CAST(to_char(organizations.inscription_date,'yyyy-MM-dd HH24:MI') as varchar),'-') as \"#{I18n.t("organization_exporter.inscription_date")}\", 
            COALESCE(CAST(to_char(organizations.renovation_date,'yyyy-MM-dd HH24:MI') as varchar),'-') as \"#{I18n.t("organization_exporter.renovation_date")}\", 
            COALESCE(CAST(
                CASE WHEN organizations.renovation_date is not null THEN  to_char(organizations.renovation_date + interval '#{date_year} year','yyyy-MM-dd HH24:MI')
                WHEN organizations.inscription_date is not null THEN  to_char(organizations.inscription_date + interval '#{date_year} year','yyyy-MM-dd HH24:MI')
                ELSE to_char('#{reference}'::date + interval '#{date_year} year','yyyy-MM-dd HH24:MI') END as varchar),'-') as \"#{I18n.t("organization_exporter.expired_date")}\", 
            COALESCE(CAST(to_char(organizations.updated_at,'yyyy-MM-dd HH24:MI') as varchar),'-')  as \"#{I18n.t("organization_exporter.updated_at")}\", 
            COALESCE(CAST(to_char(organizations.termination_date,'yyyy-MM-dd HH24:MI') as varchar),'-')  as \"#{I18n.t("organization_exporter.termination_date")}\", 
            (select os.name from organization_statuses os where os.id=organizations.organization_status_id) as \"#{I18n.t("organization_exporter.status")}\", 
            COALESCE(CAST(CASE WHEN organizations.status = 'active' AND ((organizations.renovation_date + interval '#{date_year} year') - interval '#{data_days} day' < NOW()) THEN '#{I18n.t('backend.management_type.management')}'
                WHEN organizations.status = 'active' AND (organizations.renovation_date is null and ((organizations.inscription_date + interval '#{date_year} year') - interval '#{data_days} day' < NOW())) THEN '#{I18n.t('backend.management_type.management')}'
                WHEN organizations.status = 'active' AND (((organizations.canceled_at + interval '#{date_year} year') - interval '#{data_days} day' < NOW())) THEN '#{I18n.t('backend.management_type.management')}'
                ELSE '-' END  as varchar),'-') as \"#{I18n.t("organization_exporter.management")}\", 
            COALESCE(CAST(CASE organizations.entity_type WHEN 0 THEN 'Asociación'
                WHEN 1 THEN 'Federación'
                WHEN 2 THEN 'Lobby'
                ELSE '-' END  as varchar),'-') as \"#{I18n.t("organization_exporter.entity_type")}\", 
            COALESCE(CAST((select categories.name from categories where categories.id = organizations.category_id)  as varchar),'-') as \"#{I18n.t("organization_exporter.get_category")}\",             
            #{" COALESCE(CAST((Select CONCAT(legal_representants.name,' ',legal_representants.first_surname,' ',legal_representants.second_surname) from legal_representants where legal_representants.organization_id=organizations.id)   as varchar),'-') as \"#{I18n.t("organization_exporter.legal_representant_full_name")}\", 
            COALESCE(CAST((select addresses.address_type from addresses,legal_representants where legal_representants.organization_id = organizations.id AND legal_representants.address_id = addresses.id)  as varchar),'-') as \"#{I18n.t("organization_exporter.legal_representant_address_type")}\",
            COALESCE(CAST((select addresses.address from addresses,legal_representants where legal_representants.organization_id = organizations.id AND legal_representants.address_id = addresses.id)  as varchar),'-') as \"#{I18n.t("organization_exporter.legal_representant_address")}\",
            COALESCE(CAST((select addresses.address_number_type from addresses,legal_representants where legal_representants.organization_id = organizations.id AND legal_representants.address_id = addresses.id)  as varchar),'-') as \"#{I18n.t("organization_exporter.legal_representant_address_number_type")}\",
            COALESCE(CAST((select addresses.number from addresses,legal_representants where legal_representants.organization_id = organizations.id AND legal_representants.address_id = addresses.id)  as varchar),'-') as \"#{I18n.t("organization_exporter.legal_representant_number")}\",
            COALESCE(CAST((select addresses.gateway from addresses,legal_representants where legal_representants.organization_id = organizations.id AND legal_representants.address_id = addresses.id)  as varchar),'-') as \"#{I18n.t("organization_exporter.legal_representant_gateway")}\",
            COALESCE(CAST((select addresses.stairs from addresses,legal_representants where legal_representants.organization_id = organizations.id AND legal_representants.address_id = addresses.id)  as varchar),'-') as \"#{I18n.t("organization_exporter.legal_representant_stairs")}\",
            COALESCE(CAST((select addresses.floor from addresses,legal_representants where legal_representants.organization_id = organizations.id AND legal_representants.address_id = addresses.id)  as varchar),'-') as \"#{I18n.t("organization_exporter.legal_representant_floor")}\",
            COALESCE(CAST((select addresses.door from addresses,legal_representants where legal_representants.organization_id = organizations.id AND legal_representants.address_id = addresses.id)  as varchar),'-') as \"#{I18n.t("organization_exporter.legal_representant_door")}\",
            COALESCE(CAST((select addresses.postal_code from addresses,legal_representants where legal_representants.organization_id = organizations.id AND legal_representants.address_id = addresses.id)  as varchar),'-') as \"#{I18n.t("organization_exporter.legal_representant_postal_code")}\",
            COALESCE(CAST((select addresses.town from addresses,legal_representants where legal_representants.organization_id = organizations.id AND legal_representants.address_id = addresses.id)  as varchar),'-') as \"#{I18n.t("organization_exporter.legal_representant_town")}\",
            COALESCE(CAST((select addresses.province from addresses,legal_representants where legal_representants.organization_id = organizations.id AND legal_representants.address_id = addresses.id)  as varchar),'-') as \"#{I18n.t("organization_exporter.legal_representant_province")}\",
            COALESCE(CAST((select addresses.country from addresses,legal_representants where legal_representants.organization_id = organizations.id AND legal_representants.address_id = addresses.id)  as varchar),'-') as \"#{I18n.t("organization_exporter.legal_representant_country")}\",
            COALESCE(CAST((select addresses.email from addresses,legal_representants where legal_representants.organization_id = organizations.id AND legal_representants.address_id = addresses.id)  as varchar),'-') as \"#{I18n.t("organization_exporter.legal_representant_email")}\",
            COALESCE(CAST((select addresses.phones from addresses,legal_representants where legal_representants.organization_id = organizations.id AND legal_representants.address_id = addresses.id)  as varchar),'-') as \"#{I18n.t("organization_exporter.legal_representant_phones")}\",            
            COALESCE(CAST((Select CONCAT(notification_effects.name,' ',notification_effects.first_surname,' ',notification_effects.second_surname) from notification_effects where notification_effects.organization_id=organizations.id)   as varchar),'-') as \"#{I18n.t("organization_exporter.notification_effect_full_name")}\", 
            COALESCE(CAST((select addresses.address_type from addresses,notification_effects where notification_effects.organization_id = organizations.id AND notification_effects.address_id = addresses.id)  as varchar),'-') as \"#{I18n.t("organization_exporter.notification_effect_address_type")}\",
            COALESCE(CAST((select addresses.address from addresses,notification_effects where notification_effects.organization_id = organizations.id AND notification_effects.address_id = addresses.id)  as varchar),'-') as \"#{I18n.t("organization_exporter.notification_effect_address")}\",
            COALESCE(CAST((select addresses.address_number_type from addresses,notification_effects where notification_effects.organization_id = organizations.id AND notification_effects.address_id = addresses.id)  as varchar),'-') as \"#{I18n.t("organization_exporter.notification_effect_address_number_type")}\",
            COALESCE(CAST((select addresses.number from addresses,notification_effects where notification_effects.organization_id = organizations.id AND notification_effects.address_id = addresses.id)  as varchar),'-') as \"#{I18n.t("organization_exporter.notification_effect_number")}\",
            COALESCE(CAST((select addresses.gateway from addresses,notification_effects where notification_effects.organization_id = organizations.id AND notification_effects.address_id = addresses.id)  as varchar),'-') as \"#{I18n.t("organization_exporter.notification_effect_gateway")}\",
            COALESCE(CAST((select addresses.stairs from addresses,notification_effects where notification_effects.organization_id = organizations.id AND notification_effects.address_id = addresses.id)  as varchar),'-') as \"#{I18n.t("organization_exporter.notification_effect_stairs")}\",
            COALESCE(CAST((select addresses.floor from addresses,notification_effects where notification_effects.organization_id = organizations.id AND notification_effects.address_id = addresses.id)  as varchar),'-') as \"#{I18n.t("organization_exporter.notification_effect_floor")}\",
            COALESCE(CAST((select addresses.door from addresses,notification_effects where notification_effects.organization_id = organizations.id AND notification_effects.address_id = addresses.id)  as varchar),'-') as \"#{I18n.t("organization_exporter.notification_effect_door")}\",
            COALESCE(CAST((select addresses.postal_code from addresses,notification_effects where notification_effects.organization_id = organizations.id AND notification_effects.address_id = addresses.id)  as varchar),'-') as \"#{I18n.t("organization_exporter.notification_effect_postal_code")}\",
            COALESCE(CAST((select addresses.town from addresses,notification_effects where notification_effects.organization_id = organizations.id AND notification_effects.address_id = addresses.id)  as varchar),'-') as \"#{I18n.t("organization_exporter.notification_effect_town")}\",
            COALESCE(CAST((select addresses.province from addresses,notification_effects where notification_effects.organization_id = organizations.id AND notification_effects.address_id = addresses.id)  as varchar),'-') as \"#{I18n.t("organization_exporter.notification_effect_province")}\",
            COALESCE(CAST((select addresses.country from addresses,notification_effects where notification_effects.organization_id = organizations.id AND notification_effects.address_id = addresses.id)  as varchar),'-') as \"#{I18n.t("organization_exporter.notification_effect_country")}\",
            COALESCE(CAST((select addresses.email from addresses,notification_effects where notification_effects.organization_id = organizations.id AND notification_effects.address_id = addresses.id)  as varchar),'-') as \"#{I18n.t("organization_exporter.notification_effect_email")}\",
            COALESCE(CAST((select addresses.phones from addresses,notification_effects where notification_effects.organization_id = organizations.id AND notification_effects.address_id = addresses.id)  as varchar),'-') as \"#{I18n.t("organization_exporter.notification_effect_phones")}\",
            COALESCE(CAST((select CONCAT(users.first_name, ' ', users.last_name) from users where users.organization_id = organizations.id limit 1)  as varchar),'-') as \"#{I18n.t("organization_exporter.user_name")}\", 
            COALESCE(CAST((select users.email from users where users.organization_id = organizations.id limit 1)  as varchar),'-') as \"#{I18n.t("organization_exporter.user_email")}\", 
            COALESCE(CAST((select users.phones from users where users.organization_id = organizations.id limit 1)  as varchar),'-') as \"#{I18n.t("organization_exporter.user_phones")}\", " if extended}            
            COALESCE(CAST(CASE WHEN organizations.invalidated_at is null THEN 'No' ELSE 'Sí' END  as varchar),'-') as \"#{I18n.t("organization_exporter.invalidated?")}\",
            COALESCE(CAST(CASE WHEN (organizations.own_lobby_activity=true OR (organizations.own_lobby_activity is null AND organizations.foreign_lobby_activity = false)) THEN 'Sí' ELSE 'NO' END  as varchar),'-') as \"#{I18n.t("organization_exporter.self_employed_lobby")}\", 
            COALESCE(CAST(CASE WHEN (organizations.own_lobby_activity=true OR (organizations.own_lobby_activity is null AND organizations.foreign_lobby_activity = false)) THEN CAST(organizations.fiscal_year as varchar) ELSE null END  as varchar),'-') as \"#{I18n.t("organization_exporter.get_fiscal_year")}\", 
            COALESCE(CAST(CASE WHEN (organizations.own_lobby_activity=true OR (organizations.own_lobby_activity is null AND organizations.foreign_lobby_activity = false)) THEN 
                    CASE organizations.range_fund WHEN 0 THEN 'De 0 a 9.999 euros' 
                    WHEN 1 THEN 'De 10.000 a 24.999 euros' 
                    WHEN 2 THEN 'De 25.000 a 49.000 euros'
                    WHEN 3 THEN 'Más de 50.000 euros'
                    ELSE '-' END   
            ELSE null END  as varchar),'-') as \"#{I18n.t("organization_exporter.get_range_fund")}\", 
            COALESCE(CAST(CASE WHEN (organizations.own_lobby_activity=true OR (organizations.own_lobby_activity is null AND organizations.foreign_lobby_activity = false)) AND organizations.in_group_public_administration = true THEN 'Si'
                WHEN (organizations.own_lobby_activity=true OR (organizations.own_lobby_activity is null AND organizations.foreign_lobby_activity = false)) AND organizations.in_group_public_administration = false THEN 'No' ELSE null END  as varchar),'-') as \"#{I18n.t("organization_exporter.get_in_group_public_administration")}\", 
            COALESCE(CAST(CASE WHEN (organizations.own_lobby_activity=true OR (organizations.own_lobby_activity is null AND organizations.foreign_lobby_activity = false)) AND organizations.in_group_public_administration = true  THEN organizations.text_group_public_administration ELSE null END  as varchar),'-') as \"#{I18n.t("organization_exporter.get_text_group_public_administration")}\", 
            COALESCE(CAST(CASE WHEN (organizations.own_lobby_activity=true OR (organizations.own_lobby_activity is null AND organizations.foreign_lobby_activity = false)) AND organizations.subvention = true THEN 'Si'
                WHEN (organizations.own_lobby_activity=true OR (organizations.own_lobby_activity is null AND organizations.foreign_lobby_activity = false)) AND organizations.subvention = false THEN 'No' ELSE null END  as varchar),'-') as \"#{I18n.t("organization_exporter.get_subvention")}\", 
            COALESCE(CAST(CASE WHEN (organizations.own_lobby_activity=true OR (organizations.own_lobby_activity is null AND organizations.foreign_lobby_activity = false)) AND organizations.subvention_public_administration = true THEN 'Si'
                WHEN (organizations.own_lobby_activity=true OR (organizations.own_lobby_activity is null AND organizations.foreign_lobby_activity = false)) AND organizations.subvention_public_administration = false THEN 'No' ELSE null END  as varchar),'-') as \"#{I18n.t("organization_exporter.get_subvention_public_administration")}\",
            COALESCE(CAST(CASE WHEN (organizations.own_lobby_activity=true OR (organizations.own_lobby_activity is null AND organizations.foreign_lobby_activity = false)) AND organizations.subvention_public_administration = true  THEN 
                (select range_subventions.import from range_subventions where range_subventions.item_type ='Organization' AND range_subventions.item_id = organizations.id limit 1 offset 0)
                ELSE null END   as varchar),'-') as \"#{I18n.t("organization_exporter.get_range_1_import")}\", 
            COALESCE(CAST(CASE WHEN (organizations.own_lobby_activity=true OR (organizations.own_lobby_activity is null AND organizations.foreign_lobby_activity = false)) AND organizations.subvention_public_administration = true  THEN 
                (select range_subventions.entity_name from range_subventions where range_subventions.item_type ='Organization' AND range_subventions.item_id = organizations.id limit 1 offset 0)
                ELSE null END   as varchar),'-') as \"#{I18n.t("organization_exporter.get_range_1_entity_name")}\", 
            COALESCE(CAST(CASE WHEN (organizations.own_lobby_activity=true OR (organizations.own_lobby_activity is null AND organizations.foreign_lobby_activity = false)) AND organizations.subvention_public_administration = true  THEN 
                (select range_subventions.import from range_subventions where range_subventions.item_type ='Organization' AND range_subventions.item_id = organizations.id limit 1 offset 1)
                ELSE null END   as varchar),'-') as \"#{I18n.t("organization_exporter.get_range_2_import")}\", 
            COALESCE(CAST(CASE WHEN (organizations.own_lobby_activity=true OR (organizations.own_lobby_activity is null AND organizations.foreign_lobby_activity = false)) AND organizations.subvention_public_administration = true  THEN 
                (select range_subventions.entity_name from range_subventions where range_subventions.item_type ='Organization' AND range_subventions.item_id = organizations.id limit 1 offset 1)
                ELSE null END   as varchar),'-') as \"#{I18n.t("organization_exporter.get_range_2_entity_name")}\", 
            COALESCE(CAST(CASE WHEN (organizations.own_lobby_activity=true OR (organizations.own_lobby_activity is null AND organizations.foreign_lobby_activity = false)) AND organizations.subvention_public_administration = true  THEN 
                (select range_subventions.import from range_subventions where range_subventions.item_type ='Organization' AND range_subventions.item_id = organizations.id limit 1 offset 2)
                ELSE null END   as varchar),'-') as \"#{I18n.t("organization_exporter.get_range_3_import")}\", 
            COALESCE(CAST(CASE WHEN (organizations.own_lobby_activity=true OR (organizations.own_lobby_activity is null AND organizations.foreign_lobby_activity = false)) AND organizations.subvention_public_administration = true  THEN 
                (select range_subventions.entity_name from range_subventions where range_subventions.item_type ='Organization' AND range_subventions.item_id = organizations.id limit 1 offset 2)
                ELSE null END   as varchar),'-') as \"#{I18n.t("organization_exporter.get_range_3_entity_name")}\", 
            COALESCE(CAST(CASE WHEN (organizations.own_lobby_activity=true OR (organizations.own_lobby_activity is null AND organizations.foreign_lobby_activity = false)) AND organizations.subvention_public_administration = true  THEN 
                (select range_subventions.import from range_subventions where range_subventions.item_type ='Organization' AND range_subventions.item_id = organizations.id limit 1 offset 3)
                ELSE null END   as varchar),'-') as \"#{I18n.t("organization_exporter.get_range_4_import")}\", 
            COALESCE(CAST(CASE WHEN (organizations.own_lobby_activity=true OR (organizations.own_lobby_activity is null AND organizations.foreign_lobby_activity = false)) AND organizations.subvention_public_administration = true  THEN 
                (select range_subventions.entity_name from range_subventions where range_subventions.item_type ='Organization' AND range_subventions.item_id = organizations.id limit 1 offset 3)
                ELSE null END   as varchar),'-') as \"#{I18n.t("organization_exporter.get_range_4_entity_name")}\", 
            COALESCE(CAST(CASE WHEN (organizations.own_lobby_activity=true OR (organizations.own_lobby_activity is null AND organizations.foreign_lobby_activity = false)) AND organizations.contract = true THEN 'Si'
                WHEN (organizations.own_lobby_activity=true OR (organizations.own_lobby_activity is null AND organizations.foreign_lobby_activity = false)) AND organizations.contract = false THEN 'No' ELSE null END as varchar),'-') as \"#{I18n.t("organization_exporter.get_contract")}\", 
            COALESCE(CAST(CASE WHEN (organizations.own_lobby_activity=true OR (organizations.own_lobby_activity is null AND organizations.foreign_lobby_activity = false)) AND organizations.contract = true THEN organizations.contract_turnover ELSE null END  as varchar),'-') as \"#{I18n.t("organization_exporter.get_contract_turnover")}\", 
            COALESCE(CAST(CASE WHEN (organizations.own_lobby_activity=true OR (organizations.own_lobby_activity is null AND organizations.foreign_lobby_activity = false)) AND organizations.contract = true THEN organizations.contract_total_budget ELSE null END  as varchar),'-') as \"#{I18n.t("organization_exporter.get_contract_total_budget")}\", 
            COALESCE(CAST(CASE WHEN (organizations.own_lobby_activity=true OR (organizations.own_lobby_activity is null AND organizations.foreign_lobby_activity = false)) AND organizations.contract = true THEN organizations.contract_breakdown ELSE null END  as varchar),'-') as \"#{I18n.t("organization_exporter.get_contract_breakdown")}\", 
            COALESCE(CAST(CASE WHEN (organizations.own_lobby_activity=true OR (organizations.own_lobby_activity is null AND organizations.foreign_lobby_activity = false)) AND organizations.contract = true THEN organizations.contract_financing ELSE null END  as varchar),'-') as \"#{I18n.t("organization_exporter.get_contract_financing")}\", 
            COALESCE(CAST((select string_agg(CONCAT(agents.name,' ',agents.first_surname,' ',agents.second_surname),' / ') from agents where agents.organization_id = organizations.id)  as varchar),'-') as \"#{I18n.t("organization_exporter.agents")}\", 
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) THEN 'Sí' ELSE 'NO' END  as varchar),'-') as \"#{I18n.t("organization_exporter.employee_lobby")}\",            
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) THEN 
                (select CONCAT(represented_entities.name,' ',represented_entities.first_surname,' ', represented_entities.second_surname) from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 0)
                ELSE null END  as varchar),'-') as \"#{I18n.t("organization_exporter.represented_entity_1")}\", 
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) THEN 
                (select CAST(represented_entities.fiscal_year as varchar) from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 0)
                ELSE null END  as varchar),'-') as \"#{I18n.t("organization_exporter.fiscal_year_1")}\",
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) THEN 
                (select

                    CASE represented_entities.range_fund WHEN 0 THEN 'De 0 a 9.999 euros' 
                    WHEN 1 THEN 'De 10.000 a 24.999 euros' 
                    WHEN 2 THEN 'De 25.000 a 49.000 euros'
                    WHEN 3 THEN 'Más de 50.000 euros'
                    ELSE '-' END 
                
                from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 0)
                ELSE null END  as varchar),'-') as \"#{I18n.t("organization_exporter.range_fund_1")}\",
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.in_group_public_administration from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 0) = true THEN 'Si'
                WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.in_group_public_administration from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 0) = false THEN 'No' ELSE null END  as varchar),'-') as \"#{I18n.t("organization_exporter.in_group_public_administration_1")}\", 
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.in_group_public_administration from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 0) = true THEN 
                (select represented_entities.text_group_public_administration from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 0)
                ELSE null END  as varchar),'-') as \"#{I18n.t("organization_exporter.text_group_public_administration_1")}\", 
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.subvention from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 0) = true THEN 'Si'
                WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.subvention from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 0) = false THEN 'No' ELSE null END  as varchar),'-') as \"#{I18n.t("organization_exporter.subvention_1")}\",
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.subvention_public_administration from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 0) = true THEN 'Si'
                WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.subvention_public_administration from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 0) = false THEN 'No' ELSE null END  as varchar),'-') as \"#{I18n.t("organization_exporter.subvention_public_administration_1")}\", 
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.subvention_public_administration from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 0) = true THEN 
                (select range_subventions.import from range_subventions,represented_entities where represented_entities.id = (select represented_entities.id from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 0) AND represented_entities.id = (select represented_entities.id from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 0) AND range_subventions.item_type ='RepresentedEntity' AND range_subventions.item_id = represented_entities.id AND represented_entities.organization_id = organizations.id limit 1 offset 0)
                ELSE null END   as varchar),'-') as \"#{I18n.t("organization_exporter.get_range_1_1_import")}\", 
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.subvention_public_administration from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 0) = true THEN 
                (select range_subventions.entity_name from range_subventions,represented_entities where represented_entities.id = (select represented_entities.id from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 0) AND range_subventions.item_type ='RepresentedEntity' AND range_subventions.item_id = represented_entities.id AND represented_entities.organization_id = organizations.id limit 1 offset 0)
                ELSE null END   as varchar),'-') as \"#{I18n.t("organization_exporter.get_range_1_1_entity_name")}\",
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.subvention_public_administration from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 0) = true THEN 
                (select range_subventions.import from range_subventions,represented_entities where represented_entities.id = (select represented_entities.id from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 0) AND range_subventions.item_type ='RepresentedEntity' AND range_subventions.item_id = represented_entities.id AND represented_entities.organization_id = organizations.id limit 1 offset 1)
                ELSE null END   as varchar),'-') as \"#{I18n.t("organization_exporter.get_range_1_2_import")}\", 
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.subvention_public_administration from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 0) = true THEN 
                (select range_subventions.entity_name from range_subventions,represented_entities where represented_entities.id = (select represented_entities.id from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 0) AND range_subventions.item_type ='RepresentedEntity' AND range_subventions.item_id = represented_entities.id AND represented_entities.organization_id = organizations.id limit 1 offset 1)
                ELSE null END   as varchar),'-') as \"#{I18n.t("organization_exporter.get_range_1_2_entity_name")}\",
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.subvention_public_administration from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 0) = true THEN 
                (select range_subventions.import from range_subventions,represented_entities where represented_entities.id = (select represented_entities.id from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 0) AND range_subventions.item_type ='RepresentedEntity' AND range_subventions.item_id = represented_entities.id AND represented_entities.organization_id = organizations.id limit 1 offset 2)
                ELSE null END   as varchar),'-') as \"#{I18n.t("organization_exporter.get_range_1_3_import")}\", 
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.subvention_public_administration from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 0) = true THEN 
                (select range_subventions.entity_name from range_subventions,represented_entities where represented_entities.id = (select represented_entities.id from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 0) AND range_subventions.item_type ='RepresentedEntity' AND range_subventions.item_id = represented_entities.id AND represented_entities.organization_id = organizations.id limit 1 offset 2)
                ELSE null END   as varchar),'-') as \"#{I18n.t("organization_exporter.get_range_1_3_entity_name")}\",
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.subvention_public_administration from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 0) = true THEN 
                (select range_subventions.import from range_subventions,represented_entities where represented_entities.id = (select represented_entities.id from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 0) AND range_subventions.item_type ='RepresentedEntity' AND range_subventions.item_id = represented_entities.id AND represented_entities.organization_id = organizations.id limit 1 offset 3)
                ELSE null END   as varchar),'-') as \"#{I18n.t("organization_exporter.get_range_1_4_import")}\", 
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.subvention_public_administration from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 0) = true THEN 
                (select range_subventions.entity_name from range_subventions,represented_entities where represented_entities.id = (select represented_entities.id from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 0) AND range_subventions.item_type ='RepresentedEntity' AND range_subventions.item_id = represented_entities.id AND represented_entities.organization_id = organizations.id limit 1 offset 3)
                ELSE null END   as varchar),'-') as \"#{I18n.t("organization_exporter.get_range_1_4_entity_name")}\",
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.contract from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 0) = true THEN 'Si'
                WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.contract from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 0) = false THEN 'No' ELSE null END  as varchar),'-') as \"#{I18n.t("organization_exporter.contract_1")}\", 
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.contract from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 0) = true THEN 
                (select represented_entities.contract_turnover from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 0)
                ELSE null END  as varchar),'-') as \"#{I18n.t("organization_exporter.contract_turnover_1")}\", 
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.contract from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 0) = true THEN 
                (select represented_entities.contract_total_budget from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 0)
                ELSE null END  as varchar),'-') as \"#{I18n.t("organization_exporter.contract_total_budget_1")}\", 
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.contract from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 0) = true THEN 
                (select represented_entities.contract_breakdown from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 0)
                ELSE null END  as varchar),'-') as \"#{I18n.t("organization_exporter.contract_breakdown_1")}\", 
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.contract from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 0) = true THEN 
                (select represented_entities.contract_financing from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 0)
                ELSE null END  as varchar),'-') as \"#{I18n.t("organization_exporter.contract_financing_1")}\", 

            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) THEN 
                (select CONCAT(represented_entities.name,' ',represented_entities.first_surname,' ', represented_entities.second_surname) from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 1)
                ELSE null END  as varchar),'-') as \"#{I18n.t("organization_exporter.represented_entity_2")}\", 
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) THEN 
                (select CAST(represented_entities.fiscal_year as varchar) from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 1)
                ELSE null END  as varchar),'-') as \"#{I18n.t("organization_exporter.fiscal_year_2")}\",
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) THEN 
                (select 
                    CASE represented_entities.range_fund WHEN 0 THEN 'De 0 a 9.999 euros' 
                    WHEN 1 THEN 'De 10.000 a 24.999 euros' 
                    WHEN 2 THEN 'De 25.000 a 49.000 euros'
                    WHEN 3 THEN 'Más de 50.000 euros'
                    ELSE '-' END 
                
                from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 1)
                ELSE null END  as varchar),'-') as \"#{I18n.t("organization_exporter.range_fund_2")}\",
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.in_group_public_administration from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 1) = true THEN 'Si'
                WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.in_group_public_administration from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 1) = false THEN 'No' ELSE null END  as varchar),'-') as \"#{I18n.t("organization_exporter.in_group_public_administration_2")}\", 
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.in_group_public_administration from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 1) = true THEN 
                (select represented_entities.text_group_public_administration from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 1)
                ELSE null END  as varchar),'-') as \"#{I18n.t("organization_exporter.text_group_public_administration_2")}\", 
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.subvention from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 1) = true THEN 'Si'
                WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.subvention from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 1) = false THEN 'No' ELSE null END  as varchar),'-') as \"#{I18n.t("organization_exporter.subvention_2")}\",
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.subvention_public_administration from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 1) = true THEN 'Si'
                WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.subvention_public_administration from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 1) = false THEN 'No' ELSE null END  as varchar),'-') as \"#{I18n.t("organization_exporter.subvention_public_administration_2")}\", 
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.subvention_public_administration from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 1) = true THEN 
                (select range_subventions.import from range_subventions,represented_entities where represented_entities.id = (select represented_entities.id from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 1) AND range_subventions.item_type ='RepresentedEntity' AND range_subventions.item_id = represented_entities.id AND represented_entities.organization_id = organizations.id limit 1 offset 0)
                ELSE null END   as varchar),'-') as \"#{I18n.t("organization_exporter.get_range_2_1_import")}\", 
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.subvention_public_administration from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 1) = true THEN 
                (select range_subventions.entity_name from range_subventions,represented_entities where represented_entities.id = (select represented_entities.id from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 1) AND range_subventions.item_type ='RepresentedEntity' AND range_subventions.item_id = represented_entities.id AND represented_entities.organization_id = organizations.id limit 1 offset 0)
                ELSE null END   as varchar),'-') as \"#{I18n.t("organization_exporter.get_range_2_1_entity_name")}\",
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.subvention_public_administration from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 1) = true THEN 
                (select range_subventions.import from range_subventions,represented_entities where represented_entities.id = (select represented_entities.id from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 1) AND range_subventions.item_type ='RepresentedEntity' AND range_subventions.item_id = represented_entities.id AND represented_entities.organization_id = organizations.id limit 1 offset 1)
                ELSE null END   as varchar),'-') as \"#{I18n.t("organization_exporter.get_range_2_2_import")}\", 
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.subvention_public_administration from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 1) = true THEN 
                (select range_subventions.entity_name from range_subventions,represented_entities where represented_entities.id = (select represented_entities.id from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 1) AND range_subventions.item_type ='RepresentedEntity' AND range_subventions.item_id = represented_entities.id AND represented_entities.organization_id = organizations.id limit 1 offset 1)
                ELSE null END   as varchar),'-') as \"#{I18n.t("organization_exporter.get_range_2_2_entity_name")}\",
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.subvention_public_administration from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 1) = true THEN 
                (select range_subventions.import from range_subventions,represented_entities where represented_entities.id = (select represented_entities.id from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 1) AND range_subventions.item_type ='RepresentedEntity' AND range_subventions.item_id = represented_entities.id AND represented_entities.organization_id = organizations.id limit 1 offset 2)
                ELSE null END   as varchar),'-') as \"#{I18n.t("organization_exporter.get_range_2_3_import")}\", 
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.subvention_public_administration from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 1) = true THEN 
                (select range_subventions.entity_name from range_subventions,represented_entities where represented_entities.id = (select represented_entities.id from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 1) AND range_subventions.item_type ='RepresentedEntity' AND range_subventions.item_id = represented_entities.id AND represented_entities.organization_id = organizations.id limit 1 offset 2)
                ELSE null END   as varchar),'-') as \"#{I18n.t("organization_exporter.get_range_2_3_entity_name")}\",
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.subvention_public_administration from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 1) = true THEN 
                (select range_subventions.import from range_subventions,represented_entities where represented_entities.id = (select represented_entities.id from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 1) AND range_subventions.item_type ='RepresentedEntity' AND range_subventions.item_id = represented_entities.id AND represented_entities.organization_id = organizations.id limit 1 offset 3)
                ELSE null END   as varchar),'-') as \"#{I18n.t("organization_exporter.get_range_2_4_import")}\", 
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.subvention_public_administration from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 1) = true THEN 
                (select range_subventions.entity_name from range_subventions,represented_entities where represented_entities.id = (select represented_entities.id from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 1) AND range_subventions.item_type ='RepresentedEntity' AND range_subventions.item_id = represented_entities.id AND represented_entities.organization_id = organizations.id limit 1 offset 3)
                ELSE null END   as varchar),'-') as \"#{I18n.t("organization_exporter.get_range_2_4_entity_name")}\",
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.contract from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 1) = true THEN 'Si'
                WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.contract from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 1) = false THEN 'No' ELSE null END  as varchar),'-') as \"#{I18n.t("organization_exporter.contract_2")}\", 
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.contract from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 1) = true THEN 
                (select represented_entities.contract_turnover from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 1)
                ELSE null END  as varchar),'-') as \"#{I18n.t("organization_exporter.contract_turnover_2")}\", 
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.contract from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 1) = true THEN 
                (select represented_entities.contract_total_budget from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 1)
                ELSE null END  as varchar),'-') as \"#{I18n.t("organization_exporter.contract_total_budget_2")}\", 
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.contract from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 1) = true THEN 
                (select represented_entities.contract_breakdown from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 1)
                ELSE null END  as varchar),'-') as \"#{I18n.t("organization_exporter.contract_breakdown_2")}\", 
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.contract from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 1) = true THEN 
                (select represented_entities.contract_financing from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 1)
                ELSE null END  as varchar),'-') as \"#{I18n.t("organization_exporter.contract_financing_2")}\", 

            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) THEN 
                (select CONCAT(represented_entities.name,' ',represented_entities.first_surname,' ', represented_entities.second_surname) from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 2)
                ELSE null END  as varchar),'-') as \"#{I18n.t("organization_exporter.represented_entity_3")}\", 
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) THEN 
                (select CAST(represented_entities.fiscal_year as varchar) from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 2)
                ELSE null END  as varchar),'-') as \"#{I18n.t("organization_exporter.fiscal_year_3")}\",
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) THEN 
                (select
                    CASE represented_entities.range_fund WHEN 0 THEN 'De 0 a 9.999 euros' 
                    WHEN 1 THEN 'De 10.000 a 24.999 euros' 
                    WHEN 2 THEN 'De 25.000 a 49.000 euros'
                    WHEN 3 THEN 'Más de 50.000 euros'
                    ELSE '-' END 
                 from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 2)
                ELSE null END  as varchar),'-') as \"#{I18n.t("organization_exporter.range_fund_3")}\",
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.in_group_public_administration from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 2) = true THEN 'Si'
                WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.in_group_public_administration from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 2) = false THEN 'No' ELSE null END  as varchar),'-') as \"#{I18n.t("organization_exporter.in_group_public_administration_3")}\", 
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.in_group_public_administration from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 2) = true THEN 
                (select represented_entities.text_group_public_administration from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 2)
                ELSE null END  as varchar),'-') as \"#{I18n.t("organization_exporter.text_group_public_administration_3")}\", 
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.subvention from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 2) = true THEN 'Si'
                WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.subvention from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 2) = false THEN 'No' ELSE null END  as varchar),'-') as \"#{I18n.t("organization_exporter.subvention_3")}\",
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.subvention_public_administration from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 2) = true THEN 'Si'
                WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.subvention_public_administration from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 2) = false THEN 'No' ELSE null END  as varchar),'-') as \"#{I18n.t("organization_exporter.subvention_public_administration_3")}\", 
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.subvention_public_administration from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 2) = true THEN 
                (select range_subventions.import from range_subventions,represented_entities where represented_entities.id = (select represented_entities.id from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 2) AND range_subventions.item_type ='RepresentedEntity' AND range_subventions.item_id = represented_entities.id AND represented_entities.organization_id = organizations.id limit 1 offset 0)
                ELSE null END   as varchar),'-') as \"#{I18n.t("organization_exporter.get_range_3_1_import")}\", 
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.subvention_public_administration from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 2) = true THEN 
                (select range_subventions.entity_name from range_subventions,represented_entities where represented_entities.id = (select represented_entities.id from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 2) AND range_subventions.item_type ='RepresentedEntity' AND range_subventions.item_id = represented_entities.id AND represented_entities.organization_id = organizations.id limit 1 offset 0)
                ELSE null END   as varchar),'-') as \"#{I18n.t("organization_exporter.get_range_3_1_entity_name")}\",
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.subvention_public_administration from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 2) = true THEN 
                (select range_subventions.import from range_subventions,represented_entities where represented_entities.id = (select represented_entities.id from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 2) AND range_subventions.item_type ='RepresentedEntity' AND range_subventions.item_id = represented_entities.id AND represented_entities.organization_id = organizations.id limit 1 offset 1)
                ELSE null END   as varchar),'-') as \"#{I18n.t("organization_exporter.get_range_3_2_import")}\", 
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.subvention_public_administration from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 2) = true THEN 
                (select range_subventions.entity_name from range_subventions,represented_entities where represented_entities.id = (select represented_entities.id from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 2) AND range_subventions.item_type ='RepresentedEntity' AND range_subventions.item_id = represented_entities.id AND represented_entities.organization_id = organizations.id limit 1 offset 1)
                ELSE null END   as varchar),'-') as \"#{I18n.t("organization_exporter.get_range_3_2_entity_name")}\",
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.subvention_public_administration from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 2) = true THEN 
                (select range_subventions.import from range_subventions,represented_entities where represented_entities.id = (select represented_entities.id from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 2) AND range_subventions.item_type ='RepresentedEntity' AND range_subventions.item_id = represented_entities.id AND represented_entities.organization_id = organizations.id limit 1 offset 2)
                ELSE null END   as varchar),'-') as \"#{I18n.t("organization_exporter.get_range_3_3_import")}\", 
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.subvention_public_administration from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 2) = true THEN 
                (select range_subventions.entity_name from range_subventions,represented_entities where represented_entities.id = (select represented_entities.id from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 2) AND range_subventions.item_type ='RepresentedEntity' AND range_subventions.item_id = represented_entities.id AND represented_entities.organization_id = organizations.id limit 1 offset 2)
                ELSE null END   as varchar),'-') as \"#{I18n.t("organization_exporter.get_range_3_3_entity_name")}\",
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.subvention_public_administration from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 2) = true THEN 
                (select range_subventions.import from range_subventions,represented_entities where represented_entities.id = (select represented_entities.id from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 2) AND range_subventions.item_type ='RepresentedEntity' AND range_subventions.item_id = represented_entities.id AND represented_entities.organization_id = organizations.id limit 1 offset 3)
                ELSE null END   as varchar),'-') as \"#{I18n.t("organization_exporter.get_range_3_4_import")}\", 
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.subvention_public_administration from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 2) = true THEN 
                (select range_subventions.entity_name from range_subventions,represented_entities where represented_entities.id = (select represented_entities.id from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 2) AND range_subventions.item_type ='RepresentedEntity' AND range_subventions.item_id = represented_entities.id AND represented_entities.organization_id = organizations.id limit 1 offset 3)
                ELSE null END   as varchar),'-') as \"#{I18n.t("organization_exporter.get_range_3_4_entity_name")}\",
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.contract from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 2) = true THEN 'Si'
                WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.contract from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 2) = false THEN 'No' ELSE null END  as varchar),'-') as \"#{I18n.t("organization_exporter.contract_3")}\", 
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.contract from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 2) = true THEN 
                (select represented_entities.contract_turnover from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 2)
                ELSE null END  as varchar),'-') as \"#{I18n.t("organization_exporter.contract_turnover_3")}\", 
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.contract from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 2) = true THEN 
                (select represented_entities.contract_total_budget from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 2)
                ELSE null END  as varchar),'-') as \"#{I18n.t("organization_exporter.contract_total_budget_3")}\", 
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.contract from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 2) = true THEN 
                (select represented_entities.contract_breakdown from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 2)
                ELSE null END  as varchar),'-') as \"#{I18n.t("organization_exporter.contract_breakdown_3")}\", 
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.contract from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 2) = true THEN 
                (select represented_entities.contract_financing from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 2)
                ELSE null END  as varchar),'-') as \"#{I18n.t("organization_exporter.contract_financing_3")}\", 

            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) THEN 
                (select CONCAT(represented_entities.name,' ',represented_entities.first_surname,' ', represented_entities.second_surname) from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 3)
                ELSE null END  as varchar),'-') as \"#{I18n.t("organization_exporter.represented_entity_4")}\", 
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) THEN 
                (select CAST(represented_entities.fiscal_year as varchar) from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 3)
                ELSE null END  as varchar),'-') as \"#{I18n.t("organization_exporter.fiscal_year_4")}\",
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) THEN 
                (select
                    CASE represented_entities.range_fund WHEN 0 THEN 'De 0 a 9.999 euros' 
                    WHEN 1 THEN 'De 10.000 a 24.999 euros' 
                    WHEN 2 THEN 'De 25.000 a 49.000 euros'
                    WHEN 3 THEN 'Más de 50.000 euros'
                    ELSE '-' END 
                    from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 3)
                ELSE null END  as varchar),'-') as \"#{I18n.t("organization_exporter.range_fund_4")}\",
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.in_group_public_administration from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 3) = true THEN 'Si'
                WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.in_group_public_administration from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 3) = false THEN 'No' ELSE null END  as varchar),'-') as \"#{I18n.t("organization_exporter.in_group_public_administration_4")}\", 
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.in_group_public_administration from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 3) = true THEN 
                (select represented_entities.text_group_public_administration from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 3)
                ELSE null END  as varchar),'-') as \"#{I18n.t("organization_exporter.text_group_public_administration_4")}\", 
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.subvention from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 3) = true THEN 'Si'
                WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.subvention from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 3) = false THEN 'No' ELSE null END  as varchar),'-') as \"#{I18n.t("organization_exporter.subvention_4")}\",
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.subvention_public_administration from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 3) = true THEN 'Si'
                WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.subvention_public_administration from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 3) = false THEN 'No' ELSE null END  as varchar),'-') as \"#{I18n.t("organization_exporter.subvention_public_administration_4")}\", 
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.subvention_public_administration from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 3) = true THEN 
                (select range_subventions.import from range_subventions,represented_entities where represented_entities.id = (select represented_entities.id from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 3) AND range_subventions.item_type ='RepresentedEntity' AND range_subventions.item_id = represented_entities.id AND represented_entities.organization_id = organizations.id limit 1 offset 0)
                ELSE null END   as varchar),'-') as \"#{I18n.t("organization_exporter.get_range_4_1_import")}\", 
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.subvention_public_administration from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 3) = true THEN 
                (select range_subventions.entity_name from range_subventions,represented_entities where represented_entities.id = (select represented_entities.id from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 3) AND range_subventions.item_type ='RepresentedEntity' AND range_subventions.item_id = represented_entities.id AND represented_entities.organization_id = organizations.id limit 1 offset 0)
                ELSE null END   as varchar),'-') as \"#{I18n.t("organization_exporter.get_range_4_1_entity_name")}\",
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.subvention_public_administration from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 3) = true THEN 
                (select range_subventions.import from range_subventions,represented_entities where represented_entities.id = (select represented_entities.id from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 3) AND range_subventions.item_type ='RepresentedEntity' AND range_subventions.item_id = represented_entities.id AND represented_entities.organization_id = organizations.id limit 1 offset 1)
                ELSE null END   as varchar),'-') as \"#{I18n.t("organization_exporter.get_range_4_2_import")}\", 
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.subvention_public_administration from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 3) = true THEN 
                (select range_subventions.entity_name from range_subventions,represented_entities where represented_entities.id = (select represented_entities.id from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 3) AND range_subventions.item_type ='RepresentedEntity' AND range_subventions.item_id = represented_entities.id AND represented_entities.organization_id = organizations.id limit 1 offset 1)
                ELSE null END   as varchar),'-') as \"#{I18n.t("organization_exporter.get_range_4_2_entity_name")}\",
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.subvention_public_administration from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 3) = true THEN 
                (select range_subventions.import from range_subventions,represented_entities where represented_entities.id = (select represented_entities.id from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 3) AND range_subventions.item_type ='RepresentedEntity' AND range_subventions.item_id = represented_entities.id AND represented_entities.organization_id = organizations.id limit 1 offset 2)
                ELSE null END   as varchar),'-') as \"#{I18n.t("organization_exporter.get_range_4_3_import")}\", 
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.subvention_public_administration from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 3) = true THEN 
                (select range_subventions.entity_name from range_subventions,represented_entities where represented_entities.id = (select represented_entities.id from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 3) AND range_subventions.item_type ='RepresentedEntity' AND range_subventions.item_id = represented_entities.id AND represented_entities.organization_id = organizations.id limit 1 offset 2)
                ELSE null END   as varchar),'-') as \"#{I18n.t("organization_exporter.get_range_4_3_entity_name")}\",
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.subvention_public_administration from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 3) = true THEN 
                (select range_subventions.import from range_subventions,represented_entities where represented_entities.id = (select represented_entities.id from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 3) AND range_subventions.item_type ='RepresentedEntity' AND range_subventions.item_id = represented_entities.id AND represented_entities.organization_id = organizations.id limit 1 offset 3)
                ELSE null END   as varchar),'-') as \"#{I18n.t("organization_exporter.get_range_4_4_import")}\", 
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.subvention_public_administration from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 3) = true THEN 
                (select range_subventions.entity_name from range_subventions,represented_entities where represented_entities.id = (select represented_entities.id from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 3) AND range_subventions.item_type ='RepresentedEntity' AND range_subventions.item_id = represented_entities.id AND represented_entities.organization_id = organizations.id limit 1 offset 3)
                ELSE null END   as varchar),'-') as \"#{I18n.t("organization_exporter.get_range_4_4_entity_name")}\",
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.contract from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 3) = true THEN 'Si'
                WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.contract from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 3) = false THEN 'No' ELSE null END  as varchar),'-') as \"#{I18n.t("organization_exporter.contract_4")}\", 
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.contract from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 3) = true THEN 
                (select represented_entities.contract_turnover from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 3)
                ELSE null END  as varchar),'-') as \"#{I18n.t("organization_exporter.contract_turnover_4")}\", 
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.contract from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 3) = true THEN 
                (select represented_entities.contract_total_budget from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 3)
                ELSE null END  as varchar),'-') as \"#{I18n.t("organization_exporter.contract_total_budget_4")}\", 
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.contract from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 3) = true THEN 
                (select represented_entities.contract_breakdown from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 3)
                ELSE null END  as varchar),'-') as \"#{I18n.t("organization_exporter.contract_breakdown_4")}\", 
            COALESCE(CAST(CASE WHEN (organizations.foreign_lobby_activity=true) AND 
                (select represented_entities.contract from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 3) = true THEN 
                (select represented_entities.contract_financing from represented_entities where represented_entities.organization_id = organizations.id limit 1 offset 3)
                ELSE null END   as varchar),'-') as \"#{I18n.t("organization_exporter.contract_financing_4")}\",
            #{aux_interest}        
                from organizations 
        #{"WHERE organizations.id IN (#{filter.to_s.gsub(/\[|\]/,'')})" unless filter.blank?}) TO STDOUT (delimiter ';', FORMAT CSV,HEADER );"    

        Admin::StreamingHelper.execute_stream_query(query).each { |x| yield x if block_given? }
    rescue => e
        begin
          Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
    end
end