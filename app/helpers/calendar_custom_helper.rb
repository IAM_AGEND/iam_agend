module CalendarCustomHelper

    def get_week_first_monday
        th_export= ""
        (1..6).each {|day| th_export =  th_export + "<th >#{I18n.t('date.abbr_day_names')[day]}</th>"}
        th_export = th_export + "<th >#{I18n.t('date.abbr_day_names')[0]}</th>"

        th_export.html_safe
    rescue => e
        begin
          Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
        ""
    end   

    def get_days_month_range(current = Time.zone.now, events=nil, params_aux= {})
        body = ""
        start_date = @current_date_calendar.beginning_of_month
        end_date= @current_date_calendar.end_of_month

        return body if (start_date.blank? || end_date.blank?)
        parameters = params_aux.dup

        begin
            parameters[:set_date] = Time.zone.parse(parameters[:set_date]).to_date.strftime("%Y-%m-%d")
        rescue
            parameters[:set_date] = current.strftime("%Y-%m-%d")
        end

        (start_date..end_date).each do |week| 
            if week==start_date ||  week.wday == 1
                body = body + "<tr>"
                if week==start_date
                    if week.wday == 0
                        (1..6).each {|d|  body = body + get_out_day}
                        body = body + get_day(week, events, parameters)
                    else
                        (1..week.wday-1).each {|d|  body = body + get_out_day}
                        body = body + get_day(week, events, parameters)
                    end
                elsif week==end_date
                    body = body + get_day(week, events, parameters)
                    if week.wday == 6
                        body = body + get_out_day
                    elsif week.wday != 0
                        (week.wday+1..7).each {|d|  body = body + get_out_day}
                    end
                    body = body + "</tr>"
                else
                    body = body + get_day(week, events, parameters)
                    body = body + "</tr>" if  week.wday == 0
                end
            else
                if week==end_date
                    body = body + get_day(week, events, parameters)
                    if week.wday == 6
                        body = body + get_out_day
                    elsif week.wday != 0
                        (week.wday+1..7).each {|d|  body = body + get_out_day}
                    end
                    body = body + "</tr>"
                else
                    body = body + get_day(week, events, parameters)
                    body = body + "</tr>" if  week.wday == 0
                end
            end
        end

        body.html_safe
    rescue => e
        begin
          Rails.logger.error("COD-00003: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
        ""
    end

    def set_now_calendar(params_aux = {},  item=nil)
        parameters = params_aux.dup        
        current_path_with_query_params({:tab_calendar => 'today', :set_date => Date.today,:holder => parameters.blank? ? nil : parameters[:holder]})
    rescue => e
        begin
          Rails.logger.error("COD-00004: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
        current_path_with_query_params({})
    end

    def set_tab_calendar(params_aux = {},  item=nil, current=Time.zone.now)
        parameters = params_aux.dup
        return current_path_with_query_params(parameters) if parameters.blank? || item.blank?
        if item.to_s=="all"
            parameters.merge!({:tab_calendar => item,:set_date => nil, :select => {:month =>  current.to_date.month , :year => current.to_date.year, :day => current.to_date.day}}) 
        elsif item == parameters[:tab_calendar]
            parameters.merge!({:tab_calendar => nil,:set_date => parameters[:set_date], :select => {:month =>  current.to_date.month , :year => current.to_date.year, :day => current.to_date.day}}) 
        else
            parameters.merge!({:tab_calendar => item,:set_date => parameters[:set_date], :select => {:month =>  current.to_date.month , :year => current.to_date.year, :day => current.to_date.day}}) 
        end
        current_path_with_query_params(parameters)
    rescue => e
        begin
          Rails.logger.error("COD-0005: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
        current_path_with_query_params({})
    end   

    private

    def get_day(week, events, parameters)
        f_wday = (@current_date_calendar - (@current_date_calendar.wday - 1))
        f_wday = f_wday.month == @current_date_calendar.month ? f_wday.day : 1
        l_wday = (@current_date_calendar + (7 - @current_date_calendar.wday))
        l_wday = l_wday.month == @current_date_calendar.month ? l_wday.day : @current_date_calendar.end_of_month.day
        
        if week==Time.zone.now.to_date
            aux_class ='calendar_event_now'
        elsif ((!parameters[:set_date].blank? && week == parameters[:set_date].to_date) || parameters[:tab_calendar] == 'month' || parameters[:tab_calendar] == 'week') && !events.blank? && !events.where("cast(scheduled as date) = cast(? as date)", week).limit(1).blank?
            aux_class ='calendar_event'
        elsif (!parameters[:set_date].blank? && week == parameters[:set_date].to_date) || parameters[:tab_calendar] == 'month' || (parameters[:tab_calendar] == 'week' && (f_wday..l_wday).include?(week.day))
            aux_class ='calendar_selected'
        else
            aux_class ='calendar_without'
        end

        aux= "<td style='border: 0.1rem solid lightgrey;text-align: center' class='#{aux_class}'>"
        aux = aux + "<a class='day_link calendar_without' href='#{current_path_with_query_params({tab_calendar: parameters[:tab_calendar], set_date: week, holder: parameters[:holder],
            :select => {day: week.day, year: week.year, month: week.month}})}'>#{week.day}</a>"
        aux = aux + "</td>"
    end

    def get_out_day
        "<td class='calendar_out' style='border: 0.1rem solid lightgrey;'></td>"
    end

end