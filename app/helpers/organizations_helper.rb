module OrganizationsHelper

  def status_type
    [[I18n.t("main.form.active"), 1],[I18n.t("main.form.inactive"), 3], [I18n.t("main.form.inavilited"), 2], [I18n.t("main.form.expired"), 4]]
  end

  def types_lobby
    [[I18n.t("main.form.next_expired"),1],[I18n.t("backend.search_active.foreign_lobby_activity"),2],[I18n.t("backend.search_active.own_lobby_activity"),3]]
  end

  def organizations_index_subtitle
    if params[:order] == '1' || params[:order] == '2' || params[:order] == '3'
      t "organizations.results_title"
    else
      t "organizations.subtitle.default"
    end
  rescue => e
    begin
      Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def organization_represented_entities_url_pattern(format)
    url = organization_represented_entities_url(organization_id: 999, format: format, protocol: protocol_for_urls)
    url.gsub('999', 'organization_id')
  rescue => e
    begin
      Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def organization_agents_url_pattern(format)
    url = organization_agents_url(organization_id: 999, format: format, protocol: protocol_for_urls)
    url.gsub('999', 'organization_id')
  rescue => e
    begin
      Rails.logger.error("COD-00003: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def organization_category_url_pattern
    url = organization_url(id: 999, format: :json, protocol: protocol_for_urls)
    url.gsub('999', 'organization_id')
  rescue => e
    begin
      Rails.logger.error("COD-00004: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def protocol_for_urls
    if Rails.env.development? || Rails.env.test?
      :http
    else
      :https
    end
  rescue => e
    begin
      Rails.logger.error("COD-00005: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def search_by_filter?
    (params[:interests].present? || params[:category].present? || params[:keyword].present?)
  rescue => e
    begin
      Rails.logger.error("COD-00006: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def organization_status(organization)
    if !organization.canceled_at.blank? && organization.canceled_at <= Time.zone.now
      '<span class="label alert">Baja </span>'.html_safe
    elsif !organization.set_expired_date.blank? && organization.set_expired_date < Time.zone.now
      '<span class="label alert">Caducado </span>'.html_safe
    elsif organization.canceled?
      '<span class="label alert">Baja </span>'.html_safe
    elsif organization.invalidated?
      if current_user.present? && current_user.admin?
        '<span class="label warning">Inhabilitado</span>'.html_safe
      else
        '<span class="label alert">Baja </span>'.html_safe
      end
    else
      '<span class="label success">Activo</span>'.html_safe
    end
  rescue => e
    begin
      Rails.logger.error("COD-00007: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def organization_expired?(organization)
    !(!organization.canceled_at.blank? && organization.canceled_at <= Time.zone.now) && (!organization.organization_status.blank? && organization.organization_status.code=='expired'  ||  !organization.set_expired_date.blank? && organization.set_expired_date < Time.zone.now)
  rescue 
    false
  end

  def organization_list_status(organization)
    if  !organization.organization_status.blank? && organization.organization_status.code=='inactive'  || !organization.canceled_at.blank? && organization.canceled_at <= Time.zone.now
      '<span class="label alert">Baja </span>'.html_safe
    elsif  !organization.organization_status.blank? && organization.organization_status.code=='expired'  || !organization.set_expired_date.blank? && organization.set_expired_date < Time.zone.now
      '<span class="label alert">Caducado </span>'.html_safe
    elsif !organization.organization_status.blank? && ['inactive', 'invalidated'].include?(organization.organization_status.code) || organization.canceled? || organization.invalidated?
      '<span class="label alert">Baja </span>'.html_safe
    end
  rescue => e
    begin
      Rails.logger.error("COD-00008: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def events_as_lobby_by(organization)
    organization.events.where(lobby_activity: true, status: 2).count
  rescue => e
    begin
      Rails.logger.error("COD-00009: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def organization_back_button
    if current_user.present? && !params[:admin].blank? && params[:admin].to_s == "true"
      admin_organizations_path
    else
      organizations_path
    end
  rescue => e
    begin
      Rails.logger.error("COD-00010: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def link_to_organizations_sorted_by(column)
    direction = set_direction(params[:direction])
    icon = set_sorting_icon(direction, column)

    translation = t("backend.#{column}")

    link_to(
      "#{translation} <span class='icon-sortable #{icon}'></span>".html_safe,
      admin_organizations_path(sort_by: column, direction: direction, params: params.except(:direction, :sort_by))
    )
  rescue => e
    begin
      Rails.logger.error("COD-00011: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def set_sorting_icon(direction, sort_by)
    if sort_by.to_s == params[:sort_by]
      if direction == "desc"
        "desc"
      else
        "asc"
      end
    else
      ""
    end
  rescue => e
    begin
      Rails.logger.error("COD-00012: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def set_direction(current_direction)
    current_direction == "desc" ? "asc" : "desc"
  rescue => e
    begin
      Rails.logger.error("COD-00013: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

end
