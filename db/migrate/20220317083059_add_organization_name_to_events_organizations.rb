class AddOrganizationNameToEventsOrganizations < ActiveRecord::Migration
  def change
    add_column :events_organizations, :organization_name, :string
  end
end
