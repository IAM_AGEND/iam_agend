class AddColumnStatusOrganization < ActiveRecord::Migration
  def change
    add_column :organizations, :status, :string
    add_column :organizations, :status_at, :datetime
  end
end
