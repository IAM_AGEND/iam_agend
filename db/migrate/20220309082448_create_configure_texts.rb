class CreateConfigureTexts < ActiveRecord::Migration
  def change
    create_table :configure_texts do |t|
      t.text :content_body
      t.text :content_title
      t.string :code 
      t.timestamps null: true
    end
  end
end
