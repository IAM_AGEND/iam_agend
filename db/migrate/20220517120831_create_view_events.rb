class CreateViewEvents < ActiveRecord::Migration
  def change


    execute "
    CREATE OR REPLACE view visitors (\"id\",\"slug\",\"title\",\"description\",\"scheduled\",\"published_at\",\"lobby_activity\",\"holder_id\",\"position_title\",
      \"area_id\",\"area_title\",\"holder_name\") AS
      SELECT events.id as \"id\", 
events.slug as \"slug\", 
events.title as \"title\", 
events.description as \"description\",
events.scheduled as \"scheduled\", 
events.published_at  as \"published_at\", 
events.lobby_activity as \"lobby_activity\",
positions.holder_id as \"holder_id\",
positions.title as \"position_title\",
positions.area_id  as \"area_id\",
areas.title as \"area_title\",
UPPER(CONCAT(holders.first_name, ' ', holders.last_name)) as \"holder_name\"
from events, positions, holders, areas  
where positions.id = events.position_id and areas.id=positions.area_id and positions.holder_id = holders.id and events.status in (1,2) order by events.scheduled desc;"
end
  end
end
