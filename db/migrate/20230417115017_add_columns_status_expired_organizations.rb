class AddColumnsStatusExpiredOrganizations < ActiveRecord::Migration
  def up
    add_reference :organizations, :organization_status, index: true, foreign_key: true
    add_column :organizations, :expired_date, :datetime
  end

  def down
    remove_reference :organizations, :organization_status
    remove_column :organizations, :expired_date
  end
end
