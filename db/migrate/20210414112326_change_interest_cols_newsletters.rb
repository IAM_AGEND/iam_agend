class ChangeInterestColsNewsletters < ActiveRecord::Migration
  def change
    remove_column :newsletters, :interest_ids
    add_column :newsletters, :interest_ids, :string
  end
end
