class AddColumnsSearch < ActiveRecord::Migration
  def change
    add_column :events, :holder_id, :integer
    add_column :events, :holder_name, :string
    add_column :events, :participants_ids, :string
    add_column :events, :participants_name, :string
    add_column :events, :attendees_name, :string
    add_column :events, :area_title, :string
    add_column :events, :area_id, :integer
    add_column :events, :position_title, :string
  end
end
