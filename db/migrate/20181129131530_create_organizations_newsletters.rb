class CreateOrganizationsNewsletters < ActiveRecord::Migration
  def change
    create_table :organizations_newsletters do |t|
      t.references :organization, index: true
      t.references :newsletter, index: true
      t.string :email_send
      t.timestamps null: false
    end
  end
end
