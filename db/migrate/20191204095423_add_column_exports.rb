class AddColumnExports < ActiveRecord::Migration
  def change
    add_column :exports, :fields, :jsonb, null: false, default: '{}'
    add_index  :exports, :fields, using: :gin
    add_column :exports, :filter_fields, :jsonb, null: false, default: '{}'
    add_index  :exports, :filter_fields, using: :gin
  end
end
