class DeleteColumnInnecesary < ActiveRecord::Migration
  def change
    remove_column :addresses, :phones_alt
    remove_column :legal_representants, :business_name
    remove_column :notification_effects, :business_name
    rename_column :organizations, :address, :old_address
  end
end
