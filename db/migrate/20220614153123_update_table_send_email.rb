class UpdateTableSendEmail < ActiveRecord::Migration
  def change
    add_column :send_emails, :updated_scheduled, :datetime
    add_column :send_emails, :is_auto_excluded_emails, :boolean, default: false
    add_column :send_emails, :is_manual_excluded_emails, :boolean, default: false
    add_column :send_emails, :is_auto_all_emails, :boolean, default: false
    add_column :send_emails, :is_manual_all_emails, :boolean, default: false
    add_column :send_emails, :is_auto_included_emails, :boolean, default: false
    add_column :send_emails, :is_manual_included_emails, :boolean, default: false
    add_column :send_emails, :auto_included_emails, :text
    add_column :send_emails, :manual_included_emails, :text
    add_column :send_emails, :auto_start_date, :date
    add_column :send_emails, :manual_date, :date
    add_column :send_emails, :auto_from_date, :date
    add_column :send_emails, :auto_to_date, :date
  end
end
