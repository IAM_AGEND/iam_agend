class AddCodeToInterests < ActiveRecord::Migration
  def change
    add_column :interests, :code, :integer
  end
end
