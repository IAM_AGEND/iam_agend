class CreateJoinTableNewslettersInterests < ActiveRecord::Migration
  def change
    create_table :newsletters_interests do |t|
      t.references :newsletter, index: true, foreign_key: true
      t.references :interest, index: true, foreign_key: true

      t.timestamps null: false
    end

    remove_index  :newsletters, :interest_ids
    remove_column :newsletters, :interest_ids, :jsonb, null: false, default: '{}'
  end
end
