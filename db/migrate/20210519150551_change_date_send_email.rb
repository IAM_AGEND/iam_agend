class ChangeDateSendEmail < ActiveRecord::Migration
  def change
    remove_column :send_emails, :auto_date
    add_column :send_emails, :auto_date, :integer
  end
end
