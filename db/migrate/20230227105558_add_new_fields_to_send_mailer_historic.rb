class AddNewFieldsToSendMailerHistoric < ActiveRecord::Migration
  def change
      add_column :send_mailer_historics, :type_data, :string
      add_reference :send_mailer_historics, :organization, index: true
      add_column :send_mailer_historics, :sent_to, :string
      add_column :send_mailer_historics, :sent, :boolean, default: true
  end
end
