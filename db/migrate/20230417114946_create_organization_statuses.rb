class CreateOrganizationStatuses < ActiveRecord::Migration
  def change
    create_table :organization_statuses do |t|
      t.text :name, null: false, index: {unique: true}
      t.text :code, null: false, index: {unique: true}
      t.timestamps
    end
  end
end
