class ChangeNameColsSendEmail < ActiveRecord::Migration
  def change
    rename_column :send_emails, :manua_user_type, :manual_user_type
    rename_column :send_emails, :manua_excluded_emails, :manual_excluded_emails
  end
end
