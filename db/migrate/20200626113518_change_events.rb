class ChangeEvents < ActiveRecord::Migration
  def change
    add_column :events, :old_event_lobby, :boolean, :default => true
  end
end
