class ChangeOrganizations < ActiveRecord::Migration
  def change
    add_reference :organizations, :address, :index => true, foreign_key: true
    add_column :organizations, :renovation_date, :datetime
    add_column :organizations, :in_group_public_administration, :boolean, :default => false
    add_column :organizations, :text_group_public_administration, :text
    add_column :organizations, :subvention_public_administration, :boolean, :default => false
    add_column :organizations, :contract_turnover, :text
    add_column :organizations, :contract_total_budget, :text
    add_column :organizations, :contract_breakdown, :text
    add_column :organizations, :contract_financing, :text
    add_column :organizations, :public_term, :boolean, :default => false
    add_column :organizations, :communication_term, :boolean, :default => false
    add_column :organizations, :other_term, :boolean, :default => false
  end
end
