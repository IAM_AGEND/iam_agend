class ChangeDefaultsRepresentedEntities < ActiveRecord::Migration
  def change
    change_column_default :represented_entities, :in_group_public_administration, nil
    change_column_default :represented_entities, :subvention_public_administration, nil
  end
end
