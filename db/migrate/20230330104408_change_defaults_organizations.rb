class ChangeDefaultsOrganizations < ActiveRecord::Migration
  def change
    change_column_default :organizations, :in_group_public_administration, nil
    change_column_default :organizations, :subvention_public_administration, nil
  end
end
