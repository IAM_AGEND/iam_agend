class AddColumnPersonalCode < ActiveRecord::Migration
  def up
    add_column :holders, :personal_code, :string
    add_column :users, :personal_code, :string
  end

  def down 
    remove_column :holders, :personal_code
    remove_column :users, :personal_code
  end
end
