class ChangeCategories < ActiveRecord::Migration
  def change
    add_reference :categories, :category
    add_column :categories, :new_category, :boolean, :default => false
  end
end
