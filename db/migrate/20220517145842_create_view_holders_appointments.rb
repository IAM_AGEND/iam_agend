class CreateViewHoldersAppointments < ActiveRecord::Migration
  def change
    execute "CREATE OR REPLACE view holders_appointments (\"id\",\"first_name\",\"last_name\",\"start\",\"to\") AS
    select h.id,h.first_name ,h.last_name ,p.start,p.to  from positions p, holders h 
    where h.id=p.holder_id and p.id  in 
    (select p1.id from positions p1 where p1.holder_id =h.id order by p.start desc, p.updated_at desc limit 1);"
  end
end
