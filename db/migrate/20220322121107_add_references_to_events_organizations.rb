class AddReferencesToEventsOrganizations < ActiveRecord::Migration
  def change
    add_reference :event_represented_entities, :events_organization, index: true
    add_reference :event_agents, :events_organization,  index: true
  end
end