class AddManageEmailsTable < ActiveRecord::Migration
  def change
    create_table :manage_emails do |t|
      t.string :type
      t.string :sender
      t.string :fields_cc
      t.string :fields_cco
      t.text :subject
      t.timestamps
    end
  end
end
