class UpdateColumnsCcSndCco < ActiveRecord::Migration
  def change
    change_column :manage_emails, :fields_cc, :string, :null => true
    change_column :manage_emails, :fields_cco, :string, :null => true
  end
end
