class ChangeInterestColNewsletters < ActiveRecord::Migration
  def change
    add_column :newsletters, :interest_ids, :text, default: []
  end
end
