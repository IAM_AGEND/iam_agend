class CreateSendMailerHistoric < ActiveRecord::Migration
  def change
    create_table :send_mailer_historics do |t|
      t.text :description
      t.text :log_url
      t.timestamps
    end
  end
end
