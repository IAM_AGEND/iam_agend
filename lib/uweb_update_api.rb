class UwebUpdateApi < MadridApi

  def client
    @client = Savon.client(wsdl: Rails.application.secrets.uweb_update_api_endpoint, encoding: 'ISO-8859-1')
  end

  def request(params)
    h=Hash.new
    h[:appKey] = Rails.application.secrets.uweb_api_app_key
    params.each do |k,v|
      h[k] = v
    end
    {request: h}
  end

  def request_update(params)
    h=Hash.new
    h[:keyApp] = Rails.application.secrets.uweb_api_app_key
    h[:keyWService] = Rails.application.secrets.uweb_api_key_ws
    h[:pwdWservice] = Rails.application.secrets.uweb_api_pwd_ws
    params.each do |k,v|
      h[k] = v
    end
    {request: h}
  end

  def get_service_info
    data = client.call(:get_services_info).body
    puts "=" *30
    puts data
    puts "=" *30
    data
  rescue => e
    {}
  end


  def remove_profile_user(keyProfileConnect, keyUserConnect, keyUserRelation)
    data = data_update(:remove_profile_user, {keyProfileConnect: keyProfileConnect, keyUserConnect: keyUserConnect, keyProfileRelation: keyProfileConnect, keyUserRelation: keyUserRelation})
    data = data.encode('ISO-8859-1')
    puts "=" *30
    puts data
    puts "=" *30
    Hash.from_xml(data)
  rescue => e
    {}
  end

  def remove_profile_login(keyProfileConnect, keyUserConnect, keyProfileRelation, loginUserRelation)
    data = data_update(:remove_profile_login, {keyProfileConnect: keyProfileConnect, keyUserConnect: keyUserConnect, keyProfileRelation: keyProfileConnect, loginUserRelation: loginUserRelation})
    data = data.encode('ISO-8859-1')
    Hash.from_xml(data)
  rescue => e
    {}
  end

  def insert_profile_login(keyProfileConnect, keyUserConnect, loginUserRelation)
    data = data_update(:insert_profile_login, {keyProfileConnect: keyProfileConnect, keyUserConnect: keyUserConnect, keyProfileRelation: keyProfileConnect, loginUserRelation: loginUserRelation})
    data = data.encode('ISO-8859-1')
    Hash.from_xml(data)
  rescue => e
    {}
  end

  def insert_profile_user(keyProfileConnect, keyUserConnect, keyUserRelation)
    data = data_update(:insert_profile_user, {keyProfileConnect: keyProfileConnect, keyUserConnect: keyUserConnect, keyProfileRelation: keyProfileConnect, keyUserRelation: keyUserRelation})
    data = data.encode('ISO-8859-1')
    Hash.from_xml(data)
  rescue => e
    {}
  end


end
