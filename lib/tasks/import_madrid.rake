namespace :import_madrid do

    desc "Mostrar proceso de importación y sincronización de cargos"
    task :show_data => :environment do
        imported
    end

    desc "Ejecutar proceso de importación y sincronización de cargos"
    task :import => :environment do
        imported(true)
    end

    private

    def load_api
        @uweb_api = UwebApi.new
        @directory_api = DirectoryApi.new
    end

    def user_create(type, mc, update=false)
        user = User.create_from_uweb(type,@uweb_api.get_user(mc['CLAVE_IND']))
        if update
            if type.to_s != 'user' || type.to_s == 'user' && !@admins.include?(user.id)
                if user.save
                    @admins.push(user.id) if type.to_s == 'admin'
                    puts "Se ha generado/actualizado el #{type} id - #{user.id}"
                else
                    puts "ERROR: No se ha actualizado el #{type} id - #{user.id}"
                    puts user.errors.full_messages
                end
            end
        end
    rescue => e
        puts e.message            
        puts e.backtrace.inspect
    end

    def create_tree_area(data) 
        data_directory = @directory_api.get_personal_data(data['NUM_PERSONAL'])
        return if data_directory.blank?  || data_directory['ID_UNIDAD_FUNCIONAL'].blank?
        @unit = @directory_api.get_unit(data_directory['ID_UNIDAD_FUNCIONAL'])
        
        @directory_api.create_tree( @unit) if !data_directory.blank? && !data_directory['ID_UNIDAD_FUNCIONAL'].blank?
       
        @unit
    rescue => e
        puts e.message            
        puts e.backtrace.inspect
    end

    def down_positions(holder=nil, update=false)
        return if holder.blank?
        if update
            holder.positions.where(to: nil).each do |pos| 
                pos.to = Time.zone.now.to_date
                if pos.save
                    puts "001: down - Se ha actualizado el cargo del titular con id - #{pos.id}"
                else
                    puts "ERROR: No se ha actualizado el cargo del titular con id - #{pos.id}"
                    puts pos.errors.full_messages
                end
            end
        end
    rescue => e
        puts e.message            
        puts e.backtrace.inspect
    end

    def imported(update = false)
        load_api
        create_users(update)
        create_holders(update)
        update_old_holders(update)
        delete_old_users(update)
    end

    def create_users(update)
        @admins = []
        uweb_get_users('uweb_api_admins_key').each {|mc| user_create('admin', mc, update) }    
        uweb_get_users('uweb_api_users_key').each {|mc|  user_create('user', mc, update) }
    rescue => e
        puts e.message            
        puts e.backtrace.inspect
    end

    def create_holders(update)        
        uweb_get_users('uweb_api_holders_key').each do |mc|  
            begin
                data = @uweb_api.get_user(mc['CLAVE_IND'])
            rescue => e
                puts "ERROR: No se han cargado los datos del titular:"
                puts e.message            
                puts e.backtrace.inspect
                data= nil
            end

            if !data.blank?
                holder = Holder.create_from_uweb(data)
                unit = create_tree_area(data)

                if !holder.blank? && update
                    if holder.save(validate: false)
                        puts "Se ha generado/actualizado el titular id - #{holder.id}"
                    else
                        puts "ERROR: No se ha actualizado el titular id - #{holder.id}"
                        puts holder.errors.full_messages
                    end
                end

                if !unit.blank? && !holder.blank?
                    positions_exist = holder.positions.where(title: data['CARGO'].try{|x| x.strip}, area: Area.find_by(active: 1, internal_id: unit['ID_UNIDAD'].try{|x| x.strip}))
                
                    if positions_exist.count >0
                        down_positions(holder, update)
                    
                        positions_exist.each do |pos|
                            pos.start = Time.zone.parse(unit['FCH_ACTUALIZ'].try{|x| x.strip}) if  pos.start.blank?                 
                            pos.to = pos.title.blank? || data['BAJA_LOGICA'].try{|x| x.strip}.to_i == 1 ? Time.zone.now.to_date : nil
                            pos.title = data['CARGO'].try{|x| x.strip}
                            pos.area = Area.find_by(internal_id: unit['ID_UNIDAD'].try{|x| x.strip}, active: 1) 
                            if update
                                if pos.save(validate: false)
                                    puts "002: Se ha actualizado el cargo del titular con id - #{pos.id}"
                                else
                                    puts "ERROR: No se ha actualizado el cargo del titular con id - #{pos.id}"
                                    puts pos.errors.full_messages
                                end
                            end
                        end
                    else
                        down_positions(holder, update)

                        position = Position.new
                        position.title = data['CARGO'].try{|x| x.strip}
                        position.area = Area.find_by(internal_id: unit['ID_UNIDAD'].try{|x| x.strip}, active: 1)
                        position.start = Time.zone.parse(unit['FCH_ACTUALIZ'].try{|x| x.strip})                    
                        position.to = position.title.blank? || data['BAJA_LOGICA'].try{|x| x.strip}.to_i == 1 ? Time.zone.now.to_date : nil
                        
                        position.holder = holder
                        if update
                            if position.save(validate: false)
                                puts "003: Se ha generado el cargo del titular con id - #{position.id}"
                            else
                                puts "ERROR: No se ha generado el cargo del titular  id - #{holder.id}"
                                puts position.errors.full_messages
                            end
                        end
                    end
                end   

                clean_pos(holder)
            end     
        end
    rescue => e
        puts e.message            
        puts e.backtrace.inspect
    end

    def update_old_holders(update)
        Holder.all.each do |holder|
          begin
            data = @uweb_api.get_user(holder.user_key)
            if data["BAJA_LOGICA"].to_i == 1 || !uweb_get_users('uweb_api_holders_key').map{|mc| mc['CLAVE_IND']}.include?(holder.user_key)
                down_positions(holder, update)
            end
          rescue => e
            puts "-------------------ERROR---------------------"
            puts "Holder with uweb_key #{holder.user_key} exception"
            puts e.message            # Test de excepción
            puts "---------------------------------------------"
            down_positions(holder, update)
          end
        end
    end

    def delete_old_users(update)
        User.where("user_key is not null").find_each do |user|
            begin
              data = @uweb_api.get_user(user.user_key)
              if update
                user.active = data['BAJA_LOGICA'].try{|x| x.strip}.to_i == 0 || uweb_get_users('uweb_api_users_key').map{|mc| mc['CLAVE_IND']}.include?(user.user_key) ? 1 : 0
                user.save
              end
            rescue => e
              puts "-------------------ERROR---------------------"
              puts "El usuario no existe: uweb_key #{user.user_key}"
              puts e.message            # Test de excepción
              puts "---------------------------------------------"
              if update
                user.active = 0
                user.save
              end
            end
        end
    rescue => e
        puts e.message            
        puts e.backtrace.inspect
    end

    def clean_pos(holder)
        puts  "="*30
      
        valid_pos = holder.positions.where('"to" IS NULL').last
        if !valid_pos.blank?
            holder.positions.where('"to" IS NULL').each do |pos2|
                if pos2.id != valid_pos.id
                    if valid_pos.id.to_i != pos2.id.to_i && valid_pos.title.to_s == pos2.title.to_s && valid_pos.area_id == pos2.area_id
                        begin
                            Event.where(position_id: pos2.id).each do |event|
                                event.position_id = valid_pos.id
                                if event.save(validate: false)
                                    puts "-"*10
                                    puts "Evento del titular: #{pos2.id} modificado al: #{valid_pos.id}"
                                    puts "-"*10
                                else
                                    puts "-"*10
                                    puts pos2.id
                                    puts event.errors.full_messages
                                    puts "-"*10
                                end
                            end
                            Participant.where(position_id: pos2.id).each do |participant|
                                participant.position_id = valid_pos.id
                                if participant.save(validate: false)
                                    puts "-"*10
                                    puts "Participación del titular: #{pos2.id} modificado al: #{valid_pos.id}"
                                    puts "-"*10
                                else
                                    puts "-"*10
                                    puts pos2.id
                                    puts participant.errors.full_messages
                                    puts "-"*10
                                end
                            end
                            puts "Cargo eliminado: #{pos2.id}"
                            pos2.destroy
                        rescue => e
                        end
                    end
                end
            end
        end
        puts  "="*30
    rescue => e
        puts e.message            
        puts e.backtrace.inspect
    end

end