namespace :users do

    task :add => :environment do
        admin_user = User.create(password: '12345678', password_confirmation: "12345678", email: 'admin@agendas.dev', first_name: 'Admin', last_name: 'Istrator', active: 1)
admin_user.admin!
    end

    task :destroy => :environment do
        admin_user = User.find_by( email: 'admin@agendas.dev', first_name: 'Admin', last_name: 'Istrator')
        admin_user.destroy
    end


    desc "Actualizar datos desde directorio"
    task :update => :environment do
        uweb = UwebApi.new
        User.all.each do |user|
            if !user.user_key.blank?
                data = uweb.get_user(user.user_key)
                if !data.blank?
                    user.positions = data['CARGO']
                    if user.save!
                        puts "="*20
                        puts "Cargo actualizado para el usuario #{user.id}."
                        puts "="*20
                    else
                        puts "="*20
                        puts user.errors.full_messages
                        puts "="*20
                    end
                end
            end
        end
    end

    desc "Actualizar datos desde directorio"
    task :create_deleted_users => :environment do
        begin
            u1 = User.new(email: "elena.carpintero@jcdecaux.com", sign_in_count: 0, password:"7572f458366748cf", password_confirmation:"7572f458366748cf", role: "lobby",
                first_name: "ELENA", last_name: "CARPINTERO", active: 1, phones: "", organization_id: 2381, frist_init: true, changed_password: true,  second_last_name: "")
            u1.save
            u2 = User.new(email: "jblardony@hosteleriamadrid.com", sign_in_count: 0, role: "lobby", password:"db4ce9bf24be8228", password_confirmation:"db4ce9bf24be8228",
                first_name: "JUAN JOSÉ", last_name: "BLARDONY", active: 1, phones: "913600909;639341837",organization_id: 2616,frist_init: true,changed_password: true,second_last_name: "ARRANZ")
            u2.save
            u3 = User.new(email: "gonzalo.caro@hogarsi.org",sign_in_count: 0,role: "lobby", password:"b7bcde5f40885d7d", password_confirmation:"b7bcde5f40885d7d",
                first_name: "LUIS",last_name: "PEREA",active: 1,phones: "661870070",organization_id: 2411,frist_init: true,changed_password: true,second_last_name: "MARCOS")
            u3.save
        rescue
        end
    end

end