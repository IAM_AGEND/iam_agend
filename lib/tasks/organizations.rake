namespace :organizations do

  desc "Cambia el estado cada día"
  task :change_status => :environment do
    # data_days = DataPreference.find_by(title: "alert_first")
    # data_days = data_days.blank? ? 60 : data_days.content_data.to_i
    Organization.all.find_each do |org|
      status = org.status_type_code
      status_org = org.organization_status.try(:code)
      puts status
      puts status_org
      puts "Actualizado #{org.id}: #{status_org == status}"
      puts "="*50
      # if !org.canceled_at.blank? && org.canceled_at <= Time.zone.now
      #   org.status = 'inactive'
      #   I18n.t('backend.status_type.inactive')
      # elsif !org.set_expired_date.blank? && org.set_expired_date < Time.zone.now
      #   I18n.t('backend.status_type.expired')
      #   org.status = 'expired'
      # elsif ((org.invalidated_at.blank? || Date.today.to_date < org.invalidated_at.to_date) && ((org.canceled_at.blank? || Date.today.to_date < org.canceled_at.to_date) || org.entity_type_id.to_i!=2)) &&
      #  (!org.set_expired_date.blank? && Time.zone.now < org.set_expired_date  && org.set_expired_date <= Time.zone.now+2.months)
      #   I18n.t('backend.status_type.active')
      #   org.status = 'active'
      # elsif (org.invalidated_at.blank? || Date.today.to_date < org.invalidated_at.to_date) && ((org.canceled_at.blank? || Date.today.to_date < org.canceled_at.to_date) || org.entity_type_id.to_i!=2)
      #   I18n.t('backend.status_type.active')
      #   org.status = 'active'
      # elsif (!org.invalidated_at.blank? && Date.today.to_date >= org.invalidated_at.to_date ) && ((org.canceled_at.blank? || Date.today.to_date < org.canceled_at.to_date) || org.entity_type_id.to_i!=2)  
      #   I18n.t('backend.status_type.invalidated')
      #   org.status = 'invalidated'
      # else
      #   I18n.t('backend.status_type.inactive')
      #   org.status = 'inactive'
      # end
      # org.save(validate: false) 

      # puts org.status_type
    end
  end

  desc "Add new categories to database if they do not exists"
  task :add_categories => :environment do
    names = ['Consultoría profesional y despachos de abogados', 'Empresas', 'Asociaciones', 'Fundaciones',
             'Sindicatos y organizaciones profesionales', 'Organizaciones empresariales',
             'ONGs y plataformas sin personalidad jurídica',
             'Universidades y centros de investigación',
             'Corporaciones de Derecho Público (colegios profesionales, cámaras oficiales, etc.)',
             'Iglesia y otras confesiones', 'Otro tipo de sujetos']

    names.each_with_index do |name,index|
      Category.find_or_create_by(name: name, code: index+1, display: true)
    end
  end

  desc "Add new interests to database if they do not exists"
  task :add_interests => :environment do
    interests = ['Actividad económica y empresarial',
                 'Distritos',
                 'Administración de personal y recursos humanos',
                 'Administración electrónica',
                 'Administración económica, financiera y tributaria de la Ciudad',
                 'Atención a la ciudadanía',
                 'Comercio',
                 'Consumo',
                 'Cultura (bibliotecas, archivos, museos, patrimonio histórico artístico, etc.)',
                 'Deportes',
                 'Desarrollo tecnológico',
                 'Educación y Juventud',
                 'Emergencias y seguridad',
                 'Empleo',
                 'Medio Ambiente',
                 'Medios de comunicación',
                 'Movilidad, transporte y aparcamientos',
                 'Salud',
                 'Servicios sociales',
                 'Transparencia y participación ciudadana',
                 'Turismo',
                 'Urbanismo',
                 'Vivienda']

    interests.each_with_index do |name,index|
      Interest.find_or_create_by(name: name, code: index+1)
    end
  end

  desc "Add new registered_lobbies to database if they do not exists"
  task :add_registered_lobbies => :environment do
    registered_lobbies = ['no_record',
                          'generalitat_catalunya',
                          'cnmc',
                          'europe_union',
                          'others']


    registered_lobbies.each_with_index do |name,index|
      RegisteredLobby.find_or_create_by(name: name, code: index+1)
    end
  end

  desc "Update registered_lobbies names"
  task :update_registered_lobbies_names => :environment do
    registered_lobbies = ['no_record',
                          'generalitat_catalunya',
                          'cnmc',
                          'europe_union',
                          'others']

    real_names         = ['Ninguno',
                          'Generalidad catalunya',
                          'CNMC',
                          'Unión Europea',
                          'Otro']

    registered_lobbies.each_with_index do |name, index|
      registered_lobby = RegisteredLobby.find_by(name: name)
      registered_lobby.update(name: real_names[index]) if registered_lobby.present?
    end
  end

  desc "Actualizar los campos subvention_public_administration e in_group_public_administration de los lobbies inscritos previamente al 20-03-2021"
  task :update_organizations_fields_to_nil => :environment do
    date_year = DataPreference.find_by(title: "expired_year")
    date_year= date_year.blank? ? 2 : date_year.content_data
    data_days = DataPreference.find_by(title: "alert_first")
    data_days = data_days.blank? ? 60 : data_days.content_data.to_i
    reference = Date.parse("2020-07-01")
    time = Time.zone.now - date_year.to_i.year
    Organization.lobbies.where("inscription_date::date < '2021-03-20'::date and 
      subvention_public_administration = false and in_group_public_administration = false and
      (((canceled_at is not null AND canceled_at <= NOW()) ) or 
      (canceled_at is null and invalidated_at is null and ((renovation_date is null and (inscription_date is null or inscription_date < '#{time}')) or renovation_date < '#{time}')))").each do |organization|
      organization.in_group_public_administration = nil
      organization.subvention_public_administration = nil
      organization.save(validate: false)
      p "Actualizada organización con id #{organization.id}"
      organization.represented_entities.each do |represented_entity|
        represented_entity.in_group_public_administration = nil
        represented_entity.subvention_public_administration = nil
        represented_entity.save(validate: false)
        p "Actualizada entidad representante con id #{represented_entity.id} de la organización con id #{organization.id}"
      end
    end
  end
    
end
