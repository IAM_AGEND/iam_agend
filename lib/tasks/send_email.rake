require "whenever"
namespace :send_email do
    include Rails.application.routes.url_helpers

    desc "Update crontab"
    task whenever: :environment do 
        if Rails.env.production?
            `whenever --update-crontab`
        end
    end

    desc "Send LobbyOneMonthToExpiration email"
    task expiration_warning: :environment do 
        date_year = DataPreference.find_by(title: "expired_year")
        date_year= date_year.blank? ? 2 : date_year.content_data
        reference = Date.parse("2020-07-01")
        
        Organization.lobbies.where("((invalidated_at is null or NOW() < invalidated_at) AND (canceled_at is null or NOW() < canceled_at) and (CASE WHEN organizations.renovation_date is not null THEN cast(organizations.renovation_date + interval '#{date_year} year' as date)
        WHEN organizations.inscription_date is not null AND organizations.inscription_date > cast('#{reference}' as date) THEN cast(organizations.inscription_date + interval '#{date_year} year' as date) 
        ELSE cast('#{reference}' as date) END) > NOW())").each do |organization|
            expiration_date = organization.set_expired_date.try(:to_date)
            if !expiration_date.blank? && send_email_expiration_one_month?(expiration_date)
                send_mail_expiration(organization, "LobbyOneMonthToExpiration", expiration_date)
            end
        end
    end

    desc "Send LobbyExpiresToday email"
    task expires_today: :environment do 
        date_year = DataPreference.find_by(title: "expired_year")
        date_year= date_year.blank? ? 2 : date_year.content_data
        reference = Date.parse("2020-07-01")
        time = Time.zone.now - date_year.to_i.year
        Organization.lobbies.where("(canceled_at is null and invalidated_at is null and ((renovation_date is null and (inscription_date is null or inscription_date::date = '#{(time).strftime("%F")}')) or renovation_date::date = '#{(time).strftime("%F")}'))").each do |organization|
            send_mail_expiration(organization, "LobbyExpiresToday", organization.set_expired_date)
        end
    end

    desc "Auto send email events"
    task auto: :environment do
        settings = SendEmail.first
        if !settings.auto_start_date.blank? && check_date_equal(settings)
            if settings.auto_send.to_s == "true"
                smh = SendMailerHistoric.new
                case settings.auto_user_type
                when 1
                    send_target = "Titular"
                when 2 
                    send_target = "Gestores"
                when 3
                    send_target = "Titulares y gestores"
                else
                    send_target = ""
                end

                case settings.auto_frecuence
                when 1 
                    frecuence_text = "de forma mensual"
                when 2
                    frecuence_text = "de forma bimensual"
                when 3
                    frecuence_text = "de forma trimestral"
                when 4
                    frecuence_text = "de forma semestral"
                when 5 
                    frecuence_text = "de forma anual"
                end
                
                smh.description = "Automático, desde #{settings.next_send_events_from_date(settings.auto_from_date).try(:to_date)} hasta #{settings.next_send_events_to_date(settings.auto_to_date).try(:to_date)}, enviado a 
                #{settings.list_auto_send_emails.count} #{send_target} #{frecuence_text}."
                smh.save
                type = "auto"
                excluded_emails = settings.auto_excluded_emails
                frecuence = settings.auto_frecuence

                File.open("#{Rails.root}/log/emails/email#{smh.id}.log", File::RDWR | File::APPEND | File::CREAT) do |file|
                    mail_log = Logger.new(file)
                    mail_log.formatter = proc do |severity, datetime, progname, msg|
                    "#{Time.current} - #{msg}\n"
                    end
                    mail_log.info("Comienzo del envío de mails automáticos con eventos desde #{settings.next_send_events_from_date(settings.auto_from_date).try(:to_date)} hasta #{settings.next_send_events_to_date(settings.auto_to_date).try(:to_date)}, enviado a #{settings.list_auto_send_emails.count} #{send_target} #{frecuence_text}.")
                    if settings.auto_user_type != 2
                        user_type = "Holder"
                        Holder.all.each do |holder|
                            if !holder.user_email.blank? && (settings.is_auto_all_emails || (settings.is_auto_included_emails && settings.auto_included_emails.include?(holder.user_email)) || (settings.is_auto_excluded_emails && !settings.auto_excluded_emails.include?(holder.user_email)))
                                send_mail(holder, user_type, type, settings, mail_log)
                            end
                        end
                    end
                    if settings.auto_user_type != 1
                        user_type = "Manager"
                        Manage.all.each do |m|
                            email = User.find(m.user_id).try(:email)
                            if !email.blank? && (settings.is_auto_all_emails || (settings.is_auto_included_emails && settings.auto_included_emails.include?(email)) || (settings.is_auto_excluded_emails && !settings.auto_excluded_emails.include?(email)))
                                send_mail(m, user_type, type, settings, mail_log)
                            end
                        end
                    end
                end
            else
                puts "El envío automático de emails está desactivado."
            end
        end   
    end

    desc "Manual send email events"
    task manual: :environment do
        settings = SendEmail.first
        if settings.manual_send.to_s == "true" && settings.try(:manual_date).to_s == Time.zone.today.to_s
            type = "manual"
            send_user_type = settings.manual_user_type

            smh = SendMailerHistoric.new
            case settings.auto_user_type
            when 1
                send_target = "Titular"
            when 2 
                send_target = "Gestores"
            when 3
                send_target = "Titulares y gestores"
            else
                send_target = ""
            end

            smh.description = "Manual, desde #{settings.manual_from_date.try(:to_date)} hasta #{settings.manual_to_date.try(:to_date)}, enviado a 
            #{settings.list_manual_send_emails.count} #{send_target}."
            smh.save

            File.open("#{Rails.root}/log/emails/email#{smh.id}.log", File::RDWR | File::APPEND | File::CREAT) do |file|
                mail_log = Logger.new(file)
                mail_log.formatter = proc do |severity, datetime, progname, msg|
                "#{Time.current} - #{msg}\n"
                end
                mail_log.info("Comienzo del envío de mails manuales con eventos desde #{settings.manual_from_date.try(:to_date)} hasta #{settings.manual_to_date.try(:to_date)}, enviado a #{settings.list_manual_send_emails.count} #{send_target}.")
                if settings.manual_user_type != 2
                    user_type = "Holder"
                    Holder.all.each do |holder|
                        if !holder.user_email.blank? && (settings.is_manual_all_emails || (settings.is_manual_included_emails && settings.manual_included_emails.include?(holder.user_email)) || (settings.is_manual_excluded_emails && !settings.manual_excluded_emails.include?(holder.user_email)))
                            send_mail(holder, user_type, type, settings, mail_log)
                        end
                    end
                end
                if settings.manual_user_type != 1
                    user_type = "Manager"
                    Manage.all.each do |m|
                        email = User.find(m.user_id).try(:email)
                        if !email.blank? && (settings.is_manual_all_emails || (settings.is_manual_included_emails && settings.manual_included_emails.include?(email)) || (settings.is_manual_excluded_emails && !settings.manual_excluded_emails.include?(email)))
                            send_mail(m, user_type, type, settings, mail_log)
                        end
                    end
                end
            end
        else
            puts "El envío manual de emails está desactivado o fuera de fecha."
        end
    end

    def data_mail(mail, holder, ids, events, date_title = "")
        if Rails.env.staging? 
            default_url_options[:host] = 'tomadedecisionesdesa.madrid.es'
        elsif Rails.env.preproduction? 
            default_url_options[:host] = 'tomadedecisionespre.madrid.es'
        elsif Rails.env.production?
            default_url_options[:host] = 'tomadedecisiones.madrid.es'
        else
            default_url_options[:host] = 'localhost:3000'
        end
        mail.subject = mail.subject.gsub!("@holder@", holder.full_name)
        body = "<style>
                #events {
                    font-family: Arial, Helvetica, sans-serif;
                    border-collapse: collapse;
                    width: 100%;
                }
                
                #events td, #events th {
                    border: 1px solid #ddd;
                    padding: 8px;
                }
                
                #events tr:nth-child(even){background-color: #f2f2f2;}
                
                #events tr:hover {background-color: #ddd;}
                
                #events th {
                    padding-top: 12px;
                    padding-bottom: 12px;
                    text-align: left;
                    background-color: #003df6;
                    color: white;
                }
                </style>
                <table id='events'><tr><th>Fecha</th><th>Título evento</th><th>Actividad de lobby</th></tr>"
        mail.email_body = mail.email_body.gsub("@date@", date_title).gsub("@holder@", holder.full_name)
        if !events.blank?
            events.order("scheduled desc").each { |e| body = body + "<tr>
                <td>#{e.try(:scheduled).to_date}</td>
                <td><a href='#{show_url(e)}'>#{e.try(:title).to_s}</a></td>
                <td>#{e.try(:lobby_activity).to_s == 'true' ? 'Si' : 'No'}</td></tr>"
            }
            body = body + "</table>"
            mail.email_body = mail.email_body.gsub("@events@", body)
        end
    end

    def send_mail(resource, user_type, type, settings, mail_log)
        puts "Envio de Holder...."
        if user_type.to_s == "Holder"

            ids = Position.where(holder_id: resource.id, to: nil).ids
            if !ids.blank?
                events = get_events(ids, type, settings)
                puts events.count
                if !events.blank? 
                    mail = ManageEmail.find_by(type_data: 'Events')
                else
                    mail = ManageEmail.find_by(type_data: 'NoEvents')
                end
                date_title = get_date_title(type, settings)
                if !mail.blank?
                    data_mail(mail,resource,ids,events,date_title)
                    puts "Email enviado al titular: #{resource.id}"
                    begin
                        if !Rails.env.production?
                            ['jimenezel@madrid.es', 'registrodelobbies@madrid.es', 'transparenciaydatos@madrid.es', 'crespodhe@madrid.es',
                            'ajmorenoh@minsait.com','rupertomj@madrid.es','sbretoneso@eservicios.indra.es'].each do |email|
                                mail_log.info("Copia del email enviado al titular: #{resource.id} con email #{resource.user_email} enviada a #{email}")
                                puts ManageMailer.sender(email,mail).deliver_now
                            end
                        else
                            mail_log.info("Email enviado al titular: #{resource.id} con email #{resource.user_email}")
                            ManageMailer.sender(resource.user_email,mail).deliver_now
                        end
                    rescue =>e
                        begin
                            Rails.logger.error("SMS-ERROR: #{e}")
                        rescue
                        end
                    end
                end
            end
        end
        puts "Envio de Manager...."
        if user_type.to_s == "Manager"       
            ids = Position.where(holder_id: resource.holder_id, to: nil).ids
            if !ids.blank?
                events = get_events(ids, type, settings)
                if !events.blank? 
                    mail = ManageEmail.find_by(type_data: 'Events')
                else
                    mail = ManageEmail.find_by(type_data: 'NoEvents')
                end
                date_title = get_date_title(type, settings)
                if !mail.blank?
                    data_mail(mail,Holder.find(resource.holder_id), ids,events,date_title)
                    puts "Email enviado al gestor: #{resource.user_id}"
                    begin
                        if !Rails.env.production?
                            ['jimenezel@madrid.es', 'registrodelobbies@madrid.es', 'transparenciaydatos@madrid.es', 'crespodhe@madrid.es',
                            'ajmorenoh@minsait.com','rupertomj@madrid.es','sbretoneso@eservicios.indra.es'].each do |email|
                                mail_log.info("Copia del email enviado al gestor: #{resource.id} con email #{User.find(resource.user_id).try(:email)} enviada a #{email}")
                                puts ManageMailer.sender(email,mail).deliver_now
                            end
                        else
                            mail_log.info("Email enviado al gestor: #{resource.id} con email #{User.find(resource.user_id).try(:email)}")
                            ManageMailer.sender(User.find(resource.user_id).email,mail).deliver_now
                        end
                    rescue => e
                        begin
                            Rails.logger.error("SMS-ERROR: #{e}")
                        rescue
                        end
                    end
                end
            end
        end
    end

    def get_events(ids, type, settings)
        events = []
        if type == "auto"
            if !settings.auto_from_date.blank? || !settings.auto_to_date.blank?
                if settings.try(:auto_start_date) == Date.today
                    if !settings.auto_from_date.blank? && !settings.auto_to_date.blank?
                        events = Event.where("cast(scheduled as date) BETWEEN cast(? as date) AND cast(? as date) AND position_id IN (?) and (status = 2 or status = 1)",Time.zone.parse(settings.auto_from_date.to_date.to_s),Time.zone.parse(settings.auto_to_date.to_date.to_s), ids)
                    elsif !settings.auto_from_date.blank?
                        events = Event.where("cast(scheduled as date) >= cast(? as date) AND position_id IN (?) and (status = 2 or status = 1)",Time.zone.parse(settings.auto_from_date.to_date.to_s), ids)
                    elsif !settings.auto_to_date.blank?
                        events = Event.where("cast(scheduled as date)  <= cast(? as date) AND position_id IN (?) and (status = 2 or status = 1)",Time.zone.parse(settings.auto_to_date.to_date.to_s), ids)
                    end
                else
                    months_diff = ((Date.today - settings.auto_start_date)/30).to_i
                    end_month_today = get_end_of_month(Date.today)
                    end_month_auto_start = get_end_of_month(settings.auto_start_date)
                    end_month_from = get_end_of_month(settings.auto_from_date)
                    end_month_to = get_end_of_month(settings.auto_to_date)
                    dates = nil
                    case settings.auto_frecuence
                    when 1
                        if Date.today.day == settings.auto_start_date.day && end_month_auto_start.blank? && end_month_today.blank?
                            dates = get_dates(settings, months_diff, end_month_to)
                        elsif !end_month_auto_start.blank? && !end_month_today.blank?
                            dates = get_dates(settings, months_diff, end_month_to)
                        end
                    when 2
                        if ((months_diff % 2) == 0)
                            if Date.today.day == settings.auto_start_date.day && end_month_auto_start.blank? && end_month_today.blank?
                                dates = get_dates(settings, months_diff, end_month_to)
                            elsif !end_month_auto_start.blank? && !end_month_today.blank?
                                dates = get_dates(settings, months_diff, end_month_to)
                            end
                        end
                    when 3
                        if ((months_diff % 3) == 0)
                            if Date.today.day == settings.auto_start_date.day && end_month_auto_start.blank? && end_month_today.blank?
                                dates = get_dates(settings, months_diff, end_month_to)
                            elsif !end_month_auto_start.blank? && !end_month_today.blank?
                                dates = get_dates(settings, months_diff, end_month_to)
                            end
                        end
                    when 4
                        if ((months_diff % 6) == 0)
                            if Date.today.day == settings.auto_start_date.day && end_month_auto_start.blank? && end_month_today.blank?
                                dates = get_dates(settings, months_diff, end_month_to)
                            elsif !end_month_auto_start.blank? && !end_month_today.blank?
                                dates = get_dates(settings, months_diff, end_month_to)
                            end
                        end
                    when 5
                        if ((months_diff % 12) == 0)
                            if Date.today.day == settings.auto_start_date.day && end_month_auto_start.blank? && end_month_today.blank?
                                dates = get_dates(settings, months_diff, end_month_to)
                            elsif !end_month_auto_start.blank? && !end_month_today.blank?
                                dates = get_dates(settings, months_diff, end_month_to)
                            end
                        end
                    end
                    if !dates.blank?
                        if !dates[:date_from].blank? && !dates[:date_to].blank?
                            events = Event.where("cast(scheduled as date) BETWEEN cast(? as date) AND cast(? as date) AND position_id IN (?) and (status = 2 or status = 1)",Time.zone.parse(dates[:date_from].to_date.to_s),Time.zone.parse(dates[:date_to].to_date.to_s), ids)
                        elsif !dates[:date_from].blank? 
                            events = Event.where("cast(scheduled as date) >= cast(? as date) AND position_id IN (?) and (status = 2 or status = 1)",Time.zone.parse(dates[:date_from].to_date.to_s), ids)
                        elsif !dates[:date_to].blank?  #!settings.auto_to_date.blank?
                            events = Event.where("cast(scheduled as date)  <= cast(? as date) AND position_id IN (?) and (status = 2 or status = 1)",Time.zone.parse(dates[:date_to].to_date.to_s), ids)
                        end
                    end
                end
            end
        elsif type == "manual"
            if !settings.manual_from_date.blank? && !settings.manual_to_date.blank?
                events = Event.where("cast(scheduled as date) BETWEEN cast(? as date) AND cast(? as date) AND position_id IN (?) and (status = 2 or status = 1)",Time.zone.parse(settings.manual_from_date.to_date.to_s),Time.zone.parse(settings.manual_to_date.to_date.to_s), ids)
            elsif !settings.manual_from_date.blank?
                events = Event.where("cast(scheduled as date) >= cast(? as date) AND position_id IN (?) and (status = 2 or status = 1)",Time.zone.parse(settings.manual_from_date.to_date.to_s), ids)
            elsif !settings.manual_to_date.blank?
                events = Event.where("cast(scheduled as date)  <= cast(? as date) AND position_id IN (?) and (status = 2 or status = 1)",Time.zone.parse(settings.manual_to_date.to_date.to_s), ids)
            end
        else
            events = ""
        end
        events
    end

    def get_date_title(type, settings)
        if type == "auto"
            if settings.try(:auto_start_date) == Date.today
                if !settings.auto_from_date.blank? && !settings.auto_to_date.blank?
                    date_title = " desde el #{Time.zone.parse(settings.auto_from_date.to_date.to_s).strftime("%d/%m/%Y")}, hasta el #{Time.zone.parse(settings.auto_to_date.to_date.to_s).strftime("%d/%m/%Y")}"
                elsif !settings.auto_from_date.blank?
                    date_title = " desde el #{Time.zone.parse(settings.auto_from_date.to_date.to_s).strftime("%d/%m/%Y")}"
                elsif !settings.auto_to_date.blank?
                    date_title = " hasta el #{Time.zone.parse(settings.auto_to_date.to_date.to_s).strftime("%d/%m/%Y")}"
                end
            else
                months_diff = ((Date.today - settings.auto_start_date)/30).to_i
                end_month_today = get_end_of_month(Date.today)
                end_month_auto_start = get_end_of_month(settings.auto_start_date)
                end_month_from = get_end_of_month(settings.auto_from_date)
                end_month_to = get_end_of_month(settings.auto_to_date)
                dates = nil
                case settings.auto_frecuence
                when 1
                    if Date.today.day == settings.auto_start_date.day && end_month_auto_start.blank? && end_month_today.blank?
                        dates = get_dates(settings, months_diff, end_month_to)
                    elsif !end_month_auto_start.blank? && !end_month_today.blank?
                        dates = get_dates(settings, months_diff, end_month_to)
                    end
                when 2
                    if ((months_diff % 2) == 0)
                        if Date.today.day == settings.auto_start_date.day && end_month_auto_start.blank? && end_month_today.blank?
                            dates = get_dates(settings, months_diff, end_month_to)
                        elsif !end_month_auto_start.blank? && !end_month_today.blank?
                            dates = get_dates(settings, months_diff, end_month_to)
                        end
                    end
                when 3
                    if ((months_diff % 3) == 0)
                        if Date.today.day == settings.auto_start_date.day && end_month_auto_start.blank? && end_month_today.blank?
                            dates = get_dates(settings, months_diff, end_month_to)
                        elsif !end_month_auto_start.blank? && !end_month_today.blank?
                            dates = get_dates(settings, months_diff, end_month_to)
                        end
                    end
                when 4
                    if ((months_diff % 6) == 0)
                        if Date.today.day == settings.auto_start_date.day && end_month_auto_start.blank? && end_month_today.blank?
                            dates = get_dates(settings, months_diff, end_month_to)
                        elsif !end_month_auto_start.blank? && !end_month_today.blank?
                            dates = get_dates(settings, months_diff, end_month_to)
                        end
                    end
                when 5
                    if ((months_diff % 12) == 0)
                        if Date.today.day == settings.auto_start_date.day && end_month_auto_start.blank? && end_month_today.blank?
                            dates = get_dates(settings, months_diff, end_month_to)
                        elsif !end_month_auto_start.blank? && !end_month_today.blank?
                            dates = get_dates(settings, months_diff, end_month_to)
                        end
                    end
                end

                if !dates.blank?
                    if !dates[:date_from].blank? && !dates[:date_to].blank?
                        date_title = " desde el #{Time.zone.parse(dates[:date_from].to_date.to_s).strftime("%d/%m/%Y")}, hasta el #{Time.zone.parse(dates[:date_to].to_date.to_s).strftime("%d/%m/%Y")}"
                    elsif !dates[:date_from].blank?
                        date_title = " desde el #{Time.zone.parse(dates[:date_from].to_date.to_s).strftime("%d/%m/%Y")}"
                    elsif !dates[:date_to].blank?
                        date_title = " hasta el #{Time.zone.parse(dates[:date_to].to_date.to_s).strftime("%d/%m/%Y")}"
                    end
                end
            end
        elsif type == "manual"
            if !settings.manual_from_date.blank? && !settings.manual_to_date.blank?
                date_title = " desde el #{Time.zone.parse(settings.manual_from_date.to_date.to_s).strftime("%d/%m/%Y")}, hasta el #{Time.zone.parse(settings.manual_to_date.to_date.to_s).strftime("%d/%m/%Y")}"
            elsif !settings.manual_from_date.blank?
                date_title = " desde el #{Time.zone.parse(settings.manual_from_date.to_date.to_s).strftime("%d/%m/%Y")}"
            elsif !settings.manual_to_date.blank?
                date_title = " hasta el #{Time.zone.parse(settings.manual_to_date.to_date.to_s).strftime("%d/%m/%Y")}"
            end
        end
        date_title
    end

    def check_date_equal(settings)
        if settings.auto_start_date == Date.today
            return true
        else
            if Date.today.day == settings.auto_start_date.day
                return true
            else
                end_month_auto = Date.new(settings.auto_start_date.year, settings.auto_start_date.month, -1)
                end_month_today = Date.new(Date.today.year, Date.today.month, -1)
                if end_month_auto == settings.auto_start_date && end_month_today == Date.today
                    return true
                else
                    return false
                end
            end
        end
    end

    def get_end_of_month(date, diff = nil)
        if !date.blank?
            if diff.blank?
                end_of_month = Date.new(date.year, date.month, -1)
                if end_of_month == date
                    end_of_month
                else
                    nil
                end
            else
                date = date + diff.month
                end_of_month = Date.new(date.year, date.month, -1)
                end_of_month
            end
        else
            nil
        end
    end

    def get_dates(settings, months_diff, end_month_to, date_from = nil, date_to = nil)
        if !settings.auto_from_date.blank? && !settings.auto_to_date.blank?
            date_from = settings.auto_from_date + months_diff.month
            if !end_month_to.blank?
                date_to = get_end_of_month(settings.auto_to_date, months_diff)
            else
                date_to = settings.auto_to_date + months_diff.month
            end
        elsif !settings.auto_from_date.blank?
            date_from = settings.auto_from_date + months_diff.month
        elsif !settings.auto_to_date.blank?
            if !end_month_to.blank?
                date_to = get_end_of_month(settings.auto_to_date, months_diff)
            else
                date_to = settings.auto_to_date + months_diff.month
            end                            
        end
        {date_from: date_from, date_to: date_to}
    end

    def send_email_expiration_one_month?(expiration_date, current_date = Date.today)
        if ((expiration_date - current_date) / 30).round.to_i == 1
            if expiration_date.to_date.day == current_date.to_date.day
                if (!(expiration_date.end_of_month == expiration_date && current_date.end_of_month != current_date) || (!(expiration_date.end_of_month != expiration_date && current_date.end_of_month == current_date) || expiration_date.day >= current_date.day) )
                    true
                else
                    false
                end
            elsif (expiration_date.end_of_month == expiration_date && current_date.end_of_month == current_date) || (expiration_date.end_of_month != expiration_date && current_date.end_of_month == current_date && expiration_date.day >= current_date.day)
                true
            else
                false
            end
        else 
            false
        end
    end

    def send_mail_expiration(organization, type, expiration_date)
        begin
            mail = ManageEmail.find_by(type_data: type)
            send_mailer_historic = SendMailerHistoric.create(type_data: type, organization: organization, 
                sent_to: "#{organization.try(:user).try(:email).blank? ? '' : organization.try(:user).try(:email).to_s + ', '}#{mail.fields_cc.blank? ? '' : mail.fields_cc.to_s + ', '}#{mail.fields_cco.blank? ? '' : mail.fields_cco.to_s}".try(:strip))
            mail.email_body = mail.try(:email_body).blank? ? "" : mail.try(:email_body).to_s.gsub("@expiration_date@", I18n.l(expiration_date, format: :long))
            ManageMailer.sender(organization.try(:user).try(:email), mail).deliver_now
        rescue => e
            begin
              send_mailer_historic.update(sent: false) if !send_mailer_historic.blank?
            rescue
            end
        end
    end
end