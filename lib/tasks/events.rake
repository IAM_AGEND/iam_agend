namespace :events do
  desc "Update event status to done"
  task update_search: :environment do
    Event.where("updated_at::date != '2022-06-06'::date or created_at::date = '2022-06-06'::date").each do |e|
      EventUpdateSearch.update_search(e)
    end
  end

  desc "Update event status to done"
  task update_event_status: :environment do
    Event.where("scheduled <= ? AND status = ?", Time.zone.now, 1).each do |event|
      event.status = :done
      event.save(validate: false)
      puts "Se ha actualizado el evento #{event.id}"
    end
  end

  desc "Update old event status to done or accepted"
  task update_old_event_status: :environment do
    puts "Starting conversion of old events without status to accepted"
    Event.where("status is ? AND scheduled > ?", nil, Time.now).each do |event|
      event.update_attributes(:status, :accepted)
      event.update_attributes(:published_at, event.scheduled.to_time)
    end
    puts "Finished conversion of old events without status to accepted"

    puts "Starting conversion of old events without status to done"
    Event.where("status is ? AND scheduled <= ?", nil, Time.now).each do |event|
      event.update_attributes(:status, :done)
      event.update_attributes(:published_at, event.scheduled.to_time)
    end
    puts "Finished conversion of old events without status to done"
  end

  desc "Get events scheduled error"
  task get_error: :environment do
    puts "=================Eventos===================="
    Event.where("scheduled < '2015-01-01'").order(scheduled: :asc).each do |event|     
      puts "Evento con ID: #{event.id}, fecha scheduled: '#{event.scheduled}'"
    end
    puts "============================================"
  end

  desc "Update event scheduled error"
  task :update_scheduled, [:id, :fecha]  => :environment  do |t, args|
    if !args.id.blank? && !args.fecha.blank?
      puts "Se va a actualizar el proceso con el id: #{args.id} y la fecha: #{args.fecha}"
      results = ActiveRecord::Base.connection.execute("update events set scheduled='#{args.fecha}' where id=#{args.id}")
      if results.blank?
        puts "ERROR: No se puede actualizar"
      else
        puts "Se ha actualizado correctametne"
      end
    else
      puts "ERROR: No se puede actualizar falta el id: #{args.id} o la fecha: #{args.fecha}"
    end

  end

  # desc "Update old event status to done or accepted"
  # task s: :environment do
  #   require 'icalendar'

  #   # Create a calendar with an event (standard method)
  #   cal = Icalendar::Calendar.new
  #   cal.event do |e|
  #     e.dtstart     = Icalendar::Values::Date.new('20050428')
  #     e.dtend       = Icalendar::Values::Date.new('20050429')
  #     e.summary     = "Meeting with the man."
  #     e.description = "Have a long lunch meeting and decide nothing..."
  #     e.ip_class    = "PRIVATE"
  #   end
    
  #   cal.publish

  #   # require 'icalendar'
  #   # event=Event.all.first
  #   # cal = Icalendar::Calendar.new
  #   # filename = "Foo at #{event.title}"

  #   # # if params[:format] == 'vcs'
  #   # #   cal.prodid = '-//Microsoft Corporation//Outlook MIMEDIR//EN'
  #   # #   cal.version = '1.0'
  #   # #   filename += '.vcs'
  #   # # else # ical
  #   #   cal.prodid = '-//Acme Widgets, Inc.//NONSGML ExportToCalendar//EN'
  #   #   cal.version = '2.0'
  #   #   filename += '.ics'
  #   # # end

  #   # cal.event do |e|
  #   #   e.dtstart     = Icalendar::Values::DateTime.new(event.scheduled, tzid: event.scheduled.time_zone)
  #   #   e.dtend       = Icalendar::Values::DateTime.new(event.scheduled+1.day, tzid: event.scheduled.time_zone)
  #   #   e.summary     = event.title
  #   #   e.description = event.description
  #   #   e.url         = "#"#event_url(foo)
  #   #   e.location    = event.location
  #   # end

  #   # send_data cal.to_ical, type: 'text/calendar', disposition: 'attachment', filename: filename
  # end
end
