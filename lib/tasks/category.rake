namespace :category do
    desc "Fill Category code"
    task insert_code: :environment do
        arr_name = ['Persona física','Entidades privadas sin ánimo de lucro','Entidades representativas de intereses colectivos',
            'Agrupaciones de personas que se conformen como plataformas, movimientos, foros o redes ciudadanas sin personalidad jurídica, incluso las constituidas circunstancialmente',
            'Organizaciones empresariales, colegios profesionales y demás entidades representativas de intereses colectivos','Entidades organizadoras de actos sin ánimo lucrativo',
            'Organizaciones no gubernamentales','Grupos de reflexión e instituciones académicas y de investigación','Organizaciones que representan a comunidades religiosas',
            'Organizaciones que representan a autoridades municipales','Organizaciones que representan a autoridades regionales','Organismos públicos o mixtos',
            'Empresas y agrupaciones comerciales, empresariales y profesionales','Consultorías profesionales','Asociaciones comerciales, empresariales y profesionales',
            'Coaliciones y estructuras temporales con fines de lucro','Entidades organizadoras de actos con ánimo de lucro','Cualquier otra entidad con ánimo de lucro',
            'Entidad sin ánimo de lucro','Entidad con ánimo de lucro']
            arr_name.each_with_index do |name,index| 
                Category.find_by(name: name)&.update(code: (index+1))
            end

    end
end

