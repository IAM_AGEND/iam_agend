namespace :events_organizations do

    desc "Mandar datos de la relación entre eventos y organizaciones a la nueva tabla"
    task insert_data: :environment do
        Event.where.not(organization_id: nil).each do |e|
            EventsOrganization.create(event_id: e.id, organization_id: e.organization_id, organization_name: e.organization_name)
        end
    end

    desc "Actualizar events_organization_id en event_agents y event_represented_attributes"
    task update_data: :environment do
        Event.where("DATE(created_at) < ?", "01-05-2022".to_date).each do |e|
            e.event_agents.each do |ea|
                ea.update(events_organization_id: e.events_organization_ids[0])
            end

            e.event_represented_entities.each do |ere|
                ere.update(events_organization_id: e.events_organization_ids[0])
            end
        end
    end

    

end