namespace :manage_emails do

    desc "Nuevo ManageEmail cuando no hay eventos"
    task :no_events_new => :environment do
        ManageEmail.create(id: 5,type_data: "NoEvents", sender: "transparenciaydatos@madrid.es", fields_cc: "transparenciaydatos@madrid.es",fields_cco: "crespodhe@madrid.es")
    end

    desc "Nuevos tipos de Email para acciones sobre organizaciones"
    task :new_lobbies_emails => :environment do
        ManageEmail.find_or_create_by(type_data: "LobbyOneMonthToExpiration", sender: "transparenciaydatos@madrid.es", fields_cco: "registrodelobbies@madrid.es", subject: "Próxima caducidad de su inscripción en el Registro de Lobbies del Ayuntamiento de Madrid.")
        ManageEmail.find_or_create_by(type_data: "LobbyExpiresToday", sender: "transparenciaydatos@madrid.es", fields_cco: "registrodelobbies@madrid.es", subject: "Caducidad de su inscripción en el Registro de Lobbies del Ayuntamiento de Madrid.")
        ManageEmail.find_or_create_by(type_data: "LobbyRenovation", sender: "transparenciaydatos@madrid.es", fields_cco: "registrodelobbies@madrid.es", subject: "Registro de Lobbies. Renovación de inscripción: @lobby_name@")
        ManageEmail.find_or_create_by(type_data: "LobbyModificationNotification", sender: "transparenciaydatos@madrid.es", fields_cco: "registrodelobbies@madrid.es", subject: "Registro de Lobbies. Modificación de lobby mediante notificación: @lobby_name@")
        ManageEmail.find_or_create_by(type_data: "LobbyModificationManual", sender: "transparenciaydatos@madrid.es", fields_cco: "registrodelobbies@madrid.es", subject: "Registro de Lobbies. Modificación de lobby manualmente: @lobby_name@")
        ManageEmail.find_or_create_by(type_data: "LobbyCanceled", sender: "transparenciaydatos@madrid.es", fields_cco: "registrodelobbies@madrid.es", subject: "Registro de Lobbies. Baja de lobby: @lobby_name@")
    end

end