namespace :madrid do
  desc "Mostrar proceso de importación y sincronización de cargos"
  task :show_data => :environment do
    imported
  end

  desc "Ejecutar proceso de importación y sincronización de cargos"
  task :import => :environment do
    imported(true)
  end

  desc "Ejecuta el proceso de importación para titulares no actualiza"
  task :holders => :environment do
    imported_holders
  end

  desc "Ejecuta el proceso de importación para titulares sí actualiza"
  task :import_holders => :environment do
    imported_holders(true)
  end

  desc "Comprueba el titular con clave de usuario en :data"
  task :comprobar,[:data] => [:environment] do |t,args|
    load_api
    # puts "============================================="
    # puts "Titular con #{args[:data]}"
    # puts "============================================="
    # puts "id;Nombre;Apellidos;Clave usuario"
    Holder.where(user_key: args[:data]).each do |holder|
      begin
        # puts "#{holder.id};#{holder.first_name};#{holder.last_name};#{holder.user_key}"
        data = @uweb_api.get_user(holder.user_key)
        # puts "---------------------------------------------"
        # puts "DATA UWEB"
        # puts "---------------------------------------------"
        # puts data
        # puts "---------------------------------------------"
        # puts "CARGOS ACTIVOS"
        # puts "---------------------------------------------"
        # puts holder.positions.where(to: nil).count > 0
        # puts "---------------------------------------------"
        # puts (data["BAJA_LOGICA"].to_i == 1 && holder.positions.where(to: nil).count > 0) ?
        #   "--> Los datos son incorrectos" : "--> Datos correctos y actualizados"
      rescue => e
        puts "-------------------ERROR---------------------"
        puts "Holder with uweb_key #{holder.user_key} exception"
        puts e.message            # Test de excepción
        puts "---------------------------------------------"
      end
    end

    # puts "============================================="
    # puts "Fin de actualización"
    # puts "============================================="
  end

  desc "Elimina las repeticiones de posiciones, se queda con el id más pequeño"
  task clean_pos: :environment do
    # puts "============================================="
    # puts "Eliminación duplicados"
    # puts "============================================="
    Holder.all.each do |holder|
      holder.positions.each do |pos1|
        holder.positions.each do |pos2|
          if pos1.id.to_i != pos2.id.to_i && pos1.title.to_s == pos2.title.to_s && pos1.area_id == pos2.area_id
            # puts "Eliminación de duplicado posición #{pos2.id}"
            begin
              pos2.destroy
            rescue => e
            end
          end
        end
      end
    end
    # puts "============================================="
    # puts "Fin de eliminación"
    # puts "============================================="
  end

  private

  def load_api
    @uweb_api = UwebApi.new
    @directory_api = DirectoryApi.new
  end

  def imported_holders(update = false)
    # puts "============================================="
    # puts "Importación titulares con actualización=#{update.to_s}"
    # puts "============================================="
    load_api
    create_holders(update)
    # puts "============================================="
    # puts "Fin de importación"
    # puts "============================================="
  end


  def imported(update = false)
    # puts "============================================="
    # puts "Importación con actualización=#{update.to_s}"
    # puts "============================================="
    load_api
    create_admins(update)
    create_users(update)
    create_holders(update)
    update_old_holders(update)
    delete_old_users(update)
   
    # puts "============================================="
    # puts "Fin de importación"
    # puts "============================================="
  end

  def generated_header(title)
    # puts "============================================="
    # puts "Importación de #{title}"
    # puts "============================================="
    # puts "id;Correo electrónico;Rol;Nombre;Apellidos;Activo;Clave usuario;Teléfonos"
  end

  def generated_footer
    # puts "============================================="
    # puts ""
    # puts ""
  end

  def generate_users(user, update, type)
    # puts "#{user.id};#{user.email};#{user.role};#{user.first_name};#{user.last_name};#{user.active};#{user.user_key};#{user.phones}"
    if update
      unless user.save
        puts "ERROR: No se ha actualizado el #{type} id - #{user.id}"
        puts user.errors.full_messages
      end
    end
  rescue
  end

  def uweb_get_users(method)
    @uweb_api.get_users(Rails.application.secrets.send(method))
  rescue
  end

  def user_create(type, mc)
    User.create_from_uweb(type,@uweb_api.get_user(mc['CLAVE_IND']))
  rescue
  end

  def create_admins(update)
    @admins = []
    generated_header("administradores")
    uweb_get_users('uweb_api_admins_key').each do |mc|
      user = user_create('admin', mc)
      @admins.push(user.id)
      generate_users(user, update, "administrador")
    end
    generated_footer
  rescue
  end

  def create_users(update)
    generated_header("usuarios")
    uweb_get_users('uweb_api_users_key').each do |mc|
      user = user_create('user', mc)
      already_admin = false
      @admins.each do |id|
        if user.id.to_s == id.to_s
          already_admin = true
          break
        end
      end
      !already_admin ? generate_users(user, update, "usuario") : "" 
    end
    generated_footer
  rescue
  end

  def create_holders(update)
    # puts "============================================="
    # puts "Importación de titulares"
    # puts "============================================="
    # puts "id;Nombre;Apellidos;Clave usuario"
    uweb_get_users('uweb_api_holders_key').each do |mc|
      begin
        data = @uweb_api.get_user(mc['CLAVE_IND'])
      rescue => e
        puts "-------------------ERROR---------------------"
        puts "Holder with uweb_key #{@uweb_api.get_user(mc['CLAVE_IND'])} exception"
        puts e.message            # Test de excepción
        puts e.backtrace.inspect
        puts "---------------------------------------------"
      end
      if e.blank?
        create_tree_area(data)
        
        holder = Holder.create_from_uweb(data)
        #puts "#{holder.id};#{holder.first_name};#{holder.last_name};#{holder.user_key}"
        if update
          unless holder.save
            puts "-------------------ERROR---------------------"
            puts "ERROR: No se ha actualizado el titular id - #{holder.id}"
            puts holder.errors.full_messages
            puts "-------------------ERROR---------------------"
          end
        end
       # puts ";Cargos"
        new_history = true
        holder.positions.each do |position|
          already_created = false
          position.title = position.try(:title).try{|x| x.strip}
          internal_code = position.try(:area).try(:internal_code)
          # create_history = false
          if !data['CARGO'].blank? && data['CARGO'].try{|x| x.strip}.to_s == position.try(:title).try{|x| x.strip} && !data['COD_UNIDAD'].blank? && !@unit['DENOMINACION'].blank? && @unit['DENOMINACION'].try{|x| x.strip}.to_s==position.try(:area).try(:title).to_s && (!@unit['ID_UNIDAD'].blank? && @unit['ID_UNIDAD'].try{|x| x.strip}.to_s == position.try(:area).try(:internal_id).to_s || !internal_code.blank? && internal_code.try{|x| x.strip}.to_s == data['COD_UNIDAD'].try{|x| x.strip}.to_s)
            already_created = true
            new_history = false
            if data['BAJA_LOGICA'].try{|x| x.strip}.to_i == 1 #|| (holder.users.blank? || holder.users.count.to_i == 0)
              position.to = Date.today
            elsif data['BAJA_LOGICA'].try{|x| x.strip}.to_i == 0 #&& !(holder.users.blank? || holder.users.count.to_i == 0)
              position.to = nil
            end
          elsif position.to.blank?
            position.to = Date.today
            # create_history = true
          end

          if !data['CARGO'].blank? && data['CARGO'].try{|x| x.strip}.to_s == position.try(:title).try{|x| x.strip} && !@unit['DENOMINACION'].blank? && @unit['DENOMINACION'].try{|x| x.strip}.to_s==position.try(:area).try(:title).to_s
            new_history = false
          end

          #puts ";#{position.id};#{position.title};#{position.start};#{position.to};#{position.area_id};#{data['BAJA_LOGICA']}"
          if !data['CARGO'].blank? && data['CARGO'].try{|x| x.strip}.to_s == position.try(:title).try{|x| x.strip} && !@unit['DENOMINACION'].blank? && @unit['DENOMINACION'].try{|x| x.strip}.to_s==position.try(:area).try(:title).to_s
            new_history = false
          end
          if update
            unless position.save
              puts "-------------------ERROR---------------------"
              puts "ERROR: No se ha podido actualizar el cargo id - #{position.id}"
              puts position.errors.full_messages
              puts "-------------------ERROR---------------------"
            # else
              #Comprobar el último histórico para el ya creado
              # history_last = position.holder_status_histories.last(1)[0] 
              
              # if already_created
              #   if history_last.blank?
              #     create_history = true
              #   elsif history_last.status == "Activo" && !position.to.blank?
              #     create_history = true
              #   elsif history_last.status == "Inactivo" && position.to.blank?
              #     create_history = true
              #   end
              # end

              # if create_history 
              #   if position.to.blank?
              #     status = I18n.t('backend.active')
              #   else
              #     status = I18n.t('backend.inactive')
              #   end 
              #   create_holder_status_history(position, status)
              # end
            end
          end
        end
        if new_history && !data['COD_UNIDAD'].blank? && !data['CARGO'].blank?
          # puts ";Asignando cargo"
          # puts ";id;Título;Desde;Hasta;Area"

          exist = Position.where(holder: holder, area: Area.find_by(internal_code: data['COD_UNIDAD'].try{|x| x.strip}, active: true), title:  data['CARGO'].try{|x| x.strip})
          if exist.blank?
            position = Position.new
            position.start = Date.today
            position.title = data['CARGO'].try{|x| x.strip} unless data['CARGO'].blank?
            if data['BAJA_LOGICA'].try{|x| x.strip}.to_i == 0 #&& !(holder.users.blank? || holder.users.count.to_i == 0)
              position.to = nil
            # status = I18n.t('backend.active')
            elsif position.to.blank? && data['BAJA_LOGICA'].try{|x| x.strip}.to_i == 1 #|| (holder.users.blank? || holder.users.count.to_i == 0)
              position.to = Date.today
            # status = I18n.t('backend.inactive')
            end
            area = Area.find_by(internal_code: data['COD_UNIDAD'].try{|x| x.strip}, active: true)
            unless area.blank?
              position.area = area
              # puts ";#{position.id};#{position.title};#{position.start};#{position.to};#{position.area_id}"
            else
              # puts ";#{position.id};#{position.title};#{position.start};#{position.to};"
            end
            position.holder = holder
            if update
              unless position.save
                puts "ERROR: No se ha podido generar el cargo id - #{position.id}"
                puts position.errors.full_messages
              # else
              #   create_holder_status_history(position, status)
              end
            end
          end
        end
      end
    end
  rescue
  end

  def create_holder_status_history(position, status)
    history=HolderStatusHistory.generateHistory(position, status)
    unless history.save
      puts "ERROR: No se ha podido generar el historial para la posicion id - #{position.id}"
      puts history.errors.full_messages
    else
      # puts ";Generando historico para el cargo... Estado #{status} - Cargo #{position.id}"
    end
  rescue
  end

  def delete_old_users(update)
    # puts "============================================="
    # puts "Comprobación de usuarios"
    # puts "============================================="
    User.all.find_each do |user|
      unless user.user_key.blank?
        begin
          # puts "#{user.id};#{user.first_name};#{user.last_name};#{user.user_key}"
          data = @uweb_api.get_user(user.user_key)
          
          if data['BAJA_LOGICA'].try{|x| x.strip}.to_i == 0
            # puts "--> Existe el usuario y tiene marco, SIN baja lógica"
            aux_baja = 1
          else
            # puts "--> Existe el usuario y tiene marco, CON baja lógica"
            aux_baja = 0
          end
        rescue => e
          puts "-------------------ERROR---------------------"
          puts "El usuario no existe: uweb_key #{user.user_key}"
          puts e.message            # Test de excepción
          puts "---------------------------------------------"
          aux_baja = 0
        end

        if !aux_baja.blank?
          if update
            user.active = aux_baja
            # user.save ? "--> Usuario corregido" : "--> El usuario no se ha podido corregir"
          end
        end
      end
    end
    # puts "============================================="
    # puts "Fin de comprobación y actualización"
    # puts "============================================="
  rescue
  end

  def create_tree_area(data) 
    # puts "============================================="
    # puts "Importación de Area relacionada con el titular"
    # puts "============================================="
    unless data['COD_UNIDAD'].blank?
      units_global = @directory_api.get_units(data['COD_UNIDAD'])
      unless units_global.blank?
        units = units_global['UNIDAD_ORGANIZATIVA']
        if units.kind_of?(Array)
          unidad_pos = units.index{|u|u['COD_ORGANICO'] == data['COD_UNIDAD']}
          @unit = units[unidad_pos]
        else
          @unit = units
        end
        # puts ";ID UNIDAD;COD. ORGANICO; DENOMINACIÓN; ID ASCENDENCIA; DENOMINACION ASCENDENCIA"
        # puts ";#{@unit['ID_UNIDAD']};#{@unit['COD_ORGANICO']};#{@unit['DENOMINACION']};#{@unit['AREA']};#{@unit['DENOM_AREA']}"
        @directory_api.create_tree(@unit)
      end
    end
    # puts "============================================="
    # puts "Fin importación"
    # puts "============================================="
  rescue
  end

 

  def update_old_holders(update)
    # puts "============================================="
    # puts "Titulares existentes"
    # puts "============================================="
    # puts "id;Nombre;Apellidos;Clave usuario"
    Holder.all.each do |holder|
      begin
        # puts "============================================="
        # puts "#{holder.id};#{holder.first_name};#{holder.last_name};#{holder.user_key}"
        data = @uweb_api.get_user(holder.user_key)
        if data["BAJA_LOGICA"].to_i == 1 && holder.positions.where(to: nil).count > 0
          baja_logica = true
          # puts "--> Los datos son incorrectos tiene baja lógica"
        else
          baja_logica = false
          datos_sin_marco = false
          uweb_get_users('uweb_api_holders_key').each do |mc|
            if mc['CLAVE_IND'].to_s == holder.user_key
              datos_sin_marco = true
              break
            end
          end
          unless datos_sin_marco
            # puts "#--> Los datos son incorrectos no se ha encontrado al titular"
            # puts "#--> Su cargo es: #{data['CARGO']}"
            new_pos = true
            holder.positions.each do |pos|
              # puts "#--> #{pos.title} - Fecha ini: #{pos.start} - Fecha fin: #{pos.to}"
              if !data['CARGO'].try{|x| x.strip}.blank? && pos.title.to_s == data['CARGO'].try{|x| x.strip}
                new_pos = false
              end
              if pos.to.blank?
                pos.to = Time.zone.now
                if pos.save
                  # puts "#--> Corregido"
                else
                  # puts "#--> No se ha podido corregir"
                end
              end
            end

            # if new_pos && !data['CARGO'].try{|x| x.strip}.blank?

            #   position = Position.new(holder: holder, start: Time.zone.now, to: Time.zone.now, title: data['CARGO'].try{|x| x.strip})
            #   area = Area.find_by(internal_code: data['COD_UNIDAD'].try{|x| x.strip}, active: true)
            #   unless area.blank?
            #     position.area = area
            #   end
            #   if position.save
            #     puts "--> Se ha generado un nuevo cargo: #{data['CARGO']}"
            #   else
            #     puts "--> No se ha generado el cargo: #{data['CARGO']} -- ERORR: #{position.errors.full_messages}"
            #   end
            # end
          else
            # puts "--> Datos correctos y actualizados"
          end
        end
      rescue => e
        puts "-------------------ERROR---------------------"
        puts "Holder with uweb_key #{holder.user_key} exception"
        puts e.message            # Test de excepción
        puts "---------------------------------------------"
      end
      if !e.blank? || data.blank? || baja_logica
        holder.positions.each do |position| 
          if position.to.blank?
            # puts "--> Datos incorrectos, actualizando..."
            position.to = Date.today
            #status = I18n.t('backend.inactive')
            if update
              unless position.save
                puts "-------------------ERROR---------------------"
                puts "ERROR: No se ha podido generar el cargo id - #{position.id}"
                puts position.errors.full_messages
                puts "-------------------ERROR---------------------"
              # else
              #   create_holder_status_history(position, status)
              end
            end
          end
        end
      end
    end

    # puts "============================================="
    # puts "Fin de actualización"
    # puts "============================================="
  rescue
  end

end
