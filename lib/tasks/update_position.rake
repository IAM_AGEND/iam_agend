namespace :update_position do

    desc "Visualizar datos a modificar"
    task :show => :environment do
        change_pos(false)
    end

    desc "Actualizar datos"
    task :change => :environment do
        change_pos(true)
    end

    private 

    def change_pos(update)
        Position.all.find_each do |pos|
            if pos.start.strftime("%d/%m/%Y").to_s != pos.get_previous_time.to_s
                puts "Cambio de #{pos.start.strftime("%d/%m/%Y")} a #{pos.get_previous_time} de #{pos.id}"
                pos.start = pos.get_previous_time
                
                if update
                    if pos.save
                        puts "Se ha modificado #{pos.id}"
                    else
                        puts "ERROR: No se ha modificado #{pos.id}"
                    end
                end
            else
                puts "No cambia #{pos.id}"
            end
        end
    end

end