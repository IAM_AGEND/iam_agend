class PublicOrganizationImporter
  require 'csv'
  require 'open-uri'
  require 'net/https'

  def self.parse_associations
    parse(Rails.application.config.associations_csv_url, associations_mapping)
  end

  def self.parse_federations
    parse(Rails.application.config.federations_csv_url, federations_mapping)
  end

  def self.parse(url, type_mapping)
    file = get_file(url)
    return unless file
    CSV.new(file, col_sep: ";", quote_char: "|", headers: true).each do |line|      
      data = line.to_hash
      exist=Organization.find_by(inscription_reference: data["Num_Inscripcion"])
      if exist.blank?
        organization = Organization.new("entity_type" => type_mapping["entity_type"])
      else
        organization=exist
      end
      
      organization.address = Address.new
      common_mapping.each do |org, csv|
        ["address_type", "address", "number", "postal_code", "town", "phones","email"].include?(org.to_s) ?  organization.address[org] =  data[csv] : organization[org] =  data[csv]
      end
      type_mapping["fields"].each do |org, csv|
        ["address_type", "address", "number", "postal_code", "town", "phones","email"].include?(org.to_s) ?  organization.address[org] =  data[csv] : organization[org] =  data[csv]
      end
      organization.category = get_category(data)

      begin
        organization.save!
      rescue
        # ignore errors, for now
      end
    end
  end

  def self.get_file(url)
    begin
      File.open(open(url), "r:iso-8859-1:utf-8").read
    rescue
      # ignore file issues, mostily url troubles
    end
  end

  def self.get_category(data)
    Category.find_or_create_by(name: data["Categoría"], display: false)
  end

  def self.associations_mapping
    { "entity_type" => :association,
      "fields" => { "neighbourhood" => "Barrio",
                    "district" => "Distrito" } }
  end

  def self.federations_mapping
    { "entity_type" => :federation,
      "fields" => { "associations_count" => "Asoc_Totales",
                    "members_count" => "Socios_Totales" } }
  end

  def self.common_mapping
    { "inscription_reference" => "Num_Inscripcion",
      "identifier" => "NIF",
      "name" => "Razon_Social",
      "address_type" => "Tipo_Via",
      "address" => "Calle",
      "number" => "Numero",
      "postal_code" => "Cod_Postal",
      "town" => "Poblacion",
      "phones" => "Primer_Tlfn",
      "email" => "Email",
      "web" => "Internet",
      "inscription_date" => "Fecha_Registro",
      "scope" => "Ambito",
      "approach" => "Aproximacion" }
  end

  private_class_method :parse, :get_file, :get_category, :associations_mapping,
                       :federations_mapping, :common_mapping

end
